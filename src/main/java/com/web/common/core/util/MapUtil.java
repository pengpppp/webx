/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.util;

import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * map工具类
 */

public class MapUtil {

    private static Logger logger = Logger.getLogger(MapUtil.class);

    private MapUtil() {
    }

    private Map<String, Object> data = new HashMap<>();

    /**
     * 链式创建对象
     *
     * @return
     */
    public static MapUtil create() {
        return new MapUtil();
    }

    /**
     * 创建对象并赋值
     *
     * @param key
     * @param value
     * @return
     */
    public static MapUtil create(String key, Object value) {
        return create().put(key, value);
    }

    /**
     * 赋值
     *
     * @param key
     * @param value
     * @return
     */
    public MapUtil put(String key, Object value) {
        data.put(key, value);
        return this;
    }

    /**
     * 返回 map 对象，非静态方法，需前置调用 create
     *
     * @return
     */
    public Map<String, Object> getData() {
        return data;
    }

    /**
     * 实例化 hashmap
     *
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> newHashMap() {
        return new HashMap<>();
    }

    /**
     * 实例化 linkedHashMap
     *
     * @param <K>
     * @param <V>
     * @return
     */
    public static <K, V> Map<K, V> newLinkedHashMap() {
        return new LinkedHashMap<>();
    }


    public static <T> T map2Obj(Map<String, Object> map, Class<T> clz) throws Exception {
        T obj = clz.newInstance();
        Field[] declaredFields = obj.getClass().getDeclaredFields();
        for (Field field : declaredFields) {
            int mod = field.getModifiers();
            if (Modifier.isStatic(mod) || Modifier.isFinal(mod)) {
                continue;
            }
            field.setAccessible(true);
            field.set(obj, map.get(field.getName()));
        }
        return obj;
    }

    public static Map<String, Object> Obj2Map(Object obj) {
        Map<String, Object> map = new HashMap<String, Object>();
        Field[] fields = obj.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                if (null != field.get(obj) && !"serialVersionUID".equalsIgnoreCase(field.getName()))
                    map.put(field.getName(), field.get(obj));
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return map;
    }


    /**
     * 获取String类型的值
     *
     * @param map
     * @param key
     * @return
     */
    public static String getString(Map<String, ? extends Object> map, String key) {
        return getString(map, key, null);
    }

    /**
     * 获取String类型的值
     *
     * @param map
     * @param key
     * @param defaultVal
     * @return
     */
    public static String getString(Map<String, ? extends Object> map, String key, String defaultVal) {
        if (map == null) {
            return defaultVal;
        }
        Object value = map.get(key);
        if (value == null) {
            return defaultVal;
        }
        try {
            if ("null".equalsIgnoreCase(value.toString())) {
                return defaultVal;
            }
            return String.valueOf(value.toString());
        } catch (Exception ex) {
            logger.debug("转换失败，原 value==" + value);
            return defaultVal;
        }
    }

    /**
     * 获取Int类型的值
     *
     * @param map
     * @param key
     * @return
     */
    public static Integer getInt(Map<String, ? extends Object> map, String key) {
        return getInt(map, key, null);
    }

    /**
     * 获取Int类型的值
     *
     * @param map
     * @param key
     * @param defaultVal
     * @return
     */
    public static Integer getInt(Map<String, ? extends Object> map, String key, Integer defaultVal) {
        if (map == null) {
            return defaultVal;
        }
        Object value = map.get(key);
        if (value == null) {
            return defaultVal;
        }
        try {
            return Integer.valueOf(getString(map, key));
        } catch (Exception ex) {
            logger.debug("转换失败，原 value==" + value);
            return defaultVal;
        }
    }


    /**
     * 获取Long类型的值
     *
     * @param map
     * @param key
     * @return
     */
    public static Long getLong(Map<String, ? extends Object> map, String key) {
        return getLong(map, key, null);
    }

    /**
     * 获取Long类型的值
     *
     * @param map
     * @param key
     * @param defaultVal
     * @return
     */
    public static Long getLong(Map<String, ? extends Object> map, String key, Long defaultVal) {
        if (map == null) {
            return defaultVal;
        }
        Object value = map.get(key);
        if (value == null) {
            return defaultVal;
        }
        try {
            return Long.valueOf(getString(map, key));
        } catch (Exception ex) {
            logger.debug("转换失败，原 value==" + value);
            return defaultVal;
        }
    }

    /**
     * 获取Float类型的值
     *
     * @param map
     * @param key
     * @return
     */
    public static Float getFloat(Map<String, ? extends Object> map, String key) {
        return getFloat(map, key, null);
    }

    /**
     * 获取Float类型的值
     *
     * @param map
     * @param key
     * @param defaultVal
     * @return
     */
    public static Float getFloat(Map<String, ? extends Object> map, String key, Float defaultVal) {
        if (map == null) {
            return defaultVal;
        }
        Object value = map.get(key);
        if (value == null) {
            return defaultVal;
        }
        try {
            return Float.valueOf(getString(map, key));
        } catch (Exception ex) {
            logger.debug("转换失败，原 value==" + value);
            return defaultVal;
        }
    }

    /**
     * 获取Double类型的值
     *
     * @param map
     * @param key
     * @return
     */
    public static Double getDouble(Map<String, ? extends Object> map, String key) {
        return getDouble(map, key, null);
    }

    /**
     * 获取Double类型的值
     *
     * @param map
     * @param key
     * @param defaultVal
     * @return
     */
    public static Double getDouble(Map<String, ? extends Object> map, String key, Double defaultVal) {
        if (map == null) {
            return defaultVal;
        }
        Object value = map.get(key);
        if (value == null) {
            return defaultVal;
        }
        try {
            return Double.valueOf(getString(map, key));
        } catch (Exception ex) {
            logger.debug("转换失败，原 value==" + value);
            return defaultVal;
        }
    }

    /**
     * 获取Boolean类型的值
     *
     * @param map
     * @param key
     * @return
     */
    public static Boolean getBoolean(Map<String, ? extends Object> map, String key) {
        return getBoolean(map, key, null);
    }

    /**
     * 获取Boolean类型的值
     *
     * @param map
     * @param key
     * @param defaultVal
     * @return
     */
    public static Boolean getBoolean(Map<String, ? extends Object> map, String key, Boolean defaultVal) {
        if (map == null) {
            return defaultVal;
        }
        Object value = map.get(key);
        if (value == null) {
            return defaultVal;
        }
        try {
            return (value.toString().equalsIgnoreCase("true") ||
                    value.toString().equalsIgnoreCase("on") ||
                    value.toString().equalsIgnoreCase("yes") ||
                    value.toString().equals("1"));
        } catch (Exception ex) {
            logger.debug("转换失败，原 value==" + value);
            return defaultVal;
        }
    }

    /**
     * 获取Date类型的值
     *
     * @param map
     * @param key
     * @return
     */
    public static Date getDate(Map<String, ? extends Object> map, String key) {
        if (map == null) {
            return null;
        }
        Object value = map.get(key);
        if (value == null) {
            return null;
        }
        if (value instanceof Date) {
            return (Date) value;
        }
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            return sdf.parse(value.toString());
        } catch (Exception ex) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                return sdf.parse(value.toString());
            } catch (Exception ex2) {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    return sdf.parse(value.toString());
                } catch (Exception ex3) {
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                        return sdf.parse(value.toString());
                    } catch (Exception ex4) {
                        logger.debug("转换失败，原 value==" + value);
                        return null;
                    }
                }
            }
        }
    }

    /**
     * 根据 key，模糊匹配 map 内容，返回 list 集合
     *
     * @param key
     * @param map
     * @param <V>
     * @return
     */
    public static <V> List<V> likeString(Map<String, V> map, String key) {
        List<V> list = new ArrayList<V>();
        if (map == null || map.size() <= 0) {
            return list;
        }
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, V> entry = (Map.Entry<String, V>) it.next();
            if (entry.getKey().indexOf(key) != -1) {
                list.add(entry.getValue());
            }
        }
        return list;
    }
}