package com.web.common.core.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.*;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author jiangyl
 */
public class HttpClientUtil {
    private static Log log = LogFactory.getLog(HttpClientUtil.class);
    private static ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINESE));
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    }

    private static PoolingHttpClientConnectionManager poolingHttpClientConnectionManager;

    static {
        poolingHttpClientConnectionManager = new PoolingHttpClientConnectionManager(RegistryBuilder
                .<ConnectionSocketFactory>create().register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", SSLConnectionSocketFactory.getSocketFactory()).build());
        poolingHttpClientConnectionManager.setMaxTotal(300);
        poolingHttpClientConnectionManager.setDefaultMaxPerRoute(30);
        new Thread(new CloseExpiredConnectionsRunner(poolingHttpClientConnectionManager)).start();

    }

    private HttpClientUtil() {
    }

    /**
     * 发送http,https方法
     *
     * @param httpMethod                         可选值"GET","POST",HttpMethod.PUT,"DELETE",,HttpMethod.HEAD,HttpMethod.OPTIONS
     * @param url                                请求url
     * @param headers                            请求header
     * @param httpEntity(只有当PUT，POST。PATCH时才起作用) 响应体的编码
     * @param connectTimeout                     请求连接的超时时间，单位为毫秒
     * @param socketTimeout                      响应超时时间，单位为毫秒
     * @param keyStorePath                       证书路径(https时可能设置，如果没有证书设置为null或者"")
     * @param keyStorepass                       证书密码（https时并且证书路径非空的时候才有必要设置，如果证书没有密码，请设置把密码为"nopassword"） *
     * @param maxTotal                           最大连接数(https时可能设置，默认值为20)
     * @param maxPerRout                         每个路由默认连接数（https时可能设置，默认值为2）
     * @return
     * @throws IOException
     */
    public static String send(String httpMethod, String url, Map<String, String> headers, HttpEntity httpEntity,
                              int connectTimeout, int socketTimeout, String keyStorePath, String keyStorepass, int maxTotal,
                              int maxPerRout) throws Exception {
        log.debug("send start");
        Object[] objects = getRequestAndResponse(httpMethod, url, headers, httpEntity, connectTimeout,
                socketTimeout, keyStorePath, keyStorepass, maxTotal, maxPerRout);

        if (objects == null) return "{\"code\":-2,\"msg\":\"网络异常\",\"data\":\"" + "返回的response为空" + "\"}";
        CloseableHttpResponse httpResponse = (CloseableHttpResponse) objects[1];


        if (httpResponse == null) {
            return "{\"code\":-2,\"msg\":\"网络异常\",\"data\":\"" + ((HttpEntityEnclosingRequestBase) objects[0]) + "\"}";
        }
        int http_statuscode = httpResponse.getStatusLine().getStatusCode();
        if (http_statuscode >= 500 && http_statuscode <= 599) {
            return "{\"code\":-2,\"msg\":\"认证异常，返回码=" + http_statuscode + ",头部=" + mapper.writeValueAsString(httpResponse.getAllHeaders()) + ",响应体=" + EntityUtils.toString(httpResponse.getEntity(), "UTF-8") + "\",\"data\":\"\"}";
        }
        HttpEntity entity = httpResponse.getEntity();
        String result = EntityUtils.toString(entity, "UTF-8");
        httpResponse.close();
        if (httpMethod.equals("POST") || httpMethod.equals("PUT"))
            ((HttpEntityEnclosingRequestBase) objects[0]).releaseConnection();
        return result;
    }


    public static Object[] getRequestAndResponse(String httpMethod, String url, Map<String, String> headers,
                                                 HttpEntity httpEntity, int connectTimeout, int socketTimeout, String keyStorePath, String keyStorepass,
                                                 int maxTotal, int maxPerRout) throws Exception {
        // 创建HttpClientBuilder.create()
        HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        // 设置超时
        RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(-1)
                .setConnectTimeout(connectTimeout).setSocketTimeout(socketTimeout).build();
        httpClientBuilder.setDefaultRequestConfig(requestConfig);
        httpClientBuilder.setConnectionManager(poolingHttpClientConnectionManager);

        // 发送 HTTP 请求,重试3次
        CloseableHttpResponse httpResponse = null;
        HttpRequestBase request = null;
        boolean b = false;
        int retrycount = 3;
        for (int retry = 0; retry < retrycount; retry++) {
            // 用HttpClientBuilder创建HttpClient
            CloseableHttpClient httpClient = httpClientBuilder.build();


            // 实例化 连接//省略trace和patch
            request = new HttpGet(url);
            if ("POST" == httpMethod)
                request = new HttpPost(url);
            else if ("PUT" == httpMethod)
                request = new HttpPut(url);
            else if ("DELETE" == httpMethod)
                request = new HttpDelete(url);
            else if ("HEAD" == httpMethod)
                request = new HttpHead(url);
            else if ("OPTIONS" == httpMethod)
                request = new HttpOptions(url);
            // 公共header
            request.setHeader("Accept", "*/*");// COS
            request.setHeader("Connection", "Keep-Alive");// 长连接
            request.setHeader("User-Agent",
                    "Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.87 Safari/537.36");// 发送请求的客户端，伪装成浏览器发送请求，欺骗服务器，不写的话可能被服务端拒绝
            if (headers != null && headers.size() > 0) {// 自定义配置 header 信息
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    request.setHeader(entry.getKey(), entry.getValue());
                }
            }
            // 设置 请求的实体部分(只有POST和PUT和patch才需要设置请求体)
            if (HttpEntityEnclosingRequestBase.class.isAssignableFrom(request.getClass()) && httpEntity != null) {
                // 设置body
                ((HttpEntityEnclosingRequestBase) request).setEntity(httpEntity);
            }


            try {
                httpResponse = httpClient.execute(request);
                if (httpResponse == null) {
                    log.error("HttpClient的response为空");
                    request.abort();
                    request.releaseConnection();
                    Thread.sleep(1000 * (new Random().nextInt(10) + 1));
                    continue;
                } else {
                    b = true;
                    break;// 如果成功，直接返回，无论是第一次，还是第二次，还是第三次
                }
            } catch (Exception e) {
                request.abort();
                request.releaseConnection();
                log.error("HttpClient抛异常" + ExceptionUtils.getStackTrace(e));
                Thread.sleep(1000 * (new Random().nextInt(10) + 1));
                continue;
            } finally {

            }
        }
        if (b == true)
            return new Object[]{request, httpResponse};
        else return null;


    }

    /**
     * @param urlString
     * @param token
     * @param srcFile
     * @param pdfFile
     * @return
     * @throws Exception
     */
    public static void convertToPDF(String urlString, String token, File srcFile, File pdfFile) throws Exception {
        Map<String, Object> requestMap = new HashMap<String, Object>();
        String fileContent = new String(FileUtils.readFileToByteArray(srcFile), Charset.forName("ISO-8859-1"));
        String fileType = FileUtil.getExtension(srcFile);
        long fileSize = srcFile.length();
        requestMap.put("fileContent", fileContent);
        requestMap.put("fileType", fileType);
        requestMap.put("fileSize", fileSize);
        String requestBody = mapper.writeValueAsString(requestMap);
        DataOutputStream dataOutputStream = null;
        BufferedReader responseReader = null;
        HttpURLConnection httpURLConnection = null;
        FileOutputStream fos = null;
        BufferedOutputStream bos = null;
        String result = null;
        try {
            URL url = new URL(urlString);
            URLConnection urlConnection = url.openConnection();
            httpURLConnection = (HttpURLConnection) urlConnection;
            httpURLConnection.setConnectTimeout(3000000);
            httpURLConnection.setReadTimeout(3000000);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestProperty("toke", token);
            httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            httpURLConnection.setRequestProperty("Accept", "application/json;charset=UTF-8");
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.connect();
            OutputStream outputStream = httpURLConnection.getOutputStream();
            dataOutputStream = new DataOutputStream(outputStream);
            dataOutputStream.write(requestBody.getBytes());
            dataOutputStream.flush();
            dataOutputStream.close();
            int reponseCode = httpURLConnection.getResponseCode();
            log.debug("responseCode=" + reponseCode);
            Map<String, List<String>> map = urlConnection.getHeaderFields();
            for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                log.debug(entry.getKey() + "=" + entry.getValue());
            }
            StringBuilder sb = new StringBuilder();
            String readLine = null;
            InputStream inputStream = null;
            if (reponseCode == 200) {
                inputStream = httpURLConnection.getInputStream();
                responseReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                while ((readLine = responseReader.readLine()) != null) {
                    sb.append(readLine).append("\n");
                }
                result = sb.toString();
                log.debug(result);
                Map<String, Object> resultMap = mapper.readValue(result, Map.class);
                String data = (String) resultMap.get("data");
                byte[] buf = data.getBytes("ISO8859-1");

                fos = new FileOutputStream(pdfFile);
                bos = new BufferedOutputStream(fos);
                bos.write(buf);
                bos.close();
                fos.close();

                log.info("转换的pdf文件路径=" + pdfFile.getAbsolutePath());
                responseReader.close();
                httpURLConnection.disconnect();

            }

        } catch (Exception e) {
            log.error("", e);
        } finally {

            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            if (responseReader != null) {
                try {
                    responseReader.close();
                } catch (Exception e) {
                    log.error("", e);
                }
            }

            if (fos != null) {
                try {
                    fos.close();
                } catch (Exception e) {
                    log.error("", e);
                }
            }
            if (bos != null) {
                try {
                    bos.close();
                } catch (Exception e) {
                    log.error("", e);
                }
            }

            if (dataOutputStream != null) {
                try {
                    dataOutputStream.close();
                } catch (Exception e) {
                    log.error("", e);
                }
            }
        }
    }

    /**
     * @param httpMethod
     * @param url
     * @param token
     * @param object
     * @param contentType 值设置为application/json;charset=UTF-8或者application/x-www-form-urlencoded或者multipart/form-data"
     * @return
     * @throws Exception
     */
    private static String postOrPut(String httpMethod, String url, String token, Object object, String contentType)
            throws Exception {
        Map<String, String> headers = new HashMap<>();

        if (StringUtils.isNotBlank(token)) {
            headers.put("token", token);
            headers.put("Authorization", token);// 兼容有些网站把认证信息放入头部的Authorization
        }
        HttpEntity httpEntity = null;
        if (object == null) {

        } else if (object instanceof File) {
            File file = (File) object;
            MultipartEntityBuilder multipartEntityBuilder = MultipartEntityBuilder.create();
            multipartEntityBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            multipartEntityBuilder.setCharset(Consts.UTF_8);
            multipartEntityBuilder.addBinaryBody("file", file);
            httpEntity = multipartEntityBuilder.build();
        } else if (object instanceof String) {
            String s = object.toString().trim();
            // headers.put("Accept", "application/json;charset=UTF-8");
            if (!s.startsWith("[") && !s.startsWith("{") && s.contains("=")) {// 传统表单式
                StringEntity stringEntity = new StringEntity(s, "UTF-8");
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                stringEntity.setContentType("application/x-www-form-urlencoded");
                httpEntity = stringEntity;
            } else {// json字符串
                StringEntity stringEntity = new StringEntity(s, "UTF-8");
                headers.put("Content-Type", "application/json;charset=UTF-8");
                stringEntity.setContentType("application/json;charset=UTF-8");
                httpEntity = stringEntity;
            }
        } else {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
            if (contentType == null || contentType.equals("application/json;charset=UTF-8")) {
                mapper.writeValueAsString(object);
                StringEntity stringEntity = new StringEntity(mapper.writeValueAsString(object), "UTF-8");
                headers.put("Accept", "application/json;charset=UTF-8");
                headers.put("Content-Type", "application/json;charset=UTF-8");
                stringEntity.setContentType("application/json;charset=UTF-8");
                httpEntity = stringEntity;
            } else if (contentType.equals("application/x-www-form-urlencoded")) {// 如果是x-www-form-urlencoded
                // 只处理第一层，其他层依然为json

                if (object instanceof Map) {// map类
                    Map<String, Object> map = (Map<String, Object>) object;

                    StringBuilder buff = new StringBuilder();
                    for (Map.Entry<String, Object> entry : map.entrySet()) {
                        String value2 = "";
                        if (entry.getKey() != null && entry.getValue() instanceof Date) {
                            value2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(entry.getValue());
                        } else if (entry.getKey() != null && entry.getValue() instanceof String) {
                            value2 = entry.getValue().toString();
                        } else {
                            value2 = mapper.writeValueAsString(object);
                        }
                        buff.append(entry.getKey()).append("=").append(value2).append("&");

                    }
                    if (buff.length() > 0) {
                        buff.setLength(buff.length() - 1);
                    }
                    StringEntity stringEntity = new StringEntity(buff.toString(), "UTF-8");
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    stringEntity.setContentType("application/x-www-form-urlencoded");
                    httpEntity = stringEntity;

                } else {// 自定义类
                    StringBuilder buff = new StringBuilder();
                    BeanInfo beanInfo = Introspector.getBeanInfo(object.getClass());
                    PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
                    for (PropertyDescriptor property : propertyDescriptors) {
                        String key = property.getName();
                        if (!key.equals("class")) {// 过滤不需要的字段
                            Method getter = property.getReadMethod();
                            Object value = getter.invoke(object);
                            String value2 = "";
                            if (property.getPropertyType() == Date.class && value != null) {
                                value2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(value);
                            } else if (property.getPropertyType() == String.class && value != null) {
                                value2 = value.toString();
                            } else {
                                value2 = mapper.writeValueAsString(object);
                            }
                            buff.append(key).append("=").append(value2).append("&");
                        }
                    }

                    if (buff.length() > 0) {
                        buff.setLength(buff.length() - 1);
                    }
                    StringEntity stringEntity = new StringEntity(buff.toString(), "UTF-8");
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    stringEntity.setContentType("application/x-www-form-urlencoded");
                    httpEntity = stringEntity;
                }

            } else if (contentType.equals("multipart/form-data")) {// 腾讯
                if (object instanceof Map) {// map类
                    Map<String, Object> map = (Map<String, Object>) object;

                    MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
                    for (String paramKey : map.keySet()) {
                        if (paramKey.equalsIgnoreCase("fileContent")) {// 兼容腾讯对象存储，分片上传
                            entityBuilder.addBinaryBody("fileContent",
                                    ((String) map.get("fileContent")).getBytes(Charset.forName("ISO-8859-1")));
                        } else if (map.get(paramKey) instanceof Date) {
                            String value2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(map.get(paramKey));
                            entityBuilder.addTextBody(paramKey, value2, ContentType.create("text/plain", Consts.UTF_8));
                        } else {
                            entityBuilder.addTextBody(paramKey, map.get(paramKey).toString(),
                                    ContentType.create("text/plain", Consts.UTF_8));
                        }
                    }

                    httpEntity = entityBuilder.build();
                    // headers.put("Content-Type",
                    // "multipart/form-data;boundary="+System.currentTimeMillis());
                    // //这一行不能添加，原因为止
                }

            } else {
                return "contentType参数不对，请把值设置为application/json;charset=UTF-8或者application/x-www-form-urlencoded或者multipart/form-data";
            }
        }
        return HttpClientUtil.send(httpMethod, url, headers, httpEntity, 10000000, 60000000, null, null, 20, 2);
    }

    /**
     * @param httpMethod
     * @param url
     * @param token
     * @param object
     * @return
     * @throws Exception
     */
    private static String getOrDelete(String httpMethod, String url, String token, Object object) throws Exception {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        // headers.put("Accept", "application/json;charset=UTF-8");
        if (StringUtils.isNotBlank(token)) {
            headers.put("token", token);
            headers.put("Authorization", token);// 兼容有些网站把认证信息放入头部的Authorization
        }
        String urlparams = "";
        if (object == null) {

        } else if (object instanceof String) {
            urlparams = object.toString();
        } else if (object instanceof Map) {
            Map<String, Object> map = (Map<String, Object>) object;
            for (Map.Entry<String, Object> entry : map.entrySet()) {

                if (entry.getKey() != null && entry.getValue() instanceof Date) {
                    urlparams += "&" + entry.getKey() + "="
                            + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(entry.getValue());
                } else
                    urlparams += "&" + entry.getKey() + "=" + entry.getValue();
            }
        } else {
            BeanInfo beanInfo = Introspector.getBeanInfo(object.getClass());
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();
            for (PropertyDescriptor property : propertyDescriptors) {
                String key = property.getName();
                if (!key.equals("class")) {// 过滤不需要的字段
                    Method getter = property.getReadMethod();
                    Object value = getter.invoke(object);
                    String value2 = "";
                    if (property.getPropertyType() == Date.class && value != null) {
                        value2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(value);
                    } else
                        value2 = (value == null ? "" : value.toString());
                    urlparams += "&" + key + "=" + value2;
                }
            }
        }
        if (urlparams.startsWith("&"))
            urlparams = urlparams.substring(1, urlparams.length());
        if (url.contains("?"))
            url = url + "&" + urlparams;
        else
            url = url + "?" + urlparams;
        return HttpClientUtil.send(httpMethod, url, headers, null, 5000000, 5000000, null, null, 20, 2);
    }

    /**
     * Get请求
     *
     * @param url    GET请求的url，后面key带上?a=1&b=2格式的参数，请求头部默认有两个属性Content-Type,Accept，值分别为application/x-www-form-urlencoded，application/json;charset=UTF-8
     * @param token  访问令牌，放到header中，如果没有，设置为空
     * @param object null或者字符串类型或者map类型或者或者实体对象， 如果是null，url不做处理，直接发送请求
     *               如果是字符串，正确格式应该类似这样，a=1&b=2&b=3&c=4，此字符串将直接被追加到url的后面，
     *               如果是Map<String,Object>,将会被转换为key1=value1&key2=value2&key3=value3字符串追加到url的后面,其中剑值对的值类型要是Date，将自动转换为"yyyy-MM-dd
     *               HH:mm:ss"字符串
     *               如果是自定义对象，包含属性和值,将会被转换为prop1=value1&prop2=value2&prop3=value3字符串追加到url的后面,其中属性的类型要是Date，属性值将自动转换为"yyyy-MM-dd
     *               HH:mm:ss"字符串
     * @return 响应内容
     * @throws Exception
     */
    public static String get(String url, String token, Object object) throws Exception {
        log.debug("get start");
        return getOrDelete("GET", url, token, object);

    }

    public static String get(String url, String token) throws Exception {
        log.debug("get start");
        return get(url, token, null);

    }

    /**
     * 说明同Get请求
     *
     * @param url
     * @param token
     * @param object
     * @return
     * @throws Exception
     */
    public static String delete(String url, String token, Object object) throws Exception {
        log.debug("delete start");
        return getOrDelete("DELETE", url, token, object);

    }

    /**
     * Post请求
     *
     * @param url    POST请求的url
     * @param token  访问令牌，放到header中，如果没有，设置为空
     * @param object null或者文件或者json字符串或者map类型或者或者实体对象， 如果是null，直接发送请求
     *               如果是文件,文件应当是具体的文件（不是文件夹）存在并可读
     *               如果是json字符串，正确格式应该类似这样，{'a':'1','b','2','c','3'}等等，此字符串将直接放入请求体中，请求头部默认有两个属性Content-Type,Accept，值分别为application/json;charset=UTF-8，application/json;charset=UTF-8
     *               如果是传统表单式字符串，类似这样，userName=testsj&passWord=test321"，此字符串将直接放入请求体中，请求头部默认有两个属性Content-Type,Accept，值分别为application/x-www-form-urlencoded，application/json;charset=UTF-8
     *               如果是其他类型，将会被转换为标准JSON字符串放入请求体,其中属性的类型要是Date，属性值将自动转换为"yyyy-MM-dd
     *               HH:mm:ss"字符串，请求头部默认有两个属性Content-Type,Accept，值分别为application/json;charset=UTF-8，application/json;charset=UTF-8
     * @return 响应内容
     * @throws Exception
     */
    public static String post(String url, String token, Object object) throws Exception {
        log.debug("post start");
        return postOrPut("POST", url, token, object, "application/json;charset=UTF-8");
    }

    /**
     * @param url
     * @param token
     * @param object
     * @param contentType 值设置为application/json;charset=UTF-8或者application/x-www-form-urlencoded或者multipart/form-data"
     * @return
     * @throws Exception
     */
    public static String post(String url, String token, Object object, String contentType) throws Exception {
        log.debug("post start");
        return postOrPut("POST", url, token, object, contentType);
    }

    /**
     * 说明同post
     *
     * @param url
     * @param token
     * @param object
     * @return
     * @throws Exception
     */
    public static String put(String url, String token, Object object) throws Exception {
        log.debug("put start");
        return postOrPut("PUT", url, token, object, "application/json;charset=UTF-8");
    }

    /**
     * 说明同post
     *
     * @param url
     * @param token
     * @param object
     * @param contentType 值设置为application/json;charset=UTF-8或者application/x-www-form-urlencoded或者multipart/form-data"
     * @return
     * @throws Exception
     */
    public static String put(String url, String token, Object object, String contentType) throws Exception {
        log.debug("put start");
        return postOrPut("PUT", url, token, object, contentType);
    }


    public static String send(String httpmethod, String urlString, String token, String bodyString) throws Exception {

        DataOutputStream dataOutputStream = null;
        BufferedReader responseReader = null;
        HttpURLConnection httpURLConnection = null;
        String result = null;
        try {
            URL url = new URL(urlString);
            URLConnection urlConnection = url.openConnection();
            httpURLConnection = (HttpURLConnection) urlConnection;
            httpURLConnection.setConnectTimeout(3000000);
            httpURLConnection.setReadTimeout(3000000);
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(false);
            httpURLConnection.setRequestMethod(httpmethod);

            httpURLConnection.setRequestProperty("token", token);
            httpURLConnection.setRequestProperty("Accept", "application/json;charset=UTF-8");
            if ("POST".equalsIgnoreCase(httpmethod) || "PUT".equalsIgnoreCase(httpmethod)) {
                httpURLConnection.setRequestProperty("Content-Type", "application/json;charset=UTF-8");
            } else {
                httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            }
            httpURLConnection.connect();
            if (("POST".equalsIgnoreCase(httpmethod) || "PUT".equalsIgnoreCase(httpmethod)) && StringUtils.isNotBlank(bodyString)) {
                OutputStream outputStream = httpURLConnection.getOutputStream();
                dataOutputStream = new DataOutputStream(outputStream);
                dataOutputStream.writeUTF(bodyString);
                dataOutputStream.flush();
                dataOutputStream.close();
            }
            // 构建请求体结束
            // 获得响应状态
            int reponseCode = httpURLConnection.getResponseCode();
            log.info("responseCode=" + reponseCode);
            // 获得响应头部
            Map<String, List<String>> map = urlConnection.getHeaderFields();
            for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                log.info(entry.getKey() + "=" + entry.getValue());
            }
            StringBuilder sb = new StringBuilder();
            String readLine = null;
            InputStream inputStream = null;
            inputStream = httpURLConnection.getInputStream();
            responseReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            while ((readLine = responseReader.readLine()) != null) {
                sb.append(readLine).append("\n");
            }
            result = sb.toString();
            log.info(result);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            if (httpURLConnection != null) {
                httpURLConnection.disconnect();
            }
            if (responseReader != null) {
                try {
                    responseReader.close();
                } catch (Exception e) {
                    log.error("", e);
                }
            }
            if (dataOutputStream != null) {
                try {
                    dataOutputStream.close();
                } catch (Exception e) {
                    log.error("", e);
                }
            }
        }
        return result;


    }


}


class CloseExpiredConnectionsRunner implements Runnable {
    public static Log log = LogFactory.getLog(CloseExpiredConnectionsRunner.class);
    HttpClientConnectionManager httpClientConnectionManager;
    private volatile boolean shutdown;

    public CloseExpiredConnectionsRunner(HttpClientConnectionManager httpClientConnectionManager) {
        this.httpClientConnectionManager = httpClientConnectionManager;
    }

    public void run() {
        try {
            while (!shutdown) {
                synchronized (this) {
                    wait(5000);
                    httpClientConnectionManager.closeExpiredConnections();
                }
            }
        } catch (Exception e) {
            log.error("", e);
        }
    }

    public void shutdown() {
        synchronized (this) {
            shutdown = true;
            notifyAll();
        }
    }


}