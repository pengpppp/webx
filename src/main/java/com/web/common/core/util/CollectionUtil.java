/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.util;

import java.util.*;

/**
 * 集合工具类
 */

public class CollectionUtil {

    public static <K> List<K> newArrayList() {
        return new ArrayList<>();
    }

    public static <K> List<K> newArrayList(Collection<K> c) {
        return new ArrayList<>(c);
    }

    public static <K> List<K> newArrayList(K... c) {
        return new ArrayList<>(Arrays.asList(c));
    }

    public static <K> List<K> newArrayListSize(int size) {
        return new ArrayList<>(size);
    }

    public static <K> LinkedList<K> newLinkedList() {
        return new LinkedList<>();
    }

    public static <K> LinkedList<K> newLinkedList(Collection<K> c) {
        return new LinkedList<>(c);
    }


    /**
     * 为空
     **/
    public static boolean isEmpty(Collection<?> collection) {
        return (collection == null || collection.isEmpty());
    }

    /**
     * 不空
     **/
    public static boolean isNotEmpty(Collection<?> collection) {
        return !isEmpty(collection);
    }

    /**
     * 为空
     **/
    public static boolean isEmpty(Map<?, ?> map) {
        return (map == null || map.isEmpty());
    }

    /**
     * 不空
     **/
    public static boolean isNotEmpty(Map<?, ?> map) {
        return !isEmpty(map);
    }


    /**
     * 数组是否为空
     *
     * @param <T>   数组元素类型
     * @param array 数组
     * @return 是否为空
     */
    @SuppressWarnings("unchecked")
    public static <T> boolean isEmpty(final T... array) {
        return array == null || array.length == 0;
    }

    /**
     * 数组是否不为空
     *
     * @param array
     * @param <T>
     * @return
     */
    public static <T> boolean isNotEmpty(final T... array) {
        return !isEmpty(array);
    }

    /**
     * 是否包含{@code null}元素
     *
     * @param <T>   数组元素类型
     * @param array 被检查的数组
     * @return 是否包含{@code null}元素
     * @since 3.0.7
     */
    @SuppressWarnings("unchecked")
    public static <T> boolean hasNull(T... array) {
        if (isNotEmpty(array)) {
            for (T element : array) {
                if (null == element) {
                    return true;
                }
            }
        }
        return false;
    }
}
