package com.web.common.core.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * IP工具类
 * 
 * @author jiangyl
 * 
 */
public class IpUtil {
	private static Log log = LogFactory.getLog(IpUtil.class);

	private IpUtil() {

	}

	private static ObjectMapper mapper;
	static {
		mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINESE));
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
	}
	
	private static final String unknown = "unknown";

	/**
	 * 测试指定ip是否能够连通
	 * 
	 * @param ip
	 *            正确的ipv4或者ipv6，或者主机名或者域名
	 * @return true代表能够连通，false代表无法连通
	 * @throws Exception
	 */
	public static boolean getIpStatus(String ip) {
		log.debug("getPingStatus start");
		BufferedReader bufferedReader =  null;
		try {
			List<String> commend = new java.util.ArrayList<String>();
			commend.add("ping");
			commend.add(ip);
			if (System.getProperty("os.name").toLowerCase().indexOf("windows") >= 0)

				commend.add("-n");
			else
				commend.add("-c");
			commend.add("3");
			commend.add("-w");
			commend.add("5");
			String pingCommand = commend.toString().replace("[", " ").replace(",", " ").replace("]", " ");
			log.debug(pingCommand);

			bufferedReader = new BufferedReader(
					new InputStreamReader(Runtime.getRuntime().exec(pingCommand).getInputStream()));
			for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
				if (line.matches(".*\\s+TTL=\\d+.*")) {
					bufferedReader.close();
					return true;
				}
			}
		} catch (Exception e) {
			log.error("", e);
		}finally {
			try {
				if (bufferedReader != null)
					bufferedReader.close();
			} catch (Exception e) {
				log.error("", e);			
			}
		}
		return false;
	}

	/**
	 * 测试指定ip的端口是否能够连通
	 * 
	 * @param ip
	 *            正确的ipv4或者ipv6，或者主机名或者域名
	 * @param ip
	 *            主机端口
	 * @return true代表能够连通，false代表无法连通
	 * @throws Exception
	 */
	public static boolean getPortStatus(String ip, int port) {
		log.debug("getPortStatus start");
		Socket socket = null;
		try {
			socket = new Socket();
			socket.connect(new InetSocketAddress(ip, port));
			return true;
		} catch (Exception e) {
			log.error("", e);
		} finally {
			try {
				if (socket != null)
					socket.close();
			} catch (Exception e) {
				log.error("", e);			
			}
		}

		return false;
	}

	public static boolean getUrlStatus(String url) {
		URLConnection connection = null;
		try {
			URL u = new URL(url);
			connection = u.openConnection();
			connection.setConnectTimeout(5000);
			connection.connect();
			return true;
		} catch (Exception e1) {
			log.error("",e1);			
		}
		return false;
	}

	/**
	 * 获取客户端ip地址(可以穿透代理)
	 * 
	 * @param request
	 * @return
	 */
	public static String getIpAddr(HttpServletRequest request) {
		log.debug("getIpAddr start");
		String ip = request.getHeader("X-Real-IP");
		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
			ip = request.getHeader("x-forwarded-for");
		}
		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}

		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}
		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_FORWARDED");
		}
		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_X_CLUSTER_CLIENT_IP");
		}

		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_CLIENT_IP");
		}

		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_FORWARDED_FOR");
		}

		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_FORWARDED");
		}

		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
			ip = request.getHeader("HTTP_VIA");
		}

		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
			ip = request.getHeader("REMOTE_ADDR");
		}

		if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();// 没有正反向代理
		}
		if (ip.equals("0:0:0:0:0:0:0:1")) {
			ip = "127.0.0.1";
		}
		if (ip.split(",").length > 1) {// 因为有些有些登录是通过代理，所以取第一个（第一个为真是ip）
			ip = ip.split(",")[0];
		}
		return ip;
	}

	/**
	 * 通过IP获取地址(需要联网，调用淘宝的IP库)
	 * 
	 * @param ip
	 *            正确的ip地址
	 * @return
	 */
	public static String getIpInfo(String ip) {
		log.debug("getIpInfo start");
		// 如果输入的是域名，先转换为ip
		HttpURLConnection httpURLConnection = null;
		BufferedReader responseReader = null;
		try {
			URL url = new URL("http://ip.taobao.com/service/getIpInfo.php?ip=" + ip);
			httpURLConnection = (HttpURLConnection) url.openConnection();
			httpURLConnection.setRequestMethod("GET");
			httpURLConnection.setDoOutput(true);
			httpURLConnection.setDoInput(true);
			httpURLConnection.setUseCaches(false);
			responseReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(), "UTF-8"));
			StringBuilder temp = new StringBuilder();
			for (String readLine = null; (readLine = responseReader.readLine()) != null;) {
				temp.append(readLine).append("\n");
			}
			responseReader.close();
			String result = temp.toString();
			httpURLConnection.disconnect();
			Map<String, Object> responseMap = new HashMap<String, Object>();
			Map<String, Object> map = mapper.readValue(result, Map.class);
			if ((int) map.get("code") == 0) {
				Map<String, Object> data = (Map<String, Object>) map.get("data");
				responseMap.put("status", true);
				responseMap.put("country", data.get("country"));
				responseMap.put("region", data.get("region"));
				responseMap.put("city", data.get("city"));
				responseMap.put("isp", data.get("isp"));
			} else {
				responseMap.put("status", false);
			}
			return mapper.writeValueAsString(responseMap);
		} catch (Exception e) {
			log.error("", e);
		} finally {
			if (httpURLConnection != null) {
				httpURLConnection.disconnect();
			}
			if (responseReader != null) {
				try {
					responseReader.close();
				} catch (Exception e) {
					log.error("", e);
				}
			}
		}
		return null;
	}

}
