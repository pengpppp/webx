package com.web.common.core.util;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.core.type.classreading.CachingMetadataReaderFactory;
import org.springframework.core.type.classreading.MetadataReader;
import org.springframework.core.type.classreading.MetadataReaderFactory;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.core.type.filter.TypeFilter;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RestInfoUtil {

	private static Log log = LogFactory.getLog(RestInfoUtil.class);
	private static ObjectMapper mapper;
	static {
		mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINESE));
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
	}

	private RestInfoUtil() {
	}

	/**
	 * 扫描指定路径下的所有非法rest 
	 * @param basePackage:指定类路径，比如com.ylp
	 * @return 如果全部符合标准，返回true，否则返回false
	 * @throws Exception
	 */
	public static List<Map<String, Object>> listErrorRestInfo(String basePackage) throws Exception {
		boolean b = true;
		String result = RestInfoUtil.listRestInfo(basePackage);
		List<Map<String, Object>> list = (List<Map<String, Object>>) mapper.readValue(result, List.class);
		List<Map<String, Object>> errorList = new ArrayList<Map<String, Object>>();
		for (Map map : list) {
			String rest = (String) map.get("rest");
			rest = rest.substring(rest.lastIndexOf("/") + 1);
			if (rest.startsWith("get") || rest.startsWith("list") || rest.startsWith("count") || rest.startsWith("check") || rest.startsWith("save")
					|| rest.startsWith("update") || rest.startsWith("delete") || rest.startsWith("upload")
					|| rest.startsWith("download") || rest.startsWith("import") || rest.startsWith("export")
					|| rest.startsWith("batchSave") || rest.startsWith("batchUpdate") || rest.startsWith("batchDelete")
					|| rest.startsWith("batchUpload") || rest.startsWith("batchImport")) {
				continue;
			}
			errorList.add(map);
			log.error(map);

		}

		return errorList;
	}

	/**
	 * 扫描指定路径下的所有rest
	 * 
	 * @param basePackage:指定类路径，比如com.ylp
	 * @return 返回为json格式的数组，每个数组元素包含三个属性，分别为：类名，rest路径，http请求方法
	 * @throws Exception
	 */
	public static String listRestInfo(String basePackage) throws Exception {
		log.debug("getRestURL start 扫描路径=" + basePackage);

		Set<Class<?>> clazzSet = new HashSet<Class<?>>();
		String resourcePattern = "**/*.class";
		ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
		MetadataReaderFactory metadataReaderFactory = new CachingMetadataReaderFactory(resourcePatternResolver);
		String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX
				+ ClassUtils.convertClassNameToResourcePath(basePackage) + File.separator + resourcePattern;
		Resource[] resources = resourcePatternResolver.getResources(packageSearchPath);
		for (Resource resource : resources) {
			MetadataReader reader = metadataReaderFactory.getMetadataReader(resource);
			String clazzName = reader.getClassMetadata().getClassName();
			TypeFilter filter = new AnnotationTypeFilter(RequestMapping.class, false);
			if (filter.match(reader, metadataReaderFactory)) {
				clazzSet.add(Class.forName(clazzName));
			}
		}

		// ==============================

		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		for (Class c : clazzSet) {
			RequestMapping req = (RequestMapping) c.getAnnotation(RequestMapping.class);
			String[] bath = req.path().length > 0 ? req.path() : req.value();
			if (bath.length == 0) {
				continue;
			}
			Method[] ms = c.getDeclaredMethods();
			for (Method m : ms) {
				RequestMethod[] requestMethods = null;
				String[] bath2 = null;
				String httpMethod = null;
				RequestMapping rm = m.getAnnotation(RequestMapping.class);
				if (rm != null) {
					bath2 = rm.path().length > 0 ? rm.path() : rm.value();
					requestMethods = rm.method();
					if (requestMethods.length == 0)
						httpMethod = "GET";
					else {
						httpMethod = requestMethods[0].toString();
					}
				} else {
					GetMapping gm = m.getAnnotation(GetMapping.class);
					if (gm != null) {
						bath2 = gm.path().length > 0 ? gm.path() : gm.value();
						httpMethod = "GET";
					} else {
						PutMapping pm = m.getAnnotation(PutMapping.class);
						if (pm != null) {
							bath2 = pm.path().length > 0 ? pm.path() : pm.value();
							httpMethod = "PUT";
						} else {
							PostMapping postmapping = m.getAnnotation(PostMapping.class);
							if (postmapping != null) {
								bath2 = postmapping.path().length > 0 ? postmapping.path() : postmapping.value();
								httpMethod = "POST";
							} else {
								DeleteMapping dm = m.getAnnotation(DeleteMapping.class);
								if (dm != null) {
									bath2 = dm.path().length > 0 ? dm.path() : dm.value();
									httpMethod = "DELETE";
								}
							}
						}
					}
				}
				if (bath2 == null || bath2.length == 0) {
					log.error("扫描错误=" + c.getName() + " " + bath[0]);
					continue;
				}

				Map<String, Object> map = new HashMap<String, Object>();
				map.put("clazzName", c.getName());
				map.put("rest", bath[0] + bath2[0]);
				map.put("httpmethod", httpMethod);
				list.add(map);

			}
		}

		log.debug("getRestInfo end 所在类个数=" + clazzSet.size() + ",rest个数=" + list.size());

		return mapper.writeValueAsString(list);
	}

	/**
	 * 格式化jsonStr
	 * @param jsonStr : 要格式化的json字符串
	 * @return：格式化后的字符串
	 */
	public static String formatJson(String jsonStr) {
		if (null == jsonStr || "".equals(jsonStr))
			return "";
		StringBuilder sb = new StringBuilder();
		char current = '\0';
		int indent = 0;
		boolean isInQuotationMarks = false;
		for (int i = 0; i < jsonStr.length(); i++) {
			char last = current;
			current = jsonStr.charAt(i);
			switch (current) {
			case '"':
				if (last != '\\') {
					isInQuotationMarks = !isInQuotationMarks;
				}
				sb.append(current);
				break;
			case '{':
			case '[':
				sb.append(current);
				if (!isInQuotationMarks) {
					sb.append('\n');
					indent++;
					addIndentBlank(sb, indent);
				}
				break;
			case '}':
			case ']':
				if (!isInQuotationMarks) {
					sb.append('\n');
					indent--;
					addIndentBlank(sb, indent);
				}
				sb.append(current);
				break;
			case ',':
				sb.append(current);
				if (last != '\\' && !isInQuotationMarks) {
					sb.append('\n');
					addIndentBlank(sb, indent);
				}
				break;
			default:
				sb.append(current);
			}
		}

		return sb.toString();
	}

	/**
	 * 添加space
	 * 
	 */
	private static void addIndentBlank(StringBuilder sb, int indent) {
		for (int i = 0; i < indent; i++) {
			sb.append('\t');
		}
	}
}