package com.web.common.core.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.web.common.core.config.G;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author jiangyl
 * Redis工具类:支持get，set及超时设置，如果是复杂对象，建议先用jackon转换成json格式的字符串作为value存入，注意时间类型添加注解
 */
public class RedisUtil {

    private static Logger log = Logger.getLogger(ExceptionUtil.class);
    private static ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINESE));
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
    }

    private RedisUtil() {
    }

    private static JedisCluster jedisCluster;
    protected static String systemEnv;
    private static JedisPool jedisPool;

    static {
        try {
            systemEnv = G.SYSTEM + ":" + G.ENV + ":";

            Configuration configuration = new Configurations().properties(new File("/", "redis.properties"));
            JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
            jedisPoolConfig.setMaxIdle(configuration.getInt("maxidle"));//能控制的实例，默认为8
            jedisPoolConfig.setMaxTotal(configuration.getInt("maxtotal"));//当前时刻能控制的实例，默认8
//            jedisPoolConfig.setMaxWaitMillis(-1);//默认为-1
//            jedisPoolConfig.setMinIdle(0);//默认为0

//
//            Set<HostAndPort> set = new HashSet<HostAndPort>();
//            String[] servers = configuration.getString("servers").split(",");
//            for (int i = 0; i < servers.length; i++) {
//                int port = 6379;
//                String[] info = servers[i].split(":");
//                String ip = info[0];
//                if (info.length > 1) {
//                    port = Integer.parseInt(info[1]);
//                }
//                log.debug("redis ip=" + ip + ",port=" + port);
//                set.add(new HostAndPort(ip, port));
//            }
//            System.out.println(set);
            try {
                jedisPool = new JedisPool(jedisPoolConfig,configuration.getString("host"),configuration.getInt("port"),configuration.getInt("connectiontimeout"),null,1);
//                jedisCluster = new JedisCluster();
//                jedisCluster = new JedisCluster(new HostAndPort("127.0.0.1",6379), configuration.getInt("connectiontimeout"),
//                        configuration.getInt("sotimeout"), configuration.getInt("maxattempts"),
//                        null, jedisPoolConfig);
            }catch (Exception e)
            {
                e.printStackTrace();
            }

      //      log.debug("redis cluster name=" + jedisCluster.get("name"));
            shutDownHook();
        } catch (Exception e) {
            log.error(ExceptionUtils.getStackTrace(e));
        }
    }

    public static JedisPool getRedis() {
        return jedisPool;
    }

    /**
     * 添加关闭钩子
     */
    private static void shutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                try {
                    jedisPool.close();
                    //jedisCluster.close();
                } catch (Exception e) {
                    log.error("Redis关闭错误");
                }
            }
        });
    }


    /**
     * 根据key获取记录
     *
     * @param key
     * @return 值
     */
    public static String get(String key) {
        return jedisPool.getResource().get(key);
       // return jedisCluster.get(systemEnv + key);

    }

    /**
     * 添加没有过期时间的记录
     *
     * @param key
     * @param value
     * @param time  过期时间，以秒为单位
     * @return String 操作状态
     */
    public static String set(String key, String value, String nxxx, String expx, long time) {
        return jedisPool.getResource().set(key, value, nxxx, expx, time);
    }

    /**
     * 添加没有过期时间的记录
     *
     * @param key
     * @param value
     * @return String 操作状态
     */
    public static String set(String key, String value) {

        return jedisPool.getResource().set(key,value);
     //   return jedisCluster.set(systemEnv + key, value);
    }

    /**
     * 添加有过期时间的记录
     *
     * @param key
     * @param value
     * @param seconds 过期时间，以秒为单位
     * @return String 操作状态
     */
    public static String setex(String key, int seconds, String value) {
        return jedisPool.getResource().setex( key, seconds, value);

    }

    /**
     * 添加存在不新增的记录,并且有超时功能
     *
     * @param key
     * @param value
     * @param seconds 过期时间，以秒为单位
     * @return String 操作状态
     */
    public static String setnx(String key, int seconds, String value) {
        return jedisPool.getResource().set(key, value, "NX", "EX", seconds);
    }


    /**
     * 删除keys对应的记录,可以是多个key
     *
     * @param keys
     * @return 删除的记录数
     */
    public static long del(String... keys) {
//        String[] newkeys = new String[keys.length];
//        for (int i = 0; i < keys.length; i++) {
//            newkeys[i] = systemEnv + keys[i];
//        }
        return jedisPool.getResource().del(keys);
    }

    /**
     * 判断key是否存在，存在返回true
     *
     * @param key
     * @return
     */
    public static boolean exists(String key) {
        return jedisPool.getResource().exists(key);
     //   return jedisCluster.exists(systemEnv + key);
    }

    /**
     * 设置过期时间
     *
     * @param key
     * @param seconds
     * @return 返回影响的记录数
     */
    public static void expire(String key, int seconds) {
        jedisPool.getResource().expire(key,seconds);
      //  jedisCluster.expire(systemEnv + key, seconds);

    }

    /**
     * 查询key的过期时间
     *
     * @param key
     * @return 以秒为单位的时间表示
     */
    public static long ttl(String key) {
        return jedisPool.getResource().ttl(key);
       // return jedisCluster.ttl(systemEnv + key);
    }


    /**
     * 左边入队
     *
     * @param key
     * @return
     */
    public static long lpush(String key, String value) {
        return jedisPool.getResource().lpush(key,value);
     //   return jedisCluster.lpush(systemEnv + key, value);
    }

    /**
     * 右边出对
     *
     * @param key
     * @return
     */
    public static String rpop(String key) {
        return jedisPool.getResource().rpop(key);
     //   return jedisCluster.rpop(systemEnv + key);
    }


    /**
     * 为哈希表中的字段赋值 。如果哈希表不存在，一个新的哈希表被创建并进行 HSET 操作。如果字段已经存在于哈希表中，旧值将被覆盖
     *
     * @param key   键
     * @param field 哈希key
     * @param value 要赋的值
     * @return
     */
    public static long hset(String key, String field, String value) {
        return jedisPool.getResource().hset(key,field,value);
        //return jedisCluster.hset(systemEnv + key, field, value);
    }

    /**
     * 删除哈希表 key 中的一个或多个指定字段，不存在的字段将被忽略
     *
     * @param key   键
     * @param field 哈希key数组
     * @return
     */
    public static long hdel(String key, String... field) {
        return jedisPool.getResource().hdel(key, field);
    }

    /**
     * 获取哈希表中的哈希键对应的值
     *
     * @param key   键
     * @param field 哈希key
     * @return
     */
    public static String hget(String key, String field) {
        return jedisPool.getResource().hget(key, field);
    }

    /**
     * 取出hashMap
     *
     * @param key
     * @return
     */
    public static Map<String, String> hgetAll(String key) {
        return jedisPool.getResource().hgetAll(key);
    }


    /**
     * 长度
     *
     * @param key
     * @return
     */
    public static Long llen(String key) {
        return jedisPool.getResource().llen(key);
    }

    /**
     * 将一个或多个 member 元素加入到集合 key 当中
     *
     * @param key
     * @param member
     * @return
     */
    public static Long sadd(String key, String... member) {
        return jedisPool.getResource().sadd(key, member);
    }

    /**
     * 移除集合中的一个或多个成员元素，不存在的成员元素会被忽略。
     *
     * @param key
     * @param member
     * @return
     */
    public static Long srem(String key, String... member) {
        return jedisPool.getResource().srem(key, member);
    }

    /**
     * 对key对应的值+1,返回相加后的结果
     *
     * @param key
     * @return
     */
    public static Long incr(String key) {
        return jedisPool.getResource().incr(key);
    }


    /**
     * 添加有序集合
     *
     * @param key    键
     * @param score  排序分数，可用来做范围取值
     * @param member 值
     * @return
     */
    public static long zadd(String key, double score, String member) {
        return jedisPool.getResource().zadd(key, score, member);
    }

    /**
     * 通过map添加有序集合
     *
     * @param key          键
     * @param scoreMembers
     * @return
     */
    public static long zadd(String key, Map<String, Double> scoreMembers) {
        return jedisPool.getResource().zadd(key, scoreMembers);
    }

    /**
     * 返回有序集合中指定分数区间的成员列表(分数是指zadd添加进来的score参数)
     *
     * @param key 键
     * @param min
     * @param max
     * @return
     */
    public static Set<String> zrangeByScore(String key, double min, double max) {
        return jedisPool.getResource().zrangeByScore(key, min, max);
    }

    /**
     * 返回有序集合中指定分数区间的成员列表(分数是指zadd添加进来的score参数)
     *
     * @param key 键
     * @param min
     * @param max
     * @return
     */
    public static Set<String> zrangeByScore(String key, String min, String max) {
        return jedisPool.getResource().zrangeByScore(key, min, max);
    }
}