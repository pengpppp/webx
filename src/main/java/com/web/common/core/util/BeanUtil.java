/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.util;

import com.alibaba.fastjson.JSON;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.util.*;

/**
 * bean工具类
 */

public class BeanUtil {
    private static Logger logger = Logger.getLogger(BeanUtil.class);

    private BeanUtil() {
    }

    private static void debugInfo(Object source, Object target) {
        logger.debug("source Object->" + JSON.toJSONString(source));
        logger.debug("target Object->" + JSON.toJSONString(target));
    }

    /**
     * 拷贝对象，要求对象必须复写toString
     *
     * @param source
     * @param target
     */
    public static void copyProperties(Object source, Object target) {
        debugInfo(source, target);
        BeanUtils.copyProperties(source, target);
    }

    /**
     * 拷贝对象，要求对象必须复写toString
     *
     * @param source
     * @param target
     */
    public static void copyProperties(Object source, Object target, Class<?> editable) {
        debugInfo(source, target);
        BeanUtils.copyProperties(source, target, editable);
    }

    /**
     * 拷贝对象，要求对象必须复写toString
     *
     * @param source
     * @param target
     */
    public static void copyProperties(Object source, Object target, String... ignoreProperties) {
        debugInfo(source, target);
        BeanUtils.copyProperties(source, target, ignoreProperties);
    }

    /**
     * 复制对象
     *
     * @param source
     * @param clazz
     */
    public static <T> T copyProperties(Object source, Class<T> clazz) {
        if (source == null) {
            return null;
        }
        T t = null;
        try {
            t = clazz.newInstance();
            BeanUtils.copyProperties(source, t);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return t;
    }

    /**
     * @param source
     * @param clazz
     * @return
     */
    public static <T> List<T> copyProperties(List<?> source, Class<T> clazz) {
        if (source == null || source.size() == 0) {
            return Collections.emptyList();
        }
        List<T> res = new ArrayList<>(source.size());
        for (Object obj : source) {
            try {
                T t = clazz.newInstance();
                res.add(t);
                BeanUtils.copyProperties(obj, t);
            } catch (Exception e) {
                logger.error("copyList error", e);
            }
        }
        return res;
    }

    /**
     * 获取非空字段
     *
     * @param source
     * @return
     */
    public static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<String>();
        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) {
                emptyNames.add(pd.getName());
            }
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }
}
