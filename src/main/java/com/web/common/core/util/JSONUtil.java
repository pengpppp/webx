/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * json 工具类
 */

public class JSONUtil {
    private static Logger logger = Logger.getLogger(JSONUtil.class);
    private static ObjectMapper mapper;

    static {
        mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINESE));

        //Include.Include.ALWAYS 默认
        //Include.NON_DEFAULT 属性为默认值不序列化
        //Include.NON_EMPTY 属性为 空（“”） 或者为 NULL 都不序列化
        //Include.NON_NULL 属性为NULL 不序列化
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        //关闭 当遇到未知属性时，不抛出异常
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        //该特性决定是否接受强制非数组（JSON）值到Java集合类型。如果允许，集合反序列化将尝试处理非数组值。
        mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);

        //该特性决定parser将是否允许解析使用Java/C++ 样式的注释（包括'/'+'*' 和'//' 变量）
        mapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);

        //这个特性决定parser是否将允许使用非双引号属性名字， （这种形式在Javascript中被允许，但是JSON标准说明书中没有）。
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

        //该特性决定parser是否允许单引号来包住属性名称和字符串值。
        mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);

        //该特性决定parser是否允许JSON字符串包含非引号控制字符（值小于32的ASCII字符，包含制表符和换行符）。 如果该属性关闭，则如果遇到这些字符，则会抛出异常。
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);

        //该特性决定了当遇到未知属性（没有映射到属性，没有任何setter或者任何可以处理它的handler），是否应该抛出一个
        // JsonMappingException异常。这个特性一般式所有其他处理方法对未知属性处理都无效后才被尝试，属性保留未处理状态。
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    private JSONUtil() {

    }

    /**
     * 对象 转 json，只针对非null字段
     *
     * @param obj
     * @return
     */
    public static String toString(Object obj) {
        try {
            return mapper.writeValueAsString(obj);
        } catch (Exception e) {
            logger.error("对象 转 json 失败。请检查---->" + e.getMessage(), e);
        }
        return "";
    }

    /**
     * 对象 转 json，对所有字段都进行转
     *
     * @param obj
     * @return
     */
    public static String toStringAlways(Object obj) {
        try {
            ObjectMapper copy = mapper.copy();
            copy.setSerializationInclusion(JsonInclude.Include.ALWAYS);
            return copy.writeValueAsString(obj);
        } catch (Exception e) {
            logger.error("对象 转 json 失败。请检查---->" + e.getMessage(), e);
        }
        return "";
    }

    /**
     * json 转 对象
     *
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T toObj(String json, Class<T> clazz) {
        try {
            return mapper.readValue(json, clazz);
        } catch (IOException e) {
            logger.error("json 转 对象 失败。请检查---->" + e.getMessage(), e);
        }
        return null;
    }

    /**
     * json 转 对象
     *
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T toObj(JsonNode json, Class<T> clazz) {
        try {
            return mapper.readValue(json.toString(), clazz);
        } catch (IOException e) {
            logger.error("json 转 对象 失败。请检查---->" + e.getMessage(), e);
        }
        return null;
    }

    /**
     * 将json字符串转为集合类型 List、Map等
     *
     * @param jsonStr         json字符串
     * @param collectionClass 集合类型
     * @param elementClasses  泛型类型
     * @throws IOException
     */
    public static <T> T toObj(String jsonStr, Class<?> collectionClass, Class<?>... elementClasses) {
        try {
            JavaType javaType = mapper.getTypeFactory().constructParametricType(collectionClass, elementClasses);
            return mapper.readValue(jsonStr, javaType);
        } catch (IOException e) {
            logger.error("json 转 集合类型 失败。请检查---->" + e.getMessage(), e);
        }
        return null;
    }

    /**
     * 字符串转换Json
     */
    public static JsonNode parseJson(String input) {
        try {
            return mapper.readTree(input);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    /**
     * 根据json字符以及给定的key，获取对应的value
     *
     * @param json
     * @param key
     * @return
     */
    public static JsonNode getJsonValue(String json, String key) {
        JsonNode jsonNode = parseJson(json);
        return jsonNode.get(key);
    }

}
