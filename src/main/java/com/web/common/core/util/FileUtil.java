package com.web.common.core.util;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author jiangl
 *
 */
public class FileUtil {
	private static Log log = LogFactory.getLog(FileUtil.class);
	private static ObjectMapper mapper;
	static {
		mapper = new ObjectMapper();
		mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.CHINESE));
		mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		mapper.configure(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY, true);
	}

	private FileUtil() {
	}

	/**
	 * 复制文件
	 * 
	 * @param srcFile
	 * @param destFile
	 */
	public static void copyFile(File srcFile, File destFile) throws Exception {
		org.apache.commons.io.FileUtils.copyFile(srcFile, destFile);
	}

	/**
	 * 复制文件到目标文件夹
	 * 
	 * @param srcFile
	 * @param destDir
	 * @throws Exception
	 */
	public static void copyFileToDirectory(File srcFile, File destDir) throws Exception {
		org.apache.commons.io.FileUtils.copyFileToDirectory(srcFile, destDir);
	}

	/**
	 * 读取文件
	 * 
	 * @param file
	 * @param encoding
	 * @return
	 * @throws Exception
	 */
	public static List<String> readLines(File file, String encoding) throws Exception {
		return org.apache.commons.io.FileUtils.readLines(file, encoding);
	}

	/**
	 * 写入文件
	 * 
	 * @param file
	 * @param encoding
	 * @param lines
	 * @throws Exception
	 */
	public static void writeLines(File file, String encoding, Collection<?> lines) throws Exception {
		org.apache.commons.io.FileUtils.writeLines(file, encoding, lines);
	}

	/**
	 * 网络流拷贝文件
	 * 
	 * @param source
	 * @param destination
	 * @throws Exception
	 */
	public static void copyURLToFile(URL source, File destination) throws Exception {
		org.apache.commons.io.FileUtils.copyURLToFile(source, destination);
	}

	/**
	 * 获取前缀
	 * 
	 * @param filename
	 * @return
	 */
	public static String getPrefix(String filename) {
		return org.apache.commons.io.FilenameUtils.getPrefix(filename);
	}

	/**
	 * 获取后缀
	 * 
	 * @param file
	 * @return
	 */
	public static String getExtension(File file) {
		return org.apache.commons.io.FilenameUtils.getExtension(file.getAbsolutePath()).toLowerCase();
	}

	/**从输入流中读取制定起始位置和长度的字节
	 * @param inputStream
	 * @param offset
	 * @param length
	 * @return
	 * @throws Exception
	 */
	public static byte[] getFileContentByte(InputStream inputStream, long offset, long length) throws Exception {
		log.debug("getFileContentByte start");
		byte[] fileContent = null;
		byte[] tempBuf = new byte[(int) length];

		inputStream.skip(offset);
		int readLen = inputStream.read(tempBuf);
		if (readLen < 0) {
			fileContent = new byte[0];
			return fileContent;
		}
		if (readLen < length) {
			fileContent = new byte[readLen];
			System.arraycopy(tempBuf, 0, fileContent, 0, readLen);
		} else {
			fileContent = tempBuf;
		}
		inputStream.close();
		return fileContent;
	}

}
