package com.web.common.core.util;

import com.web.Tools.staticData.Code;
import com.web.common.core.entity.JsonData;
import com.web.common.core.exception.BizException;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author jiangyl
 */
public class ExceptionUtil {
    private static Log log = LogFactory.getLog(ExceptionUtil.class);

    private ExceptionUtil() {
    }

    private static String environment;

    static {
        try {
            Configuration configuration = new Configurations().properties(new File("/", "exception.properties"));
            environment = configuration.getString("environment");
        } catch (Exception e) {
            log.error("", e);
            log.info("请在类路径下配置exception.properties文件");
            System.exit(1);// 配置不准确，直接退出
        }
    }


    /**
     * 把后台的异常信息记录到日志文件，友好信息输出给客户端
     * 开发，测试，准生产会打印详细堆栈到日志文件和返回详细堆栈，正式生产环境会打印堆栈到日志文件，不会返回详细堆栈，
     * <p>
     * 无论是controller层，service层还是dao层类，出现数据校验不通过或者数据处理失败，都应当throw new
     * BizException()
     * <p>
     * 比如数据校验失败（客户端发起的请求不符合api规范原因）：throw new
     * BizException(JsonData.FAIL,"密码长度应该大于8位，并且包含大小写字母");
     * 再比如后台处理失败（程序逻辑原因，内存原因，网络原因等）：throw new
     * BizException(JsonData.ERROR,"网络缓慢，请稍后重试");
     * <p>
     * 如果不进行任何处理， 如果程序抛出的异常类型为BindException,则默认抛出异常为
     * BizException(JsonData.FAIL, sb.toString());
     * <p>
     * 程序抛出的其他类型的异常，则默认异常为throw new
     * BizException(JsonData.FAIL,JsonData.FAIL_MSG);
     * <p>
     * <p>
     * 用户看到的只有数据异常等提示
     *
     * @param e
     * @return
     * @throws Exception
     */
    public static JsonData convertExceptionToJsonData(Exception e) {
        // 记录到日志文件，便于elk等监控系统实时监控

        return convertExceptionToJsonData(null, e);
    }

    /*
     *
     * @param request
     *
     * @param e
     *
     * @return
     *
     * @throws Exception
     */
    public static JsonData convertExceptionToJsonData(HttpServletRequest request, Exception e) {
        // 记录到日志文件，便于elk等监控系统实时监控
        log.error("convertExceptionToJsonData start=" + ExceptionUtils.getStackTrace(e));

        String stackTrace = ExceptionUtils.getStackTrace(e);

        if (!(e instanceof BizException)) {
            if (e instanceof BindException) {
                StringBuilder sb = new StringBuilder();
                List<ObjectError> list = ((BindException) e).getAllErrors();
                if (!list.isEmpty()) {
                    for (ObjectError objectError : list) {
                        sb.append(objectError.getDefaultMessage() + ",");
                    }
                    sb.deleteCharAt(sb.length() - 1);
                }
                e = new BizException(Code.FAIL, sb.toString());
            } else if (e instanceof MethodArgumentNotValidException) {
                StringBuilder sb = new StringBuilder();
                List<ObjectError> list = ((MethodArgumentNotValidException) e).getBindingResult().getAllErrors();
                if (!list.isEmpty()) {
                    for (ObjectError objectError : list) {
                        sb.append(objectError.getDefaultMessage() + ",");
                    }
                    sb.deleteCharAt(sb.length() - 1);
                }
                e = new BizException(Code.FAIL, sb.toString());
            } else {
                e = new BizException(Code.FAIL, Code.information.get(Code.FAIL));
            }
        }

        if (environment.equals("PROD") || environment.equals("QA00") || environment.equals("TEST")) {
            if (e instanceof BizException) {
                return new JsonData(Code.FAIL, e.getMessage(), e.getMessage());
            }
            return new JsonData(Code.FAIL, e.getMessage(), Code.information.get(Code.FAIL));
        }

        JsonData jsonData = new JsonData(((BizException) e).getCode(), null, ((BizException) e).getMsg());
        jsonData.setCode(((BizException) e).getCode());
        jsonData.setMessage(((BizException) e).getMsg() + stackTrace);
        if (request != null) {
            StringBuffer r = new StringBuffer();
            r.append("requestURL=");
            r.append(request.getRequestURL().toString());
            r.append(",requestURI=");
            r.append(request.getRequestURI());
            r.append(",queryString=");
            r.append(request.getQueryString());
            r.append(",remoteAddr=");
            r.append(IpUtil.getIpAddr(request));
            r.append(",remoteHost=");
            r.append(request.getRemoteHost());
            r.append(",remotePort=");
            r.append(request.getRemotePort());
            r.append(",localAddr=");
            r.append(request.getLocalAddr());
            r.append(",localName=");
            r.append(request.getLocalName());
            r.append(",method=");
            r.append(request.getMethod());

            Map<String, String> map = new HashMap<>();
            Enumeration headerNames = request.getHeaderNames();
            while (headerNames.hasMoreElements()) {
                String key = (String) headerNames.nextElement();
                String value = request.getHeader(key);
                map.put(key, value);
            }

            r.append(",headers=");
            r.append(map);
            r.append(",parameters=");
            r.append(request.getParameterMap());
            jsonData.setData(r.toString() + ExceptionUtils.getStackTrace(e));
        } else {
            jsonData.setData(null + ExceptionUtils.getStackTrace(e));
        }
        return jsonData;

    }

}
