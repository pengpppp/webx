/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.entity;

import com.web.Tools.staticData.Code;

import java.io.Serializable;

/**
 * 封装的json数据
 */
public class JsonData implements Serializable {

    private static final long serialVersionUID = 5794164519406731274L;

    /***
     * 状态码 0 成功 -1 失败 -2 异常 返回时必填
     */
    private String code = Code.SUCCESS;
    /**
     * 返回信息描述，根据业务需求返回业务描述信息
     */
    private String message = Code.information.get(Code.SUCCESS);

    /**
     * 返回数据详细参数，不需要返回参数时，该字段返回空刮括号 {}
     */
    private Object data = new Object();

    /**
     * 私有化构造方法
     */
    private JsonData() {
    }

    public JsonData(String code, Object data, String message) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    /**
     * 成功
     *
     * @return
     */
    public static JsonData buildSuccess() {
        return new JsonData(Code.SUCCESS, null, Code.information.get(Code.SUCCESS));
    }

    public static JsonData success() {
        return buildSuccess();
    }

    /**
     * 成功
     *
     * @param data 数据对象
     * @return
     */
    public static JsonData buildSuccess(Object data) {
        return new JsonData(Code.SUCCESS, data, Code.information.get(Code.SUCCESS));
    }

    public static JsonData success(Object data) {
        return buildSuccess(data);
    }

    /**
     * /成功，传入数据,及描述信息
     *
     * @param data 数据对象
     * @param msg  描述信息
     * @return
     */
    public static JsonData buildSuccess(Object data, String msg) {
        return new JsonData(Code.SUCCESS, data, msg);
    }

    public static JsonData success(Object data, String msg) {
        return buildSuccess(data, msg);
    }

    /**
     * 失败
     *
     * @param msg 失败信息
     * @return
     */
    public static JsonData buildError(String msg) {
        return new JsonData(Code.FAIL, null, msg);
    }

    public static JsonData error(String msg) {
        return buildError(msg);
    }

    /**
     * 失败，传入描述信息,状态码，-2表示公众号无相关权限
     *
     * @param msg  失败信息
     * @param code 状态码
     * @return
     */
    public static JsonData buildError(String msg, String code) {
        return new JsonData(code, null, msg);
    }

    public static JsonData error(String msg, String code) {
        return buildError(msg, code);
    }

    public static JsonData buildErrorCode(String code)
    {
        return new JsonData(code,null,Code.information.get(code));
    }
    public static JsonData errorCode(String code)
    {
        return buildErrorCode(code);
    }

    /**
     * 失败，程序错误
     *
     * @param throwable 错误对象
     * @return
     */
    public static JsonData buildError(Throwable throwable) {
        return new JsonData(Code.FAIL, null, throwable.getMessage());
    }

    public static JsonData error(Throwable throwable) {
        return buildError(throwable);
    }

    public static JsonData bulidError(Exception e) {
        return new JsonData(Code.FAIL, null, e.getMessage());
    }

    public static JsonData error(Exception e) {
        return bulidError(e);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "JsonData{" + "code=" + code + ", msg='" + message + '\'' + ", data=" + data + '}';
    }
}
