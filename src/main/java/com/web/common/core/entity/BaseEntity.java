package com.web.common.core.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * @描述: 基础实体类，包含各实体公用属性 .
 */
public class BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long id;
	private Long version = 0L;
	/** 创建时间 */
	protected Date createTime = new Date();
	protected Date updateTime = new Date();

	private String remark;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Override
	public String toString() {
		return "BaseEntity{" +
				"id=" + id +
				", version=" + version +
				", createTime=" + createTime +
				", updateTime=" + updateTime +
				", remark='" + remark + '\'' +
				'}';
	}
}
