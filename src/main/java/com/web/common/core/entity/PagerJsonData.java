/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.entity;


import com.web.Tools.staticData.Code;
import com.web.common.core.page.Pager;

import java.util.List;

/**
 * 分页Json返回数据模型
 */

public class PagerJsonData extends JsonData {
    private static final long serialVersionUID = 1527825644277567448L;

    private PagerJsonData() {
        super(Code.SUCCESS, Code.information.get(Code.SUCCESS), null);
    }

    public PagerJsonData(String code, String msg, Object data) {
        super(code, data, msg);
    }

    /**
     * 成功
     *
     * @param code
     * @param msg
     * @param data     集合数据
     * @param summary  汇总数据
     * @param total    总数
     * @param page     当前页
     * @param pageSize 每页数据大小
     * @return
     */
    private static PagerJsonData init(String code, String msg, List data, Object summary, long total, int page,
                                      int pageSize) {
        return new PagerJsonData(code, msg, new Pager(total, page, pageSize, data, summary));
    }

    private static PagerJsonData init(String code, String msg, Pager pager) {
        return new PagerJsonData(code, msg, pager);
    }

    /**
     * 成功
     *
     * @param data     集合数据
     * @param total    总数
     * @param page     当前页
     * @param pageSize 每页数据大小
     * @return
     */
    public static PagerJsonData success(List data, long total, int page, int pageSize) {
        return init(Code.SUCCESS, Code.information.get(Code.SUCCESS), data, null, total, page, pageSize);
    }

    /**
     * 成功
     *
     * @param data     集合数据
     * @param total    总数
     * @param page     当前页
     * @param pageSize 每页数据大小
     * @param summary  集合数据，或特殊数据
     * @return
     */
    public static PagerJsonData success(List data, long total, int page, int pageSize, Object summary) {
        return init(Code.SUCCESS, Code.information.get(Code.SUCCESS), data, summary, total, page, pageSize);
    }

    /**
     * 成功
     *
     * @param pager 分页数据
     * @return
     */
    public static PagerJsonData success(Pager pager) {
        return init(Code.SUCCESS, Code.information.get(Code.SUCCESS), pager);
    }


}
