/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.entity;

import com.web.common.core.util.CollectionUtil;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统上下文对象
 * 用于在当前线程中存储与获取对象
 */

public class SystemContext implements Serializable {

    private static Logger logger = Logger.getLogger(SystemContext.class);


    private static ThreadLocal<Map<String, Object>> resources = new ThreadLocal<Map<String, Object>>();


    private SystemContext() {
    }

    /**
     * 获取所有资源
     *
     * @return
     */
    public static Map<String, Object> getResources() {
        if (resources.get() == null) {
            return Collections.emptyMap();
        } else {
            return new HashMap<String, Object>(resources.get());
        }
    }

    /**
     * 添加资源
     *
     * @param newResources
     */
    public static void setResources(Map<String, Object> newResources) {
        if (CollectionUtil.isEmpty(newResources)) {
            return;
        }
        ensureResourcesInitialized();
        resources.get().clear();
        resources.get().putAll(newResources);
    }

    /**
     * 根据 key 获取对应的资源
     *
     * @param key
     * @return
     */
    private static Object getValue(String key) {
        Map<String, Object> perThreadResources = resources.get();
        return perThreadResources != null ? perThreadResources.get(key) : null;
    }

    /**
     * 获取默认对象
     */
    private static void ensureResourcesInitialized() {
        if (resources.get() == null) {
            resources.set(new HashMap<String, Object>());
        }
    }

    /**
     * 根据 key 获取对应的资源
     *
     * @param key
     * @return
     */
    public static Object get(String key) {
        if (logger.isTraceEnabled()) {
            String msg = "get() - in thread [" + Thread.currentThread().getName() + "]";
            logger.trace(msg);
        }

        Object value = getValue(key);
        if ((value != null) && logger.isTraceEnabled()) {
            String msg = "Retrieved value of type [" + value.getClass().getName() + "] for key [" +
                    key + "] " + "bound to thread [" + Thread.currentThread().getName() + "]";
            logger.trace(msg);
        }
        return value;
    }

    /**
     * 添加指定资源
     *
     * @param key
     * @param value
     */
    public static void put(String key, Object value) {
        if (key == null) {
            throw new IllegalArgumentException("key cannot be null");
        }

        if (value == null) {
            remove(key);
            return;
        }

        ensureResourcesInitialized();
        resources.get().put(key, value);

        if (logger.isTraceEnabled()) {
            String msg = "Bound value of type [" + value.getClass().getName() + "] for key [" +
                    key + "] to thread " + "[" + Thread.currentThread().getName() + "]";
            logger.trace(msg);
        }
    }


    /**
     * 根据 key 移除资源
     *
     * @param key
     * @return
     */
    public static Object remove(String key) {
        Map<String, Object> perThreadResources = resources.get();
        Object value = perThreadResources != null ? perThreadResources.remove(key) : null;

        if ((value != null) && logger.isTraceEnabled()) {
            String msg = "Removed value of type [" + value.getClass().getName() + "] for key [" +
                    key + "]" + "from thread [" + Thread.currentThread().getName() + "]";
            logger.trace(msg);
        }

        return value;
    }

    /**
     * 移除所有资源
     */
    public static void remove() {
        resources.remove();
    }


}
