/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.http;


import cn.hutool.core.util.XmlUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.web.common.core.entity.JsonData;
import com.web.common.core.exception.BizException;
import com.web.common.core.util.JSONUtil;
import org.apache.http.Header;
import org.apache.http.StatusLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * http请求返回对象
 *
 * @author Jone.lin
 * @date 2018年11月22日 下午4:20
 * @version V2.0.0
 */

public class HttpResult {

    private static final Logger logger = LoggerFactory.getLogger(HttpResult.class);

    /**
     * 返回数据用的字符编码，默认UTF-8
     **/
    private String respCharset = "UTF-8";

    /**
     * 返回的结果
     */
    private byte[] data;

    private long contentLength;

    private Header contentType;

    private StatusLine statusLine;




    @Override
    public String toString() {
        return getResult();
    }

    /**
     * 获取字符串结果，默认字符编码
     * @return
     */
    public String getResult() {
        return getResult(respCharset);
    }

    /**
     * 获取字符串结果，指定字符编码
     * @param charset 字符编码
     * @return    对应的类型
     */
    public String getResult(String charset) {
        try {
            return new String(data,charset);
        } catch (UnsupportedEncodingException e) {
            logger.error("转换数据失败！字符编码：" + charset);
        }
        return null;
    }


    /**
     * 将结果集进行转换，转换成指定对象
     * @param clazz   需转换的类对象
     * @return    对应的类型
     */
    public <T> T getResult(Class<T> clazz){
        return getResult(clazz, respCharset);
    }

    /**
     * 将结果集进行转换，转换成指定对象(目前只支持JSON对象)
     * @param clazz   需转换的类对象
     * @param charset 字符编码
     * @return    对应的类型
     */
    public <T> T getResult(Class<T> clazz, String charset) {
        String str = getResult(charset).trim();
        return JSONUtil.toObj(str, clazz);
    }
    /**
     * 获取结果集的msg
     * @return  String对象
     */
    public String getResult4Msg() {
        JsonNode jsonNode = JSONUtil.getJsonValue(getResult(), "msg");
        if (jsonNode == null) {
            return "";
        }
        return jsonNode.toString();
    }

    /**
     * 获取结果集的code
     * @return  code对象
     */
    public int getResult4Code() {
        JsonNode jsonNode = JSONUtil.getJsonValue(getResult(), "code");
        if (jsonNode == null) {
            return -1;
        }
        return Integer.valueOf(jsonNode.toString());
    }

    /**
     * 校验结果集 是否jsondata，并且接口返回正常
     * @return
     */
    public HttpResult validate4Code(){
        JsonData jsonData = getResult(JsonData.class);
//         || jsonData.getCode() !="0"
        if (jsonData == null) {
            throw new BizException(jsonData == null ? "调用接口失败！" : jsonData.getMessage());
        }
        return this;
    }

    /**
     * 将结果集进行转换，转换成map
     * @return  map对象
     */
    public Map<String, Object> getResult4Map() {
        return getResult4Map(respCharset);
    }


    /**
     *将结果集进行转换，转换成map
     * @param charset 字符编码
     * @return map对象
     */
    public Map<String, Object> getResult4Map(String charset) {
        return (Map<String, Object>)JSONUtil.toObj(getResult(charset), Map.class);
    }


    /**
     * 将结果集进行转换，转换成指定对象。(目前只支持JSON对象)
     * 如果已知对象是个JsonData，那么会对JsonData进行解析。取出其中的Data，再进行转换
     * jsondata对象格式 : {"code":0,"msg":"",data:{}}
     * @param clazz   需转换的类对象
     * @return  对应的类型
     */
    public <T> T getResult4JsonData(Class<T> clazz){
        return getResult4JsonData(clazz, respCharset);
    }

    /**
     * 将结果集进行转换，转换成指定对象。(目前只支持JSON对象)
     * 如果已知对象是个JsonData，那么会对JsonData进行解析。取出其中的Data，再进行转换
     * jsondata对象格式 : {"code":0,"msg":"",data:{}}
     * @param clazz   需转换的类对象
     * @param charset 字符编码
     * @return    对应的类型
     */
    public <T> T getResult4JsonData(Class<T> clazz, String charset) {
        JsonNode data = JSONUtil.getJsonValue(getResult(charset), "data");
        if (data == null) {
            return null;
        }
        return JSONUtil.toObj(data.toString(), clazz);
    }


    /**
     * 将xml结果集转换成map
     * @return
     */
    public Map<String, Object> getXmlResult(){
        return getXmlResult(respCharset);
    }


    /**
     * 将xml结果集转换成map
     * @param charset
     * @return
     */
    public Map<String, Object> getXmlResult(String charset){
        return XmlUtil.xmlToMap(getResult(charset));
    }





    public byte[] getData() {
        return data;
    }


    public void setData(byte[] data) {
        this.data = data;
    }

    public String getRespCharset() {
        return respCharset;
    }

    public void setRespCharset(String respCharset) {
        this.respCharset = respCharset;
    }

    public long getContentLength() {
        return contentLength;
    }

    public void setContentLength(long contentLength) {
        this.contentLength = contentLength;
    }

    public Header getContentType() {
        return contentType;
    }

    public void setContentType(Header contentType) {
        this.contentType = contentType;
    }

    public StatusLine getStatusLine() {
        return statusLine;
    }

    public void setStatusLine(StatusLine statusLine) {
        this.statusLine = statusLine;
    }
}
