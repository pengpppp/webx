/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.http;


import cn.hutool.core.util.XmlUtil;
import com.web.common.core.exception.BizException;
import com.web.common.core.util.CollectionUtil;
import com.web.common.core.util.JSONUtil;
import com.web.common.core.util.StrUtil;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.ServiceUnavailableRetryStrategy;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.*;
import org.apache.http.conn.DnsResolver;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.net.HttpCookie;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.*;

/**
 * http客户端连接工具
 *
 * @author Jone.lin
 * @version V2.0.0
 * @date 2018年11月22日 上午10:47
 */

public class HttpClient {
    private static final Logger logger = LoggerFactory.getLogger(HttpClient.class);

    /**
     * 请求地址
     **/
    private String url;

    /**
     * 请求方式
     */
    private Method method;
    /**
     * cookie
     **/
    private String cookie;
    /**
     * accept
     **/
    private String accept;
    /**
     * contentType
     **/
    private ContentType contentType;
    /**
     * 错误重试次数，默认重试3次
     **/
    private int tryCount = 3;

    /**
     * 可忽略的重试状态码，如200
     */
    private Set<Integer> whiteStatusCodeList = new HashSet<>();

    /**
     * 重试间隔时间 默认无间隔，单位毫秒
     */
    private int retryInterval = 0;

    /**
     * 传输数据的字符串形式，dataStr 与 data 只能设置一个
     */
    private String body;
    /**
     * 存储表单数据
     **/
    private Map<String, Object> data;
    /**
     * 请求头信息
     **/
    private Map<String, String> headers = new HashMap<String, String>();

    /**
     * 指定dns请求 key为域名，value为解析出来的多个ip
     */
    private Map<String, List<InetAddress>> hostList = new HashMap<>();

    /**
     * 请求数据用的字符编码，默认UTF-8
     **/
    private String reqCharset = "UTF-8";
    /**
     * 返回数据用的字符编码，默认UTF-8
     **/
    private String respCharset = "UTF-8";

    /**
     * 是否https链接，默认是http
     */
    private boolean isHttps = false;
    /**
     * 代理
     */
    private HttpHost proxy;

    /**
     * SSLContext，用于HTTPS安全连接
     */
    private SSLContext ssl;

    private SSLConnectionSocketFactory sslFactory;

    /**
     * 连接超时：指的是连接一个url的连接等待时间，默认10s
     */
    private int connectTimeout = 10 * 1000;
    /**
     * 读取数据超时：指的是连接上一个url，获取response的返回等待时间, 默认10s
     **/
    private int socketTimeout = 10 * 1000;


    /**
     * 构造方法，指定url
     **/
    private HttpClient(String url) {
        //添加默认过滤状态码200
        whiteStatusCodeList.add(200);
        Assert.notNull(url, "url不能为空！");
        this.url = url;
    }

    public HttpClient method(Method method) {
        this.method = method;
        return this;
    }

    /**
     * POST请求
     *
     * @param url URL
     * @return HttpClient
     */
    public static HttpClient post(String url) {
        return new HttpClient(url).method(Method.POST);
    }

    /**
     * GET请求
     *
     * @param url URL
     * @return HttpClient
     */
    public static HttpClient get(String url) {
        return new HttpClient(url).method(Method.GET);
    }

    /**
     * PUT请求
     *
     * @param url URL
     * @return HttpClient
     */
    public static HttpClient put(String url) {
        return new HttpClient(url).method(Method.PUT);
    }


    /**
     * DELETE请求
     *
     * @param url URL
     * @return HttpClient
     */
    public static HttpClient delete(String url) {
        return new HttpClient(url).method(Method.DELETE);
    }

    /**
     * HEAD请求
     *
     * @param url URL
     * @return HttpClient
     */
    public static HttpClient head(String url) {
        return new HttpClient(url).method(Method.HEAD);
    }

    /**
     * OPTIONS请求
     *
     * @param url URL
     * @return HttpClient
     */
    public static HttpClient options(String url) {
        return new HttpClient(url).method(Method.OPTIONS);
    }

    /**
     * PATCH请求
     *
     * @param url URL
     * @return HttpClient
     */
    public static HttpClient patch(String url) {
        return new HttpClient(url).method(Method.PATCH);
    }

    /**
     * TRACE请求
     *
     * @param url URL
     * @return HttpClient
     */
    public static HttpClient trace(String url) {
        return new HttpClient(url).method(Method.TRACE);
    }


    /**
     * 设置超时，单位：毫秒
     *
     * @param connectTimeout 链接超时毫秒数
     * @param socketTimeout  读取超时毫秒数
     * @return this
     */
    public HttpClient timeout(int connectTimeout, int socketTimeout) {
        this.connectTimeout = connectTimeout;
        this.socketTimeout = socketTimeout;
        return this;
    }

    /**
     * 设置超时，单位：毫秒
     *
     * @param connectTimeout 链接超时毫秒数
     * @return this
     */
    public HttpClient connectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
        return this;
    }

    /**
     * 设置超时，单位：毫秒
     *
     * @param socketTimeout 读取超时毫秒数
     * @return this
     */
    public HttpClient socketTimeout(int socketTimeout) {
        this.socketTimeout = socketTimeout;
        return this;
    }

    /**
     * 设置重试次数，默认无间隔，单位毫秒
     *
     * @param tryCount      重试次数
     * @param retryInterval 重试间隔，单位毫秒
     * @return this
     */
    public HttpClient tryCount(int tryCount, int retryInterval) {
        this.tryCount = tryCount;
        this.retryInterval = retryInterval;
        return this;
    }

    /**
     * 设置重试次数
     *
     * @param tryCount 重试次数
     * @return this
     */
    public HttpClient tryCount(int tryCount) {
        this.tryCount = tryCount;
        return this;
    }

//    /**
//     * 添加过滤状态码
//     *
//     * @param statusCode
//     * @return
//     */
//    public HttpClient addStatusCode(int statusCode) {
//        this.whiteStatusCodeList.add(statusCode);
//        return this;
//    }
//
//    /**
//     * 添加过滤状态码
//     *
//     * @param list 集合
//     * @return
//     */
//    public HttpClient addStatusCode(List<Integer> list) {
//        this.whiteStatusCodeList.addAll(list);
//        return this;
//    }

    /**
     * 是否启用https
     *
     * @param isHttps 是否启用https
     * @return this
     */
    public HttpClient setHttps(boolean isHttps) {
        this.isHttps = isHttps;
        return this;
    }

    /**
     * 设置SSLSocketFactory<br>
     * 只针对HTTPS请求，如果不设置，使用默认的SSLSocketFactory<br>
     * 默认SSLSocketFactory为：SSLSocketFactoryBuilder.create().build();
     *
     * @param ssf SSLScketFactory
     * @return this
     */
    public HttpClient setSSLSocketFactory(SSLContext ssf) {
        this.ssl = ssf;
        this.isHttps = true;
        return this;
    }

    public HttpClient setSSL(SSLContext ssl) {
        this.ssl = ssl;
        this.isHttps = true;
        return this;
    }

    public HttpClient setSSLFactory(SSLConnectionSocketFactory ssf) {
        this.sslFactory = ssf;
        this.isHttps = true;
        return this;
    }


    /**
     * 是否对请求参数进行编码，默认UTF-8
     *
     * @param reqCharset 是否对请求参数进行编码
     * @return this
     */
    public HttpClient setReqCharset(String reqCharset) {
        this.reqCharset = reqCharset;
        return this;
    }

    /**
     * 是否对返回参数进行编码，默认UTF-8
     *
     * @param respCharset 是否对返回参数进行编码
     * @return this
     */
    public HttpClient setRespCharset(String respCharset) {
        this.respCharset = respCharset;
        return this;
    }

    /**
     * 设置contentType 先私有化
     *
     * @param contentType contentType
     * @return HttpClient
     */
    private HttpClient contentType(ContentType contentType) {
        this.contentType = contentType;
        return this;
    }

    /**
     * acceptType 先私有化
     *
     * @param acceptType acceptType
     * @return HttpClient
     */
    private HttpClient accept(String acceptType) {
        this.accept = acceptType;
        return this;
    }


    /**
     * 设置代理 先私有化
     *
     * @param proxy 代理 {@link HttpHost}
     * @return this
     */
    private HttpClient setProxy(HttpHost proxy) {
        this.proxy = proxy;
        return this;
    }


    /**
     * 设置某个域名的DNS解析，允许自定义Host解析处理
     *
     * @param host 指定域名，不含http或https 如 www.baidu.com
     * @param ip   指定解析ip，
     * @return
     */
    public HttpClient setHost(String host, String ip) {
        if (StrUtil.isNotBlank(host) && StrUtil.isNotBlank(ip)) {
            try {
                List<InetAddress> values = hostList.get(host.trim());
                if (CollectionUtil.isEmpty(values)) {
                    ArrayList<InetAddress> valueList = new ArrayList<InetAddress>();
                    valueList.add(InetAddress.getByName(ip.trim()));
                    hostList.put(host.trim(), valueList);
                } else {
                    values.add(InetAddress.getByName(ip.trim()));
                }
            } catch (UnknownHostException e) {
                logger.warn("设置自定义DNS解析异常! host=" + host + ", ip=" + ip);
            }
        }
        return this;
    }

    /**
     * 设置某个域名的DNS解析，允许自定义Host解析处理
     *
     * @param host 指定域名，不含http或https 如 www.baidu.com
     * @param ips  指定解析ip，允许多个ip
     * @return
     */
    public HttpClient setHost(String host, String... ips) {
        if (ips != null && ips.length > 0) {
            for (String ip : ips) {
                setHost(host, ip);
            }
        }
        return this;
    }

    /**
     * 设置某个域名的DNS解析，允许自定义Host解析处理
     *
     * @param host   指定域名，不含http或https 如 www.baidu.com
     * @param ipList 指定解析ip，允许多个ip
     * @return
     */
    public HttpClient setHost(String host, List<String> ipList) {
        if (CollectionUtil.isNotEmpty(ipList)) {
            for (String ip : ipList) {
                setHost(host, ip);
            }
        }
        return this;
    }


    /**
     * 设置Cookie<br>
     * 自定义Cookie后会覆盖Hutool的默认Cookie行为
     *
     * @param cookies Cookie值数组，如果为{@code null}则设置无效，使用默认Cookie行为
     * @return this
     */
    public HttpClient cookie(HttpCookie... cookies) {
        if (cookies != null && cookies.length > 0) {
            cookie(StrUtil.join(cookies, ";"));
        }
        return this;
    }

    /**
     * 设置Cookie<br>
     * 自定义Cookie后会覆盖Hutool的默认Cookie行为
     *
     * @param cookie Cookie值，如果为{@code null}则设置无效，使用默认Cookie行为
     * @return this
     */
    public HttpClient cookie(String cookie) {
        this.cookie = cookie;
        return this;
    }


    /**
     * 设置是否为长连接
     *
     * @param isKeepAlive 是否长连接
     * @return HttpClient
     */
    public HttpClient keepAlive(boolean isKeepAlive) {
        header(Header.CONNECTION, isKeepAlive ? "Keep-Alive" : "Close");
        return this;
    }

    /**
     * 设置一个token <br>
     * 覆盖模式，则替换之前的值
     *
     * @param value token
     * @return T 本身
     */
    public HttpClient token(String value) {
        return header("token", value);
    }


    /**
     * 设置一个header<br>
     * 覆盖模式，则替换之前的值
     *
     * @param name  Header名
     * @param value Header值
     * @return T 本身
     */
    public HttpClient header(Header name, String value) {
        return header(name.toString(), value);
    }


    /**
     * 设置一个header<br>
     * 覆盖模式，则替换之前的值
     *
     * @param name  Header名
     * @param value Header值
     * @return T 本身
     */
    public HttpClient header(String name, String value) {
        Header header = Header.get(name);
        if (header != null) {
            switch (header) {
                case ACCEPT:
                    accept(value);
                    break;
                case CONTENT_TYPE:
                    contentType(ContentType.parse(value));
                    break;
                default:
                    headers.put(name, value);
            }
        } else {
            headers.put(name, value);
        }
        return this;
    }

    /**
     * 设置请求头<br>
     * 覆盖原有请求头
     *
     * @param headers 请求头
     * @return this
     */
    public HttpClient header(Map<String, String> headers) {
        if (CollectionUtil.isEmpty(headers)) {
            return this;
        }
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            this.header(entry.getKey(), StrUtil.defaultString(entry.getValue()));
        }
        return this;
    }

    /**
     * 设置表单数据<br>
     *
     * @param name   名
     * @param value  值
     * @param encode 是否进行编码
     * @return this
     */
    public HttpClient form(String name, Object value, boolean encode) {
        form(name, value, encode, null);
        return this;
    }

    /**
     * 设置表单数据<br>
     *
     * @param name    名
     * @param value   值
     * @param encode  是否进行编码
     * @param charset 编码格式
     * @return this
     */
    public HttpClient form(String name, Object value, boolean encode, String charset) {
        if (StrUtil.isBlank(name) || value == null) {
            return this; // 忽略非法的form表单项内容;
        }

        if (this.data == null) {
            data = new HashMap<String, Object>();
        }

        String strValue;
        if (value instanceof List) {
            // 列表对象
            strValue = StrUtil.join((List<?>) value, ",");
        } else if (value.getClass().isArray()) {
            // 数组对象
            strValue = StrUtil.join((Object[]) value, ",");
        } else {
            // 其他对象一律转换为字符串
            strValue = value.toString();
        }

        if (encode) {
            try {
                data.put(name, URLEncoder.encode(strValue, charset == null ? "UTF-8" : charset));
            } catch (Exception e) {
            }
        } else {
            data.put(name, value);
        }
        // 停用body
        this.body = null;
        return this;
    }

    /**
     * 设置表单数据<br>
     *
     * @param name  名
     * @param value 值
     * @return this
     */
    public HttpClient form(String name, Object value) {
        form(name, value, false);
        return this;
    }

    /**
     * 设置map类型表单数据
     *
     * @param formMap 表单内容
     * @return this
     */
    public HttpClient form(Map<String, ? extends Object> formMap) {
        if (CollectionUtil.isNotEmpty(formMap)) {
            for (Map.Entry<String, ? extends Object> entry : formMap.entrySet()) {
                form(entry.getKey(), entry.getValue());
            }
        }
        return this;
    }

    /**
     * 设置map类型表单数据
     *
     * @param formMap 表单内容
     * @return this
     */
    public HttpClient form(Map<String, ? extends Object> formMap, boolean encode) {
        if (CollectionUtil.isNotEmpty(formMap)) {
            for (Map.Entry<String, ? extends Object> entry : formMap.entrySet()) {
                form(entry.getKey(), entry.getValue(), encode);
            }
        }
        return this;
    }

    /**
     * 设置内容主体
     *
     * @param body 请求体
     * @return this
     */
    public HttpClient body(String body) {
        this.body = body;

        // 当使用body时，停止data的使用
        this.data = null;

        return this;
    }


    /**
     * 初始化请求
     */
    private void initRequest() {
        if (this.method == null || StrUtil.isBlank(url)) {
            throw new BizException("缺少url或method参数！");
        }
        if (!url.startsWith("http:") && !url.startsWith("https:")) {
            throw new BizException("url必须带http或者https协议！");
        }
        if (url.startsWith("https")) {
            this.isHttps = true;
        }
        if (isHttps) {
            if (!url.startsWith("https:")) {
                url = url.replace("http:", "https:");
            }
        }
        reqCharset = StrUtil.isBlank(reqCharset) ? "UTF-8" : reqCharset;
        respCharset = StrUtil.isBlank(respCharset) ? "UTF-8" : respCharset;

        if (cookie != null) {
            header(Header.COOKIE.getValue(), this.cookie);
        }
    }

    /**
     * 初始化请求参数内容
     */
    private void initBody() {
        //处理form数据
        if (CollectionUtil.isNotEmpty(data)) {
            if (this.contentType != null) {
                if (ContentType.APPLICATION_JSON.getMimeType().equals(this.contentType.getMimeType())) {
                    this.body = JSONUtil.toStringAlways(this.data);
                    this.accept = StrUtil.isNotEmpty(this.accept) ? this.accept : getJsonAccept();
                } else if (ContentType.APPLICATION_XML.getMimeType().equals(this.contentType.getMimeType())) {
                    this.body = XmlUtil.mapToXmlStr(data, "");
                    this.accept = StrUtil.isNotEmpty(this.accept) ? this.accept : getXmlAccept();
                } else {
                    this.body = getParam(data);
                    //form模式。不需要设置这个
                    // this.accept = StrUtil.isNotEmpty(this.accept) ? this.accept : getFormAccept();
                }
            } else {
                this.contentType = ContentType.create(ContentType.APPLICATION_FORM_URLENCODED.getMimeType(), reqCharset);
                this.body = getParam(data);
                //form模式。不需要设置这个
                // this.accept = StrUtil.isNotEmpty(this.accept) ? this.accept : getFormAccept();
            }
        }
        //非 put, post, patch请求。将body数据，拼接到url
        if (!Method.PUT.equals(this.method)
                && !Method.POST.equals(this.method) && !Method.PATCH.equals(this.method)) {
            this.url = getUrl(this.url, body);
            this.body = null;
        }
    }

    /**
     * 获取json格式传输的Accept
     *
     * @return
     */
    private String getJsonAccept() {
        return ContentType.APPLICATION_JSON.getMimeType() + "," + ContentType.TEXT_HTML.getMimeType();
    }

    /**
     * 获取xml格式传输的Accept
     *
     * @return
     */
    private String getXmlAccept() {
        return ContentType.APPLICATION_XML.getMimeType();
    }

    /**
     * 获取form格式传输的Accept
     *
     * @return
     */
    private String getFormAccept() {
        return ContentType.APPLICATION_FORM_URLENCODED.getMimeType() + "," + ContentType.TEXT_HTML.getMimeType();
    }

    //=============================执行发送请求==========================================================

    /**
     * 使用json进行数据传输
     *
     * @return
     */
    public HttpResult executeJson() {
        initRequest();
        this.contentType = ContentType.create(ContentType.APPLICATION_JSON.getMimeType(), reqCharset);
        this.accept = getJsonAccept();
        initBody();
        return execute();
    }

    /**
     * 使用xml进行数据传输
     *
     * @return
     */
    public HttpResult executeXml() {
        initRequest();
        this.contentType = ContentType.create(ContentType.APPLICATION_XML.getMimeType(), reqCharset);
        this.accept = getXmlAccept();
        initBody();
        return execute();
    }

    /**
     * 使用form进行数据传输
     *
     * @return
     */
    public HttpResult executeForm() {
        initRequest();
        this.contentType = ContentType.create(ContentType.APPLICATION_FORM_URLENCODED.getMimeType(), reqCharset);
        //form模式。不需要设置这个
        //this.accept = getFormAccept();
        initBody();
        return execute();
    }

    /**
     * 使用指定格式进行传输
     *
     * @param contentType
     * @param accept
     * @return
     */
    public HttpResult execute(ContentType contentType, String accept) {
        initRequest();
        initBody();
        this.contentType = contentType;
        this.accept = accept;
        return execute();
    }

    private HttpResult execute() {
        try {
            logger.debug(method + " " + (isHttps ? "HTTPS" : "HTTP") + " " + url + ", body-> " + body);
            logger.debug("accept==> " + accept + " contentType==>" + contentType);

            CloseableHttpClient client = initClient();
            HttpRequestBase request = null;
            CloseableHttpResponse response = null;
            switch (this.method) {
                case PUT:
                    request = new HttpPut(url);
                    if (StrUtil.isNotBlank(body)) {
                        ((HttpPut) request).setEntity(new ByteArrayEntity(body.getBytes(reqCharset), contentType));
                    }
                    break;
                case POST:
                    request = new HttpPost(url);
                    if (StrUtil.isNotBlank(body)) {
                        ((HttpPost) request).setEntity(new ByteArrayEntity(body.getBytes(reqCharset), contentType));
                    }
                    break;
                case PATCH:
                    request = new HttpPatch(url);
                    if (StrUtil.isNotBlank(body)) {
                        ((HttpPatch) request).setEntity(new ByteArrayEntity(body.getBytes(reqCharset), contentType));
                    }
                    break;
                case GET:
                    request = new HttpGet(url);
                    break;
                case DELETE:
                    request = new HttpDelete(url);
                    break;
                case HEAD:
                    request = new HttpHead(url);
                    break;
                case TRACE:
                    request = new HttpTrace(url);
                    break;
                case OPTIONS:
                    request = new HttpOptions(url);
                    break;
            }
//            request.setConfig(initReqConfig());//由 initClient()  完成处理
            //这个请求头，决定是用普通的form，还是json，还是xml等格式传输[指定客户端能够接收的内容类型]
            request.setHeader(Header.ACCEPT.getValue(), this.accept);
            // 请求表示提交内容类型或返回返回内容的MIME类型
            request.setHeader(Header.CONTENT_TYPE.getValue(), this.contentType.getMimeType());
            //添加其他请求头
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                request.setHeader(entry.getKey(), entry.getValue());
            }

            response = client.execute(request);
            try {
                HttpEntity entity = response.getEntity();
                if (entity == null) {
                    logger.error("Method:" + method + " isHttps: " + isHttps
                            + " params: " + body + " url: " + url
                            + " response is null");
                    return null;
                }
                HttpResult result = new HttpResult();
                result.setRespCharset(this.respCharset);
                result.setContentLength(entity.getContentLength());
                result.setContentType(entity.getContentType());
                result.setData(EntityUtils.toByteArray(entity));
                result.setStatusLine(response.getStatusLine());

                return result;
            } finally {
                response.close();
                client.close();
            }
        } catch (Exception e) {
            logger.error(
                    "Method:" + method + " isHttps: " + isHttps + " params: "
                            + body + " url: " + url + " request failed", e);
        }
        return null;
    }


    /**
     * 初始化链接客户端
     *
     * @return
     */
    private CloseableHttpClient initClient() {
        HttpClientBuilder builder = HttpClients.custom();

        if (this.isHttps) {
            builder.setSSLSocketFactory(new SSLConnectionSocketFactory(createSSLClient()));
        }
        if (this.ssl != null) {
            builder.setSSLSocketFactory(new SSLConnectionSocketFactory(this.ssl));
        }
        if (this.sslFactory != null) {
            builder.setSSLSocketFactory(this.sslFactory);
        }
        //重试
        if (tryCount > 0) {
            //创建 基于IO异常的重试hanlder
            builder.setRetryHandler(createReTryHandler());

            //TODO 状态码异常需配合连接池，但启用连接池会影响自定义DNS解析
//            //创建 基于状态码的重试strategy
//            builder.setServiceUnavailableRetryStrategy(createReTryStrategy());
//            //创建连接池
//            builder.setConnectionManager(connectionManager());
        }
        //创建自定义DNS解析对象
        if (CollectionUtil.isNotEmpty(this.hostList)) {
            builder.setDnsResolver(createDnsResolver());
        }
        //请求配置，如链接超时时间，代理等
        builder.setDefaultRequestConfig(initReqConfig());

        return builder.build();
    }


    /**
     * 创建 基于IO异常的重试hanlder
     *
     * @return
     */
    private HttpRequestRetryHandler createReTryHandler() {
        HttpRequestRetryHandler retryHandler = new HttpRequestRetryHandler() {
            @Override
            public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
                if (executionCount <= tryCount) {
                    if (retryInterval > 0) {
                        try {
                            Thread.sleep(retryInterval);
                        } catch (InterruptedException e) {
                            logger.warn("休眠异常！");
                        }
                    }
                    logger.info("IO异常，进入请求重试。。。。重试次数：" + executionCount + ", url==>" + url);
                    return true;
                }

                return false;
            }
        };
        return retryHandler;
    }

    /**
     * 创建基于http请求状态码 的 重试机制
     *
     * @return
     */
    private ServiceUnavailableRetryStrategy createReTryStrategy() {
        ServiceUnavailableRetryStrategy strategy = new ServiceUnavailableRetryStrategy() {
            @Override
            public boolean retryRequest(HttpResponse response, int executionCount, HttpContext context) {
                //可以指定具体返回码如400，
                int statusCode = response.getStatusLine().getStatusCode();

                boolean flag = whiteStatusCodeList.contains(statusCode);
                if (!flag) {
                    logger.info("HTTP状态异常，进入状态码重试机制中，当前状态码：" + statusCode);
                }

                //重试次数
                if (!flag && executionCount <= tryCount) {
                    logger.info("HTTP状态异常，进入请求重试。。。。状态码：" + statusCode + ",重试次数：" + executionCount + ", url==>" + url);
                    return true;
                } else {
                    return false;
                }
            }

            //重试间隔时间
            @Override
            public long getRetryInterval() {
                return retryInterval;
            }
        };
        return strategy;
    }


    /**
     * 设置整个连接池最大连接数 根据自己的场景决定
     * MaxtTotal是整个池子的大小；
     * DefaultMaxPerRoute是根据连接到的主机对MaxTotal的一个细分；比如：
     * MaxtTotal=400 DefaultMaxPerRoute=200
     * 当只连接到http://sishuok.com时，到这个主机的并发最多只有200；而不是400；
     * 当连接到http://sishuok.com 和 http://qq.com时，到每个主机的并发最多只有200；即加起来是400（但不能超过400）；所以起作用的设置是DefaultMaxPerRoute。
     * <p>
     * 暂不做成可配置化，因为这个属于工具类。任何请求都是进行初始化并赋值，暂未用到连接池场景，这里使用连接池，是由于内部重试机制需要
     *
     * @return
     */
    private PoolingHttpClientConnectionManager connectionManager() {
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(20);
        cm.setDefaultMaxPerRoute(20);
        return cm;
    }


    /**
     * 创建https证书
     *
     * @return
     */
    private SSLContext createSSLClient() {
        try {
            return new SSLContextBuilder().loadTrustMaterial(
                    null, new TrustStrategy() {
                        // 信任所有证书
                        @Override
                        public boolean isTrusted(X509Certificate[] chain,
                                                 String authType) throws CertificateException {
                            return true;
                        }
                    }).build();
        } catch (Exception e) {
            logger.error("get ssl client failed", e);
            throw new BizException("创建SSL证书失败！请重试！");
        }
    }

    private DnsResolver createDnsResolver() {
        DnsResolver dnsResolver = new DnsResolver() {
            @Override
            public InetAddress[] resolve(String host) throws UnknownHostException {
                List<InetAddress> ipList = hostList.get(host);
                if (CollectionUtil.isEmpty(ipList)) {
                    logger.info("查询不到自定义DNS解析，将使用默认系统DNS。" + host);
                    //没有配置指定的dns解析ip，则使用默认的
                    return InetAddress.getAllByName(host);
                }
                InetAddress[] inetAddresses = ipList.toArray(new InetAddress[ipList.size()]);
                logger.info("使用自定义DNS解析。。" + JSONUtil.toString(inetAddresses));
                return inetAddresses;
            }
        };
        return dnsResolver;
    }

    /**
     * 初始化请求参数
     *
     * @return
     */
    private RequestConfig initReqConfig() {
        RequestConfig requestConfig = RequestConfig.custom()
                .setConnectionRequestTimeout(3000)//使用连接池来管理连接时，从连接池获取连接的超时时间，毫秒。
                .setConnectTimeout(connectTimeout <= 0 ? 10000 : connectTimeout)
                .setSocketTimeout(socketTimeout <= 0 ? 10000 : socketTimeout)
                .setProxy(proxy == null ? null : proxy)
                .build();
        return requestConfig;
    }

    private static String getUrl(String url, String body) {
        if (StrUtil.isBlank(body)) {
            return url;
        }
        StringBuilder requestUrl = new StringBuilder(url);
        int i = 0;
        if (url.matches(".*\\?.*")) {
            requestUrl.append("&");
        } else {
            requestUrl.append("?");
        }
        requestUrl.append(body);
        return requestUrl.toString();
    }

    private static String getParam(Map<String, Object> params) {
        if (params == null || params.isEmpty()) {
            return "";
        }
        //转成 &连接字符串
        List<String> paramStr = new ArrayList<>();
        for (Map.Entry<String, Object> entry : params.entrySet()) {
            if (entry.getValue() == null || "".equals(entry.getValue())) {
                continue;
            }
            paramStr.add(entry.getKey() + "=" + entry.getValue());
        }
        return StrUtil.collectionToDelimitedString(paramStr, "&");
    }

    /**
     * 从请求参数的body中判断请求的Content-Type类型，支持的类型有：
     * <p>
     * <pre>
     * 1. application/json
     * 1. application/xml
     * </pre>
     *
     * @param body 请求参数体
     * @return Content-Type类型，如果无法判断返回null
     */
    private static ContentType getContentType(String body) {
        //默认使用 form 格式
        if (StrUtil.isBlank(body)) {
            return ContentType.APPLICATION_FORM_URLENCODED;
        }
        char firstChar = body.trim().charAt(0);
        switch (firstChar) {
            case '{':
            case '[':
                // JSON请求体
                return ContentType.APPLICATION_JSON;
            case '<':
                // XML请求体
                return ContentType.APPLICATION_XML;
            default:
                return ContentType.APPLICATION_FORM_URLENCODED;
        }
    }


    public enum Method {
        GET, POST, HEAD, OPTIONS, PUT, DELETE, TRACE, PATCH;
    }
}
