package com.web.common.core.cache.annotation;


import com.web.common.core.cache.CacheType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by apple on 2018/7/23.
 * 使用该注解的方法被调用后将其返回值缓存起来，以保证下次利用同样的参数来执行该方法时可以直接从缓存中获取结果，而不需要再次执行该方法
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Cacheable {
    /**
     * 缓存名称
     *
     * @return
     */
    String value() default "";

    /**
     * 缓存参数变量
     *
     * @return
     */
    String key() default "";

    /**
     * 缓存类型（默认使用redis缓存）
     *
     * @return
     */
    String cacheType() default CacheType.REDIS;

    /**
     * 缓存有效期（默认7天）
     *
     * @return
     */
    int seconds() default 604800;

    /**
     * 泛型的Class类型
     */
    Class<?> classType() default Exception.class;

}
