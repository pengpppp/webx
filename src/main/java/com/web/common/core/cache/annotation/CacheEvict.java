package com.web.common.core.cache.annotation;


import com.web.common.core.cache.CacheType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by apple on 2018/7/23.
 * 使用该注解的方法被调用后清除指定的缓存
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CacheEvict {
    /**
     * 缓存名称
     *
     * @return
     */
    String value() default "";

    /**
     * 缓存参数变量
     *
     * @return
     */
    String key() default "";

    /**
     * 缓存类型（默认使用redis缓存）
     *
     * @return
     */
    String cacheType() default CacheType.REDIS;

}
