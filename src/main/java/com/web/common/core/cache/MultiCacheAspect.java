package com.web.common.core.cache;

import com.web.common.core.cache.annotation.CacheEvict;
import com.web.common.core.cache.annotation.CachePut;
import com.web.common.core.cache.annotation.Cacheable;
import com.web.common.core.entity.SystemContext;
import com.web.common.core.util.JSONUtil;
import com.web.common.core.util.RedisUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.LocalVariableTableParameterNameDiscoverer;
import org.springframework.core.annotation.Order;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.lang.reflect.Method;

/**
 * Created by apple on 2018/7/23.
 *
 * @author: linhq
 * @Description: 多级缓存aop
 */
@Aspect
@Order(1)
@Component
public class MultiCacheAspect {
    private static final Logger logger = Logger.getLogger(MultiCacheAspect.class);

    /**
     * 定义切入点
     */
    @Pointcut("@annotation(com.web.common.core.cache.annotation.Cacheable)")
    public void cacheable() {
    }

    @Pointcut("@annotation(com.web.common.core.cache.annotation.CacheEvict)")
    public void cacheEvict() {
    }

    @Pointcut("@annotation(com.web.common.core.cache.annotation.CachePut)")
    public void CachePut() {
    }

    /**
     * 在方法调用时判断是否有缓存，有则直接取缓存，无则获取调用方法返回值，存储缓存，并返回
     *
     * @param joinPoint
     * @return
     */
    @Around("cacheable()")
    public Object cacheable(ProceedingJoinPoint joinPoint) {
        //得到被切面修饰的方法的参数列表
        Object[] args = joinPoint.getArgs();
        //result是方法的最终返回结果
        Object result = null;

        //得到被代理方法的返回值类型
        Class returnType = ((MethodSignature) joinPoint.getSignature()).getReturnType();
        //得到被代理的方法
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        //得到被代理的方法上的注解
        Cacheable ca = method.getAnnotation(Cacheable.class);
        //获得注解的属性，组成key值
        String key = this.parseKey(ca.key(), method, args);
        //拼接key值
        key = StringUtils.isBlank(key) ? ca.value() : ca.value() + ":" + key;
        Class<?> elementClass = ca.classType();
        try {
            //判断缓存的类型
            if (ca.cacheType().equals(CacheType.REDIS)) {
                //先从redis中取数据
                String cacheValue = RedisUtil.get(key);
                if (StringUtils.isBlank(cacheValue)) {
                    //调用业务方法得到结果
                    result = joinPoint.proceed(args);
                    //判断结果是否为null、空集合
                    if (ObjectUtils.isEmpty(result)) {
                        return result;
                    }
                    //如果redis中没有数据，将结果转json后放入redis
                    RedisUtil.setex(key, ca.seconds(), JSONUtil.toString(result));
                } else {
                    if (elementClass == Exception.class) {
                        //如果redis中可以取到数据，将缓存中获取到的数据转换后返回
                        result = JSONUtil.toObj(cacheValue, returnType);
                    } else {
                        result = JSONUtil.toObj(cacheValue, returnType, elementClass);
                    }
                }
            } else if (ca.cacheType().equals(CacheType.LOCALTHREAD)) {
                //先从当前线程中取数据
                Object target = SystemContext.get(key);
                if (target == null) {
                    //调用业务方法得到结果
                    result = joinPoint.proceed(args);
                    //判断结果是否为null、空集合
                    if (ObjectUtils.isEmpty(result)) {
                        return result;
                    }
                    SystemContext.put(key, result);
                } else {
                    result = target;
                }
            }
        } catch (Throwable e) {
            logger.error("缓存处理异常", e);
        }
        return result;
    }

    /**
     * 在方法调用后，将结果存入缓存中
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @AfterReturning(value = "CachePut()", returning = "result")
    public void CachePut(JoinPoint joinPoint, Object result) {
        //判断调用接口是否为null、或者空集合
        if (!ObjectUtils.isEmpty(result)) {
            //得到被代理的方法
            Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
            //得到被切面修饰的方法的参数列表
            Object[] args = joinPoint.getArgs();
            //得到被代理的方法上的注解
            CachePut cp = method.getAnnotation(CachePut.class);
            //获得经过el解析后的key值
            String key = this.parseKey(cp.key(), method, args);
            //拼接key值
            key = StringUtils.isBlank(key) ? cp.value() : cp.value() + ":" + key;

            try {
                //判断缓存的类型
                if (cp.cacheType().equals(CacheType.REDIS)) {
                    RedisUtil.setex(key, cp.seconds(), JSONUtil.toString(result));
                } else if (cp.cacheType().equals(CacheType.LOCALTHREAD)) {
                    SystemContext.put(key, result);
                }
            } catch (Throwable e) {
                logger.error("缓存处理异常", e);
            }
        }
    }

    /**
     * 在方法调用前清除缓存，然后调用业务方法
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    @Before("cacheEvict()")
    public void cacheEvict(JoinPoint joinPoint) {
        //得到被代理的方法
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        //得到被切面修饰的方法的参数列表
        Object[] args = joinPoint.getArgs();
        //得到被代理的方法上的注解
        CacheEvict ce = method.getAnnotation(CacheEvict.class);
        //获得经过el解析后的key值
        String key = this.parseKey(ce.key(), method, args);
        //拼接key值
        key = StringUtils.isBlank(key) ? ce.value() : ce.value() + ":" + key;

        //判断缓存的类型，清除对应缓存
        if (ce.cacheType().equals(CacheType.REDIS)) {
            RedisUtil.del(key);
        } else if (ce.cacheType().equals(CacheType.LOCALTHREAD)) {
            SystemContext.remove(key);
        }
    }

    /**
     * 获取缓存的key
     * key 定义在注解上，支持SPEL表达式
     *
     * @return
     */
    private String parseKey(String key, Method method, Object[] args) {
        if (StringUtils.isEmpty(key)) {
            return "";
        }

        //获取被拦截方法参数名列表(使用Spring支持类库)
        LocalVariableTableParameterNameDiscoverer u = new LocalVariableTableParameterNameDiscoverer();
        String[] paraNameArr = u.getParameterNames(method);

        //使用SPEL进行key的解析
        ExpressionParser parser = new SpelExpressionParser();
        //SPEL上下文
        StandardEvaluationContext context = new StandardEvaluationContext();
        //把方法参数放入SPEL上下文中
        for (int i = 0; i < paraNameArr.length; i++) {
            context.setVariable(paraNameArr[i], args[i]);
        }
        return parser.parseExpression(key).getValue(context, String.class);
    }
}
