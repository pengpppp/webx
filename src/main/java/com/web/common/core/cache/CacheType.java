package com.web.common.core.cache;

/**
 * Created by apple on 2018/8/18.
 * 缓存类型全局变量
 */
public class CacheType {

	/** redis **/
	public static final String REDIS = "REDIS";
	/** 线程 **/
	public static final String LOCALTHREAD = "LOCALTHREAD";
}
