package com.web.common.core.enums;

/**
 * 公共枚举. 针对只有“是”，“否”两种状态枚举
 *
 * @author Peter
 */
public enum PublicEnum {

    NO(0, "否", "N", "no", false),

    YES(1, "是", "Y", "yes", true);

    /**
     * 状态
     */
    private int state;

    /**
     * 描述
     */
    private String desc;

    /**
     * 缩写
     */
    private String ab;

    /**
     * 英文
     */
    private String en;

    private boolean flag;

    PublicEnum(int state, String desc, String ab, String en, boolean flag) {
        this.state = state;
        this.desc = desc;
        this.ab = ab;
        this.en = en;
        this.flag = flag;
    }

    public int getState() {
        return state;
    }

    public String getDesc() {
        return desc;
    }

    public String getAb() {
        return ab;
    }

    public String getEn() {
        return en;
    }

    public boolean isFlag() {
        return flag;
    }

    public static PublicEnum get(Integer state) {
        if (state == null) {
            return NO;
        }
        if (state == YES.getState()) {
            return YES;
        }
        return NO;
    }


}
