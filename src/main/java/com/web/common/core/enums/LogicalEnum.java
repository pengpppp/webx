/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.enums;

/**
 * 逻辑枚举
 */

public enum LogicalEnum {
    AND, OR
}
