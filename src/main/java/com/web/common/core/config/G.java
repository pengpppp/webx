/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.config;

import com.web.common.core.config.properties.DefaultPropertiesConfig;
import com.web.common.core.enums.EnvEnum;
import com.web.common.core.exception.ErrorMsgLevelEnum;
import com.web.common.core.util.StrUtil;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

/**
 * 配置
 */
public class G extends DefaultPropertiesConfig {
    private static Logger log = Logger.getLogger(G.class);
    /**
     * 系统文件配置 加载。
     */
    public static Map<String, String> CONFIG = new HashMap<>();

    /**  系统参数 */
    /**
     * 系统
     */
    public static String SYSTEM;
    /**
     * 环境 dev，test，qa，prod
     */
    public static String ENV;
    /**
     * 调试模式  true，false
     */
    public static boolean DEBUG;
    /**
     * 错误级别 NONE， SINGLE， ALL
     */
    public static String ERROR_LEVEL;

    static {
        //只加载一次即可
        CONFIG = init("config", true);

        SYSTEM = get(CONFIG, "easyGo.core.system");
        ENV = get(CONFIG, "easyGo.core.env");
        DEBUG = Boolean.parseBoolean(get(CONFIG, "easyGo.core.debug"));
        ERROR_LEVEL = get(CONFIG, "easyGo.core.error.level");

        if (StrUtil.isEmpty(SYSTEM)) {
            log.warn("项目文件中，查询不到配置项：easyGo.core.system，将使用缺省配置：UNKNOWN");
            SYSTEM = "UNKNOWN";
        }
        if (StrUtil.isEmpty(ENV)) {
            log.warn("项目文件中，查询不到配置项：easyGo.core.env，将使用缺省配置： prod（正式环境）");
            ENV = EnvEnum.PROD.name();
        }

        if (!DEBUG) {
            log.warn("当前项目未打开 debug 开关。。");
        }

        if (StrUtil.isEmpty(ERROR_LEVEL)) {
            log.warn("项目文件中，查询不到配置项：easyGo.core.error.level，将使用缺省配置：NONE（不显示信息）");
            ERROR_LEVEL = ErrorMsgLevelEnum.NONE.name();
        }
    }

    public static void init() {
        log.info("初始化缺省系统配置 init.....");
    }


}
