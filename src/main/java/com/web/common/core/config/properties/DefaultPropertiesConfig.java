/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.config.properties;

import com.web.common.core.util.MapUtil;
import com.web.common.core.util.ResourceUtils;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * 缺省实现的配置文件接口
 */
public class DefaultPropertiesConfig {

    private static final Logger logger = Logger.getLogger(DefaultPropertiesConfig.class);

    /**
     * 配置文件加载初始化，需指定文件名，指定是否初始化时检查
     *
     * @param fileName  指定文件名，但不需要后缀名。会自动加载.properties文件
     * @param initCheck 是否初始化时检查。请注意:如果是，则会检查配置文件是否存在，如果不存在，程序直接退出！
     * @return
     */
    public static Map<String, String> init(String fileName, boolean initCheck) {

        logger.debug("-------配置文件加载初始化，" + fileName + ".properties，初始化时检查=" + initCheck + "------");

        Map<String, String> configMap = new HashMap<String, String>();

        try {
            configMap = ResourceUtils.getResource(fileName).getMap();
        } catch (Exception e) {
            if (initCheck) {
                //配置文件必须存在，那在加载时出错。则直接退出系统。
                logger.error("-------无法加载配置文件 " + fileName + ".properties，请确认文件是否存在！", e);
                System.exit(1);
            } else {
                //提示告警信息即可,需自行处理缺省值。
                logger.warn("-------无法加载配置文件 " + fileName + ".properties，已忽略或直接读取缺省配置数据！");
            }
        }
        return configMap;

    }


    /**
     * 从 map 中获取值
     *
     * @param map map集合
     * @param key 读取的 key
     * @return
     */
    public static String get(Map<String, String> map, String key) {
        return MapUtil.getString(map, key);
    }

    /**
     * 从 map 中获取值
     *
     * @param map        map集合
     * @param key        读取的 key
     * @param defaultVal 读取不到时的缺省值
     * @return
     */
    public static String get(Map<String, String> map, String key, String defaultVal) {
        return MapUtil.getString(map, key, defaultVal);
    }

}
