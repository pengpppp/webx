/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.config;

/**
 * 配置文件接口
 */

public interface IConfig {

    /**
     * 是否启动时进行检查
     *
     * @return
     */
    boolean initCheck();

}
