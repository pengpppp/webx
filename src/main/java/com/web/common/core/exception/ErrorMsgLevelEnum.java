/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.exception;

/**
 * 错误信息级别
 */

public enum ErrorMsgLevelEnum {

    /**
     * 不显示消息
     */
    NONE,

    /**
     * 显示单个
     */
    SINGLE,

    /**
     * 显示全部
     */
    ALL;
}
