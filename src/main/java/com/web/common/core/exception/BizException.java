package com.web.common.core.exception;

/**
 * 业务异常基类，所有业务异常都必须继承于此异常
 * 
 * @author Along
 * 
 *         定义异常时，需要先确定异常所属模块。例如：添加账户报错 可以定义为 [10010001] 前四位数为系统模块编号，后4位为错误代码 ,唯一
 *         <br>
 *         账户服务异常 1001 <br>
 */
public class BizException extends BizFatherException {

	private static final long serialVersionUID = -5875371379845226068L;

	/**
	 * 获取序列出错
	 */
	public static final BizException DB_GET_SEQ_NEXT_VALUE_ERROR = new BizException("90040001", "获取序列出错");

	/**
	 * 接口json参数为空
	 */
	public static final BizException JSON_PARAM_IS_NULL = new BizException("90040002", "接口json参数为空");

	/**
	 * 接口json参数格式不正确
	 */
	public static final BizException JSON_PARAM_FORMAT_ERROR = new BizException("90040003", "接口json参数格式错误");

	/**
	 * 会话超时 获取session时，如果是空，throws 下面这个异常 拦截器会拦截爆会话超时页面
	 */
	public static final BizException SESSION_IS_OUT_TIME = new BizException("90040006", "会话超时");

	/***
	 * 传入参数为null
	 */
	public static final String INPUT_DATA_IS_NULL = "90040007";
	/**
	 * 异常信息
	 */
	// protected String msg;

	/**
	 * 具体异常码
	 */
	// protected int code;

	public BizException(String code, String msgFormat, Object... args) {
		super(String.format(msgFormat, args));
		this.code = code;
		this.msg = String.format(msgFormat, args);
	}

	public BizException(String code, String msgFormat, Throwable cause, Object... args) {
		super(String.format(msgFormat, args), cause);
		this.code = code;
		this.msg = String.format(msgFormat, args);
	}

	public BizException() {
		super();
	}
	@Override
	public String getMsg() {
		return msg;
	}
	@Override
	public String getCode() {
		return code;
	}

	/**
	 * 实例化异常
	 * 
	 * @param msgFormat
	 * @param args
	 * @return
	 */
	@Override
	public BizException newInstance(String msgFormat, Object... args) {
		return new BizException(this.code, msgFormat, args);
	}

	public BizException(String message, Throwable cause) {
		super(message, cause);
		this.msg = message;
	}

	public BizException(Throwable cause) {
		super(cause);
		this.msg = cause.getMessage();
	}

	public BizException(String message) {
		super(message);
		this.msg = message;
	}
}
