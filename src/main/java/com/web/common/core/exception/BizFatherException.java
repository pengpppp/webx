package com.web.common.core.exception;

/**
 * 业务异常基类的父类，用户处理dubbo服务抛bizException，web端异常拦截不到bizException问题
 * 
 * @author Along
 */
public class BizFatherException extends RuntimeException {

	private static final long serialVersionUID = -5875371379845226068L;

	/**
	 * 异常信息
	 */
	protected String msg;

	/**
	 * 具体异常码
	 */
	protected String code;

	public BizFatherException(String code, String msgFormat, Object... args) {
		super(String.format(msgFormat, args));
		this.code = code;
		this.msg = String.format(msgFormat, args);
	}

	public BizFatherException() {
		super();
	}

	public String getMsg() {
		return msg;
	}

	public String getCode() {
		return code;
	}

	/**
	 * 实例化异常
	 * 
	 * @param msgFormat
	 * @param args
	 * @return
	 */
	public BizFatherException newInstance(String msgFormat, Object... args) {
		return new BizFatherException(this.code, msgFormat, args);
	}

	public BizFatherException(String message, Throwable cause) {
		super(message, cause);
	}

	public BizFatherException(Throwable cause) {
		super(cause);
	}

	public BizFatherException(String message) {
		super(message);
	}
}
