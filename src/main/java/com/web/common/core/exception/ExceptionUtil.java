package com.web.common.core.exception;

import com.web.Tools.staticData.Code;
import com.web.common.core.config.G;
import com.web.common.core.entity.JsonData;
import com.web.common.core.enums.EnvEnum;
import com.web.common.core.util.StrUtil;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

/**
 * 异常处理工具类
 */
public class ExceptionUtil {

    private ExceptionUtil() {
    }

    private static String environment = G.ENV;
    private static String level = G.ERROR_LEVEL;


    /**
     * 把后台的异常信息记录到日志文件，友好信息输出给客户端
     * 开发，测试，准生产会打印详细堆栈到日志文件和返回详细堆栈，正式生产环境会打印堆栈到日志文件，不会返回详细堆栈，
     * <p>
     * 无论是controller层，service层还是dao层类，出现数据校验不通过或者数据处理失败，都应当throw new
     * com.ylp.common.core.exception.BizException()
     * <p>
     * 比如数据校验失败（客户端发起的请求不符合api规范原因）：throw new
     * com.ylp.common.core.exception.BizException(JsonData.FAIL,"密码长度应该大于8位，并且包含大小写字母");
     * 再比如后台处理失败（程序逻辑原因，内存原因，网络原因等）：throw new
     * com.ylp.common.core.exception.BizException(JsonData.ERROR,"网络缓慢，请稍后重试");
     * <p>
     * 如果不进行任何处理， 如果程序抛出的异常类型为BindException,则默认抛出异常为
     * com.ylp.common.core.exception.BizException(JsonData.FAIL, sb.toString());
     * <p>
     * 程序抛出的其他类型的异常，则默认异常为throw new
     * com.ylp.common.core.exception.BizException(JsonData.FAIL,JsonData.FAIL_MSG);
     * <p>
     * <p>
     * 用户看到的只有数据异常等提示
     *
     * @param e
     * @return
     * @throws Exception
     */
    public static JsonData convertExceptionToJsonData(Exception e) {
        // 记录到日志文件，便于elk等监控系统实时监控

        return convertExceptionToJsonData(null, e);
    }

    /**
     * @param request
     * @param e
     * @return
     * @throws Exception
     */
    public static JsonData convertExceptionToJsonData(HttpServletRequest request, Exception e) {

        String stackTrace = getStackTrace(e);

        if (EnvEnum.PROD.name().equals(environment)) {
            return new JsonData(Code.FAIL, null, getLevelMsg(e));

        } else if (EnvEnum.QA.name().equals(environment)) {
            return new JsonData(Code.FAIL, e.getMessage(), getLevelMsg(e));

        } else if (EnvEnum.TEST.name().equals(environment)) {
            return new JsonData(Code.FAIL, e.getMessage(), getLevelMsg(e));

        } else if (EnvEnum.DEV.name().equals(environment)) {
            return new JsonData(Code.FAIL, getRequestInfo(request) + "|||" + stackTrace, getLevelMsg(e));
        }

        return new JsonData(Code.FAIL, getRequestInfo(request) + "|||" + stackTrace, getLevelMsg(e));

    }

    private static String getStackTrace(Exception e) {
        String stackTrace = ExceptionUtils.getStackTrace(e);

        if (e instanceof BizException) {
            BizException bizException = (BizException) e;
            stackTrace = "## " + bizException.getMsg() + getErrMsg(e);
        } else if (e instanceof BindException) {
            stackTrace = "## " + e.getMessage();
        } else if (e instanceof MethodArgumentNotValidException) {
            stackTrace = "## " + e.getMessage();
        }
        return stackTrace;
    }


    /**
     * 过滤 ylp 项目异常信息
     *
     * @param e
     * @return
     */
    private static String getErrMsg(Exception e) {
        if (e == null) {
            return "";
        }
        StackTraceElement[] stacks = e.getStackTrace();
        if (stacks == null || stacks.length <= 0) {
            return "";
        }
        StringBuffer buffer = new StringBuffer();
        return buffer.toString();
    }

    /**
     * 根据级别获取对应的日志信息
     *
     * @param e
     * @return
     */
    private static String getLevelMsg(Exception e) {

        List<String> errorMsgList = null;
        if (e instanceof BindException) {
            errorMsgList = getMsg(((BindException) e).getAllErrors());

        } else if (e instanceof MethodArgumentNotValidException) {
            errorMsgList = getMsg(((MethodArgumentNotValidException) e).getBindingResult().getAllErrors());
        } else if (e instanceof BizException) {
            errorMsgList = new ArrayList<>();
            errorMsgList.add(((BizException) e).getMsg());
        } else {
            errorMsgList = null;
        }
        //如果集合没数据，直接返回"请求失败"！
        if (errorMsgList == null || errorMsgList.size() <= 0) {
            return Code.information.get(Code.FAIL);
        }
        //根据级别显示
        if (ErrorMsgLevelEnum.ALL.name().equals(level)) {

            return StrUtil.collectionToDelimitedString(errorMsgList, ",");
        } else if (ErrorMsgLevelEnum.SINGLE.name().equals(level)) {
            //取第一条
            return errorMsgList.get(0);

        } else if (ErrorMsgLevelEnum.NONE.name().equals(level)) {
            return Code.information.get(Code.FAIL);
        }


        return Code.information.get(Code.FAIL);
    }


    /**
     * 获取所有错误信息
     *
     * @param objectErrors
     * @return
     */
    private static List<String> getMsg(List<ObjectError> objectErrors) {
        List<String> list = new LinkedList<>();
        if (objectErrors == null || objectErrors.size() <= 0) {
            return null;
        }
        for (ObjectError objectError : objectErrors) {
            list.add(objectError.getDefaultMessage());
        }
        return list;
    }

    /**
     * 获取请求信息
     *
     * @param request
     * @return
     */
    private static String getRequestInfo(HttpServletRequest request) {
        if (request == null) {
            return "";
        }
        StringBuilder r = new StringBuilder();
        r.append("requestURL=");
        r.append(request.getRequestURL().toString());
        r.append(",requestURI=");
        r.append(request.getRequestURI());
        r.append(",queryString=");
        r.append(request.getQueryString());
        r.append(",remoteHost=");
        r.append(request.getRemoteHost());
        r.append(",remotePort=");
        r.append(request.getRemotePort());
        r.append(",localAddr=");
        r.append(request.getLocalAddr());
        r.append(",localName=");
        r.append(request.getLocalName());
        r.append(",method=");
        r.append(request.getMethod());

        Map<String, String> map = new HashMap<>();
        Enumeration headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String key = (String) headerNames.nextElement();
            String value = request.getHeader(key);
            map.put(key, value);
        }

        r.append(",headers=");
        r.append(map);
        r.append(",parameters=");
        r.append(request.getParameterMap());
        return r.toString();

    }

}
