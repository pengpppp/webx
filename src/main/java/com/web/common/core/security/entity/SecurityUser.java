/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.security.entity;

import com.web.common.core.util.StrUtil;

import java.io.Serializable;

/**
 * 认证权限抽象用户，开发人员可根据实际情况，继承此类，添加自定义字段。
 */

public class SecurityUser implements Serializable {
    /**
     * 账号
     **/
    private String userName;

    /**
     * 角色
     **/
    private String[] securityRoles;

    /**
     * 鉴权token
     **/
    private String token;

    /**
     * 鉴权刷新token
     **/
    private String refreshToken;


    /**
     * 是否包含角色
     *
     * @param roleName
     * @return
     */
    public boolean hasRole(String roleName) {
        if (securityRoles == null || securityRoles.length <= 0) {
            return false;
        }
        if (StrUtil.isEmpty(roleName)) {
            return false;
        }
        for (String role : securityRoles) {
            if (roleName.equalsIgnoreCase(role)) {
                return true;
            }
        }
        return false;
    }


    public String[] getSecurityRoles() {
        return securityRoles;
    }

    public void setSecurityRoles(String[] securityRoles) {
        this.securityRoles = securityRoles;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    @Override
    public String toString() {
        return "SecurityUser{" +
                "userName='" + userName + '\'' +
                ", token='" + token + '\'' +
                ", refreshToken='" + refreshToken + '\'' +
                '}';
    }
}
