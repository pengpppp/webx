/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.security.entity;


import com.web.common.core.entity.SystemContext;

/**
 * 安全上下文
 */

public class SecurityContext {

    private static final String SECURITY_USER_KEY = SecurityContext.class.getName() + "_SECURITY_USER_KEY";


    /**
     * 从线程中获取用户
     *
     * @return
     */
    public static SecurityUser getSecurityUser() {
        return (SecurityUser) SystemContext.get(SECURITY_USER_KEY);
    }

    /**
     * 把登录用户存放到线程中
     *
     * @param user
     */
    public static void setSecurityUser(SecurityUser user) {
        if (user != null) {
            SystemContext.put(SECURITY_USER_KEY, user);
        }
    }

    /**
     * 从线程中移除用户信息
     */
    public static void removeSecurityUser() {
        SystemContext.remove(SECURITY_USER_KEY);
    }
}
