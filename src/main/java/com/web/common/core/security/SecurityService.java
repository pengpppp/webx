/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.security;


import com.web.common.core.security.entity.SecurityUser;

/**
 * 安全服务接口
 */

public interface SecurityService<T> {


    /**
     * 登录接口
     *
     * @return
     */
    void login(String username, String password);


    /**
     * 处理登录用户
     *
     * @param entity 用户对象
     * @return
     */
    void setSecurityUser(SecurityUser entity);


    SecurityUser getSecurityUser();

    SecurityUser doGetAuthenticationInfo(String username, String password);

    SecurityUser doGetAuthorizationInfo(SecurityUser entity);
}
