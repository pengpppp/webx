/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.security.util;

import com.web.common.core.entity.SystemContext;
import com.web.common.core.security.entity.SecurityUser;
import org.apache.commons.codec.digest.DigestUtils;

/**
 * 安全认证工具处理类
 */

public class SecurityUtil {


    private static final String SECURITY_KEY = SecurityUtil.class.getName() + "_SECURITY_KEY";

    private SecurityUtil() {
    }

    /**
     * 密码加密
     *
     * @param password 密码
     * @param salt     密码盐
     * @return
     */
    public static String sha1Hex(String password, String salt) {
        return sha1Hex(password + salt);
    }

    /**
     * 字符串加密
     *
     * @param str 字符
     * @return
     */
    public static String sha1Hex(String str) {
        return DigestUtils.sha1Hex(str);
    }

    /**
     * 从线程中获取SecurityUser对象
     *
     * @return
     */
    public static SecurityUser getSecurityContext() {
        return (SecurityUser) SystemContext.get(SECURITY_KEY);
    }

    /**
     * 将SecurityUser存到线程中
     *
     * @param user
     */
    public static void setSecurityContext(SecurityUser user) {
        if (user != null) {
            SystemContext.put(SECURITY_KEY, user);
        }
    }

    /**
     * 获取封装的 SecurityUser
     *
     * @return SecurityUser
     */
    public static SecurityUser getUser() {
        if (isGuest()) {
            return null;
        } else {
            return getSecurityContext();
        }
    }

    /**
     * 验证当前用户是否属于该角色？,使用时与lacksRole 搭配使用
     *
     * @param roleName 角色名
     * @return 属于该角色：true，否则false
     */
    public static boolean hasRole(String roleName) {
        return getSecurityContext() != null && roleName != null
                && roleName.length() > 0 && getSecurityContext().hasRole(roleName);
    }

    /**
     * 与hasRole标签逻辑相反，当用户不属于该角色时验证通过。
     *
     * @param roleName 角色名
     * @return 不属于该角色：true，否则false
     */
    public static boolean lacksRole(String roleName) {
        return !hasRole(roleName);
    }


    /**
     * 认证通过或已记住的用户。与guset搭配使用。
     *
     * @return 用户：true，否则 false
     */
    public static boolean isUser() {
        return getSecurityContext() != null;
    }

    /**
     * 验证当前用户是否为“访客”，即未认证（包含未记住）的用户。用user搭配使用
     *
     * @return 访客：true，否则false
     */
    public static boolean isGuest() {
        return !isUser();
    }


}
