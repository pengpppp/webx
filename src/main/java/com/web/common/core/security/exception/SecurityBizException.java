/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.security.exception;


import com.web.Tools.staticData.Code;
import com.web.common.core.exception.BizException;

import java.util.Map;

/**
 * 安全异常
 */

public class SecurityBizException extends BizException {


    private Map<String, Object> data;


    public SecurityBizException(String msgFormat, Object... args) {
        super(Code.FAIL, msgFormat, args);
    }

    public SecurityBizException(String msgFormat, Map<String, Object> data, Object... args) {
        super(Code.FAIL, msgFormat, args);
        this.data = data;
    }

    public Map<String, Object> getData() {
        return data;
    }
}
