package com.web.common.core.security.util;

import com.web.common.core.security.exception.SecurityBizException;
import com.web.common.core.util.ResourceUtils;
import com.web.common.core.util.StrUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import io.jsonwebtoken.*;
import org.slf4j.LoggerFactory;

import java.util.Base64;
import java.util.Base64.Decoder;

import java.security.Key;
import java.security.KeyFactory;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Json Web Token 工具类
 *
 * @author Jone.lin
 */
public class JwtUtils {

    private static final Logger log = LoggerFactory.getLogger(JwtUtils.class);

    /**
     * 网站或应用clientId
     */
    public static String CLIENT_ID;
    /**
     * 用户 ID
     */
    public static String AUDIENCE;
    /**
     * 有效时间
     */
    public static long MINUTES;

    /**
     * 私钥对象
     */
    private static Key privateKey;

    /**
     * 公钥对象
     */
    private static Key publicKey;

    static {
        try {
            Map<String, String> param = ResourceUtils.getResource("jwt").getMap();
            CLIENT_ID = param.get("jwt.cilentId");
            AUDIENCE = param.get("jwt.audience");
            //默认一天
            MINUTES = param.get("jwt.minutes") == null ? 24 * 60 : Integer.parseInt(param.get("jwt.minutes"));

            Decoder decoder = Base64.getDecoder();

            byte[] keyBytes=decoder.decode(param.get("jwt.publicKey"));
//            byte[] keyBytes = (new BASE64Decoder()).decodeBuffer(param.get("jwt.publicKey"));
            X509EncodedKeySpec keySpec_publicKey = new X509EncodedKeySpec(keyBytes);
            KeyFactory keyFactory_publicKey = KeyFactory.getInstance("RSA");
            publicKey = keyFactory_publicKey.generatePublic(keySpec_publicKey);

            keyBytes=decoder.decode(param.get("jwt.privateKey"));
//            keyBytes = (new BASE64Decoder()).decodeBuffer(param.get("jwt.privateKey"));
            PKCS8EncodedKeySpec keySpec_privateKey = new PKCS8EncodedKeySpec(keyBytes);
            KeyFactory keyFactory_privateKey = KeyFactory.getInstance("RSA");
            privateKey = keyFactory_privateKey.generatePrivate(keySpec_privateKey);

        } catch (Exception e) {
            log.error("--------------------------无法加载配置文件 jwt.properties，请确认文件是否存在！");
        }
    }


    /**
     * 检查参数
     */
    private static void check() {

        if (StrUtil.isEmpty(CLIENT_ID)) {
            throw new RuntimeException("缺少参数！，请检查 jwt.cilentId 参数是否存在！");
        }
        if (StrUtil.isEmpty(AUDIENCE)) {
            throw new RuntimeException("缺少参数！，请检查 jwt.audience 参数是否存在！");
        }
        if (privateKey == null) {
            throw new RuntimeException("缺少参数！，请检查 jwt.privateKey 参数是否存在！");
        }
        if (publicKey == null) {
            throw new RuntimeException("缺少参数！，请检查 jwt.publicKey 参数是否存在！");
        }

    }

    /**
     * 生成token方法
     *
     * @param payLoadMap 令牌有效负载参数，即需要在token中保存的用户信息
     * @return
     */
    public static String generateToken(Map<String, Object> payLoadMap) {
        check();
        return generateToken(CLIENT_ID, AUDIENCE, MINUTES, payLoadMap);
    }

    /**
     * 生成token方法
     *
     * @param minutes    有效期时间 分钟
     * @param payLoadMap 令牌有效负载参数，即需要在token中保存的用户信息
     * @return
     */
    public static String generateToken(long minutes, Map<String, Object> payLoadMap) {
        check();
        return generateToken(CLIENT_ID, AUDIENCE, minutes, payLoadMap);
    }

    /**
     * 生成token方法
     *
     * @param issUser    令牌的创建者 网站或应用clientId
     * @param audience   令牌使用者 用户ID
     * @param minutes    有效期时间
     * @param payLoadMap 令牌有效负载参数，即需要在token中保存的用户信息
     * @return
     */
    public static String generateToken(String issUser, String audience, long minutes, Map<String, Object> payLoadMap) {
        if (StringUtils.isBlank(issUser)) {
            log.info("令牌创建者为空");
            return null;
        }

        if (StringUtils.isBlank(audience)) {
            log.info("令牌使用者为空");
            return null;
        }

        if (minutes <= 0) {
            log.info("令牌有效时间为空");
            return null;
        }
        //生成JWT的时间
        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);
        long expMillis = nowMillis + minutes * 60 * 1000;
        Date exp = new Date(expMillis);

//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        log.info("当前时间-->" + sdf.format(now));
//        log.info("设定过期时间-->" + sdf.format(exp));
        //下面就是在为payload添加各种标准声明和私有声明了
        //这里其实就是new一个JwtBuilder，设置jwt的body
        String token = "";
        try {
            token = Jwts.builder()
                    //如果有私有声明，一定要先设置这个自己创建的私有的声明，这个是给builder的claim赋值，一旦写在标准的声明赋值之后，就是覆盖了那些标准的声明的
                    .setClaims(payLoadMap)
                    //设置jti(JWT ID)：是JWT的唯一标识，根据业务需要，这个可以设置为一个不重复的值，主要用来作为一次性token,从而回避重放攻击。
//                .setId(id)
                    //iat: jwt的签发时间
                    .setIssuedAt(now)
                    //sub(Subject)：代表这个JWT的主体，即它的所有人，这个是一个json格式的字符串，可以存放什么userid，roldid之类的，作为什么用户的唯一标志。
                    .setSubject(issUser)
                    .setAudience(issUser)
                    .setIssuer(issUser)
                    //设置签名使用的签名算法和签名使用的秘钥
                    .signWith(SignatureAlgorithm.RS256, privateKey)
                    .setExpiration(exp)
                    .compact();


        } catch (ClaimJwtException e) {
            Claims claims = e.getClaims();
            payLoadMap = new HashMap<String, Object>();
            payLoadMap.putAll(claims);
            throw new SecurityBizException("获取 token 令牌错误！" + e.getMessage(), payLoadMap);
        } catch (JwtException e2) {
            throw new SecurityBizException("获取 token 令牌错误！" + e2.getMessage());
        }
        if (StrUtil.isEmpty(token)) {
            throw new SecurityBizException("获取 token 令牌错误！令牌为空");
        }
        return token;
    }


    /**
     * token 校验方法
     *
     * @param token token串
     * @return 令牌有效负载参数
     */
    public static Map<String, Object> checkToken(String token) {
        check();
        return checkToken(token, CLIENT_ID, AUDIENCE);
    }

    /**
     * token 校验方法
     *
     * @param token    token串
     * @param isUser   token创建者
     * @param audience token使用者
     * @return 令牌有效负载参数
     */
    public static Map<String, Object> checkToken(String token, String isUser, String audience) {
        Map<String, Object> payLoadMap = null;
        try {
            payLoadMap = Jwts.parser()
                    .setSigningKey(publicKey)
                    .requireIssuer(isUser)
                    .requireAudience(audience)
                    .parseClaimsJws(token)
                    .getBody();
            //TODO  请注意！token 解析出来后的 exp 字段，过期时间是秒单位，所以需要 *1000 转为毫秒

        } catch (ClaimJwtException e) {
            Claims claims = e.getClaims();
            payLoadMap = new HashMap<String, Object>();
            payLoadMap.putAll(claims);
            throw new SecurityBizException("校验 token 失败！" + e.getMessage(), payLoadMap);
        } catch (JwtException e2) {
            throw new SecurityBizException("校验 token 失败！" + e2.getMessage());
        }
        if (payLoadMap == null) {
            throw new SecurityBizException("校验 token 失败！");
        }
        return payLoadMap;
    }


}
