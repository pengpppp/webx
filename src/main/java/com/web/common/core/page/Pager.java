/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.page;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 分页对象
 *
 * @author Jone.lin
 * @version V2.0.0
 * @date 2017年06月08日
 */

public class Pager<T> implements Serializable {

    /**
     * 总数量
     **/
    private long total;

    /**
     * 页码
     **/
    private int page;

    /**
     * 每页条数
     **/
    private int pageSize;

    /**
     * 分页数据
     **/
    private List<T> data = new ArrayList();

    /**
     * 汇总数据
     */
    private Object summary;

    public Pager<T> total(int total) {
        this.total = total;
        return this;
    }

    public Pager<T> page(int page) {
        this.page = page;
        return this;
    }

    public Pager<T> pageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public Pager<T> data(List<T> data) {
        this.data = data;
        return this;
    }

    public Pager<T> summary(Object summary) {
        this.summary = summary;
        return this;
    }

    private Pager() {
        super();
    }

    public Pager(long total, int page, int pageSize, List<T> data) {
        this.total = total;
        this.page = page;
        this.pageSize = pageSize;

        if (data != null) {
            this.data = data;
        }

    }

    public Pager(long total, int page, int pageSize, List<T> data, Object summary) {
        this.total = total;
        this.page = page;
        this.pageSize = pageSize;
        this.summary = summary;

        if (data != null) {
            this.data = data;
        }

    }

    public Object getSummary() {
        return summary;
    }

    public void setSummary(Object summary) {
        this.summary = summary;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public void setPageData(List<T> pageData) {
        this.data = data;
    }

    public long getTotal() {
        return total;
    }

    public int getPage() {
        return page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public List<T> getPageData() {
        return data;
    }

    @Override
    public String toString() {
        return "Pager{" +
                "total=" + total +
                ", page=" + page +
                ", pageSize=" + pageSize +
                ", data=" + data +
                ", summary=" + summary +
                '}';
    }
}
