/**
 * Copyright © 2014 - 2017 Yolipai. All Rights Reserved. 深圳有礼派网络科技有限公司 版权所有
 */
package com.web.common.core.page;

import com.web.common.core.entity.SystemContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.Arrays;

/**
 * @version V2.0.0
 * @描述: 分页参数传递工具类 .
 * @作者: linhz
 */
public class PageParam implements Serializable {

    private static final long serialVersionUID = 6297178964005032338L;

    private static final Logger logger = LoggerFactory.getLogger(PageParam.class);

    private static final String PAGE_PARAM_KEY = PageParam.class.getName() + "_PAGE_PARAM_KEY";

    /**
     * 当前页数
     */
    private Integer page = 1;
    /**
     * 每页记录数
     */
    private Integer pageSize = 10;

    /**
     * <p>
     * SQL 排序 ORDER BY 字段，例如： id DESC（根据id倒序查询）
     * </p>
     * <p>
     * DESC 表示按倒序排序(即：从大到小排序)<br>
     * ASC 表示按正序排序(即：从小到大排序)
     * </p>
     */
    private String orderByField;

    /**
     * 是否为升序 ASC（ 默认： false 倒序）
     */
    private boolean isAsc = false;
    /**
     * <p>
     * SQL 排序 ASC 数组
     * </p>
     */
    private String[] ascs;
    /**
     * <p>
     * SQL 排序 DESC 数组
     * </p>
     */
    private String[] descs;


    /**
     * 从线程中获取分页对象
     *
     * @return
     */
    public static PageParam getPageContext() {
        return (PageParam) SystemContext.get(PAGE_PARAM_KEY);
    }

    /**
     * 将分页对象存放到线程中
     *
     * @param pageParam
     */
    public static void setPageContext(PageParam pageParam) {
        if (pageParam != null) {
            SystemContext.put(PAGE_PARAM_KEY, pageParam);
        }
    }

    /**
     * 将分页对象从线程中移除
     */
    public static void removePageContext() {
        SystemContext.remove(PAGE_PARAM_KEY);
    }

    public PageParam() {
    }

    // public PageParam(int pageNum, int numPerPage) {
    // super();
    // this.pageNum = pageNum;
    // this.numPerPage = numPerPage;
    // }

    public PageParam(int page, int pageSize) {
        if (page > 1) {
            this.page = page;
        }
        this.pageSize = pageSize;
    }


    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        if (page != null) {
            try {
                this.page = Integer.valueOf(page) <= 0 ? 1 : Integer.valueOf(page);
            } catch (Exception e) {
                logger.warn("Integer.valueOf(page) is error,page=>" + page);
            }
        }

    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        if (pageSize != null) {
            try {
                this.pageSize = Integer.valueOf(pageSize);
            } catch (Exception e) {
                logger.warn("Integer.valueOf(pageSize) is error,pageSize=>" + pageSize);
            }
        }
    }

    public String getOrderByField() {
        return orderByField;
    }

    public void setOrderByField(String orderByField) {
        this.orderByField = orderByField;
    }

    public boolean isAsc() {
        return isAsc;
    }

    public boolean getAsc() {
        return isAsc;
    }

    public void setAsc(boolean asc) {
        isAsc = asc;
    }

    //
    // public int getStart() {
    // if(pageNum > 0 )
    // return (pageNum - 1) * numPerPage;
    // return 0;
    // }

    public String getOrderByStr() {
        if (orderByField == null || orderByField.length() <= 0) {
            return "";
        }
        return orderByField + " " + (isAsc ? "ASC" : "DESC");
    }

    public String[] getAscs() {
        return ascs;
    }

    public void setAscs(String[] ascs) {
        this.ascs = ascs;
    }

    public String[] getDescs() {
        return descs;
    }

    public void setDescs(String[] descs) {
        this.descs = descs;
    }


    @Override
    public String toString() {
        return "PageParam{" +
                "page=" + page +
                ", pageSize=" + pageSize +
                ", orderByField='" + orderByField + '\'' +
                ", isAsc=" + isAsc +
                ", ascs=" + Arrays.toString(ascs) +
                ", descs=" + Arrays.toString(descs) +
                '}';
    }
}
