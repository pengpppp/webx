package com.web.Root.Service;

import com.web.common.core.entity.JsonData;
import com.web.dao.MyNew.ChannelDao;
import com.web.entity.MyNew.Channel;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class RootChannelService {
    @Resource
    ChannelDao channelDao;

    public Object get()
    {
        try {
            return JsonData.buildSuccess(channelDao.queryAll(new Channel()));
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    public Object insert(Channel channel)
    {
        try {
            channelDao.insert(channel);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    public Object update(Channel channel)
    {
        try {
            channelDao.update(channel);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }

    }

    public Object delete(int id)
    {
        try {
            channelDao.deleteById(id);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }
}
