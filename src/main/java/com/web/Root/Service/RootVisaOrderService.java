package com.web.Root.Service;

import com.google.gson.Gson;
import com.web.Tools.Check.check;
import com.web.common.core.entity.JsonData;
import com.web.dao.UserDao;
import com.web.dao.UseridDao;
import com.web.dao.VisaDao;
import com.web.dao.VisaorderDao;
import com.web.entity.User.User;
import com.web.entity.User.Userid;
import com.web.entity.Visaorder;
import com.web.entity.entityImp.Visa.*;
import com.web.entity.entityImp.Visa.VisaDetails;
import com.web.entity.entityImp.Visa.VisaList;
import com.web.entity.entityImp.Visa.VisaOrderDetailsImp;
import com.web.entity.entityImp.Visa.VisaOrderList;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class RootVisaOrderService {
    @Resource
    VisaorderDao visaorderDao;
    private Integer pagesize=10;
    @Resource
    VisaDao visaDao;
    @Resource
    UseridDao useridDao;
    @Resource
    UserDao userDao;
    public Object getAll(String page)
    {
        Integer p ;
        Integer allnumb=visaorderDao.queryCount();
        try {
            if(check.checkNull(page))
                p=1;
            else
                p=Integer.valueOf(page);

            if((p-1)*pagesize>allnumb)
                return JsonData.buildSuccess();
            else
            {
                Gson gson  = new Gson();
                List<Visaorder> list  = visaorderDao.queryAllByLimit(((p-1)*pagesize),pagesize);
                List<VisaOrderList> lists  =new ArrayList<>();
                for(Visaorder visaorder1:list)
                {
                    VisaOrderList visaOrderList = VisaOrderList.VisaOrder2VisaOrderList(visaorder1);
                    visaOrderList.setVisaList(VisaList.visa2visaList(visaDao.queryById(visaorder1.getId())));
                    lists.add(visaOrderList);
                }
                JSONObject jsonObject = JSONObject.fromObject(JsonData.buildSuccess(lists));
                jsonObject.put("total",String.valueOf(allnumb));
                return gson.fromJson(jsonObject.toString(),Object.class);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object showDetails(String orderid)
    {
        try {
            if(check.checkNull(orderid))
                throw new Exception("orderid错误");
            Visaorder visaorder = visaorderDao.queryById(orderid);
            VisaOrderDetailsImp visaOrderDetails = VisaOrderDetailsImp.visaOrder2visaOrderDetails(visaorder);
            try {
                Userid userid = useridDao.queryById(visaorder.getUserid());
                visaOrderDetails.setMobile(userid.getMobile());
                User user = userDao.queryById(userid.getMobile());
                visaOrderDetails.setUsername(user.getUsername());
            }catch (Exception e)
            {}
            visaOrderDetails.setVisaDetails(VisaDetails.visa2visadetails(visaDao.queryById(visaorder.getId())));
            return JsonData.buildSuccess(visaOrderDetails);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }
}
