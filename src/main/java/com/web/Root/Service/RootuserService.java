package com.web.Root.Service;

import com.web.Config.ConfigClass;
import com.web.Tools.BeanAndMap.BeanUtil;
import com.web.Tools.Redis.RedisUtil;
import com.web.Tools.staticData.Code;
import com.web.Tools.staticData.SMS;
import com.web.common.core.entity.JsonData;
import com.web.common.core.util.MapUtil;
import com.web.dao.Root.RootuserDao;
import com.web.dao.UserDao;
import com.web.entity.Root.Rootuser;
import com.web.entity.entityImp.Basic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class RootuserService {
    @Resource
    UserDao userDao;

    @Autowired
    ConfigClass configClass;
    @Resource
    RootuserDao rootuserDao;

    @Autowired
    RedisUtil redisUtil;
    private int pagesize =20;
    /**
     * 获取所有注册的客户端用户
     */
    public Object getalluser(Integer page)
    {
        try {
            if((page-1)*pagesize>userDao.querycount1())
                return JsonData.buildSuccess();
            else
                return  JsonData.buildSuccess(userDao.queryAllByLimit((page-1)*pagesize,pagesize));
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    /**
     * 获取所有管理员用户
     * @return
     */
    public Object getallrootuser()
    {
        try {
            return JsonData.buildSuccess(rootuserDao.queryAll(new Rootuser()));
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    /**
     * 管理员登录
     * @param rootuser
     * @return
     */
    public Map login(Rootuser rootuser)
    {
        Basic basic = new Basic();
        List<Rootuser> l = rootuserDao.queryAll(rootuser);//以登录名和密码作为条件查询
        if(l.size()!=0)//如果查询出东西了，便将
        {
            Rootuser rootuser1 = l.get(0);
            rootuser1.setToken(Integer.valueOf(SMS.vcode(6)));
            rootuserDao.update(rootuser1);
            basic.setCode(Code.SUCCESS);
            basic.setMessage(Code.information.get(Code.SUCCESS));
            basic.setData(new HashMap<String,Object>(){{
                put("name",rootuser1.getName());
                put("status",rootuser1.getStatus());
                put("token",rootuser1.getToken());
                put("username",rootuser1.getUsername());
            }});
            //缓存相关
            redisUtil.set(rootuser1.getUsername(),rootuser1.getToken(),configClass.getSms_phone_time());//用以校验是否重复登录 key:username  value:token
            redisUtil.set(String.valueOf(rootuser1.getToken()),rootuser1.getUsername()+"-"+rootuser1.getId()+"-"+rootuser1.getName()+"-"+rootuser1.getStatus(),configClass.getSms_phone_time());//用以校验登录状态是否过期 key:token  value:username+id+name+status
        }else {
          basic.SetFail();
        }
        Map map=null;
        try {
            map= BeanUtil.javabean2map(basic);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 管理员添加
     * @param rootuser
     * @return
     */
    public Object register(Rootuser rootuser)
    {
        try {
            if(rootuserDao.queryAll(rootuser).size()!=0)
            {
                return JsonData.buildErrorCode(Code.FAIL);
            }else {
                if(rootuserDao.insert(rootuser)==1)
                {
                    rootuser.setId(null);
                    rootuser.setUsername(null);
                    rootuser.setPassword(null);
                    Map map = MapUtil.Obj2Map(rootuser);
                    Map map1 =MapUtil.Obj2Map(JsonData.buildSuccess());
                    map1.putAll(map);
                    return map1;
                }else {
                    return JsonData.buildErrorCode(Code.SYSTEM_INTERNAL_ERROR);
                }
            }
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    /**
     * 根据登录token获取单个用户名称
     * @param token
     * @return
     */
    public Object getRoot(String token)
    {
        try {
            List<Rootuser> l = rootuserDao.queryAll(new Rootuser(){{this.setToken(Integer.valueOf(token));}});
            if(l.size()!=0)
            {
                Rootuser rootuser = l.get(0);
                rootuser.setId(null);
                rootuser.setPassword(null);
                Map map = MapUtil.Obj2Map(rootuser);
                Map map1 =MapUtil.Obj2Map(JsonData.buildSuccess());
                map1.putAll(map);
                return map1;
            }else {
                return JsonData.buildErrorCode(Code.FAIL);
            }
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    public Object delete(String username)
    {
        try {
            rootuserDao.deleteByUsername(username);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }


}
