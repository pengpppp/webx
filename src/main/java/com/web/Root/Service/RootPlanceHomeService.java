package com.web.Root.Service;

import com.google.gson.Gson;
import com.web.Tools.Check.check;
import com.web.common.core.entity.JsonData;
import com.web.dao.HomeorderDao;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class RootPlanceHomeService {
    @Resource
    HomeorderDao homeorderDao;


    private Integer pagesize=20;

    public Object getAllHomeOrder(String page)
    {
        try {
            Integer p ;
            Integer allnumb=homeorderDao.querycount1();
            if(check.checkNull(page))
                p=1;
            else
                p=Integer.valueOf(page);
            if((p-1)*pagesize>allnumb)
                return JsonData.buildSuccess();
            else {
                Gson gson  = new Gson();
                JSONObject jsonObject = JSONObject.fromObject(JsonData.buildSuccess(homeorderDao.queryAllByLimit(((p-1)*pagesize),pagesize)));
                jsonObject.put("total",allnumb);
                return gson.fromJson(jsonObject.toString(),Object.class);
            }
        }catch (Exception e)
        {
            return JsonData.buildError(e.toString());
        }
    }

    public Object deleteHomeOrder(String id)
    {
        try {
            homeorderDao.deleteById(id);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            return JsonData.buildError(e.toString());
        }
    }


}
