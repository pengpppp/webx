package com.web.Root.Service;

import com.web.Tools.Check.check;
import com.web.dao.MyNew.MynewDao;
import com.web.entity.MyNew.Mynew;
import com.web.entity.entityImp.Basic;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class RootNewService {
    @Resource
    MynewDao mynewDao;

    public Object addnew(Mynew mynew)
    {
        Basic basic = new Basic();
        mynew.setSrc("easyGo");
        //mynew.setKey(new Date().getTime());
        mynew.setTime(new Date());
        try {
            mynewDao.insert(mynew);
            basic.setData(mynewDao.querryid());
        }catch (Exception e) {
            e.printStackTrace();
        }
        return basic;
    }

    public Object edit(Mynew mynew)
    {
        mynew.setTime(new Date());
        mynewDao.update(mynew);
        return new Basic();
    }

    public Object delete(Integer id)
    {
        mynewDao.deleteById(id);
        return new Basic();
    }

    public Object getAll()
    {
        Basic basic = new Basic();
        basic.setData(mynewDao.queryAll(new Mynew()));
        return basic;
    }

    public Object setKey(String id,String key) {
        Mynew mynew = mynewDao.queryById(Integer.valueOf(id));
        try {
            if(check.checkNull(key))//关
            {
                mynew.setKey(null);
            }else {
                mynew.setKey(Long.valueOf(key));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mynewDao.update(mynew);
        return new Basic();
    }

    public Object setAllKey(String key)
    {
        try {
            if(check.checkNull(key))//关
            {
                List<Mynew> list = mynewDao.querryall();
                for (Mynew mynew:list)
                {
                    mynew.setKey(null);
                    mynewDao.update(mynew);
                }
            }else {
                List<Mynew> list = mynewDao.querryallClose();
                for (Mynew mynew:list)
                {
                    mynew.setKey(Long.valueOf(key));
                    mynewDao.update(mynew);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Basic();
    }
}
