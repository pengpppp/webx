package com.web.Root.Service;

import com.web.Tools.Check.check;
import com.web.common.core.entity.JsonData;
import com.web.common.core.util.JSONUtil;
import com.web.dao.CompanyDao;
import com.web.dao.ContentDao;
import com.web.entity.Company;
import com.web.entity.Content;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class RootCompanyService {

    @Resource
    CompanyDao companyDao;
    @Resource
    ContentDao contentDao;


    /**
     * 无ID添加AND有ID修改
     * @param content
     * @return
     */
    public Object Context(Content content) {
        try {
            Map map = new HashMap();
            if(check.checkNull(String.valueOf(content.getId())))
            {
                contentDao.insert(content);
                map =JSONUtil.toObj(JSONUtil.toString(JsonData.buildSuccess()),Map.class);
                map.put("id",contentDao.queryLast().getId());
            }else {
                contentDao.update(content);
                map =JSONUtil.toObj(JSONUtil.toString(JsonData.buildSuccess()),Map.class);
                map.put("title",content.getCompany());
            }
            return map;
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    /**
     * 有ID时修改
     * 无ID时添加
     * @param company
     * @return
     */
    public Object companyinsert(Company company)
    {
        Map map = null;
        try {
            if(check.checkNull(String.valueOf(company.getId())))
            {
                companyDao.insert(company);
                map =JSONUtil.toObj(JSONUtil.toString(JsonData.buildSuccess()),Map.class);
                map.put("id", companyDao.queryLast().getId());//返回ID
            }else {
                companyDao.update(company);
                map =JSONUtil.toObj(JSONUtil.toString(JsonData.buildSuccess()),Map.class);
            }
            return map;
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

}
