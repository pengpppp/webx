package com.web.Root.Service;

import com.google.gson.Gson;
import com.web.Config.VisaConfig;
import com.web.Tools.Check.check;
import com.web.Tools.staticData.SMS;
import com.web.common.core.entity.JsonData;
import com.web.dao.PicturepathDao;
import com.web.dao.VisaDao;
import com.web.entity.Picturepath;
import com.web.entity.Visa;
import com.web.entity.entityImp.Visa.VisaImp;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class RootVisaService {
    @Resource
    PicturepathDao picturepathDao;

    @Autowired
    VisaConfig visaConfig;

    @Resource
    VisaDao visaDao;
    private Integer pagesize=10;

    public Object upload(MultipartFile file)
    {
        String fileName = new Date().getTime() + SMS.vcode(4) + ".jpg";
        String filepath=visaConfig.getVisapath()+fileName; //拼凑文件存储位置

        Picturepath picturepath =new Picturepath();
        picturepath.setLocation(filepath);
        picturepath.setUrl(visaConfig.getVisaurl()+fileName);
        picturepathDao.insert(picturepath);

        File dest = new File(filepath);
        try {
            if(dest!=null)
                dest.createNewFile();
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return JsonData.buildSuccess(new HashMap<String,Object>(){{put("imageurl",picturepath.getUrl());}});
    }

    public Object add(VisaImp visaImp)
    {
        try {
            List<String> x = visaImp.getAcceptType();
            Visa visa =VisaImp.visaImp2visa(visaImp);
            String id = SMS.vcode(6);
            visa.setId(id);
            visa.setKey("T");
            if(checkVisa(visa))
                throw new Exception("参数错误");
            for (String s:x)
            {
                if(s.equals("1")&& check.checkNull(visaImp.getExpress()))
                    throw new Exception("缺邮寄地址");
            }
            if(!check.checkNull(visa.getTittle()))
            {
                visa.setTittle(visa.getTittle().replace(" ",""));
            }
            visaDao.insert(visa);
            return JsonData.buildSuccess(new HashMap<String,Object>(){{put("id",id);}});
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object delete(String id)
    {
        try {
            if(check.checkNull(id))
                throw new Exception("id不能为空");
            visaDao.deleteById(id);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object update(VisaImp visaImp)
    {
        try {
            Visa visa = VisaImp.visaImp2visa(visaImp);
            if(check.checkNull(visa.getId()))
                throw new Exception("id不能为空");
            if(visaImp.getKey().equals("N"))
            {
                visaDao.updateofftime(visaImp.getId(),String.valueOf(new Date().getTime()));
            }
            if(visaImp.getKey().equals("T"))
            {
                visaDao.updateofftime(visaImp.getId(),null);
            }
            visaDao.update(visa);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object getAll(String page)
    {
        Integer p ;
        Integer allnumb=visaDao.queryCount();
        try {
            if(check.checkNull(page))
                p=1;
            else
                p=Integer.valueOf(page);

            if((p-1)*pagesize>allnumb)
                return JsonData.buildSuccess();
            else
            {
                Gson gson  = new Gson();
                List<Visa> visas = visaDao.queryAllByLimit(((p-1)*pagesize),pagesize);
                List<VisaImp> visaImps = new ArrayList<>();
                for (Visa visa:visas)
                {
                    visaImps.add(VisaImp.visa2visaImp(visa));
                }
                JSONObject jsonObject = JSONObject.fromObject(JsonData.buildSuccess(visaImps));
                jsonObject.put("total",String.valueOf(allnumb));
                return gson.fromJson(jsonObject.toString(),Object.class);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object getOne(String id)
    {
        try {
            if(check.checkNull(id))
                throw new Exception("id不能为空");
            Visa visa= visaDao.queryById(id);
            System.out.println(visa.toString());
            return JsonData.buildSuccess(VisaImp.visa2visaImp(visa));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    private Boolean checkVisa(Visa visa)
    {
        try {
            if(check.checkNull(visa.getImageurl())||check.checkNull(visa.getName())||visa.getCost()==null||check.checkNull(visa.getRemark())||check.checkNull(visa.getEntrynumb())||check.checkNull(visa.getStayday())||check.checkNull(visa.getVisatype())||
                    check.checkNull(visa.getTerm())||check.checkNull(String.valueOf(visa.getDeparturedate()))||check.checkNull(visa.getHandingtime())||check.checkNull(visa.getAcceptType()))
            {
                return true;
            }else
                return false;
        }catch (Exception e)
        {
            e.printStackTrace();
            return true;
        }
    }

}
