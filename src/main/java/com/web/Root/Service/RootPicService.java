package com.web.Root.Service;

import com.web.Config.ConfigClass;
import com.web.Tools.staticData.Code;
import com.web.Tools.staticData.SMS;
import com.web.dao.MyNew.MynewpicDao;
import com.web.dao.PictureDao;
import com.web.dao.PicturepathDao;
import com.web.entity.Picture;
import com.web.entity.Picturepath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.*;


@Service
public class RootPicService {
    @Resource
    PictureDao pictureDao;
    @Resource
    PicturepathDao picturepathDao;
    @Autowired
    ConfigClass configClass;
    @Resource
    MynewpicDao mynewpicDao;

    /**
     * 上传
     * @param file
     * @param title
     * @return
     */
    public Object upload(MultipartFile file,String title)
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code","");
            put("message","");
            put("url","");
        }};
        String fileName = file.getOriginalFilename();
        String filepath =null;
        fileName = new Date().getTime() + SMS.vcode(4) + ".jpg";
        filepath=configClass.getPath() +fileName; //拼凑文件存储位置
        Picture picture = new Picture();
        picture.setPrcturepath(configClass.getIp()+configClass.getImageurl()+fileName);

        Picturepath picturepath =new Picturepath();
        picturepath.setLocation(filepath);
        picturepath.setUrl(picture.getPrcturepath());
        picturepathDao.insert(picturepath);

        map.put("url",picture.getPrcturepath());

        File dest = new File(filepath);
        try {
            if(dest!=null)
                dest.createNewFile();
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        map.put("code", Code.SUCCESS);
        map.put("message","成功");
        return map;
    }

    /**
     * 插入
     * @param fileurl
     * @param dess
     * @param type
     * @param h5
     * @return
     */
    public Map bindimage(String fileurl, String dess, String type, String h5)
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code",Code.FAIL);
            put("message",Code.information.get(Code.FAIL));
        }};
        Picture picture =new Picture();
        picture.setPrcturepath(fileurl);
        picture.setDescs(dess);
        picture.setType(Integer.valueOf(type));
        picture.setUrl(h5);
        pictureDao.insert(picture);
        map.put("code",Code.SUCCESS);
        map.put("message",Code.information.get(Code.SUCCESS));
        return map;
    }

    /**
     * 修改
     * @param id
     * @param fileurl
     * @param dess
     * @param type
     * @param h5
     * @return
     */
    public Map reimage(String id ,String fileurl,String dess,String type ,String h5)
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code",Code.FAIL);
            put("message",Code.information.get(Code.FAIL));
            put("url","");
        }};
        Picture picture = pictureDao.queryById(Integer.valueOf(id));

            if(fileurl!=null) {
                picture.setPrcturepath(fileurl);

                //在路径表中进行更改
                Picturepath picturepath =picturepathDao.queryById(picture.getPrcturepath());
                picturepath.setUrl(fileurl);
                picturepathDao.update(picturepath);

            }
            if(dess!=null)
                picture.setDescs(dess);
            if(type!=null)
                picture.setType(Integer.valueOf(type));
            if (h5!=null)
                picture.setUrl(h5);
            pictureDao.update(picture);
            map.put("code",Code.SUCCESS);
            map.put("message",Code.information.get(Code.SUCCESS));
            map.put("url",picture.getUrl());
        return map;
    }

    /**
     * 此为删除
     * @return
     */
    public Map deleteOne(String id)
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code",Code.FAIL);
            put("message",Code.information.get(Code.FAIL));
        }};

        Picture p = pictureDao.queryById(Integer.valueOf(id));
        pictureDao.deleteById(Integer.valueOf(id));

        //System.out.println("要删除的url路径："+p.getPrcturepath());
        //在路径表中删除

        File file = new File(picturepathDao.queryById(p.getPrcturepath()).getLocation());
        file.delete();
        picturepathDao.deleteById(p.getPrcturepath());
        map.put("code",Code.SUCCESS);
        map.put("message",Code.information.get(Code.SUCCESS));
        return map;
    }

    /**
     * 获取所有
     * @return
     * @param pageSize
     * @param page
     */
    public Map show(String pageSize, String page)
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code",Code.FAIL);
            put("message",Code.information.get(Code.FAIL));
            put("data",new ArrayList<Picture>());
            put("total",0);
        }};
        int allnub= pictureDao.querycount1();
        if(pageSize==null||"null".equals(pageSize)||"".equals(pageSize))
            pageSize="10";
        if((Integer.valueOf(page)-1)*Integer.valueOf(pageSize)>allnub)
        {
        }else {
            map.put("code", Code.SUCCESS);
            map.put("message", Code.information.get(Code.SUCCESS));
            map.put("total", allnub);
            map.put("data", pictureDao.queryAllByLimit((Integer.valueOf(page) - 1) * Integer.valueOf(pageSize), configClass.getPagenumb()));
        }
        return map;
    }

}
