package com.web.Root.Service;

import com.web.common.core.entity.JsonData;
import com.web.dao.MyNew.CategoryDao;
import com.web.entity.MyNew.Category;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class RootCategoryService {
    @Resource
    CategoryDao categoryDao;

    public Object get()
    {
        try {
            return JsonData.buildSuccess(categoryDao.queryAll(new Category()));
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    public Object insert(Category category)
    {
        try {
            categoryDao.insert(category);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    public Object delete(int id)
    {
        try {
            categoryDao.deleteById(id);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    public Object update(Category category)
    {
        try {
            categoryDao.update(category);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

}
