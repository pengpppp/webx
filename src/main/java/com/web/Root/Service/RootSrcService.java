package com.web.Root.Service;

import com.web.dao.MyNew.SrcDao;
import com.web.entity.MyNew.Src;
import com.web.entity.entityImp.Basic;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class RootSrcService {
    @Resource
    SrcDao srcDao;
    public Object get()
    {
        Basic basic = new Basic();
        basic.setData(srcDao.queryAll(new Src()));
        return basic;
    }

    public Object insert(Src src)
    {
        srcDao.insert(src);
        Basic basic = new Basic();
        return basic;
    }

    public Object update(Src src)
    {
        srcDao.update(src);
        return new Basic();
    }

    public Object delete(int id)
    {
        srcDao.deleteById(id);
        return new Basic();
    }

}
