package com.web.Root.Service;

import com.web.Tools.Check.check;
import com.web.dao.SmsLogDao;
import com.web.entity.entityImp.Basic;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class RootSmsLogService {
    @Resource
    SmsLogDao smsLogDao;

    private Integer pageSize = 10 ;

    public Object GetAllSms(String p)
    {
        Basic basic = new Basic();
        if(check.checkNull(p))
        {
            basic.SetFail();
        }else {
            Integer page = Integer.valueOf(p);
            basic.setData(smsLogDao.queryAllByLimit((page - 1) * pageSize, pageSize));
        }
        return basic;
    }

    public Object delete(Integer id)
    {
        smsLogDao.deleteById(id);
        return new Basic();
    }
}
