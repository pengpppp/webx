package com.web.Root.Service;

import com.web.Config.ConfigClass;
import com.web.Tools.staticData.SMS;
import com.web.dao.MyNew.MynewpicDao;
import com.web.dao.PicturepathDao;
import com.web.entity.MyNew.Mynewpic;
import com.web.entity.Picturepath;
import com.web.entity.entityImp.Basic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.Date;

@Service
public class RootMyNewPicService {
    @Resource
    MynewpicDao mynewpicDao;
    @Resource
    PicturepathDao picturepathDao;

    @Autowired
    ConfigClass configClass;


    public Object upload(MultipartFile file, String title)
    {
        Basic basic = new Basic();
        String fileName ;
        String filepath =null;
        fileName = new Date().getTime() + SMS.vcode(4) + ".jpg";
        filepath=configClass.getPath() +fileName; //拼凑文件存储位置
        Mynewpic mynewpic = new Mynewpic();
        mynewpic.setUrl(configClass.getIp()+configClass.getImageurl()+fileName);
        mynewpic.setTitle(title);
        mynewpicDao.insert(mynewpic);

        Picturepath picturepath =new Picturepath();
        picturepath.setLocation(filepath);
        picturepath.setUrl(mynewpic.getUrl());
        picturepathDao.insert(picturepath);

        mynewpic.setId(mynewpicDao.querryid());
        basic.setData(mynewpic);
        File dest = new File(filepath);
        try {
            if(dest!=null)
                dest.createNewFile();
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return basic;

    }



    public Object getAll()
    {
        Basic basic = new Basic();
        basic.setData(mynewpicDao.queryAll(new Mynewpic()));
        return basic;
    }
    public Object insert(Mynewpic mynewpic)
    {
        mynewpicDao.insert(mynewpic);
        return new Basic();
    }
    public Object update(Mynewpic mynewpic)
    {
        mynewpicDao.update(mynewpic);
        return new Basic();
    }
    public Object delete(int id)
    {
        Mynewpic mynewpic = mynewpicDao.queryById(id);
        File file = new File(picturepathDao.queryById(mynewpic.getUrl()).getLocation());
        file.delete();
        picturepathDao.deleteById(mynewpic.getUrl());
        mynewpicDao.deleteById(id);
        return new Basic();
    }
}
