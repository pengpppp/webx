package com.web.Root.Service;

import com.web.Tools.staticData.Code;
import com.web.dao.Log.RootLogDao;
import com.web.entity.Log.RootLog;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
public class RootLogService {

    @Resource
    RootLogDao rootLogDao;

    public Map showAll()
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code", Code.SUCCESS);
            put("message",Code.information.get(Code.SUCCESS));
            put("total",0);
            put("data",new ArrayList<RootLog>());
        }};

        return map;
    }
}
