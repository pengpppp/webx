package com.web.Root.Service;

import com.google.gson.Gson;
import com.web.Tools.Check.check;
import com.web.common.core.entity.JsonData;
import com.web.dao.InsuranceDao;
import com.web.entity.Insurance;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class RootInsuranceService {
    @Resource
    InsuranceDao insuranceDao;
    private Integer pagesize=10;
    public Object add(Insurance insurance)throws Exception
    {

        insurance.setIsmy("T");
        insurance.setKey("T");
        if(checkInsurance(insurance))
            throw new Exception("参数错误");
        return insuranceDao.insert(insurance);
    }

    public Object delete(String id)
    {
        try {
            if(check.checkNull(id))
                throw new Exception("id为空");
            insuranceDao.deleteById(id);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object update(Insurance insurance)
    {
        try {
            if(check.checkNull(insurance.getId()))
                throw new Exception("id为空");
            insuranceDao.update(insurance);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object getAll(String page)
    {
        Integer p ;
        Integer allnumb=insuranceDao.queryCount();
        try {
            if(check.checkNull(page))
                p=1;
            else
                p=Integer.valueOf(page);

            if((p-1)*pagesize>allnumb)
                return JsonData.buildSuccess();
            else
            {
                Gson gson  = new Gson();
                List<Insurance> insurances = insuranceDao.queryMyAllByLimit(((p-1)*pagesize),pagesize);
                JSONObject jsonObject = JSONObject.fromObject(JsonData.buildSuccess(insurances));
                jsonObject.put("total",String.valueOf(allnumb));
                return gson.fromJson(jsonObject.toString(),Object.class);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    private boolean checkInsurance(Insurance insurance)
    {
        try {
            if(insurance.getType()==null||insurance.getCost()==null||check.checkNull(insurance.getName())||check.checkNull(insurance.getTypename())||
                    check.checkNull(insurance.getRemark()))
            {
                return true;
            }else {
                return false;
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            return true;
        }

    }




























}
