package com.web.Root.Service;

import com.google.gson.Gson;
import com.web.Tools.Check.check;
import com.web.Tools.staticData.SMS;
import com.web.common.core.entity.JsonData;
import com.web.dao.VisainsuranceDao;
import com.web.entity.Visainsurance;
import com.web.entity.entityImp.Visa.VisaInsuranceImp;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class RootVisaInsuranceService {
    @Resource
    VisainsuranceDao visainsuranceDao;

    private Integer pagesize=10;
    public Object add(Visainsurance visainsurance)
    {
        try {
            visainsurance.setId(SMS.vcode(6));
            visainsurance.setKey("T");
            if(checkVisaInsurance(visainsurance))
                throw new Exception("参数错误");
            visainsuranceDao.insert(visainsurance);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object delete(String id)
    {
        try {
            if(check.checkNull(id))
                throw new Exception("id为空");
            visainsuranceDao.deleteById(id);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object update(Visainsurance visainsurance)
    {
        try {
            if(check.checkNull(visainsurance.getId()))
                throw new Exception("id为空");
            visainsuranceDao.update(visainsurance);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object getAll(String page)
    {
        Integer p ;
        Integer allnumb=visainsuranceDao.queryCount();
        try {

            Gson gson  = new Gson();
            Visainsurance visainsurance = new Visainsurance();
            visainsurance.setKey("T");
            List<Visainsurance> visainsurances = visainsuranceDao.queryAll(visainsurance);
            List<VisaInsuranceImp> visaInsuranceImps = new ArrayList<>();
            for (Visainsurance visainsurance1 :visainsurances)
            {
                visaInsuranceImps.add(VisaInsuranceImp.VisaInsurance2VisaInsuranceImp(visainsurance1));
            }
            JSONObject jsonObject = JSONObject.fromObject(JsonData.buildSuccess(visaInsuranceImps));
            jsonObject.put("total",String.valueOf(allnumb));
            return gson.fromJson(jsonObject.toString(),Object.class);

        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    private Boolean checkVisaInsurance(Visainsurance visainsurance)
    {
        try {
            if(check.checkNull(visainsurance.getName())||visainsurance.getCost()==null)
                return true;
            else
                return false;
        }catch (Exception e)
        {
            e.printStackTrace();
            return true;
        }

    }
}
