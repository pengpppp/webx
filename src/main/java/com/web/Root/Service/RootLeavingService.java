package com.web.Root.Service;

import com.web.Tools.staticData.Code;
import com.web.dao.LeavingwordDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class RootLeavingService {

    @Resource
    LeavingwordDao leavingwordDao;

    public Map showAll(String pageSize,String page)
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code", Code.SUCCESS);
            put("message",Code.information.get(Code.SUCCESS));
            put("total",0);
        }};
        int allnub= leavingwordDao.querycount1();
        map.put("total", allnub);
        if(pageSize==null||"null".equals(pageSize)||"".equals(pageSize))
            pageSize="10";
        if((Integer.valueOf(page)-1)*Integer.valueOf(pageSize)>allnub)
        {
        }else {
            map.put("data", leavingwordDao.queryAllByLimit((Integer.valueOf(page) - 1) * Integer.valueOf(pageSize), Integer.parseInt(pageSize)));
        }
        return map;
    }

    public Map delete(String id)
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code", Code.SUCCESS);
            put("message",Code.information.get(Code.SUCCESS));
        }};
        leavingwordDao.deleteById(Integer.valueOf(id));
        return map;
    }
}
