package com.web.Root;


import com.web.Root.Service.RootuserService;
import com.web.entity.Root.Rootuser;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 面向管理员  用户管理的控制层
 */
//@CrossOrigin
@RestController
public class RootuserController {
    @Autowired
    RootuserService rootuserService;



    @RequestMapping("/rootlogin")
    public Map login(@RequestBody String string)
    {
        /**
         * 返回用户名加后台权限加登录状态
         */
        System.out.println("登录");
        JSONObject object = JSONObject.fromObject(string);
        Rootuser rootuser = new Rootuser();
        rootuser.setUsername(object.getString("username"));
        rootuser.setPassword(object.getString("password"));
        return rootuserService.login(rootuser);
    }
    @RequestMapping("/getRootUser")
    public Object getRootUser(HttpServletRequest request)
    {
        return rootuserService.getRoot(request.getHeader("TN"));
    }


}
