package com.web.Root.Controller;

import com.web.Config.Aspect.annotion.Status;
import com.web.Root.Service.RootSmsLogService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 短信记录
 */
@RestController
public class RootSmsLogController {

    @Autowired
    RootSmsLogService rootSmsLogService;

    @Status
    @RequestMapping("/getAllSmsLog")
    public Object GetAllSms(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        return rootSmsLogService.GetAllSms(String.valueOf(object.get("page")));
    }

    @Status
    @RequestMapping("/deleteSmsLog")
    public Object delete(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        return rootSmsLogService.delete(Integer.valueOf(String.valueOf(object.get("id"))));
    }
}
