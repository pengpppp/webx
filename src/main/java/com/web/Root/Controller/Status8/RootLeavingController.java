package com.web.Root.Controller.Status8;

import com.fasterxml.jackson.databind.JsonNode;
import com.web.Root.Service.RootLeavingService;
import com.web.common.core.util.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * 留言
 */
@RestController
public class RootLeavingController {

    @Autowired
    RootLeavingService rootLeavingService;

    @RequestMapping("/showLeaving")
    public Map showLeaving(@RequestBody String string)
    {
        JsonNode jsonObject = JSONUtil.parseJson(string);
       // JSONObject object = JSONObject.fromObject(string);
        return rootLeavingService.showAll(String.valueOf(jsonObject.get("pageSize")), String.valueOf(jsonObject.get("page")));
    }

    @RequestMapping("/deleteLeaving")
    public Map delete(@RequestBody String string)
    {
        JsonNode jsonObject =JSONUtil.parseJson(string);
        //JSONObject object = JSONObject.fromObject(string);
        return rootLeavingService.delete(String.valueOf(jsonObject.get("id")));
    }


}
