package com.web.Root.Controller.Status8;

import com.web.Root.Service.RootCategoryService;
import com.web.entity.MyNew.Category;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 分类
 */
@RestController
public class RootCategoryController {
    @Autowired
    RootCategoryService rootCategoryService;

    @RequestMapping("/getAllCategory")
    public Object getAll()
    {
        return rootCategoryService.get();
    }

    @RequestMapping("/addCategory")
    public Object add(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        object.remove("cmd");
        return rootCategoryService.insert((Category) JSONObject.toBean(object,Category.class));
    }

    @RequestMapping("/editCategory")
    public Object edit(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        object.remove("cmd");
        return rootCategoryService.update((Category) JSONObject.toBean(object,Category.class));
    }

    @RequestMapping("/deleteCategory")
    public Object delete(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        return rootCategoryService.delete(Integer.valueOf(String.valueOf(object.get("id"))));
    }

}
