package com.web.Root.Controller.Status8;

import com.web.Root.Service.RootChannelService;
import com.web.entity.MyNew.Channel;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 频道
 */
@RestController
public class RootChannelController {
    @Autowired
    RootChannelService rootChannelService;

    @RequestMapping("/getAllChannel")
    public Object getall()
    {
        return rootChannelService.get();
    }

    /**
     * id(自定义） channel(String)
     * @return
     */
    @RequestMapping("/addChannel")
    public Object insert(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        object.remove("cmd");
        return rootChannelService.insert((Channel) JSONObject.toBean(object,Channel.class));
    }

    /**
     * id
     * @return
     */
    @RequestMapping("/deleteChannel")
    public Object delete(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        return rootChannelService.delete(Integer.parseInt(String.valueOf(object.get("id"))));
    }

    /**
     * id  channel
     * @return
     */
    @RequestMapping("/editChannel")
    public Object update(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        object.remove("cmd");
        return rootChannelService.update((Channel) JSONObject.toBean(object,Channel.class));
    }

}
