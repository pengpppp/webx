package com.web.Root.Controller.Status8;

import com.web.Root.Service.RootNewService;
import com.web.Tools.Check.check;
import com.web.entity.MyNew.Mynew;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 自定义新闻
 */
@RestController
public class RootNewController {

    @Autowired
    RootNewService rootNewService;

    /**
     * title time src category pic content url
     * @param string
     * @return
     */
    @RequestMapping("/addMyNew")
    public Object addNew(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        object.remove("cmd");
        Mynew mynew = (Mynew) JSONObject.toBean(object,Mynew.class);
        try {
            if(!check.checkNull(String.valueOf(object.get("url"))))
            {
                mynew.setWeburl(mynew.getUrl());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rootNewService.addnew(mynew);
    }

    @RequestMapping("/editMyNew")
    public Object editNew(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        object.remove("cmd");
        Mynew mynew = (Mynew) JSONObject.toBean(object,Mynew.class);
        return rootNewService.edit(mynew);
    }

    @RequestMapping("/deleteMyNew")
    public Object delete(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);

        return rootNewService.delete(Integer.valueOf(String.valueOf(object.get("id"))));
    }

    @RequestMapping("/getAllMyNew")
    public Object GetAll(@RequestBody String string)
    {
        return rootNewService.getAll();
    }

    /**
     * key id
     * key为空时关，不为空时开
     * @return
     */
    @RequestMapping("/setMyNewKey")
    public Object SetKey(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        return rootNewService.setKey(String.valueOf(object.get("id")), String.valueOf(object.get("key")));
    }

    /**
     * key
     * key为空时关，不为空时开
     * @return
     */
    @RequestMapping("/setAllMyNewKey")
    public Object SetAllKey(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        return rootNewService.setAllKey(String.valueOf(object.get("key")));
    }

}
