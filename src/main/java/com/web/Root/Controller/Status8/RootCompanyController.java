package com.web.Root.Controller.Status8;

import com.google.gson.Gson;
import com.web.Root.Service.RootCompanyService;
import com.web.common.core.util.JSONUtil;
import com.web.entity.Company;
import com.web.entity.Content;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 公司信息
 */
@RestController
public class RootCompanyController {

    @Autowired
    RootCompanyService rootCompanyService;
    @Autowired
    Gson gson;

    /**
     * 添加and编辑
     * 有id时编辑，无id时增加
     * @param string
     * @return
     */
    @RequestMapping("/content")
    public Object content(@RequestBody String string)
    {
        //JSONObject object = JSONObject.fromObject(string);
        Content content = JSONUtil.toObj(string, Content.class);
        return rootCompanyService.Context(content);
       // return rootCompanyService.Context(String.valueOf(object.get("id")), String.valueOf(object.get("title")), object.get("content"), String.valueOf(object.get("type")));
    }

    /**
     * 添加and编辑
     * 有id时编辑，无id时增加
     * @param string
     * @return
     */
    @RequestMapping("/companyInformation")
    public Object companyChange(@RequestBody String string)
    {
        Company company = JSONUtil.toObj(string, Company.class);
        return rootCompanyService.companyinsert(company);
//        return rootCompanyService.companyinsert(String.valueOf(object.get("id")), String.valueOf(object.get("officialPhone")), String.valueOf(object.get("hotLine")),
//                String.valueOf(object.get("wechatNumb")), String.valueOf(object.get("Email")), String.valueOf(object.get("address")));
    }
}
