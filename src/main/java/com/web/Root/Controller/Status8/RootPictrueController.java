package com.web.Root.Controller.Status8;

import com.web.Config.ConfigClass;
import com.web.Root.RootuserController;
import com.web.Service.PicService;

import com.web.common.core.entity.JsonData;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * 面向管理员 图片的上传与绑定的控制层
 */
@RestController
public class RootPictrueController {
    @Autowired
    ConfigClass configClass;
    @Autowired
    PicService picService;
    @Autowired
    com.web.Root.Service.RootPicService RootPicService;
    @Autowired
    com.web.Service.userService userService;
    private static final Logger LOGGER = LoggerFactory.getLogger(RootuserController.class);

    @RequestMapping("/upload")
    public Object upload(@RequestParam(value = "file") MultipartFile file,
                         @RequestParam(value = "title",required = false)String title) {
        if(file==null)
        {
            return JsonData.buildError("file为空");
        }
        System.out.println("文件上传");
        return RootPicService.upload(file,title);
    }

    /**
     * 此为插入
     * @param s
     * @return
     */
    @RequestMapping("/bindfile")
    public Map bindimage(@RequestBody String s)
    {
        JSONObject object = JSONObject.fromObject(s);
        return RootPicService.bindimage(object.getString("fileUrl"),object.getString("dss"),object.getString("type"),object.getString("h5Url"));
    }

    /**
     * 此为更改
     * @param s
     * @return
     */
    @RequestMapping("/editImage")
    public Map Reimage(@RequestBody String s)
    {
        JSONObject object = JSONObject.fromObject(s);
        return RootPicService.reimage(object.getString("id"),object.getString("fileUrl"),object.getString("dss"),object.getString("type"),object.getString("h5Url"));
    }


    /**
     * 此为删除
     * @param s
     * @return
     */
    @RequestMapping("/deleteImage")
    public Map Delete(@RequestBody String s)
    {
        JSONObject object = JSONObject.fromObject(s);
        return RootPicService.deleteOne(object.getString("id"));
    }

    /**
     * 此为后台获取图片
     * @param s
     * @return
     */
    @PostMapping("/getHomeList")
    public Map getall(@RequestBody String s)
    {
        JSONObject object = JSONObject.fromObject(s);
        if(object.get("pageSize")==null)
            return RootPicService.show("10",object.getString("page"));
        else
            return RootPicService.show(String.valueOf(object.get("pageSize")),object.getString("page"));
    }


    //    @PostMapping("/binding")
//    public String binding(@RequestParam(value = "imageurl",required = true)String image,@RequestParam(value = "h5url",required = true) String h5,Model model)
//    {
//
//        model.addAllAttributes(picService.binding(image,h5));
//        //model.asMap().put("code", Code.information.get(Code.SUCCESS));
//        return "upload";
//    }
}
