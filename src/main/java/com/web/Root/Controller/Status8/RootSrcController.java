package com.web.Root.Controller.Status8;

import com.web.Root.Service.RootSrcService;
import com.web.entity.MyNew.Src;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 来源
 */
@RestController
public class RootSrcController {
    @Autowired
    RootSrcService rootSrcService;

    @RequestMapping("/getAllSrc")
    public Object getAll()
    {
        return rootSrcService.get();
    }

    /**
     * id
     * @param string
     * @return
     */
    @RequestMapping("/deleteSrc")
    public Object delete(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        return rootSrcService.delete(Integer.valueOf(String.valueOf(object.get("id"))));
    }

    /**
     * id src
     * @param string
     * @return
     */
    @RequestMapping("/editSrc")
    public Object edit(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        object.remove("cmd");
        return rootSrcService.update((Src) JSONObject.toBean(object,Src.class));
    }

    /**
     * id src
     * @param string
     * @return
     */
    @RequestMapping("/addSrc")
    public Object insert(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        object.remove("cmd");
        return rootSrcService.insert((Src) JSONObject.toBean(object,Src.class));
    }
}
