package com.web.Root.Controller.Status8;

import com.web.Root.Service.RootMyNewPicService;
import com.web.entity.MyNew.Mynewpic;
import com.web.entity.entityImp.Basic;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 新闻图片
 */
@RestController
public class RootMyNewPicController {
    @Autowired
    RootMyNewPicService rootMyNewPicService;

    @RequestMapping("/uploadMyNewPic")
    public Object upload(@RequestParam(value = "file") MultipartFile file,
                         @RequestParam(value = "title")String title)
    {
        if (file==null||title==null)
        {
            Basic basic = new Basic();
            basic.SetFail();
            return basic;
        }else {
            return rootMyNewPicService.upload(file,title);
        }
    }

    @RequestMapping("/getAllMyNewPic")
    public Object getAll()
    {
        return rootMyNewPicService.getAll();
    }

    @RequestMapping("/addMyNewPic")
    public Object add(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        object.remove("cmd");
        return rootMyNewPicService.insert((Mynewpic) JSONObject.toBean(object,Mynewpic.class));
    }

    @RequestMapping("/editMyNewPic")
    public Object edit(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        object.remove("cmd");
        return rootMyNewPicService.update((Mynewpic) JSONObject.toBean(object,Mynewpic.class));
    }

    @RequestMapping("/deleteMyNewPic")
    public Object delete(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        return rootMyNewPicService.delete(Integer.valueOf(String.valueOf(object.get("id"))));
    }
}
