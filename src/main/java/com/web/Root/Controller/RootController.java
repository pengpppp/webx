package com.web.Root.Controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.web.Config.Aspect.annotion.Status;
import com.web.Root.Service.RootuserService;
import com.web.common.core.util.JSONUtil;
import com.web.entity.Root.Rootuser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class RootController {
    @Autowired
    RootuserService rootuserService;
    /**
     * 请求参数：
     * @return
     */
    @Status
    @RequestMapping("/getAllUser")
    public Object GetAllUser(@RequestBody String string)
    {
        JsonNode jsonObject =JSONUtil.parseJson(string);
        return rootuserService.getalluser(Integer.valueOf(String.valueOf(jsonObject.get("page"))));
    }
    @Status
    @RequestMapping("/rootRegister")
    public Object register(@RequestBody String string)
    {
        Rootuser rootuser = JSONUtil.toObj(string,Rootuser.class);
        rootuser.setCreatetime(new Date());
        return rootuserService.register(rootuser);
    }

    @Status
    @RequestMapping("/getAllRootUser")
    public Object GetAllRoot(@RequestBody String string)
    {
        JsonNode jsonObject =JSONUtil.parseJson(string);
        return rootuserService.getallrootuser();
    }

    @Status
    @RequestMapping("/deleteRootUser")
    public Object delete(@RequestBody String string)
    {
        JsonNode jsonObject =JSONUtil.parseJson(string);
        return rootuserService.delete(String.valueOf(jsonObject.get("username")));
    }



}
