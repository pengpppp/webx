package com.web.Root.Controller.Status7;

import com.web.Controller.Plance.PlanceBaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/flight")
@RestController
public class RootPlanceBaseController {

    @Autowired
    PlanceBaseController planceBaseController;

    /**
     * 城市数据  参数为空  30001
     * @return
     */
    @RequestMapping("/flightCityList")
    public Object flightcitylist(@RequestBody String string)
    {
        return planceBaseController.flightcitylist(string);
    }

    /**
     *航空公司数据  参数为空  30002
     * @param string
     * @return
     */
    @RequestMapping("/getAirCompanyList")
    public Object getaircompanylist(@RequestBody String string)
    {
        return planceBaseController.getaircompanylist(string);
    }

    /**
     * 机场数据 参数为空 30003
     * @param string
     * @return
     */
    @RequestMapping("/getAirportsList")
    public Object getairportslist(@RequestBody String string)
    {
        return planceBaseController.getairportslist(string);
    }

}
