package com.web.Root.Controller.Status7;

import com.web.Controller.Plance.PassengerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/flight")
@RestController
public class RootPassengerController {
    @Autowired
    PassengerController passengerController;
    /**
     * 添加乘客  30030
     * @param string
     * @param request
     * @return
     */
    @RequestMapping("/addPassenger")
    public Object addPassenger(@RequestBody String string, HttpServletRequest request) {
        return passengerController.addPassenger(string,request);
    }

    /**
     * 展示该用户的所有乘客信息  30031
     * @param string
     * @param request
     * @return
     */
    @RequestMapping("/showUserAllPassenger")
    public Object getUserAllPassenger(@RequestBody String string, HttpServletRequest request)
    {
        return passengerController.getUserAllPassenger(string,request);
    }

    /**
     * 删除乘客信息  30032
     * @param string
     * @return
     */
    @RequestMapping("/deletePassenger")
    public Object delete(@RequestBody String string)
    {
       return passengerController.delete(string);
    }

    /**
     * 修改乘客信息  30033
     * @param string
     * @return
     */
    @RequestMapping("/editPassenger")
    public Object edit(@RequestBody String string)
    {
        return passengerController.edit(string);
    }

    /**
     * 查询单个  30034
     * @param string
     * @return
     */
    @RequestMapping("/getPassemger")
    public Object get(@RequestBody String string)
    {
        return passengerController.get(string);
    }
}
