package com.web.Root.Controller.Status7;

import com.web.Root.Service.RootVisaOrderService;
import com.web.common.core.entity.JsonData;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/visa")
@RestController
public class RootVisaOrderController {
    @Autowired
    RootVisaOrderService rootVisaOrderService;

    @RequestMapping("/getAllVisaOrder")
    public Object getall(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return rootVisaOrderService.getAll(String.valueOf(jsonObject.get("page")));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    @RequestMapping("/getVisaOrder")
    public Object getOne(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return rootVisaOrderService.showDetails(String.valueOf(jsonObject.get("orderid")));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }


}
