package com.web.Root.Controller.Status7;

import com.web.Root.Service.RootInsuranceService;
import com.web.Tools.staticData.SMS;
import com.web.common.core.entity.JsonData;
import com.web.common.core.util.JSONUtil;
import com.web.entity.Insurance;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RequestMapping("/flight")
@RestController
public class RootInsuranceController {

    @Autowired
    RootInsuranceService rootInsuranceService;

    @RequestMapping("/addInsurance")
    public Object add(@RequestBody String string)
    {
        try {
            Insurance insurance = JSONUtil.toObj(string,Insurance.class);
            String ID = SMS.vcode(10);
            insurance.setId(ID);
            rootInsuranceService.add(insurance);
            return JsonData.buildSuccess(new HashMap<String,Object>(){{put("id",ID);}});
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    @RequestMapping("/deleteInsurance")
    public Object delete(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return rootInsuranceService.delete(jsonObject.getString("id"));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 编辑  兼  关闭保险
     * @param string
     * @return
     */
    @RequestMapping("/editInsurance")
    public Object update(@RequestBody String string)
    {
        try {
            Insurance insurance = JSONUtil.toObj(string,Insurance.class);
            return rootInsuranceService.update(insurance);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 获取所有自定义保险   page分页，每页10个
     * @param string
     * @return
     */
    @RequestMapping("/getAllInsurance")
    public Object getAll(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return rootInsuranceService.getAll(String.valueOf(jsonObject.get("page")));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

}
