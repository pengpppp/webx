package com.web.Root.Controller.Status7;

import com.web.Controller.Plance.PlanceHomeController;
import com.web.Root.Service.RootPlanceHomeService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RequestMapping("/flight")
@RestController
public class RootPlanceHomeController {

    @Autowired
    PlanceHomeController planceHomeController;

    @Autowired
    RootPlanceHomeService rootPlanceHomeService;


    /**
     * 获取所有的国内机票订单   page分页
     * @param string
     * @return
     */
    @RequestMapping("/getAllHomeOrder")
    public Object getAllHomeOrder(@RequestBody String string)
    {
        JSONObject jsonObject =JSONObject.fromObject(string);
        return rootPlanceHomeService.getAllHomeOrder(String.valueOf(jsonObject.get("page")));
    }

    /**
     * 彻底删除国内机票订单
     * id
     * @param string
     * @return
     */
    @RequestMapping("/deleteHomeOrder")
    public Object deleteHomeOrder(@RequestBody String string)
    {
        JSONObject jsonObject =JSONObject.fromObject(string);
        return rootPlanceHomeService.deleteHomeOrder(String.valueOf(jsonObject.get("id")));
    }

    /**
     * 获取保险列表
     * @param string
     * @return
     */
    @RequestMapping("/getInsuranceList")
    public Object getInsuranceList(@RequestBody String string)
    {
        return planceHomeController.getInsuranceList(string);
    }

    /**
     * 获取航班详情
     * @param string
     * @return
     */
    @RequestMapping("/getFlightList")
    public Object getflightlist(@RequestBody String string)
    {
        return planceHomeController.getflightlist(string);
    }

    @RequestMapping("/getFlightDetails")
    public Object getflightdetails(@RequestBody String string)
    {
        return planceHomeController.getflightdetails(string);
    }

    @RequestMapping("/getRefundRule")
    public Object getrefundrule(@RequestBody String string)
    {
        return planceHomeController.getrefundrule(string);
    }

    /**
     * 预定订单
     * @param string
     * @param request
     * @return
     */
    @RequestMapping("/saveOrder")
    public Object saveOderTest(@RequestBody String string, HttpServletRequest request)
    {
        return planceHomeController.saveOderTest(string,request);
    }

    @RequestMapping("/getOrder")
    public Object showOrder(@RequestBody String string)
    {
        return planceHomeController.showOrder(string);
    }

    @RequestMapping("/getMyOrder")
    public Object showAllOrder(@RequestBody String string,HttpServletRequest request)
    {
       return planceHomeController.showAllOrder(string,request);
    }

    /**
     * 取消订单   30042
     */
    @RequestMapping("/concelOrder")
    public Object deleteOrder(@RequestBody String string)
    {
        return planceHomeController.deleteOrder(string);
    }

    @RequestMapping("/createOrder")
    public Object createOrder(@RequestBody String string)
    {
        return planceHomeController.createorder(string);
    }

}
