package com.web.Root.Controller.Status7;

import com.web.Root.Service.RootVisaInsuranceService;
import com.web.common.core.entity.JsonData;
import com.web.common.core.util.JSONUtil;
import com.web.entity.Visainsurance;
import com.web.entity.entityImp.Visa.VisaInsuranceImp;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/visa")
@RestController
public class RootVisaInsuranceController {

    @Autowired
    RootVisaInsuranceService rootVisaInsuranceService;

    @RequestMapping("/addVisaInsurance")
    public Object add(@RequestBody String string)
    {
        try {
            VisaInsuranceImp visaInsuranceImp = JSONUtil.toObj(string,VisaInsuranceImp.class);
            Visainsurance visainsurance = VisaInsuranceImp.VisaInsuranceImp2VisaInsuranceInsert(visaInsuranceImp);
            return rootVisaInsuranceService.add(visainsurance);
        }catch (Exception e){
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    @RequestMapping("/deleteVisaInsurance")
    public Object delete(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return rootVisaInsuranceService.delete(jsonObject.getString("id"));
        }catch (Exception e){
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 编辑  兼关闭保险
     * @param string
     * @return
     */
    @RequestMapping("/editVisaInsurance")
    public Object edit(@RequestBody String string)
    {
        try {
            VisaInsuranceImp visaInsuranceImp = JSONUtil.toObj(string,VisaInsuranceImp.class);
            Visainsurance visainsurance = VisaInsuranceImp.VisaInsuranceImp2VisaInsuranceUpdate(visaInsuranceImp);
            return rootVisaInsuranceService.update(visainsurance);
        }catch (Exception e){
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    @RequestMapping("/getAllVisaInsurance")
    public Object getall(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return rootVisaInsuranceService.getAll(String.valueOf(jsonObject.get("page")));
        }catch (Exception e){
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

}
