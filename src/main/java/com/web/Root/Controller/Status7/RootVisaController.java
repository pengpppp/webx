package com.web.Root.Controller.Status7;

import com.web.Root.Service.RootVisaService;
import com.web.common.core.entity.JsonData;
import com.web.common.core.util.JSONUtil;
import com.web.entity.entityImp.Visa.VisaImp;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping("/visa")
@RestController
public class RootVisaController {
    @Autowired
    RootVisaService rootVisaService;

    @RequestMapping("/uploadVisaPic")
    public Object upload(@RequestParam(value = "file") MultipartFile file)
    {
        try {
            return rootVisaService.upload(file);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }
    @RequestMapping("/addVisa")
    public Object add(@RequestBody String string)
    {
        try {
            VisaImp visaImp = JSONUtil.toObj(string,VisaImp.class);
            return rootVisaService.add(visaImp);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }
    @RequestMapping("/deleteVisa")
    public Object delete(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return rootVisaService.delete(jsonObject.getString("id"));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    @RequestMapping("/editVisa")
    public Object edit(@RequestBody String string)
    {
        try {
            VisaImp visaImp = JSONUtil.toObj(string,VisaImp.class);
            return rootVisaService.update(visaImp);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    @RequestMapping("/getAllVisa")
    public Object getAll(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return rootVisaService.getAll(String.valueOf(jsonObject.get("page")));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    @RequestMapping("/getVisa")
    public Object getOne(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return rootVisaService.getOne(jsonObject.getString("id"));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

}
