package com.web.Tools.Http;

import com.web.Config.ConfigClass;
import com.web.entity.entityImp.newImp;
import com.web.entity.entityImp.neww;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
public class Get {

    @Autowired
    ConfigClass configClass;

    public static String APPKEY ;// 你的appkey
    public static final String URL = "https://api.jisuapi.com/news/get";


    public Object NewGet(String channel,int num,int page,neww newww) throws Exception {
        String result ;
        String url = URL + "?channel=" + channel + "&num=" + num + "&appkey=" + configClass.getApp_key()+"&start="+page;
        RestTemplate restTemplate = new RestTemplate();
        try {
            result = restTemplate.getForObject(url,String.class);
            JSONObject json = JSONObject.fromObject(result);
            if (json.getInt("status") != 0) {
                System.out.println(json.getString("msg"));
            } else {
                JSONObject resultarr = (JSONObject) json.opt("result");
                JSONArray list = resultarr.optJSONArray("list");
                for (int i = 0; i < list.size(); i++) {
                    JSONObject obj = (JSONObject) list.opt(i);
                    newww.getList().add((newImp) JSONObject.toBean(obj,newImp.class));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return newww;
    }
}
