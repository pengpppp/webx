package com.web.Tools.ImageV;

import java.util.Base64;
import java.util.Base64.Decoder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

public class Imagebase64 {
    static Base64.Encoder encoder = Base64.getEncoder();
    static Decoder decoder = Base64.getDecoder();

    public static String getImageBinary(BufferedImage bi) {
        try {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(bi, "jpg", baos);
        byte[] bytes = baos.toByteArray();
        return encoder.encodeToString(bytes).trim();
    } catch (IOException e) {
        e.printStackTrace();
    }
    return null;
}

public static void base64StringToImage(String base64String,String path) {
    try {
        byte[] bytes1 = decoder.decode(base64String);
        ByteArrayInputStream bais = new ByteArrayInputStream(bytes1);
        BufferedImage bi1 = ImageIO.read(bais);
        File f1 = new File(path);
        ImageIO.write(bi1, "jpg", f1);
} catch (IOException e) {
e.printStackTrace();
}
}
}
