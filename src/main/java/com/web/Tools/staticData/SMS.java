package com.web.Tools.staticData;

import java.util.Date;
import java.util.HashMap;

public class SMS {


    /**
     * 企业审核状态
     * 1：已认证
     * 2：未提交
     * 3：已提交
     * 4：认证不通过
     */
    public static HashMap<String,String> isRealName = new HashMap<String, String>(){
        {
            put("1","已认证");
            put("2","未提交");
            put("3","已提交");
            put("4","认证不通过");
        }
    };
    /**
     * 用户角色
     * 21：管理人员
     * 22：财务人员
     * 23：调度人员
     * 24：普通人员
     */
    public static HashMap<String,String> userRole =new HashMap<String, String>(){
        {
            put("21","管理人员");
            put("22","财务人员");
            put("23","调度人员");
            put("24","普通人员");
        }
    };
    /**
     * 企业认证状态
     * 1：未认证
     * 2：审核中
     * 3：审核不通过
     * 4：已认证
     */
    public static HashMap<String,String> companyAuth = new HashMap<String, String>(){
        {
            put("1","未认证");
            put("2","审核中");
            put("3","审核不通过");
            put("4","已认证");
        }
    };
    /**
     * 申请认证类型
     * 0：未进行任何认证
     * 1：企业认证
     * 2：加入企业认证
     */
    public static HashMap<String,String> companyAuthType =new HashMap<String, String>(){
        {
            put("0","未进行任何认证");
            put("1","企业认证");
            put("2","加入企业认证");
        }
    };

    /**
     * 生产x位的验证码
     * @param x
     * @return
     */
    public static String vcode(int x){
        String vcode = "";
        for (int i = 0; i < x; i++) {
            vcode = vcode + (int)(Math.random() * 9);
        }
        return vcode;
    }

    /**
     * 生产验证码的key
     * key=毫秒数的后三位+三位随机数
     * 减少验证码的key相互覆盖的可能性
     * @return
     */
    public static String vKey(){
        String vcode = "";
        for (int i = 0; i < 3; i++) {
            vcode = vcode + (int)(Math.random() * 9);
        }
        Date date =new Date();
        String s = String.valueOf(date.getTime());
        vcode=s.substring(s.length()-4,s.length()-1)+vcode;
        return vcode;
    }

    /**
     * 生产x+3位id
     * vcode = 毫秒数后五位+x位随机数
     * @param x
     * @return
     */
    public static String vRom(int x)
    {
        String vcode = "";
        for (int i = 0; i < x; i++) {
            vcode = vcode + (int)(Math.random() * 9);
        }
        Date date =new Date();
        String s = String.valueOf(date.getTime());
        vcode=s.substring(s.length()-5,s.length()-1)+vcode;
        return vcode;
    }

}
