package com.web.Tools.staticData;



import java.util.Base64;
import java.util.Base64.Decoder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;


//图片文件转Base64编码

public class FileCode {

    public static final HashMap<String,String> pictype =new HashMap<String,String>()
    {{
        put("1",".jpg");
        put("2",".html");
    }};

    /**
      * <p>将文件转成base64 字符串</p>
      * @param path 文件路径
      */
        public static String encodeBase64Filepath(String path) throws Exception {
            File file = new File(path);
            FileInputStream inputFile = new FileInputStream(file);
            byte[] buffer = new byte[(int)file.length()];
            inputFile.read(buffer);
            inputFile.close();
            Base64.Encoder encoder = Base64.getEncoder();
            return encoder.encodeToString(buffer);
            //return new BASE64Encoder().encode(buffer);
        }
    public static String encodeBase64FileInputStream(FileInputStream inputFile,long size) throws Exception {
        byte[] buffer = new byte[Math.toIntExact(size)];
        inputFile.read(buffer);
        inputFile.close();
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(buffer);
        //return new BASE64Encoder().encode(buffer);
    }
        /**
      * <p>将base64字符解码保存文件</p>
      */
        public static void decoderBase64File(String base64Code,String targetPath) throws Exception {
            Decoder decoder = Base64.getDecoder();
            byte[] buffer = decoder.decode(base64Code);
           // byte[] buffer = new BASE64Decoder().decodeBuffer(base64Code);
            FileOutputStream out = new FileOutputStream(targetPath);
            out.write(buffer);
            out.close();
        }




}

