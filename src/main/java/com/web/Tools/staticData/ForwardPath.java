package com.web.Tools.staticData;

import java.util.HashMap;

public class ForwardPath {
    public static final HashMap<String,String> map =new HashMap<String,String>()
    {{
        put("10006","/home");//获取首页商品图片信息
        put("10007","/banner");//获取首页轮播图信息
        put("10008","/addLeaving");//添加留言
        put("10009","/companyShow");//显示公司信息
        put("10010","/contentShow");//显示内容信息

        put("20000","/login");
        put("20001","/register");
        put("10003","/GetVerifyCode");
        put("10004","/CheckVerifyCode");
        put("10002","/GetRandomCode");
        put("10005","/ValidateRandomCode");

        put("10011","/getNew");
        put("10012","/getNewLeaving");
        put("10013","/addNewLeaving");
        put("10014","/addCollection");
        put("10015","/getCollection");

        put("30001","/flightCityList");
        put("30002","/getAirCompanyList");
        put("30003","/getAirportsList");
        put("30004","/getOrderList");
        put("30005","/getOrderDetails");
        put("30006","/cancelOrder");
        put("30007","/confirmOrder");
        put("30008","/refundUpload");
        put("30009","/refundApply");
        put("30010","/getRefundOrderList");
        put("30011","/getRefundOrderDetails");
        put("30012","/changeApply");
        put("30013","/getChangeOrderList");
        put("30014","/getChangeOrderDetails");
        put("30015","/cancelChange");
        put("30016","/confirmChange");

        put("30017","/getLowPriceCalendar");
        put("30018","/cheapFlights");
        put("30019","/getInsuranceList");
        put("30020","/getFlightList");
        put("30021","/getFlightDetails");
        put("30022","/getRefundRule");
        put("30023","/saveOrder");
        put("30024","/createOrder");
        put("30040","/getOrder");
        put("30041","/getMyOrder");
        put("30042","/concelOrder");

        put("30025","/getInterFlightList");
        put("30026","/checkPrice");
        put("30027","/getInterRefundRule");
        put("30028","/createInterOrder");
        put("30029","/saveInterOrder");

        put("30030","/addPassenger");
        put("30031","/showUserAllPassenger");
        put("30032","/deletePassenger");
        put("30033","/editPassenger");
        put("30034","/getPassenger");

        put("40001","/getVisaList");
        put("40002","/getVisa");

        put("40010","/addVisaOrder");
        put("40011","/getMyVisaOrder");
        put("40012","getVisaOrder");
        put("40013","/concelVisaOrder");


    }};
}
