package com.web.Tools.staticData;

import java.util.HashMap;

public class Code {
    /**
     * 成功
     */
    public static final String SUCCESS="1000";
    /**
     * 失败
     */
    public static final String FAIL="1001";
    /**
     * 密码错误
     */
    public static final String PWD_ERROR ="1002";
    /**
     * 密码锁定
     */
    public static final String PWD_LOCK="1003";
    /**
     * 账号已存在
     */
    public static final String REGISTERED="1004";
    /**
     * 推荐码不存在
     */
    public static final String RECOM_NO_EXIT="1005";
    /**
     * 图形验证码失效
     */
    public static final String GRA_LOSE="1006";
    /**
     * 图形验证码错误
     */
    public static final String GRA_ERROR="1007";
    /**
     * 用户不存在
     */
    public static final String USER_IS_NULL="1009";
    /**
     * 手机号码格式错误
     */
    public static final String PHONE_FORMAT_ERROR="1010";
    /**
     * 验签失败
     */
    public static final String SIGNATURE_LOSE="1011";
    /**
     * 参数错误
     */
    public static final String PARAMETER_ERROR="1013";
    /**
     * 短信验证码失效
     */
    public static final String SMS_LOSE="1014";
    /**
     * 短信验证码错误
     */
    public static final String SMS_ERROR="1015";
    /**
     * 该号码今日已获取五次验证码，明日再试
     */
    public static final String SMS_PASS="1016";
    /**
     * 系统内部错误
     */
    public static final String SYSTEM_INTERNAL_ERROR="1017";
    /**
     * 项目剩余份额不足，不能购买
     */
    public static final String PROJ_NO_ENOUGH="1018";
    /**
     * 暂无权限
     */
    public static final String NO_PERMISSION="1019";
    /**
     * 请在六十秒之后重新再获取验证码
     */
    public static final String SMS_REACQUIRE="1020";

    /**
     * ID为空或者ID错误
     */
    public static final String WHITE_LIST_ERROR="1021";

    /**
     * 状态码对应的汉字描述
     */
    public static final HashMap<String,String> information =new HashMap<String,String>()
    {{
        put(Code.SUCCESS,"成功");
        put(Code.FAIL,"失败");
        put(Code.PHONE_FORMAT_ERROR,"手机号码格式错误");
        put(Code.PWD_ERROR,"密码错误");
        put(Code.PWD_LOCK,"密码锁定");
        put(Code.REGISTERED,"该账号已存在");
        put(Code.GRA_LOSE,"图形验证码失效");
        put(Code.RECOM_NO_EXIT,"推荐码不存在");
        put(Code.GRA_ERROR,"图形验证码错误");
        put(Code.USER_IS_NULL,"该用户不存在");
        put(Code.SIGNATURE_LOSE,"验签失败");
        put(Code.PARAMETER_ERROR,"参数错误");
        put(Code.SMS_LOSE,"短信验证码失效或不存在");
        put(Code.SMS_ERROR,"短信验证码错误");
        put(Code.SMS_PASS,"该号码今日已获取五次验证码，请明日再试");
        put(Code.SYSTEM_INTERNAL_ERROR,"系统内部错误");
        put(Code.PROJ_NO_ENOUGH,"项目剩余份额不足，不能购买");
        put(Code.NO_PERMISSION,"暂无权限");
        put(Code.SMS_REACQUIRE,"请在六十秒之后重新再获取验证码");
        put(Code.WHITE_LIST_ERROR,"白名单错误");
    }};

    /**
     * 测试用，万能的key
     */
    public static final String TEST ="1234";

}
