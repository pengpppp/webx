package com.web.Tools.staticData;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class InterfaceID {

    /**
     * 所有的接口请求ID
     */
    public static final String[] All_ID = {"20000","20001","10003","10004","10002","10005","10044","10045",
            "10006","10007","10009","10010","10008","10011","10012","10013","10014","10015","30019","30020","30021","30022","30023",
            "30025","30029","30030","30031","30032","30033","30034","30040","30041","30042","40001","40002","40010","40011",
    "40012","40013"};

    /**
     * 不需要登陆状态的接口请求ID
     */
    public static final String[] No_Need_Token = {"20000","20001","10003","10004","10002","10005","10044","10045",
            "10006","10007","10008","10009","10010","10011","10012","10015","30019","30020","30021","30022","30025","40001","40002"};

    /**
     * 后台管理界面的接口请求ID
     */
    public static final String[] Root_ID = {"10009","10010","10008"};
    /**
     * 将错误信息发送至返回流中
     * @param response
     * @param jsonObject
     */
    public static void SendErrorInformation(HttpServletResponse response,Object jsonObject)
    {
        PrintWriter writer = null;
        try {
            response.setCharacterEncoding("UTF-8");
            response.setContentType("JSON/html; charset=utf-8");
            writer = response.getWriter();
            writer.print(jsonObject);
        } catch (IOException e) {
            //logger.error("response error", e);
            e.printStackTrace();
        } finally {
            if (writer != null)
                writer.close();
        }
    }


}
