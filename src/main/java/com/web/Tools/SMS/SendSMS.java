package com.web.Tools.SMS;

import com.web.Config.ConfigClass;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class SendSMS {
    @Autowired
    ConfigClass configClass;

    public Object SendSms(String code,String mobile) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", configClass.getAccessKeyId(), configClass.getAccessSecret());
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("SendSms");
        request.putQueryParameter("RegionId", "cn-hangzhou");

        request.putQueryParameter("SignName", "1");//短信签名
        request.putQueryParameter("TemplateCode", "1");//短信模板
        request.putQueryParameter("PhoneNumbers", mobile);
        Map map = new HashMap();
        map.put("code",code);
        JSONObject jsonObject = JSONObject.fromObject(map);
        request.putQueryParameter("TemplateParam", jsonObject.toString());
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
            return response.getData();
        } catch (ServerException e) {
            e.printStackTrace();
            return null;
        } catch (ClientException e) {
            e.printStackTrace();
            return null;
        }

    }


    public Object querySms(String mobile,String BizID) {
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", configClass.getAccessKeyId(), configClass.getAccessSecret());
        IAcsClient client = new DefaultAcsClient(profile);

        CommonRequest request = new CommonRequest();
        request.setSysMethod(MethodType.POST);
        request.setSysDomain("dysmsapi.aliyuncs.com");
        request.setSysVersion("2017-05-25");
        request.setSysAction("QuerySendDetails");
        request.putQueryParameter("RegionId", "cn-hangzhou");
        request.putQueryParameter("PageSize", "10");
        request.putQueryParameter("CurrentPage", "1");

        request.putQueryParameter("PhoneNumber", mobile);

        java.text.SimpleDateFormat df = new java.text.SimpleDateFormat("yyyyMMdd");
        df.setTimeZone(new java.util.SimpleTimeZone(0, "GMT"));
        request.putQueryParameter("SendDate", df.format(new Date()));

        request.putQueryParameter("BizId", BizID);
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
            return response.getData();
        } catch (ServerException e) {
            e.printStackTrace();
            return null;
        } catch (ClientException e) {
            e.printStackTrace();
            return null;
        }
    }









}
