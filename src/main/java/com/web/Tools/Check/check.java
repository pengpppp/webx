package com.web.Tools.Check;

import com.web.Tools.staticData.InterfaceID;

public class check {
    /**
     * 检查手机号码是否格式正确
     * 正确返回true
     * 错误返回false
     * @param mobile
     * @return
     */
    public static boolean checkMobile(String mobile)
    {
        if(mobile==null||mobile.isEmpty())
        {
            return false;
        }else if(mobile.length()!=11)
        {
            return false;
        }else if (!mobile.substring(0,1).equals("1"))
        {
            return false;
        }
        return true;
    }

    /**
     * 检查是否手机验证码是否已发送
     * @return
     */
    public static boolean checkSmsVerifyCode(String key)
    {
        return true;
    }

    /**
     * 如果token为空，则校验是否是可以不登录便使用的接口
     * 如果token不为空，则直接放回true
     * @param ID
     * @param token
     * @return
     */
    public static boolean checkLoginToken(String ID,String token)
    {
        try {
            if(check.checkNull(token))//如果不是登录状态
            {
                for (String s: InterfaceID.No_Need_Token)
                {
                    if(s.equals(ID))//如果是可以不登录便使用的接口
                    {
                        return true;
                    }
                }
                return false;
            }else {//如果是登录的状态
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 检查请求id是否为正确的
     * @param id
     * @return
     */
    public static boolean checkID(String id)
    {
        for (String s:InterfaceID.All_ID)
        {
            if(id.equals(s))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * 检查字符串是否为空
     * 为空返回true,不为空返回false
     * @param s
     * @return
     */
    public static boolean checkNull(String s)
    {
        if(s==null||"null".equals(s)||"".equals(s)||"[]".equals(s)||s.equals("\"null\""))
        {
            return true;
        }else
            return false;
    }
}
