package com.web.entity;

import java.io.Serializable;

/**
 * (Content)实体类
 *
 * @author makejava
 * @since 2020-05-08 17:38:33
 */
public class Content implements Serializable {
    private static final long serialVersionUID = -63094297010629166L;
    
    private Integer id;
    
    private String company;
    
    private String content;
    
    private String type;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}