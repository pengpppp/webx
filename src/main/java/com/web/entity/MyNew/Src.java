package com.web.entity.MyNew;

import java.io.Serializable;

/**
 * (Src)实体类
 *
 * @author makejava
 * @since 2020-05-22 10:34:07
 */
public class Src implements Serializable {
    private static final long serialVersionUID = -83650109440200533L;
    
    private Integer id;
    
    private String src;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

}