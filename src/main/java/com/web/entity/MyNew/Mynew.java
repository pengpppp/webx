package com.web.entity.MyNew;

import java.util.Date;
import java.io.Serializable;

/**
 * (Mynew)实体类
 *
 * @author makejava
 * @since 2020-05-23 09:16:01
 */
public class Mynew implements Serializable {
    private static final long serialVersionUID = 612151282359682818L;
    
    private Integer id;
    
    private Long key;
    /**
    * 标题
    */
    private String title;
    /**
    * 时间
    */
    private Date time;
    /**
    * 来源/作者
    */
    private String src;
    /**
    * 分类
    */
    private String category;
    /**
    * 图片
    */
    private String pic;
    /**
    * 内容
    */
    private String content;
    
    private String url;
    
    private String weburl;

    @Override
    public String toString() {
        return "Mynew{" +
                "id=" + id +
                ", key=" + key +
                ", title='" + title + '\'' +
                ", time=" + time +
                ", src='" + src + '\'' +
                ", category='" + category + '\'' +
                ", pic='" + pic + '\'' +
                ", content='" + content + '\'' +
                ", url='" + url + '\'' +
                ", weburl='" + weburl + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getKey() {
        return key;
    }

    public void setKey(Long key) {
        this.key = key;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWeburl() {
        return weburl;
    }

    public void setWeburl(String weburl) {
        this.weburl = weburl;
    }

}