package com.web.entity.MyNew;

import java.io.Serializable;

/**
 * (Channel)实体类
 *
 * @author makejava
 * @since 2020-05-19 17:56:36
 */
public class Channel implements Serializable {
    private static final long serialVersionUID = -81735946884984611L;
    
    private Integer id;
    /**
    * 频道
    */
    private String channel;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

}