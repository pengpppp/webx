package com.web.entity.MyNew;

import java.util.Date;
import java.io.Serializable;

/**
 * (Collection)实体类
 *
 * @author makejava
 * @since 2020-05-20 16:07:55
 */
public class Collection implements Serializable {
    private static final long serialVersionUID = -39923858327035527L;
    
    private Integer id;
    
    private String channel;
    
    private String src;
    
    private String tittle;
    
    private String leavingname;
    
    private Date creattime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getLeavingname() {
        return leavingname;
    }

    public void setLeavingname(String leavingname) {
        this.leavingname = leavingname;
    }

    public Date getCreattime() {
        return creattime;
    }

    public void setCreattime(Date creattime) {
        this.creattime = creattime;
    }

}