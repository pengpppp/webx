package com.web.entity.MyNew;

import java.io.Serializable;

/**
 * (Category)实体类
 *
 * @author makejava
 * @since 2020-05-22 14:21:40
 */
public class Category implements Serializable {
    private static final long serialVersionUID = 686307871623065390L;
    
    private Integer id;
    
    private String category;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}