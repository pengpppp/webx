package com.web.entity.MyNew;

import java.io.Serializable;

/**
 * (Mynewpic)实体类
 *
 * @author makejava
 * @since 2020-05-22 13:53:35
 */
public class Mynewpic implements Serializable {
    private static final long serialVersionUID = -50983578365199325L;
    
    private Integer id;
    /**
    * 图片标题
    */
    private String title;
    /**
    * 图片链接
    */
    private String url;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}