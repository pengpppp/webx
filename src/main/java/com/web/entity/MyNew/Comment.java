package com.web.entity.MyNew;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;
import java.io.Serializable;

/**
 * (Comment)实体类
 *
 * @author makejava
 * @since 2020-05-21 13:18:46
 */
public class Comment implements Serializable {
    private static final long serialVersionUID = 767303165406819259L;
    
    private Integer id;
    
    private String channel;
    
    private String src;
    
    private String tittle;
    
    private String userid;
    
    private String leavingname;
    
    private String information;
    
    private Date creattime;

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", channel='" + channel + '\'' +
                ", src='" + src + '\'' +
                ", tittle='" + tittle + '\'' +
                ", userid=" + userid +
                ", leavingname='" + leavingname + '\'' +
                ", information='" + information + '\'' +
                ", creattime=" + creattime +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getLeavingname() {
        return leavingname;
    }

    public void setLeavingname(String leavingname) {
        this.leavingname = leavingname;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    public Date getCreattime() {
        return creattime;
    }

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    public void setCreattime(Date creattime) {
        this.creattime = creattime;
    }

}