package com.web.entity;

import java.io.Serializable;

/**
 * (Homeorder)实体类
 *
 * @author makejava
 * @since 2020-06-12 17:00:13
 */
public class Homeorder implements Serializable {
    private static final long serialVersionUID = -37101412366394081L;
    
    private Integer i;
    
    private String id;
    
    private Integer useid;
    
    private Integer orderid;
    
    private String ordernumber;
    /**
    * 舱位
    */
    private String sessionid;
    
    private String intlcode;
    /**
    * 联系人
    */
    private String contactname;
    /**
    * 联系人手机号
    */
    private String contactmobile;
    /**
    * 订单来源
    */
    private Integer ordersource;
    /**
    * 存储乘客信息ID
    */
    private String passengers;
    /**
    * 邮寄信息
    */
    private String delivery;
    /**
    * Int 集合、所有乘机人共买的保险
    */
    private String insurances;
    /**
    * 备注
    */
    private String remark;
    /**
    * 发票信息
    */
    private String invices;
    /**
    * 航班信息
    */
    private String flight;
    /**
    * 舱位信息
    */
    private String cabin;
    
    private Double price;
    
    private String payment;
    
    private Long creattime;

    private Long paytime;

    public Long getPaytime() {
        return paytime;
    }

    public void setPaytime(Long paytime) {
        this.paytime = paytime;
    }

    public Integer getI() {
        return i;
    }

    public void setI(Integer i) {
        this.i = i;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUseid() {
        return useid;
    }

    public void setUseid(Integer useid) {
        this.useid = useid;
    }

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getIntlcode() {
        return intlcode;
    }

    public void setIntlcode(String intlcode) {
        this.intlcode = intlcode;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getContactmobile() {
        return contactmobile;
    }

    public void setContactmobile(String contactmobile) {
        this.contactmobile = contactmobile;
    }

    public Integer getOrdersource() {
        return ordersource;
    }

    public void setOrdersource(Integer ordersource) {
        this.ordersource = ordersource;
    }

    public String getPassengers() {
        return passengers;
    }

    public void setPassengers(String passengers) {
        this.passengers = passengers;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getInsurances() {
        return insurances;
    }

    public void setInsurances(String insurances) {
        this.insurances = insurances;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getInvices() {
        return invices;
    }

    public void setInvices(String invices) {
        this.invices = invices;
    }

    public String getFlight() {
        return flight;
    }

    public void setFlight(String flight) {
        this.flight = flight;
    }

    public String getCabin() {
        return cabin;
    }

    public void setCabin(String cabin) {
        this.cabin = cabin;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public Long getCreattime() {
        return creattime;
    }

    public void setCreattime(Long creattime) {
        this.creattime = creattime;
    }

}