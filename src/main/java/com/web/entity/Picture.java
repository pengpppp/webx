package com.web.entity;

import java.io.Serializable;

/**
 * (Picture)实体类
 *
 * @author makejava
 * @since 2020-05-04 09:12:10
 */
public class Picture implements Serializable {
    private static final long serialVersionUID = -16000449623095286L;
    
    private Integer id;
    
    private String prcturepath;
    
    private Integer type;
    
    private String descs;
    
    private String url;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPrcturepath() {
        return prcturepath;
    }

    public void setPrcturepath(String prcturepath) {
        this.prcturepath = prcturepath;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getDescs() {
        return descs;
    }

    public void setDescs(String descs) {
        this.descs = descs;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}