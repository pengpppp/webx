package com.web.entity;

import com.google.gson.Gson;
import com.web.entity.entityImp.Visa.VisaOrderImp;

import java.io.Serializable;

/**
 * (Visaorder)实体类
 *
 * @author makejava
 * @since 2020-06-16 17:21:30
 */
public class Visaorder implements Serializable {
    private static final long serialVersionUID = -74381300005704624L;
    
    private Integer i;
    /**
    * 本订单的ID
    */
    private String orderid;
    
    private String userid;
    /**
    * 签证项目的id
    */
    private String id;
    /**
    * 预计出发时间
    */
    private Long departuretime;
    /**
    * 申请人列表  
    */
    private String applicant;
    /**
    * 快递返还方式
    */
    private String deliverytype;
    /**
    * 配送地址
    */
    private String deliverypath;
    /**
    * 联系人名
    */
    private String contactname;
    /**
    * 联系人手机号码
    */
    private String contactmobile;
    /**
    * 联系人邮件地址
    */
    private String contactemail;
    /**
    * 保险ID列表
    */
    private String insurances;
    
    private Double price;
    
    private String payment;
    
    private Long creattime;
    
    private Long paytime;

    public Visaorder()
    {

    }

    /**
     * 申请人列表及保险列表未完成
     * @param visaOrderImp
     */
    public Visaorder(VisaOrderImp visaOrderImp)
    {
        Gson gson = new Gson();
        this.setId(visaOrderImp.getId());
        this.setDeparturetime(visaOrderImp.getDeparturetime());
        this.setDeliverytype(visaOrderImp.getDeliverytype());
        this.setDeliverypath(visaOrderImp.getDeliverypath());
        this.setContactname(visaOrderImp.getContactname());
        this.setContactmobile(visaOrderImp.getContactmobile());
        this.setContactemail(visaOrderImp.getContactemail());
    }

    public Integer getI() {
        return i;
    }

    public void setI(Integer i) {
        this.i = i;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getDeparturetime() {
        return departuretime;
    }

    public void setDeparturetime(Long departuretime) {
        this.departuretime = departuretime;
    }

    public String getApplicant() {
        return applicant;
    }

    public void setApplicant(String applicant) {
        this.applicant = applicant;
    }

    public String getDeliverytype() {
        return deliverytype;
    }

    public void setDeliverytype(String deliverytype) {
        this.deliverytype = deliverytype;
    }

    public String getDeliverypath() {
        return deliverypath;
    }

    public void setDeliverypath(String deliverypath) {
        this.deliverypath = deliverypath;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getContactmobile() {
        return contactmobile;
    }

    public void setContactmobile(String contactmobile) {
        this.contactmobile = contactmobile;
    }

    public String getContactemail() {
        return contactemail;
    }

    public void setContactemail(String contactemail) {
        this.contactemail = contactemail;
    }

    public String getInsurances() {
        return insurances;
    }

    public void setInsurances(String insurances) {
        this.insurances = insurances;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public Long getCreattime() {
        return creattime;
    }

    public void setCreattime(Long creattime) {
        this.creattime = creattime;
    }

    public Long getPaytime() {
        return paytime;
    }

    public void setPaytime(Long paytime) {
        this.paytime = paytime;
    }

}