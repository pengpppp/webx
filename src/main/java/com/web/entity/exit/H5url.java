package com.web.entity.exit;

import java.io.Serializable;

/**
 * (H5url)实体类
 *
 * @author makejava
 * @since 2020-05-04 10:06:51
 */
public class H5url implements Serializable {
    private static final long serialVersionUID = -81068063696588950L;
    
    private String url;
    
    private String location;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}