package com.web.entity;

import java.io.Serializable;

/**
 * (Picturepath)实体类
 *
 * @author makejava
 * @since 2020-05-05 15:28:03
 */
public class Picturepath implements Serializable {
    private static final long serialVersionUID = 804617114386614510L;
    
    private String url;
    
    private String location;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}