package com.web.entity;

import java.util.Date;
import java.io.Serializable;

/**
 * (Leavingword)实体类
 *
 * @author makejava
 * @since 2020-05-09 09:14:25
 */
public class Leavingword implements Serializable {
    private static final long serialVersionUID = -47604349532974713L;
    
    private Integer id;
    
    private String name;
    
    private Object information;
    
    private String phone;
    
    private Date creattime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getInformation() {
        return information;
    }

    public void setInformation(Object information) {
        this.information = information;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getCreattime() {
        return creattime;
    }

    public void setCreattime(Date creattime) {
        this.creattime = creattime;
    }

}