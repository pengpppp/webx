package com.web.entity.User;

import java.io.Serializable;

/**
 * (Userid)实体类
 *
 * @author makejava
 * @since 2020-06-10 13:39:38
 */
public class Userid implements Serializable {
    private static final long serialVersionUID = 441395728480399824L;
    
    private Integer i;
    
    private String userid;
    
    private String mobile;
    
    private String loginpwd;
    
    private String token;


    public Integer getI() {
        return i;
    }

    public void setI(Integer i) {
        this.i = i;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLoginpwd() {
        return loginpwd;
    }

    public void setLoginpwd(String loginpwd) {
        this.loginpwd = loginpwd;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}