package com.web.entity.User;

import java.io.Serializable;

/**
 * (User)实体类
 *
 * @author makejava
 * @since 2020-05-04 09:12:13
 */
public class User implements Serializable {
    private static final long serialVersionUID = 808315645464493080L;
    
    private String mobile;
    
    private String isrealname;
    
    private String username;
    
    private String company;
    
    private String userrole;
    
    private String companyauth;
    
    private String companyauthtype;


    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIsrealname() {
        return isrealname;
    }

    public void setIsrealname(String isrealname) {
        this.isrealname = isrealname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getUserrole() {
        return userrole;
    }

    public void setUserrole(String userrole) {
        this.userrole = userrole;
    }

    public String getCompanyauth() {
        return companyauth;
    }

    public void setCompanyauth(String companyauth) {
        this.companyauth = companyauth;
    }

    public String getCompanyauthtype() {
        return companyauthtype;
    }

    public void setCompanyauthtype(String companyauthtype) {
        this.companyauthtype = companyauthtype;
    }

}