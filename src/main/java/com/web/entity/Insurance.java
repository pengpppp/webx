package com.web.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

/**
 * (Insurance)实体类
 *
 * @author makejava
 * @since 2020-06-06 10:19:18
 */
public class Insurance implements Serializable {
    private static final long serialVersionUID = -58444973165582873L;
    /**
    * 保险ID
    */
    private String id;
    /**
    * 是否为自己的保险 T是 F否
    */
    @JsonIgnore
    private String ismy;
    /**
    * 开关  T开 F关
    */
    @JsonIgnore
    private String key;
    /**
    * 1.航空意外险 2航空延误险
    */
    private Integer type;
    /**
    * 保险类型名称
    */
    private String typename;
    /**
    * 保险名称
    */
    private String name;
    /**
    * 保险的简短说明
    */
    private String remark;
    /**
    * 保险价格
    */
    private Double cost;
    /**
    * 保险相关材料
    */
    private Object pdflist;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonIgnore
    public String getIsmy() {
        return ismy;
    }

    @JsonIgnore
    public void setIsmy(String ismy) {
        this.ismy = ismy;
    }

    @JsonIgnore
    public String getKey() {
        return key;
    }

    @JsonIgnore
    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", ismy='" + ismy + '\'' +
                ", key='" + key + '\'' +
                ", type=" + type +
                ", typename='" + typename + '\'' +
                ", name='" + name + '\'' +
                ", remark='" + remark + '\'' +
                ", cost=" + cost +
                ", pdflist=" + pdflist.toString() +
                '}';
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Object getPdflist() {
        return pdflist;
    }

    public void setPdflist(Object pdflist) {
        this.pdflist = pdflist;
    }
}