package com.web.entity.entityImp;

import java.io.Serializable;

public class loginEntity implements Serializable {
    private String code;
    private String message;
    private String RK;
    private Data data;

    public loginEntity()
    {
        this.data=new Data();
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRK() {
        return RK;
    }

    public void setRK(String RK) {
        this.RK = RK;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }
}
