package com.web.entity.entityImp;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.Date;

public class getAllMyOrder implements Serializable {
    private String id;
    private Object flight;
    private double price;
    private String payment;
    private long creattime;
    private long nowtime;
    public getAllMyOrder()
    {
        this.nowtime=new Date().getTime();
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", flight=" + flight +
                ", price=" + price +
                ", payment='" + payment + '\'' +
                '}';
    }

    public String getNowtime() {
        return String.valueOf(nowtime);
    }

    public void setNowtime(long nowtime) {
        this.nowtime = nowtime;
    }

    public String getCreattime() {
        return String.valueOf(creattime);
    }

    public void setCreattime(long creattime) {
        this.creattime = creattime;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Object getFlight() {
        return flight;
    }

    public void setFlight(Object flight) {
        this.flight = flight;
    }

}
