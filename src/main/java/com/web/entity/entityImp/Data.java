package com.web.entity.entityImp;

import com.web.entity.User.User;

import java.io.Serializable;

public class Data implements Serializable {
    private String token;

    private String userid;

    private String mobile;

    private User loginUserInfo;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Data()
    {
        this.loginUserInfo=new User();
    }
    public User getLoginUserInfo() {
        return loginUserInfo;
    }

    public void setLoginUserInfo(User loginUserInfo) {
        this.loginUserInfo = loginUserInfo;
    }
}
