package com.web.entity.entityImp.Order;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SaveOrder {

    /**
     * contactname :
     * intlcode :
     * delivery : {"zipcode":"","deliveryprice":0,"deliveryendaddress":"","districtname":"","provincename":"","cityname":"","recipientname":"","deliverytype":0,"expresscompanycode":"","recipientphone":""}
     * passengers : [1,2]
     * insurances : [1,2,3]
     * remark :
     * invices : [{"taxpayeridentity":"","contactertel":"","contacter":"","billtype":0,"title":"","personalorenterprise":1}]
     * sessionid : 654c88ed620944e7b24335288daa39e8
     * contactmobile :
     * ordersource : 0
     * flight : {"flightNo":"CZ3101","departDate":"2020-06-10","departTime":"08:00","departCityCode":"CAN","departCityName":"广州","departAirportCode":"CAN","departAirportName":"白云机场","departAirportTerminal":"T2","arriveCityCode":"BJS","arriveCityName":"北京","arriveDate":"2020-06-10","arriveTime":"11:10","arriveAirportCode":"PEK","arriveAirportName":"首都机场","arriveAirportTerminal":"T2","crossDay":0,"craftType":"788(大)","airlineCode":"CZ","airlineName":"中国南方航空","duration":"03:10","isShare":0,"shareFlightNo":null,"shareAirline":null,"shareAirlineName":null,"mealFlag":0,"meal":"无餐","stopInformation":null,"stopNum":"0","onTimeRate":"97%","stopPointList":null}
     * cabin : {"sessionId":"e39ced21e28a4ca6a3529c3eda90cf3f","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"R","cabinName":"经济舱","cabinClassFullPrice":2540,"seatNum":">9","cabinPrice":480,"discount":1.8,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":480,"tax":50,"fuel":0,"facePrice":480,"addPrice":0,"orgPrice":480},"child":{"price":480,"tax":0,"fuel":0,"facePrice":480,"addPrice":0,"orgPrice":480},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":45}}
     */

    private String contactname;
    private String intlcode;
    private DeliveryBean delivery = new DeliveryBean();
    private String remark;
    @JsonProperty(value = "sessionId")
    private String sessionid;
    private String contactmobile;
    private int ordersource;
    private FlightBean flight;
    private CabinBean cabin;
    private List<Integer> passengers;
    private List<Integer> insurances;
    private List<InvicesBean> invices= new ArrayList<>();

    @Override
    public String toString() {
        return "{" +
                "contactname='" + contactname + '\'' +
                ", intlcode='" + intlcode + '\'' +
                ", delivery=" + delivery.toString() +
                ", remark='" + remark + '\'' +
                ", sessionid='" + sessionid + '\'' +
                ", contactmobile='" + contactmobile + '\'' +
                ", ordersource=" + ordersource +
                ", flight=" + flight.toString() +
                ", cabin=" + cabin.toString() +
                ", passengers=" + passengers.toString() +
                ", insurances=" + insurances.toString() +
                ", invices=" + invices.toString() +
                '}';
    }

    public SaveOrder()
    {

    }
    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getIntlcode() {
        return intlcode;
    }

    public void setIntlcode(String intlcode) {
        this.intlcode = intlcode;
    }

    public DeliveryBean getDelivery() {
        return delivery;
    }

    public void setDelivery(DeliveryBean delivery) {
        this.delivery = delivery;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getContactmobile() {
        return contactmobile;
    }

    public void setContactmobile(String contactmobile) {
        this.contactmobile = contactmobile;
    }

    public int getOrdersource() {
        return ordersource;
    }

    public void setOrdersource(int ordersource) {
        this.ordersource = ordersource;
    }

    public FlightBean getFlight() {
        return flight;
    }

    public void setFlight(FlightBean flight) {
        this.flight = flight;
    }

    public CabinBean getCabin() {
        return cabin;
    }

    public void setCabin(CabinBean cabin) {
        this.cabin = cabin;
    }

    public List<Integer> getPassengers() {
        return passengers;
    }


    public void setPassengers(List<Integer> passengers) {
        this.passengers = passengers;
    }

    public List<Integer> getInsurances() {
        return insurances;
    }


    public void setInsurances(List<Integer> insurances) {
        this.insurances = insurances;
    }

    public List<InvicesBean> getInvices() {
        return invices;
    }

    public void setInvices(List<InvicesBean> invices) {
        this.invices = invices;
    }

    public static class DeliveryBean {
        /**
         * zipcode :
         * deliveryprice : 0
         * deliveryendaddress :
         * districtname :
         * provincename :
         * cityname :
         * recipientname :
         * deliverytype : 0
         * expresscompanycode :
         * recipientphone :
         */

        public DeliveryBean()
        {
            toString();
        }


        private String zipcode;
        private int deliveryprice;
        private String deliveryendaddress;
        private String districtname;
        private String provincename;
        private String cityname;
        private String recipientname;
        private int deliverytype;
        private String expresscompanycode;
        private String recipientphone;

        @Override
        public String toString() {
            return "{" +
                    "zipcode='" + zipcode + '\'' +
                    ", deliveryprice=" + deliveryprice +
                    ", deliveryendaddress='" + deliveryendaddress + '\'' +
                    ", districtname='" + districtname + '\'' +
                    ", provincename='" + provincename + '\'' +
                    ", cityname='" + cityname + '\'' +
                    ", recipientname='" + recipientname + '\'' +
                    ", deliverytype=" + deliverytype +
                    ", expresscompanycode='" + expresscompanycode + '\'' +
                    ", recipientphone='" + recipientphone + '\'' +
                    '}';
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public int getDeliveryprice() {
            return deliveryprice;
        }

        public void setDeliveryprice(int deliveryprice) {
            this.deliveryprice = deliveryprice;
        }

        public String getDeliveryendaddress() {
            return deliveryendaddress;
        }

        public void setDeliveryendaddress(String deliveryendaddress) {
            this.deliveryendaddress = deliveryendaddress;
        }

        public String getDistrictname() {
            return districtname;
        }

        public void setDistrictname(String districtname) {
            this.districtname = districtname;
        }

        public String getProvincename() {
            return provincename;
        }

        public void setProvincename(String provincename) {
            this.provincename = provincename;
        }

        public String getCityname() {
            return cityname;
        }

        public void setCityname(String cityname) {
            this.cityname = cityname;
        }

        public String getRecipientname() {
            return recipientname;
        }

        public void setRecipientname(String recipientname) {
            this.recipientname = recipientname;
        }

        public int getDeliverytype() {
            return deliverytype;
        }

        public void setDeliverytype(int deliverytype) {
            this.deliverytype = deliverytype;
        }

        public String getExpresscompanycode() {
            return expresscompanycode;
        }

        public void setExpresscompanycode(String expresscompanycode) {
            this.expresscompanycode = expresscompanycode;
        }

        public String getRecipientphone() {
            return recipientphone;
        }

        public void setRecipientphone(String recipientphone) {
            this.recipientphone = recipientphone;
        }
    }

    public static class FlightBean {

        @Override
        public String toString() {
            return "{" +
                    "flightNo='" + flightNo + '\'' +
                    ", departDate='" + departDate + '\'' +
                    ", departTime='" + departTime + '\'' +
                    ", departCityCode='" + departCityCode + '\'' +
                    ", departCityName='" + departCityName + '\'' +
                    ", departAirportCode='" + departAirportCode + '\'' +
                    ", departAirportName='" + departAirportName + '\'' +
                    ", departAirportTerminal='" + departAirportTerminal + '\'' +
                    ", arriveCityCode='" + arriveCityCode + '\'' +
                    ", arriveCityName='" + arriveCityName + '\'' +
                    ", arriveDate='" + arriveDate + '\'' +
                    ", arriveTime='" + arriveTime + '\'' +
                    ", arriveAirportCode='" + arriveAirportCode + '\'' +
                    ", arriveAirportName='" + arriveAirportName + '\'' +
                    ", arriveAirportTerminal='" + arriveAirportTerminal + '\'' +
                    ", crossDay=" + crossDay +
                    ", craftType='" + craftType + '\'' +
                    ", airlineCode='" + airlineCode + '\'' +
                    ", airlineName='" + airlineName + '\'' +
                    ", duration='" + duration + '\'' +
                    ", isShare=" + isShare +
                    ", shareFlightNo=" + shareFlightNo +
                    ", shareAirline=" + shareAirline +
                    ", shareAirlineName=" + shareAirlineName +
                    ", mealFlag=" + mealFlag +
                    ", meal='" + meal + '\'' +
                    ", stopInformation=" + stopInformation +
                    ", stopNum='" + stopNum + '\'' +
                    ", onTimeRate='" + onTimeRate + '\'' +
                    ", stopPointList=" + stopPointList +
                    '}';
        }

        /**
         * flightNo : CZ3101
         * departDate : 2020-06-10
         * departTime : 08:00
         * departCityCode : CAN
         * departCityName : 广州
         * departAirportCode : CAN
         * departAirportName : 白云机场
         * departAirportTerminal : T2
         * arriveCityCode : BJS
         * arriveCityName : 北京
         * arriveDate : 2020-06-10
         * arriveTime : 11:10
         * arriveAirportCode : PEK
         * arriveAirportName : 首都机场
         * arriveAirportTerminal : T2
         * crossDay : 0
         * craftType : 788(大)
         * airlineCode : CZ
         * airlineName : 中国南方航空
         * duration : 03:10
         * isShare : 0
         * shareFlightNo : null
         * shareAirline : null
         * shareAirlineName : null
         * mealFlag : 0
         * meal : 无餐
         * stopInformation : null
         * stopNum : 0
         * onTimeRate : 97%
         * stopPointList : null
         */

        private String flightNo;
        private String departDate;
        private String departTime;
        private String departCityCode;
        private String departCityName;
        private String departAirportCode;
        private String departAirportName;
        private String departAirportTerminal;
        private String arriveCityCode;
        private String arriveCityName;
        private String arriveDate;
        private String arriveTime;
        private String arriveAirportCode;
        private String arriveAirportName;
        private String arriveAirportTerminal;
        private int crossDay;
        private String craftType;
        private String airlineCode;
        private String airlineName;
        private String duration;
        private int isShare;
        private String shareFlightNo;
        private String shareAirline;
        private String shareAirlineName;
        private int mealFlag;
        private String meal;
        private String stopInformation;
        private String stopNum;
        private String onTimeRate;
        private List<StopPointListBean> stopPointList;

        public String getFlightNo() {
            return flightNo;
        }

        public void setFlightNo(String flightNo) {
            this.flightNo = flightNo;
        }

        public String getDepartDate() {
            return departDate;
        }

        public void setDepartDate(String departDate) {
            this.departDate = departDate;
        }

        public String getDepartTime() {
            return departTime;
        }

        public void setDepartTime(String departTime) {
            this.departTime = departTime;
        }

        public String getDepartCityCode() {
            return departCityCode;
        }

        public void setDepartCityCode(String departCityCode) {
            this.departCityCode = departCityCode;
        }

        public String getDepartCityName() {
            return departCityName;
        }

        public void setDepartCityName(String departCityName) {
            this.departCityName = departCityName;
        }

        public String getDepartAirportCode() {
            return departAirportCode;
        }

        public void setDepartAirportCode(String departAirportCode) {
            this.departAirportCode = departAirportCode;
        }

        public String getDepartAirportName() {
            return departAirportName;
        }

        public void setDepartAirportName(String departAirportName) {
            this.departAirportName = departAirportName;
        }

        public String getDepartAirportTerminal() {
            return departAirportTerminal;
        }

        public void setDepartAirportTerminal(String departAirportTerminal) {
            this.departAirportTerminal = departAirportTerminal;
        }

        public String getArriveCityCode() {
            return arriveCityCode;
        }

        public void setArriveCityCode(String arriveCityCode) {
            this.arriveCityCode = arriveCityCode;
        }

        public String getArriveCityName() {
            return arriveCityName;
        }

        public void setArriveCityName(String arriveCityName) {
            this.arriveCityName = arriveCityName;
        }

        public String getArriveDate() {
            return arriveDate;
        }

        public void setArriveDate(String arriveDate) {
            this.arriveDate = arriveDate;
        }

        public String getArriveTime() {
            return arriveTime;
        }

        public void setArriveTime(String arriveTime) {
            this.arriveTime = arriveTime;
        }

        public String getArriveAirportCode() {
            return arriveAirportCode;
        }

        public void setArriveAirportCode(String arriveAirportCode) {
            this.arriveAirportCode = arriveAirportCode;
        }

        public String getArriveAirportName() {
            return arriveAirportName;
        }

        public void setArriveAirportName(String arriveAirportName) {
            this.arriveAirportName = arriveAirportName;
        }

        public String getArriveAirportTerminal() {
            return arriveAirportTerminal;
        }

        public void setArriveAirportTerminal(String arriveAirportTerminal) {
            this.arriveAirportTerminal = arriveAirportTerminal;
        }

        public int getCrossDay() {
            return crossDay;
        }

        public void setCrossDay(int crossDay) {
            this.crossDay = crossDay;
        }

        public String getCraftType() {
            return craftType;
        }

        public void setCraftType(String craftType) {
            this.craftType = craftType;
        }

        public String getAirlineCode() {
            return airlineCode;
        }

        public void setAirlineCode(String airlineCode) {
            this.airlineCode = airlineCode;
        }

        public String getAirlineName() {
            return airlineName;
        }

        public void setAirlineName(String airlineName) {
            this.airlineName = airlineName;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public int getIsShare() {
            return isShare;
        }

        public void setIsShare(int isShare) {
            this.isShare = isShare;
        }

        public String getShareFlightNo() {
            return shareFlightNo;
        }

        public void setShareFlightNo(String shareFlightNo) {
            this.shareFlightNo = shareFlightNo;
        }

        public String getShareAirline() {
            return shareAirline;
        }

        public void setShareAirline(String shareAirline) {
            this.shareAirline = shareAirline;
        }

        public String getShareAirlineName() {
            return shareAirlineName;
        }

        public void setShareAirlineName(String shareAirlineName) {
            this.shareAirlineName = shareAirlineName;
        }

        public int getMealFlag() {
            return mealFlag;
        }

        public void setMealFlag(int mealFlag) {
            this.mealFlag = mealFlag;
        }

        public String getMeal() {
            return meal;
        }

        public void setMeal(String meal) {
            this.meal = meal;
        }

        public String getStopInformation() {
            return stopInformation;
        }

        public void setStopInformation(String stopInformation) {
            this.stopInformation = stopInformation;
        }

        public String getStopNum() {
            return stopNum;
        }

        public void setStopNum(String stopNum) {
            this.stopNum = stopNum;
        }

        public String getOnTimeRate() {
            return onTimeRate;
        }

        public void setOnTimeRate(String onTimeRate) {
            this.onTimeRate = onTimeRate;
        }

        public List<StopPointListBean> getStopPointList() {
            return stopPointList;
        }

        public void setStopPointList(List<StopPointListBean> stopPointList) {
            this.stopPointList = stopPointList;
        }

        public static class StopPointListBean {
            @Override
            public String toString() {
                return "{" +
                        "stopCityCode='" + stopCityCode + '\'' +
                        ", stopCityName='" + stopCityName + '\'' +
                        ", airPortCode='" + airPortCode + '\'' +
                        ", airPortName='" + airPortName + '\'' +
                        ", arrivalTime='" + arrivalTime + '\'' +
                        ", departrueTime='" + departrueTime + '\'' +
                        '}';
            }

            /**
             * stopCityCode : WDS
             * stopCityName : 十堰
             * airPortCode : WDS
             * airPortName : 武当山机场
             * arrivalTime : 09: 50
             * departrueTime : 10: 35
             */


            private String stopCityCode;
            private String stopCityName;
            private String airPortCode;
            private String airPortName;
            private String arrivalTime;
            private String departrueTime;

            public String getStopCityCode() {
                return stopCityCode;
            }

            public void setStopCityCode(String stopCityCode) {
                this.stopCityCode = stopCityCode;
            }

            public String getStopCityName() {
                return stopCityName;
            }

            public void setStopCityName(String stopCityName) {
                this.stopCityName = stopCityName;
            }

            public String getAirPortCode() {
                return airPortCode;
            }

            public void setAirPortCode(String airPortCode) {
                this.airPortCode = airPortCode;
            }

            public String getAirPortName() {
                return airPortName;
            }

            public void setAirPortName(String airPortName) {
                this.airPortName = airPortName;
            }

            public String getArrivalTime() {
                return arrivalTime;
            }

            public void setArrivalTime(String arrivalTime) {
                this.arrivalTime = arrivalTime;
            }

            public String getDepartrueTime() {
                return departrueTime;
            }

            public void setDepartrueTime(String departrueTime) {
                this.departrueTime = departrueTime;
            }
        }
    }

    public static class CabinBean {
        /**
         * sessionId : e39ced21e28a4ca6a3529c3eda90cf3f
         * cabinClass : Y
         * cabinClassName : 经济舱
         * cabinCode : R
         * cabinName : 经济舱
         * cabinClassFullPrice : 2540
         * seatNum : >9
         * cabinPrice : 480
         * discount : 1.8
         * billType : 1
         * freeLuggage : 20
         * freeLuggageUnit : KG
         * overloadLuggage :
         * priceInfo : {"adult":{"price":480,"tax":50,"fuel":0,"facePrice":480,"addPrice":0,"orgPrice":480},"child":{"price":480,"tax":0,"fuel":0,"facePrice":480,"addPrice":0,"orgPrice":480},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}}
         * time : {"type":1,"minutes":45}
         */

        private String sessionId;
        private String cabinClass;
        private String cabinClassName;
        private String cabinCode;
        private String cabinName;
        private int cabinClassFullPrice;
        private String seatNum;
        private int cabinPrice;
        private double discount;
        private int billType;
        private int freeLuggage;
        private String freeLuggageUnit;
        private String overloadLuggage;
        private PriceInfoBean priceInfo;
        private TimeBean time;

        @Override
        public String toString() {
            return "{" +
                    "sessionId='" + sessionId + '\'' +
                    ", cabinClass='" + cabinClass + '\'' +
                    ", cabinClassName='" + cabinClassName + '\'' +
                    ", cabinCode='" + cabinCode + '\'' +
                    ", cabinName='" + cabinName + '\'' +
                    ", cabinClassFullPrice=" + cabinClassFullPrice +
                    ", seatNum='" + seatNum + '\'' +
                    ", cabinPrice=" + cabinPrice +
                    ", discount=" + discount +
                    ", billType=" + billType +
                    ", freeLuggage=" + freeLuggage +
                    ", freeLuggageUnit='" + freeLuggageUnit + '\'' +
                    ", overloadLuggage='" + overloadLuggage + '\'' +
                    ", priceInfo=" + priceInfo +
                    ", time=" + time +
                    '}';
        }

        public String getSessionId() {
            return sessionId;
        }

        public void setSessionId(String sessionId) {
            this.sessionId = sessionId;
        }

        public String getCabinClass() {
            return cabinClass;
        }

        public void setCabinClass(String cabinClass) {
            this.cabinClass = cabinClass;
        }

        public String getCabinClassName() {
            return cabinClassName;
        }

        public void setCabinClassName(String cabinClassName) {
            this.cabinClassName = cabinClassName;
        }

        public String getCabinCode() {
            return cabinCode;
        }

        public void setCabinCode(String cabinCode) {
            this.cabinCode = cabinCode;
        }

        public String getCabinName() {
            return cabinName;
        }

        public void setCabinName(String cabinName) {
            this.cabinName = cabinName;
        }

        public int getCabinClassFullPrice() {
            return cabinClassFullPrice;
        }

        public void setCabinClassFullPrice(int cabinClassFullPrice) {
            this.cabinClassFullPrice = cabinClassFullPrice;
        }

        public String getSeatNum() {
            return seatNum;
        }

        public void setSeatNum(String seatNum) {
            this.seatNum = seatNum;
        }

        public int getCabinPrice() {
            return cabinPrice;
        }

        public void setCabinPrice(int cabinPrice) {
            this.cabinPrice = cabinPrice;
        }

        public double getDiscount() {
            return discount;
        }

        public void setDiscount(double discount) {
            this.discount = discount;
        }

        public int getBillType() {
            return billType;
        }

        public void setBillType(int billType) {
            this.billType = billType;
        }

        public int getFreeLuggage() {
            return freeLuggage;
        }

        public void setFreeLuggage(int freeLuggage) {
            this.freeLuggage = freeLuggage;
        }

        public String getFreeLuggageUnit() {
            return freeLuggageUnit;
        }

        public void setFreeLuggageUnit(String freeLuggageUnit) {
            this.freeLuggageUnit = freeLuggageUnit;
        }

        public String getOverloadLuggage() {
            return overloadLuggage;
        }

        public void setOverloadLuggage(String overloadLuggage) {
            this.overloadLuggage = overloadLuggage;
        }

        public PriceInfoBean getPriceInfo() {
            return priceInfo;
        }

        public void setPriceInfo(PriceInfoBean priceInfo) {
            this.priceInfo = priceInfo;
        }

        public TimeBean getTime() {
            return time;
        }

        public void setTime(TimeBean time) {
            this.time = time;
        }

        public static class PriceInfoBean {
            @Override
            public String toString() {
                return "{" +
                        "adult=" + adult.toString() +
                        ", child=" + child.toString() +
                        ", baby=" + baby.toString() +
                        '}';
            }

            /**
             * adult : {"price":480,"tax":50,"fuel":0,"facePrice":480,"addPrice":0,"orgPrice":480}
             * child : {"price":480,"tax":0,"fuel":0,"facePrice":480,"addPrice":0,"orgPrice":480}
             * baby : {"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}
             */

            private AdultBean adult;
            private ChildBean child;
            private BabyBean baby;

            public AdultBean getAdult() {
                return adult;
            }

            public void setAdult(AdultBean adult) {
                this.adult = adult;
            }

            public ChildBean getChild() {
                return child;
            }

            public void setChild(ChildBean child) {
                this.child = child;
            }

            public BabyBean getBaby() {
                return baby;
            }

            public void setBaby(BabyBean baby) {
                this.baby = baby;
            }

            public static class AdultBean {
                @Override
                public String toString() {
                    return "{" +
                            "price=" + price +
                            ", tax=" + tax +
                            ", fuel=" + fuel +
                            ", facePrice=" + facePrice +
                            ", addPrice=" + addPrice +
                            ", orgPrice=" + orgPrice +
                            '}';
                }

                /**
                 * price : 480
                 * tax : 50
                 * fuel : 0
                 * facePrice : 480
                 * addPrice : 0
                 * orgPrice : 480
                 */

                private int price;
                private int tax;
                private int fuel;
                private int facePrice;
                private int addPrice;
                private int orgPrice;

                public int getPrice() {
                    return price;
                }

                public void setPrice(int price) {
                    this.price = price;
                }

                public int getTax() {
                    return tax;
                }

                public void setTax(int tax) {
                    this.tax = tax;
                }

                public int getFuel() {
                    return fuel;
                }

                public void setFuel(int fuel) {
                    this.fuel = fuel;
                }

                public int getFacePrice() {
                    return facePrice;
                }

                public void setFacePrice(int facePrice) {
                    this.facePrice = facePrice;
                }

                public int getAddPrice() {
                    return addPrice;
                }

                public void setAddPrice(int addPrice) {
                    this.addPrice = addPrice;
                }

                public int getOrgPrice() {
                    return orgPrice;
                }

                public void setOrgPrice(int orgPrice) {
                    this.orgPrice = orgPrice;
                }
            }

            public static class ChildBean {
                @Override
                public String toString() {
                    return "{" +
                            "price=" + price +
                            ", tax=" + tax +
                            ", fuel=" + fuel +
                            ", facePrice=" + facePrice +
                            ", addPrice=" + addPrice +
                            ", orgPrice=" + orgPrice +
                            '}';
                }

                /**
                 * price : 480
                 * tax : 0
                 * fuel : 0
                 * facePrice : 480
                 * addPrice : 0
                 * orgPrice : 480
                 */

                private int price;
                private int tax;
                private int fuel;
                private int facePrice;
                private int addPrice;
                private int orgPrice;

                public int getPrice() {
                    return price;
                }

                public void setPrice(int price) {
                    this.price = price;
                }

                public int getTax() {
                    return tax;
                }

                public void setTax(int tax) {
                    this.tax = tax;
                }

                public int getFuel() {
                    return fuel;
                }

                public void setFuel(int fuel) {
                    this.fuel = fuel;
                }

                public int getFacePrice() {
                    return facePrice;
                }

                public void setFacePrice(int facePrice) {
                    this.facePrice = facePrice;
                }

                public int getAddPrice() {
                    return addPrice;
                }

                public void setAddPrice(int addPrice) {
                    this.addPrice = addPrice;
                }

                public int getOrgPrice() {
                    return orgPrice;
                }

                public void setOrgPrice(int orgPrice) {
                    this.orgPrice = orgPrice;
                }
            }

            public static class BabyBean {
                @Override
                public String toString() {
                    return "{" +
                            "price=" + price +
                            ", tax=" + tax +
                            ", fuel=" + fuel +
                            ", facePrice=" + facePrice +
                            ", addPrice=" + addPrice +
                            ", orgPrice=" + orgPrice +
                            '}';
                }

                /**
                 * price : 0
                 * tax : 0
                 * fuel : 0
                 * facePrice : 0
                 * addPrice : 0
                 * orgPrice : 0
                 */

                private int price;
                private int tax;
                private int fuel;
                private int facePrice;
                private int addPrice;
                private int orgPrice;

                public int getPrice() {
                    return price;
                }

                public void setPrice(int price) {
                    this.price = price;
                }

                public int getTax() {
                    return tax;
                }

                public void setTax(int tax) {
                    this.tax = tax;
                }

                public int getFuel() {
                    return fuel;
                }

                public void setFuel(int fuel) {
                    this.fuel = fuel;
                }

                public int getFacePrice() {
                    return facePrice;
                }

                public void setFacePrice(int facePrice) {
                    this.facePrice = facePrice;
                }

                public int getAddPrice() {
                    return addPrice;
                }

                public void setAddPrice(int addPrice) {
                    this.addPrice = addPrice;
                }

                public int getOrgPrice() {
                    return orgPrice;
                }

                public void setOrgPrice(int orgPrice) {
                    this.orgPrice = orgPrice;
                }
            }
        }

        public static class TimeBean {
            @Override
            public String toString() {
                return "{" +
                        "type=" + type +
                        ", minutes=" + minutes +
                        '}';
            }

            /**
             * type : 1
             * minutes : 45
             */

            private int type;
            private int minutes;

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }

            public int getMinutes() {
                return minutes;
            }

            public void setMinutes(int minutes) {
                this.minutes = minutes;
            }
        }
    }

    public static class InvicesBean {
        @Override
        public String toString() {
            return "{" +
                    "taxpayeridentity='" + taxpayeridentity + '\'' +
                    ", contactertel='" + contactertel + '\'' +
                    ", contacter='" + contacter + '\'' +
                    ", billtype=" + billtype +
                    ", title='" + title + '\'' +
                    ", personalorenterprise=" + personalorenterprise +
                    '}';
        }

        /**
         * taxpayeridentity :
         * contactertel :
         * contacter :
         * billtype : 0
         * title :
         * personalorenterprise : 1
         */

        private String taxpayeridentity;
        private String contactertel;
        private String contacter;
        private int billtype;
        private String title;
        private int personalorenterprise;

        public String getTaxpayeridentity() {
            return taxpayeridentity;
        }

        public void setTaxpayeridentity(String taxpayeridentity) {
            this.taxpayeridentity = taxpayeridentity;
        }

        public String getContactertel() {
            return contactertel;
        }

        public void setContactertel(String contactertel) {
            this.contactertel = contactertel;
        }

        public String getContacter() {
            return contacter;
        }

        public void setContacter(String contacter) {
            this.contacter = contacter;
        }

        public int getBilltype() {
            return billtype;
        }

        public void setBilltype(int billtype) {
            this.billtype = billtype;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getPersonalorenterprise() {
            return personalorenterprise;
        }

        public void setPersonalorenterprise(int personalorenterprise) {
            this.personalorenterprise = personalorenterprise;
        }
    }
}
