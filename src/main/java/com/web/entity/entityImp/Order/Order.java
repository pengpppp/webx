package com.web.entity.entityImp.Order;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.web.entity.Homeorder;
import com.web.entity.Insurance;
import com.web.entity.Passenger;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Order {

    private String id;
    private String contactname;
    private String intlcode;
    private SaveOrder.DeliveryBean delivery = new SaveOrder.DeliveryBean();
    private String remark;
    private String sessionid;
    private String contactmobile;
    private int ordersource;
    private SaveOrder.FlightBean flight;
    private SaveOrder.CabinBean cabin;
    private List<Passenger> passengers;
    private List<Insurance> insurances;
    private List<SaveOrder.InvicesBean> invices= new ArrayList<>();
    private BigDecimal price;
    private String payment;
    private String creattime;
    private String nowtime;
    private String paytime;

    public String getPaytime() {
        return paytime;
    }

    public void setPaytime(String paytime) {
        this.paytime = paytime;
    }

    public String getNowtime() {
        return nowtime;
    }

    public void setNowtime(String nowtime) {
        this.nowtime = nowtime;
    }

    public String getCreattime() {
        return creattime;
    }

    public void setCreattime(String creattime) {
        this.creattime = creattime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getIntlcode() {
        return intlcode;
    }

    public void setIntlcode(String intlcode) {
        this.intlcode = intlcode;
    }

    public SaveOrder.DeliveryBean getDelivery() {
        return delivery;
    }

    public void setDelivery(SaveOrder.DeliveryBean delivery) {
        this.delivery = delivery;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getContactmobile() {
        return contactmobile;
    }

    public void setContactmobile(String contactmobile) {
        this.contactmobile = contactmobile;
    }

    public int getOrdersource() {
        return ordersource;
    }

    public void setOrdersource(int ordersource) {
        this.ordersource = ordersource;
    }

    public SaveOrder.FlightBean getFlight() {
        return flight;
    }

    public void setFlight(SaveOrder.FlightBean flight) {
        this.flight = flight;
    }

    public SaveOrder.CabinBean getCabin() {
        return cabin;
    }

    public void setCabin(SaveOrder.CabinBean cabin) {
        this.cabin = cabin;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    public List<Insurance> getInsurances() {
        return insurances;
    }

    public void setInsurances(List<Insurance> insurances) {
        this.insurances = insurances;
    }

    public List<SaveOrder.InvicesBean> getInvices() {
        return invices;
    }

    public void setInvices(List<SaveOrder.InvicesBean> invices) {
        this.invices = invices;
    }


    public Order initOrder(Homeorder homeorder){
        Gson gson = new Gson();
        Order order = new Order();
        order.setId(homeorder.getId());
        order.setSessionid(homeorder.getSessionid());
        order.setContactname(homeorder.getContactname());
        order.setContactmobile(homeorder.getContactmobile());
        order.setOrdersource(homeorder.getOrdersource());
        order.setDelivery(gson.fromJson(homeorder.getDelivery(), SaveOrder.DeliveryBean.class));
        order.setFlight(gson.fromJson(homeorder.getFlight(), SaveOrder.FlightBean.class));
        order.setInvices(gson.fromJson(homeorder.getInvices(), new TypeToken<List< SaveOrder.InvicesBean >>(){}.getType()));
        order.setRemark(homeorder.getRemark());
        order.setPrice(BigDecimal.valueOf(homeorder.getPrice()));
        order.setPayment(homeorder.getPayment());
        order.setCabin(gson.fromJson(homeorder.getCabin(), SaveOrder.CabinBean.class));
        order.setCreattime(String.valueOf(homeorder.getCreattime()));
        order.setNowtime(String.valueOf(new Date().getTime()));
        return order;
    };

    @Override
    public String toString() {
        return "Order{" +
                "contactname='" + contactname + '\'' +
                ", intlcode='" + intlcode + '\'' +
                ", delivery=" + delivery +
                ", remark='" + remark + '\'' +
                ", sessionid='" + sessionid + '\'' +
                ", contactmobile='" + contactmobile + '\'' +
                ", ordersource=" + ordersource +
                ", flight=" + flight +
                ", cabin=" + cabin +
                ", passengers=" + passengers +
                ", insurances=" + insurances +
                ", invices=" + invices +
                '}';
    }
}
