package com.web.entity.entityImp.Visa;

import com.web.entity.Visaorder;

public class VisaOrderList {
    /**
     * 本订单的ID
     */
    private String orderid;

    /**
     * visa项目详情
     */
    private VisaList visaList;

    private Double price;

    private String payment;

    private Long creattime;

    private Long paytime;


    public VisaOrderList()
    {
    }

    public static VisaOrderList VisaOrder2VisaOrderList (Visaorder visaorder)
    {
        VisaOrderList visaOrderList =new VisaOrderList();
        visaOrderList.setOrderid(visaorder.getOrderid());
        visaOrderList.setPrice(visaorder.getPrice());
        visaOrderList.setPayment(visaorder.getPayment());
        visaOrderList.setCreattime(visaorder.getCreattime());
        visaOrderList.setPaytime(visaorder.getPaytime());
        return visaOrderList;
    }


    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public VisaList getVisaList() {
        return visaList;
    }

    public void setVisaList(VisaList visaList) {
        this.visaList = visaList;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getCreattime() {
        return String.valueOf(creattime);
    }

    public void setCreattime(Long creattime) {
        this.creattime = creattime;
    }

    public String getPaytime() {
        return String.valueOf(paytime);
    }

    public void setPaytime(Long paytime) {
        this.paytime = paytime;
    }
}
