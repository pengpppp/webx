package com.web.entity.entityImp.Visa;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.web.entity.Passenger;
import com.web.entity.Visaorder;

import java.util.List;

public class VisaOrderDetailsImp {
    /**
     * visa项目详情未转  用户手机号  用户名
     * @param visaorder
     * @return
     */
    public static VisaOrderDetailsImp visaOrder2visaOrderDetails(Visaorder visaorder)
    {
        VisaOrderDetailsImp visaOrderDetails = new VisaOrderDetailsImp();
        Gson gson = new Gson();
        try {
            visaOrderDetails.setOrderid(visaorder.getOrderid());
            visaOrderDetails.setId(visaorder.getId());
            visaOrderDetails.setDeparturetime(visaorder.getDeparturetime());
            visaOrderDetails.setDeliverytype(visaorder.getDeliverytype());
            visaOrderDetails.setDeliverypath(visaorder.getDeliverypath());
            visaOrderDetails.setContactname(visaorder.getContactname());
            visaOrderDetails.setContactmobile(visaorder.getContactmobile());
            visaOrderDetails.setContactemail(visaorder.getContactemail());
            visaOrderDetails.setPrice(visaorder.getPrice());
            visaOrderDetails.setPayment(visaorder.getPayment());
            visaOrderDetails.setCreattime(visaorder.getCreattime());
            visaOrderDetails.setPayment(visaorder.getPayment());
            visaOrderDetails.setInsurances(gson.fromJson(visaorder.getInsurances(),new TypeToken<List<VisaInsuranceImp>>(){}.getType()));
            visaOrderDetails.setApplicant(gson.fromJson(visaorder.getApplicant(),new TypeToken<List<VisaOrderPassenger>>(){}.getType()));
            visaOrderDetails.setUserid(visaorder.getUserid());
        }catch (Exception e)
        {
        }
        return visaOrderDetails;
    }
    private String userid;
    private String mobile;
    private String username;

    /**
     * 本订单的ID
     */
    private String orderid;
    /**
     * 签证项目的id
     */
    private String id;

    /**
     * visa项目详情
     */
    private VisaDetails visaDetails;

    /**
     * 预计出发时间
     */
    private Long departuretime;
    /**
     * 申请人列表
     */
    private List<Passenger> applicant;
    /**
     * 快递返还方式
     */
    private String deliverytype;
    /**
     * 配送地址
     */
    private String deliverypath;
    /**
     * 联系人名
     */
    private String contactname;
    /**
     * 联系人手机号码
     */
    private String contactmobile;
    /**
     * 联系人邮件地址
     */
    private String contactemail;
    /**
     * 保险ID列表
     */
    private List<VisaInsuranceImp> insurances;

    private Double price;

    private String payment;

    private Long creattime;

    private Long paytime;


    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public VisaDetails getVisaDetails() {
        return visaDetails;
    }

    public void setVisaDetails(VisaDetails visaDetails) {
        this.visaDetails = visaDetails;
    }

    public String getDeparturetime() {
        return String.valueOf(departuretime);
    }

    public void setDeparturetime(Long departuretime) {
        this.departuretime = departuretime;
    }

    public List<Passenger> getApplicant() {
        return applicant;
    }

    public void setApplicant(List<Passenger> applicant) {
        this.applicant = applicant;
    }

    public String getDeliverytype() {
        return deliverytype;
    }

    public void setDeliverytype(String deliverytype) {
        this.deliverytype = deliverytype;
    }

    public String getDeliverypath() {
        return deliverypath;
    }

    public void setDeliverypath(String deliverypath) {
        this.deliverypath = deliverypath;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getContactmobile() {
        return contactmobile;
    }

    public void setContactmobile(String contactmobile) {
        this.contactmobile = contactmobile;
    }

    public String getContactemail() {
        return contactemail;
    }

    public void setContactemail(String contactemail) {
        this.contactemail = contactemail;
    }

    public List<VisaInsuranceImp> getInsurances() {
        return insurances;
    }

    public void setInsurances(List<VisaInsuranceImp> insurances) {
        this.insurances = insurances;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getCreattime() {
        return String.valueOf(creattime);
    }

    public void setCreattime(Long creattime) {
        this.creattime = creattime;
    }

    public String  getPaytime() {
        return String.valueOf(paytime);
    }

    public void setPaytime(Long paytime) {
        this.paytime = paytime;
    }
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
