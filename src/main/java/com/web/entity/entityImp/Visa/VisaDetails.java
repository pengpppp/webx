package com.web.entity.entityImp.Visa;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.web.entity.Visa;

import java.util.List;

public class VisaDetails {
    /**
     * 订单ID  随机生成
     */
    private String id;
    /**
     * 该项目是否有效显示到前端
     */
    private String key;
    /**
     * 订单顶图
     */
    private String imageurl;
    /**
     * 订单名字
     */
    private String name;
    /**
     * 订单基础价格
     */
    private Double cost;
    /**
     * 订单价格说明
     */
    private String remark;
    /**
     * 订单标签，list[String]
     */
    private List<String> tittle;
    /**
     * 入境次数
     */
    private String entrynumb;
    /**
     * 可停留天数（文本）
     */
    private String stayday;
    /**
     * 签证类型
     */
    private String visatype;
    /**
     * 有效期
     */
    private String term;
    /**
     * 最早出发日期
     */
    private Long departuredate;
    /**
     * 办理时长
     */
    private String handingtime;
    /**
     * 受理范围
     */
    private String acceptancescope;
    /**
     * 注意事项
     */
    private String note;
    /**
     * 保险ID列表
     */
    private List<VisaInsuranceImp> insurance;
    private String express;

    private List<String> acceptType;

    /**
     * 缺保险实体列表未转
     * @param visa
     * @return
     */
    public static VisaDetails visa2visadetails(Visa visa)
    {
        Gson gson = new Gson();
        VisaDetails visaDetails = new VisaDetails();
        String s = visa.getTittle();
        String s1 = visa.getAcceptType();
        visa.setInsurance(null);
        visa.setTittle(null);
        visa.setAcceptType(null);
        visaDetails=gson.fromJson(gson.toJson(visa),VisaDetails.class);
        visaDetails.setTittle(gson.fromJson(s,new TypeToken<List<String>>(){}.getType()));
        visaDetails.setAcceptType(gson.fromJson(s1,new TypeToken<List<String>>(){}.getType()));
        return visaDetails;
    }

    public String getExpress() {
        return express;
    }

    public void setExpress(String express) {
        this.express = express;
    }

    public List<String> getAcceptType() {
        return acceptType;
    }

    public void setAcceptType(List<String> acceptType) {
        this.acceptType = acceptType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<String> getTittle() {
        return tittle;
    }

    public void setTittle(List<String> tittle) {
        this.tittle = tittle;
    }

    public String getEntrynumb() {
        return entrynumb;
    }

    public void setEntrynumb(String entrynumb) {
        this.entrynumb = entrynumb;
    }

    public String getStayday() {
        return stayday;
    }

    public void setStayday(String stayday) {
        this.stayday = stayday;
    }

    public String getVisatype() {
        return visatype;
    }

    public void setVisatype(String visatype) {
        this.visatype = visatype;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getDeparturedate() {
        return String.valueOf(departuredate);
    }

    public void setDeparturedate(Long departuredate) {
        this.departuredate = departuredate;
    }

    public String getHandingtime() {
        return handingtime;
    }

    public void setHandingtime(String handingtime) {
        this.handingtime = handingtime;
    }

    public String getAcceptancescope() {
        return acceptancescope;
    }

    public void setAcceptancescope(String acceptancescope) {
        this.acceptancescope = acceptancescope;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<VisaInsuranceImp> getInsurance() {
        return insurance;
    }

    public void setInsurance(List<VisaInsuranceImp> insurance) {
        this.insurance = insurance;
    }
}
