package com.web.entity.entityImp.Visa;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.web.entity.Visainsurance;

import java.util.List;

public class VisaInsuranceImp {

    /**
     * 保险ID
     */
    private String id;

    private String key;
    /**
     * 保险名称
     */
    private String name;
    /**
     * 保险标签
     */
    private List<String> tittle;
    /**
     * 保险价格
     */
    private Double cost;

    public static Visainsurance VisaInsuranceImp2VisaInsuranceInsert(VisaInsuranceImp visaInsuranceImp)
    {
        Visainsurance visainsurance = new Visainsurance();
        visainsurance.setName(visaInsuranceImp.getName());
        visainsurance.setCost(visaInsuranceImp.getCost());
        try {
            visainsurance.setTittle(visaInsuranceImp.getTittle().toString());
        }catch (Exception e)
        { }
        return visainsurance;
    }
    public static Visainsurance VisaInsuranceImp2VisaInsuranceUpdate(VisaInsuranceImp visaInsuranceImp)
    {
        Visainsurance visainsurance = new Visainsurance();
        visainsurance.setKey(visaInsuranceImp.getKey());
        visainsurance.setId(visaInsuranceImp.getId());
        visainsurance.setName(visaInsuranceImp.getName());
        visainsurance.setCost(visaInsuranceImp.getCost());
        try {
        visainsurance.setTittle(visaInsuranceImp.getTittle().toString());
        }catch (Exception e)
        { }
        return visainsurance;
    }
    public static VisaInsuranceImp VisaInsurance2VisaInsuranceImp(Visainsurance visainsurance)
    {
        Gson gson = new Gson();
        VisaInsuranceImp visaInsuranceImp = new VisaInsuranceImp();
        visaInsuranceImp.setId(visainsurance.getId());
        visaInsuranceImp.setCost(visainsurance.getCost());
        visaInsuranceImp.setKey(visainsurance.getKey());
        visaInsuranceImp.setName(visainsurance.getName());
        try {
            visaInsuranceImp.setTittle(gson.fromJson(visainsurance.getTittle(), new TypeToken<List<String>>(){}.getType()));
        }catch (Exception E)
        {
        }

        return visaInsuranceImp;
    }

    @Override
    public String toString() {
        return "{" +
                "id='" + id + '\'' +
                ", key='" + key + '\'' +
                ", name='" + name + '\'' +
                ", tittle=" + tittle +
                ", cost=" + cost +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getTittle() {
        return tittle;
    }

    public void setTittle(List<String> tittle) {
        this.tittle = tittle;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }
}
