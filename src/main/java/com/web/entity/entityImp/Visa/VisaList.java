package com.web.entity.entityImp.Visa;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.web.entity.Visa;

import java.util.List;

public class VisaList {
    /**
     * 订单ID  随机生成
     */
    private String id;
    /**
     * 订单顶图
     */
    private String imageurl;
    /**
     * 订单名字
     */
    private String name;
    /**
     * 订单基础价格
     */
    private Double cost;
    /**
     * 订单标签，list[String]
     */
    private List<String> tittle;

    public static VisaList visa2visaList(Visa visa)
    {
        VisaList visaList = new VisaList();
        Gson gson = new Gson();
        visaList.setId(visa.getId());
        visaList.setCost(visa.getCost());
        visaList.setImageurl(visa.getImageurl());
        visaList.setName(visa.getName());
        visaList.setTittle(gson.fromJson(visa.getTittle(),new TypeToken<List<String>>(){}.getType()));
        return visaList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public List<String> getTittle() {
        return tittle;
    }

    public void setTittle(List<String> tittle) {
        this.tittle = tittle;
    }
}
