package com.web.entity.entityImp.Visa;

import com.web.entity.Passenger;

public class VisaOrderPassenger {
    private Integer id;
    private String passengername;

    public static VisaOrderPassenger passenger2visaOrderpassenger(Passenger passenger)
    {
        VisaOrderPassenger visaOrderPassenger = new VisaOrderPassenger();
        visaOrderPassenger.setId(passenger.getId());
        visaOrderPassenger.setPassengername(passenger.getPassengername());
        return visaOrderPassenger;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", passengername='" + passengername + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPassengername() {
        return passengername;
    }

    public void setPassengername(String passengername) {
        this.passengername = passengername;
    }
}
