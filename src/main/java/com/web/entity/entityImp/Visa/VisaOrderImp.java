package com.web.entity.entityImp.Visa;

import java.util.List;

public class VisaOrderImp {
    /**
     * 本订单的ID
     */
    private String orderid;
    /**
     * 签证项目的id
     */
    private String id;
    /**
     * 预计出发时间
     */
    private Long departuretime;
    /**
     * 申请人列表
     */
    private List<String> applicant;
    /**
     * 快递返还方式
     */
    private String deliverytype;
    /**
     * 配送地址
     */
    private String deliverypath;
    /**
     * 联系人名
     */
    private String contactname;
    /**
     * 联系人手机号码
     */
    private String contactmobile;
    /**
     * 联系人邮件地址
     */
    private String contactemail;
    /**
     * 保险ID列表
     */
    private List<String> insurances;



    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getDeparturetime() {
        return departuretime;
    }

    public void setDeparturetime(Long departuretime) {
        this.departuretime = departuretime;
    }

    public List<String> getApplicant() {
        return applicant;
    }

    public void setApplicant(List<String> applicant) {
        this.applicant = applicant;
    }

    public String getDeliverytype() {
        return deliverytype;
    }

    public void setDeliverytype(String deliverytype) {
        this.deliverytype = deliverytype;
    }

    public String getDeliverypath() {
        return deliverypath;
    }

    public void setDeliverypath(String deliverypath) {
        this.deliverypath = deliverypath;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getContactmobile() {
        return contactmobile;
    }

    public void setContactmobile(String contactmobile) {
        this.contactmobile = contactmobile;
    }

    public String getContactemail() {
        return contactemail;
    }

    public void setContactemail(String contactemail) {
        this.contactemail = contactemail;
    }

    public List<String> getInsurances() {
        return insurances;
    }

    public void setInsurances(List<String> insurances) {
        this.insurances = insurances;
    }
}
