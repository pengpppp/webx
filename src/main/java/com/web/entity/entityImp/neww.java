package com.web.entity.entityImp;

import java.util.ArrayList;
import java.util.List;

public class neww {
    private String channel;
    private int num;
    private List list;

    public neww()
    {
        list =new ArrayList();
    }

    @Override
    public String toString() {
        return "neww{" +
                "channel='" + channel + '\'' +
                ", num=" + num +
                ", list=" + list +
                '}';
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
