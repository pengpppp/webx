package com.web.entity.entityImp.UserPic;

public class userpicimp {
    private String type;

    private String picurl;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    @Override
    public String toString() {
        return "{" +
                "type='" + type + '\'' +
                ", picurl='" + picurl + '\'' +
                '}';
    }
}
