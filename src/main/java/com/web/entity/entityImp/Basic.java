package com.web.entity.entityImp;


import com.web.Tools.staticData.Code;

public class Basic {
    private String code;
    private String message;
    private Object data;
    public Basic()
    {
        code= Code.SUCCESS;
        message=Code.information.get(Code.SUCCESS);
    }
    public void SetFail()
    {
        code=Code.FAIL;
        message=Code.information.get(code);
    }

    public void SetStatus()
    {
        code=Code.NO_PERMISSION;
        message=Code.information.get(code);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }


}
