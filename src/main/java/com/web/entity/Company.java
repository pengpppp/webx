package com.web.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * (Company)实体类
 *
 * @author makejava
 * @since 2020-05-09 11:25:41
 */
public class Company implements Serializable {
    private static final long serialVersionUID = -17061251622402823L;
    
    private Integer id;
    
    private String officialPhone;
    
    private String hotLine;
    
    private String wechatNumb;

    @JsonProperty
    private String Email;
    
    private String address;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOfficialPhone() {
        return officialPhone;
    }

    public void setOfficialPhone(String officialPhone) {
        this.officialPhone = officialPhone;
    }

    public String getHotLine() {
        return hotLine;
    }

    public void setHotLine(String hotLine) {
        this.hotLine = hotLine;
    }

    public String getWechatNumb() {
        return wechatNumb;
    }

    public void setWechatNumb(String wechatNumb) {
        this.wechatNumb = wechatNumb;
    }

    @JsonProperty
    public String getEmail() {
        return Email;
    }

    @JsonProperty
    public void setEmail(String email) {
        this.Email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}