package com.web.entity;

import java.io.Serializable;

/**
 * (Interorder)实体类
 *
 * @author makejava
 * @since 2020-06-05 09:34:01
 */
public class Interorder implements Serializable {
    private static final long serialVersionUID = 723663043479125144L;
    
    private Integer id;
    
    private String flightsessionid;
    /**
    * 验价id
    */
    private String checksessionid;
    /**
    * 联系人姓名
    */
    private String contactname;
    /**
    * 联系人手机号
    */
    private String contactmobile;
    /**
    * 乘客信息
    */
    private String passengers;
    /**
    * 订单来源
    */
    private Integer ordersource;
    /**
    * 存储航班信息
    */
    private Object flight;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFlightsessionid() {
        return flightsessionid;
    }

    public void setFlightsessionid(String flightsessionid) {
        this.flightsessionid = flightsessionid;
    }

    public String getChecksessionid() {
        return checksessionid;
    }

    public void setChecksessionid(String checksessionid) {
        this.checksessionid = checksessionid;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getContactmobile() {
        return contactmobile;
    }

    public void setContactmobile(String contactmobile) {
        this.contactmobile = contactmobile;
    }

    public String getPassengers() {
        return passengers;
    }

    public void setPassengers(String passengers) {
        this.passengers = passengers;
    }

    public Integer getOrdersource() {
        return ordersource;
    }

    public void setOrdersource(Integer ordersource) {
        this.ordersource = ordersource;
    }

    public Object getFlight() {
        return flight;
    }

    public void setFlight(Object flight) {
        this.flight = flight;
    }

}