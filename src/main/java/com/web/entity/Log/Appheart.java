package com.web.entity.Log;

import java.io.Serializable;

/**
 * (Appheart)实体类
 *
 * @author makejava
 * @since 2020-05-11 15:44:38
 */
public class Appheart implements Serializable {
    private static final long serialVersionUID = 865141349323699094L;
    /**
    * 应用ID
    */
    private String ad;
    /**
    * 请求ID
    */
    private String rd;
    /**
    * 最新发送过来的请求ID
    */
    private Integer newid;
    /**
    * 凭证
    */
    private String tn;
    /**
    * 唯一设备码
    */
    private String im;
    /**
    * app系统版本号
    */
    private String ov;
    /**
    * 客户端版本号
    */
    private String cv;
    /**
    * 渠道
    */
    private String fc;
    /**
    * APP时间戳
    */
    private String rt;
    /**
    * 手机型号
    */
    private String cm;

    @Override
    public String toString() {
        return "Appheart{" +
                "ad='" + ad + '\'' +
                ", rd='" + rd + '\'' +
                ", newid=" + newid +
                ", tn='" + tn + '\'' +
                ", im='" + im + '\'' +
                ", ov='" + ov + '\'' +
                ", cv='" + cv + '\'' +
                ", fc='" + fc + '\'' +
                ", rt='" + rt + '\'' +
                ", cm='" + cm + '\'' +
                '}';
    }

    public String getAd() {
        return ad;
    }

    public void setAd(String ad) {
        this.ad = ad;
    }

    public String getRd() {
        return rd;
    }

    public void setRd(String rd) {
        this.rd = rd;
    }

    public Integer getNewid() {
        return newid;
    }

    public void setNewid(Integer newid) {
        this.newid = newid;
    }

    public String getTn() {
        return tn;
    }

    public void setTn(String tn) {
        this.tn = tn;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

    public String getOv() {
        return ov;
    }

    public void setOv(String ov) {
        this.ov = ov;
    }

    public String getCv() {
        return cv;
    }

    public void setCv(String cv) {
        this.cv = cv;
    }

    public String getFc() {
        return fc;
    }

    public void setFc(String fc) {
        this.fc = fc;
    }

    public String getRt() {
        return rt;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getCm() {
        return cm;
    }

    public void setCm(String cm) {
        this.cm = cm;
    }

}