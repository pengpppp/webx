package com.web.entity.Log;

import java.util.Date;
import java.io.Serializable;

/**
 * (RootLog)实体类
 *
 * @author makejava
 * @since 2020-05-07 11:03:55
 */
public class RootLog implements Serializable {
    private static final long serialVersionUID = -65879501511616688L;
    
    private Integer id;
    
    private String logname;
    
    private String username;
    
    private Date createtime;
    
    private String succeed;
    
    private String im;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogname() {
        return logname;
    }

    public void setLogname(String logname) {
        this.logname = logname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

    public String getSucceed() {
        return succeed;
    }

    public void setSucceed(String succeed) {
        this.succeed = succeed;
    }

    public String getIm() {
        return im;
    }

    public void setIm(String im) {
        this.im = im;
    }

}