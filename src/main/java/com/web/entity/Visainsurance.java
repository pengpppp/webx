package com.web.entity;

import java.io.Serializable;

/**
 * (Visainsurance)实体类
 *
 * @author makejava
 * @since 2020-06-15 09:21:43
 */
public class Visainsurance implements Serializable {
    private static final long serialVersionUID = 207386284936188231L;
    
    private Integer i;
    /**
    * 保险ID
    */
    private String id;
    
    private String key;
    /**
    * 保险名称
    */
    private String name;
    /**
    * 保险标签
    */
    private String tittle;
    /**
    * 保险价格
    */
    private Double cost;


    public Integer getI() {
        return i;
    }

    public void setI(Integer i) {
        this.i = i;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "{" +
                "i=" + i +
                ", id='" + id + '\'' +
                ", key='" + key + '\'' +
                ", name='" + name + '\'' +
                ", tittle='" + tittle + '\'' +
                ", cost=" + cost +
                '}';
    }
}