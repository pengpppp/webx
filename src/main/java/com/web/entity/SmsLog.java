package com.web.entity;

import java.io.Serializable;

/**
 * (SmsLog)实体类
 *
 * @author makejava
 * @since 2020-05-26 13:31:11
 */
public class SmsLog implements Serializable {
    private static final long serialVersionUID = -80307226536192366L;
    
    private Integer id;
    /**
    * 发送类型
    */
    private String type;
    /**
    * 短信内容
    */
    private String content;
    /**
    * 运营商短信状态
    */
    private String errcode;
    /**
    * 外流水扩展
    */
    private String outid;
    /**
    * 手机号码
    */
    private String phonenum;
    /**
    * 接收时间 
    */
    private String receivedate;
    /**
    * 发送时间
    */
    private String senddate;
    /**
    * 发送状态
    */
    private String sendstatus;
    /**
    * 短信模板ID
    */
    private String templatecode;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getErrcode() {
        return errcode;
    }

    public void setErrcode(String errcode) {
        this.errcode = errcode;
    }

    public String getOutid() {
        return outid;
    }

    public void setOutid(String outid) {
        this.outid = outid;
    }

    public String getPhonenum() {
        return phonenum;
    }

    public void setPhonenum(String phonenum) {
        this.phonenum = phonenum;
    }

    public String getReceivedate() {
        return receivedate;
    }

    public void setReceivedate(String receivedate) {
        this.receivedate = receivedate;
    }

    public String getSenddate() {
        return senddate;
    }

    public void setSenddate(String senddate) {
        this.senddate = senddate;
    }

    public String getSendstatus() {
        return sendstatus;
    }

    public void setSendstatus(String sendstatus) {
        this.sendstatus = sendstatus;
    }

    public String getTemplatecode() {
        return templatecode;
    }

    public void setTemplatecode(String templatecode) {
        this.templatecode = templatecode;
    }

}