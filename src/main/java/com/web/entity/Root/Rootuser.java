package com.web.entity.Root;

import java.util.Date;
import java.io.Serializable;

/**
 * (Rootuser)实体类
 *
 * @author makejava
 * @since 2020-06-10 09:44:47
 */
public class Rootuser implements Serializable {
    private static final long serialVersionUID = -72255869148579528L;
    
    private Integer id;
    
    private String username;
    
    private String password;
    
    private String name;
    
    private String status;
    
    private Integer token;
    /**
    * 创建时间
    */
    private Date createtime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getToken() {
        return token;
    }

    public void setToken(Integer token) {
        this.token = token;
    }

    public Date getCreatetime() {
        return createtime;
    }

    public void setCreatetime(Date createtime) {
        this.createtime = createtime;
    }

}