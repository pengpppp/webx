package com.web.entity;

import java.io.Serializable;

/**
 * (Userpic)实体类
 *
 * @author makejava
 * @since 2020-06-13 13:51:24
 */
public class Userpic implements Serializable {
    private static final long serialVersionUID = 148182663401335264L;
    
    private Integer i;
    
    private String userid;
    
    private String type;
    
    private String picurl;

    @Override
    public String toString() {
        return "{" +
                "i=" + i +
                ", userid='" + userid + '\'' +
                ", type='" + type + '\'' +
                ", picurl='" + picurl + '\'' +
                '}';
    }

    public Integer getI() {
        return i;
    }

    public void setI(Integer i) {
        this.i = i;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

}