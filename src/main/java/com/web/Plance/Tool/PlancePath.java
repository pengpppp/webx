package com.web.Plance.Tool;

public class PlancePath {
    public static String flighturl="http://180.76.246.175:9999/tripapi/flight/";

    /**
     * 低价日历接口
     */
    public static String getlowpricecalendar="getlowpricecalendar";

    /**
     * 特价机票接口
     */
    public static String cheapflights="cheapflights";

    /**
     * 保险查询接口
     */
    public static String getInsuranceList="getInsuranceList";

    /**
     * 获取航班列表
     */
    public static String getflightlist="getflightlist";

    /**
     * 获取航班详情
     */
    public static String getflightdetails="getflightdetails";

    /**
     * 获取退改规则
     */
    public static String getrefundrule="getrefundrule";

    /**
     * 验舱验价
     */
    public static String checkcabinprice="checkcabinprice";

    /**
     * 下单接口
     */
    public static String createorder="createorder";
}
