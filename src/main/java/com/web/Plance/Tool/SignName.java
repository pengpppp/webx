package com.web.Plance.Tool;

import com.google.gson.Gson;
import com.web.Config.ConfigClass;
import com.web.Tools.Check.check;

import com.web.common.core.util.MD5Util;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.util.*;

@Component
public class SignName {

    @Autowired
    ConfigClass configClass;

    @Autowired
    Gson gson;

    public String sign(Object object)
    {
        String s=null;
        try {
            List list = toList(gson.toJson(object));
            Collections.sort(list);
            s =configClass.getSecretkey()+list.toString()+configClass.getSecretkey();
            s=s.replace(", ","&").replace("[","&").replace("]","&");
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(s);
        return MD5Util.md5Hex(s).toUpperCase();
    }

    public List toList(Object object)
    {
        List<String> list = new ArrayList<>();
        try {
            JSONObject jsonObject = JSONObject.fromObject(object);
            for(Object k : jsonObject.keySet()) {
                Object v = jsonObject.get(k);
                if(!check.checkNull(String.valueOf(v)))
                {
                    if(String.valueOf(k).equals("data"))//如果key值是data,直接进入下一步遍历
                    {
                        list.addAll(toList(v));
                    }else if(v instanceof JSONArray){//如果是列表型json，进入遍历；
                    Iterator<JSONObject> it = ((JSONArray)v).iterator();
                    while(it.hasNext()){
                       list.addAll(toList(it.next()));
                    }
                    }else if(v instanceof JSONObject)
                    {
                        list.addAll(toList(v));
                    }
                    else {
                        list.add(k+"="+v);
                }
                }
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return list;
    }

}


