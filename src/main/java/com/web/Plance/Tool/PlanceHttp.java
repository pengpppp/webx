package com.web.Plance.Tool;

import com.fasterxml.jackson.databind.JsonNode;
import com.web.Config.ConfigClass;
import com.web.Plance.entity.AllRequset;
import com.web.common.core.entity.JsonData;
import com.web.common.core.util.HttpClientUtil;
import com.web.common.core.util.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class PlanceHttp {
    @Autowired
    ConfigClass configClass;
    @Autowired
    SignName signName;

    /**
     * 0为基础
     * 1为国内
     * 2为国外
     * @param object 数据
     * @param string 访问地址
     * @param type 访问类型 0为基础 1为国内 2为国外
     * @return
     */
    public Object Request(Object object,String string,Integer type)
    {
        AllRequset allRequset = new AllRequset();
        allRequset.setApiKey(configClass.getAPIKey());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        allRequset.setTimestamp(formatter.format(new Date()));
        allRequset.setData(object);
        allRequset.setSign(signName.sign(allRequset));

        System.out.println( JSONUtil.toStringAlways(allRequset));

        try {
            String result;
            if(type==0)
            {
                result = HttpClientUtil.post(PlanceBasePath.flighturl+string,null, JSONUtil.toStringAlways(allRequset), "application/json;charset=UTF-8");
            }else if(type==1) {
                result = HttpClientUtil.post(PlancePath.flighturl+string,null, JSONUtil.toStringAlways(allRequset), "application/json;charset=UTF-8");
            }
            else {
                result = HttpClientUtil.post(PlanceInterPath.flightInterneturl+string,null, JSONUtil.toStringAlways(allRequset), "application/json;charset=UTF-8");
            }
            JsonNode jsonNode =JSONUtil.parseJson(result);
            if(Integer.valueOf(String.valueOf(jsonNode.get("errCode")))==0)
            {
                return JsonData.buildSuccess(jsonNode.get("data"));
            }else {
                return JsonData.buildError(String.valueOf(jsonNode.get("errMsg")),"-1");
            }
        } catch (Exception e) {
            return JsonData.buildError(e.getMessage());
        }
    }
}
