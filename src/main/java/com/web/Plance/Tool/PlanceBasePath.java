package com.web.Plance.Tool;

public class PlanceBasePath {
    public static String flighturl="http://180.76.246.175:9999/tripapi/flight/";
 //   public static String flightInterneturl="http://180.76.246.175:9999/tripapi/interflight/";

    /**
     * 城市数据接口
     */
    public static String flightcitylistt="flightcitylist";

    /**
     * 航空公司数据接口
     */
    public static String getaircompanylist="getaircompanylist";

    /**
     * 机场数据接口
     */
    public static String getairportslist="getairportslist";

    /**
     * 订单列表接口
     */
    public static String getorderlist="getorderlist";

    /**
     * 订单详情接口
     */
    public static String getorderdetails="getorderdetails";

    /**
     * 取消订单接口
     */
    public static String cancelorder="cancelorder";

    /**
     * 确认出票接口
     */
    public static String confirmorder="confirmorder";

    /**
     * 退票附件上传接口
     */
    public static String refundupload="refundupload";

    /**
     * 退票申请接口
     */
    public static String refundapply="refundapply";

    /**
     * 退票单列表接口
     */
    public static String getrefundorderlist="getrefundorderlist";

    /**
     * 退票单详情接口
     */
    public static String getrefundorderdetails="getrefundorderdetails";

    /**
     * 改签申请接口
     */
    public static String changeapply="changeapply";

    /**
     * 改签订单列表接口
     */
    public static String getchangeorderlist="getchangeorderlist";

    /**
     * 改签订单详情接口
     */
    public static String getchangeorderdetails="getchangeorderdetails";

    /**
     * 取消改签接口
     */
    public static String cancelchange="cancelchange";

    /**
     * 确认改签接口
     */
    public static String confirmchange="confirmchange";

}
