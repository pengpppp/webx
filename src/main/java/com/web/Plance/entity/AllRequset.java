package com.web.Plance.entity;

public class AllRequset<T> {
    /**
     * 公司代码
     */
    private String apiKey;
    /**
     * 签名数据
     */
    private String sign;
    /**
     * 请求时间戳
     */
    private String timestamp;
    /**
     * 业务参数
     */
    private T data;

    @Override
    public String toString() {
        return "AllRequset{" +
                "apiKey='" + apiKey + '\'' +
                ", sign='" + sign + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", data=" + data +
                '}';
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
