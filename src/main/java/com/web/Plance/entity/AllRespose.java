package com.web.Plance.entity;

public class AllRespose<T> {
    /**
     * 0为成功
     */
    private Integer errCode;
    /**
     * 返回参数
     */
    private String errMsg;
    /**
     * True/False
     */
    private Boolean success;
    /**
     * 具体内容
     */
    private T data;

    public Integer getErrCode() {
        return errCode;
    }

    public void setErrCode(Integer errCode) {
        this.errCode = errCode;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
