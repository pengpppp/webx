package com.web.Plance.entity.InternetFlight.Request;

/**
 * 获取航班列表
 */
public class GetFlightList {

    /**
     * 出发城市三字码
     */
    private String departCityCode;
    /**
     * 到达城市三字码
     */
    private String arriveCityCode;
    /**
     * 出发日期 yyyy-MM-dd
     */
    private String departDate;
    /**
     * 返程日期 yyyy-MM-dd N
     */
    private String backDate;
    /**
     * 航程类型 1.单程 2 往返  默认为1 N
     */
    private Integer tripType;
    /**
     * 成人数量 默认为1 N
     */
    private Integer adultNum;
    /**
     * 儿童数量 默认为0 N
     */
    private Integer childNum;

    public String getDepartCityCode() {
        return departCityCode;
    }

    public void setDepartCityCode(String departCityCode) {
        this.departCityCode = departCityCode;
    }

    public String getArriveCityCode() {
        return arriveCityCode;
    }

    public void setArriveCityCode(String arriveCityCode) {
        this.arriveCityCode = arriveCityCode;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getBackDate() {
        return backDate;
    }

    public void setBackDate(String backDate) {
        this.backDate = backDate;
    }

    public Integer getTripType() {
        return tripType;
    }

    public void setTripType(Integer tripType) {
        this.tripType = tripType;
    }

    public Integer getAdultNum() {
        return adultNum;
    }

    public void setAdultNum(Integer adultNum) {
        this.adultNum = adultNum;
    }

    public Integer getChildNum() {
        return childNum;
    }

    public void setChildNum(Integer childNum) {
        this.childNum = childNum;
    }
}
