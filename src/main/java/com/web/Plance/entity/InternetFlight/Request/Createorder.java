package com.web.Plance.entity.InternetFlight.Request;

import java.util.List;

/**
 * 下单
 */
public class Createorder {
    /**
     * 航班的sessionID
     */
    private String flightSessionId;
    /**
     * 验价接口返回的sessionID
     */
    private String checkSessionId;
    /**
     * 联系人姓名
     */
    private String contactname;
    /**
     * 联系人手机号
     */
    private String contactmobile;
    /**
     * 乘客信息
     */
    private List<Passenger> passengers;
    /**
     * 订单来源
     */
    private Integer ordersource;

    public String getFlightSessionId() {
        return flightSessionId;
    }

    public void setFlightSessionId(String flightSessionId) {
        this.flightSessionId = flightSessionId;
    }

    public String getCheckSessionId() {
        return checkSessionId;
    }

    public void setCheckSessionId(String checkSessionId) {
        this.checkSessionId = checkSessionId;
    }

    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getContactmobile() {
        return contactmobile;
    }

    public void setContactmobile(String contactmobile) {
        this.contactmobile = contactmobile;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    public Integer getOrdersource() {
        return ordersource;
    }

    public void setOrdersource(Integer ordersource) {
        this.ordersource = ordersource;
    }
}
