package com.web.Plance.entity.InternetFlight.Request;

/**
 * 乘客信息
 */
public class Passenger {
    /**
     * 乘客类型 1成人 2儿童
     */
    private Integer passengertype;
    /**
     * 证件类型 2护照 4港澳通行证 11台湾通行证 7台胞证 8回乡证
     */
    private String idtype;
    /**
     * 乘机人名拼音
     */
    private String firstname;
    /**
     * 证件号
     */
    private String idnumber;
    /**
     * 证件有效期
     */
    private String idvalidedate;
    /**
     * 乘机人姓拼音
     */
    private String lastname;
    /**
     * 出生日期 yyyy-MM-dd
     */
    private String birthday;
    /**
     * 国籍 CN
     */
    private String country;
    /**
     * 性别 M男 F女
     */
    private String sex;

    public Integer getPassengertype() {
        return passengertype;
    }

    public void setPassengertype(Integer passengertype) {
        this.passengertype = passengertype;
    }

    public String getIdtype() {
        return idtype;
    }

    public void setIdtype(String idtype) {
        this.idtype = idtype;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getIdnumber() {
        return idnumber;
    }

    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber;
    }

    public String getIdvalidedate() {
        return idvalidedate;
    }

    public void setIdvalidedate(String idvalidedate) {
        this.idvalidedate = idvalidedate;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
