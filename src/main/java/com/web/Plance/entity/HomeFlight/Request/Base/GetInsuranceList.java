package com.web.Plance.entity.HomeFlight.Request.Base;

import java.math.BigDecimal;
import java.util.List;

/**
 * 保险查询接口
 */
public class GetInsuranceList {
    /**
     * 保险ID  下单时传入
     */
    private String id;
    /**
     * 保险类型   1航空意外险  2航空延误险
     */
    private Integer type;
    /**
     * 保险类型名称
     */
    private String typename;
    /**
     * 保险名称
     */
    private String name;
    /**
     * 保险的简短说明
     */
    private String remark;
    /**
     * 保险价格
     */
    private BigDecimal cost;
    /**
     * 保险相关材料
     */
    private List<PDF> pdfList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTypename() {
        return typename;
    }

    public void setTypename(String typename) {
        this.typename = typename;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public List<PDF> getPdfList() {
        return pdfList;
    }

    public void setPdfList(List<PDF> pdfList) {
        this.pdfList = pdfList;
    }
}
