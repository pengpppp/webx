package com.web.Plance.entity.HomeFlight.Request.Base;

/**
 * 特价机票接口
 */
public class Cheapflights {
    /**
     * 出发城市三字码 两个城市必须填一个 N
     */
    private String orgcitycode;
    /**
     * 到达城市三字码 N
     */
    private String dstcitycode;

    public String getOrgcitycode() {
        return orgcitycode;
    }

    public void setOrgcitycode(String orgcitycode) {
        this.orgcitycode = orgcitycode;
    }

    public String getDstcitycode() {
        return dstcitycode;
    }

    public void setDstcitycode(String dstcitycode) {
        this.dstcitycode = dstcitycode;
    }
}
