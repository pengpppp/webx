package com.web.Plance.entity.HomeFlight.Request.Order;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.web.common.core.util.JSONUtil;
import com.web.entity.Homeorder;
import com.web.entity.Passenger;

import java.util.List;

/**
 * 验舱验价
 */
public class Checkcabinprice {
    /**
     *
     */
    private String sessionId;
    /**
     * 乘客信息
     */
    private List<passenger> passengers;

    public static Checkcabinprice homeorder2checkcabinpricce(Homeorder homeorder)
    {
        Checkcabinprice checkcabinprice = new Checkcabinprice();
        Gson gson = new Gson();
        checkcabinprice.setSessionId(homeorder.getSessionid());
        checkcabinprice.setPassengers(gson.fromJson(homeorder.getInvices(), new TypeToken<List<Passenger>>(){}.getType()));
        return checkcabinprice;
    }

    @Override
    public String toString() {
        return "Checkcabinprice{" +
                "sessionId='" + sessionId + '\'' +
                ", passengers=" + passengers +
                '}';
    }

    public String toParam(){
        return JSONUtil.toString(this);
    }

    public List<passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<passenger> passengers) {
        this.passengers = passengers;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

}
