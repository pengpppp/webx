package com.web.Plance.entity.HomeFlight.Request.Order;

/**
 * 发票信息
 */
public class Invoice {
    /**
     * 纳税人识别号   个人/企业标识=“企业”时必传
     */
    private String taxpayeridentity;
    /**
     * 联系人手机号
     */
    private String contactertel;
    /**
     * 联系人姓名
     */
    private String contacter;
    /**
     * 票据类型 0：未知 2：发票 1：行程单 3：差额发票+行程单 4：电子发票
     */
    private Integer billtype;
    /**
     * 发票抬头
     */
    private String title;
    /**
     * 个人/企业标识  1：个人；2：企业 
     */
    private Integer personalorenterprise;

    public String getTaxpayeridentity() {
        return taxpayeridentity;
    }

    public void setTaxpayeridentity(String taxpayeridentity) {
        this.taxpayeridentity = taxpayeridentity;
    }

    public String getContactertel() {
        return contactertel;
    }

    public void setContactertel(String contactertel) {
        this.contactertel = contactertel;
    }

    public String getContacter() {
        return contacter;
    }

    public void setContacter(String contacter) {
        this.contacter = contacter;
    }

    public Integer getBilltype() {
        return billtype;
    }

    public void setBilltype(Integer billtype) {
        this.billtype = billtype;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getPersonalorenterprise() {
        return personalorenterprise;
    }

    public void setPersonalorenterprise(Integer personalorenterprise) {
        this.personalorenterprise = personalorenterprise;
    }

    @Override
    public String toString() {
        return "{" +
                "taxpayeridentity='" + taxpayeridentity + '\'' +
                ", contactertel='" + contactertel + '\'' +
                ", contacter='" + contacter + '\'' +
                ", billtype=" + billtype +
                ", title='" + title + '\'' +
                ", personalorenterprise=" + personalorenterprise +
                '}';
    }
}
