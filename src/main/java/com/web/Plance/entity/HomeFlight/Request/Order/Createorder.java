package com.web.Plance.entity.HomeFlight.Request.Order;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.web.entity.Homeorder;
import com.web.entity.Passenger;

import java.util.List;

/**
 * 下单接口
 */
public class Createorder {
    /**
     * 联系人
     */
    private String contactname;
    /**
     * 国际区号  若联系人传号码不是中国大陆号码，该字段必填；另外特别需要关注的，传联系人号码和乘机人号码时，请不要带区号
     */
    private String intlcode;
    /**
     * 邮寄信息
     */
    private Delivery delivery;
    /**
     * 乘客信息
     */
    private List<Passenger> passengers;
    /**
     *保险Id 所有乘机人共买的保险
     */
    private List<Integer> insurances;
    /**
     * 备注信息
     */
    private String remark;
    /**
     * 发票信息
     */
    private List<Invoice> invices;
    /**
     * 舱位sessionid
     */
    private String sessionid;
    /**
     * 联系人手机号
     */
    private String contactmobile;
    /**
     * 订单来源
     */
    private Integer ordersource;

    /**
     *
     * @param homeorder
     * @return
     */
    public static Createorder homeorder2Createorder(Homeorder homeorder)
    {
        Createorder createorder = new Createorder();
        Gson gson = new Gson();
        createorder.setContactname(homeorder.getContactname());
        createorder.setIntlcode(homeorder.getIntlcode());
        createorder.setDelivery(gson.fromJson(homeorder.getDelivery(), Delivery.class));
        createorder.setPassengers(gson.fromJson(homeorder.getPassengers(),new TypeToken<List<Passenger>>(){}.getType()));
        createorder.setInsurances(gson.fromJson(homeorder.getInsurances(),new TypeToken<List<Integer>>(){}.getType()));
        createorder.setRemark(homeorder.getRemark());
        createorder.setInvices(gson.fromJson(homeorder.getInvices(),new TypeToken<List<Integer>>(){}.getType()));
        createorder.setSessionid(homeorder.getSessionid());
        createorder.setContactmobile(homeorder.getContactmobile());
        createorder.setOrdersource(homeorder.getOrdersource());
        return createorder;
    }

    @Override
    public String toString() {
        return "{" +
                "contactname='" + contactname + '\'' +
                ", intlcode='" + intlcode + '\'' +
                ", delivery=" + delivery+
                ", passengers=" + passengers +
                ", insurances=" + insurances +
                ", remark='" + remark + '\'' +
                ", invices=" + invices +
                ", sessionid='" + sessionid + '\'' +
                ", contactmobile='" + contactmobile + '\'' +
                ", ordersource=" + ordersource +
                '}';
    }
    public String getContactname() {
        return contactname;
    }

    public void setContactname(String contactname) {
        this.contactname = contactname;
    }

    public String getIntlcode() {
        return intlcode;
    }

    public void setIntlcode(String intlcode) {
        this.intlcode = intlcode;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public void setDelivery(Delivery delivery) {
        this.delivery = delivery;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    public List<Integer> getInsurances() {
        return insurances;
    }

    public void setInsurances(List<Integer> insurances) {
        this.insurances = insurances;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public List<Invoice> getInvices() {
        return invices;
    }

    public void setInvices(List<Invoice> invices) {
        this.invices = invices;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getContactmobile() {
        return contactmobile;
    }

    public void setContactmobile(String contactmobile) {
        this.contactmobile = contactmobile;
    }

    public Integer getOrdersource() {
        return ordersource;
    }

    public void setOrdersource(Integer ordersource) {
        this.ordersource = ordersource;
    }
}
