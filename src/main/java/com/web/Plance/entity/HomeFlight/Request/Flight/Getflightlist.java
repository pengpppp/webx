package com.web.Plance.entity.HomeFlight.Request.Flight;

/**
 * 获取航班列表
 */
public class Getflightlist {
    /**
     * 出发城市三字码
     */
    private String departAirportCode;
    /**
     * 到达城市三字码
     */
    private String arriveAirportCode;
    /**
     * 出发日期
     */
    private String departDate;
    /**
     * 舱位代码 N
     */
    private String cabinCode;
    /**
     * 航空公司代码 N
     */
    private String airlineCode;
    /**
     * 乘客数量 N
     */
    private Integer passengerNum;
    /**
     * 起飞时间筛选  N 起飞时间段，多个英文逗号隔开；示例06:00-12:00,12:00-18:00", example = "06:00-12:00,12:00-18:00
     */
    private String flightTime;
    /**
     * 机场代码筛选 N  机场代码；示例：BJS"
     */
    private String airdrome;
    /**
     * 排序规则
     * 排序规则：10起飞时间升序；11起飞时间降序；20价格升序；21价格降序；默认10
     */
    private String sortType;
    /**
     * sessionId   用于快速筛选航班，值为航班列表数据的sessionId,有筛选条件时必传
     */
    private String sessionId;

    public String getDepartAirportCode() {
        return departAirportCode;
    }

    public void setDepartAirportCode(String departAirportCode) {
        this.departAirportCode = departAirportCode;
    }

    public String getArriveAirportCode() {
        return arriveAirportCode;
    }

    public void setArriveAirportCode(String arriveAirportCode) {
        this.arriveAirportCode = arriveAirportCode;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getCabinCode() {
        return cabinCode;
    }

    public void setCabinCode(String cabinCode) {
        this.cabinCode = cabinCode;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public Integer getPassengerNum() {
        return passengerNum;
    }

    public void setPassengerNum(Integer passengerNum) {
        this.passengerNum = passengerNum;
    }

    public String getFlightTime() {
        return flightTime;
    }

    public void setFlightTime(String flightTime) {
        this.flightTime = flightTime;
    }

    public String getAirdrome() {
        return airdrome;
    }

    public void setAirdrome(String airdrome) {
        this.airdrome = airdrome;
    }

    public String getSortType() {
        return sortType;
    }

    public void setSortType(String sortType) {
        this.sortType = sortType;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
