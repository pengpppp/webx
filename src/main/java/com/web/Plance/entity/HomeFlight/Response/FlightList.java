package com.web.Plance.entity.HomeFlight.Response;

import java.util.List;

public class FlightList {
    private List<getflight> flightList;
    private String count;
    private String sessionId;

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public List<getflight> getFlightList() {
        return flightList;
    }

    public void setFlightList(List<getflight> flightList) {
        this.flightList = flightList;
    }

    @Override
    public String toString() {
        return "FlightList{" +
                "flightList=" + flightList.toString() +
                '}';
    }
}
