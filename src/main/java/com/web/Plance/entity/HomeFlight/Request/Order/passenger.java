package com.web.Plance.entity.HomeFlight.Request.Order;

public class passenger {
    /**
     * 乘客类型  1成人  2儿童
     */
    private Integer passengertype;
    /**
     * 证件类型   2护照  4港澳通行证  11台湾通行证  7台胞证  8回乡证
     */
    private String idtype;
    /**
     * 乘机人名拼音 name中有生僻字时必填
     */
    private String firstname;

    private String passengername;
    /**
     * 证件号
     */
    private String idnumber;

    private String mobile;
    /**
     * 证件有效期
     */
    private String idvalidedate;
    /**
     * 乘机人姓拼音  name中有生僻字时必填
     */
    private String lastname;
    /**
     * 出生日期
     */
    private String birthday;
    /**
     * 国籍
     */
    private String country;
    /**
     * 性别
     */
    private String sex;

    @Override
    public String toString() {
        return "{" +
                "passengertype=" + passengertype +
                ", idtype='" + idtype + '\'' +
                ", firstname='" + firstname + '\'' +
                ", passengername='" + passengername + '\'' +
                ", idnumber='" + idnumber + '\'' +
                ", mobile='" + mobile + '\'' +
                ", idvalidedate='" + idvalidedate + '\'' +
                ", lastname='" + lastname + '\'' +
                ", birthday='" + birthday + '\'' +
                ", country='" + country + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }

    public Integer getPassengertype() {
        return passengertype;
    }

    public void setPassengertype(Integer passengertype) {
        this.passengertype = passengertype;
    }

    public String getIdtype() {
        return idtype;
    }

    public void setIdtype(String idtype) {
        this.idtype = idtype;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getPassengername() {
        return passengername;
    }

    public void setPassengername(String passengername) {
        this.passengername = passengername;
    }

    public String getIdnumber() {
        return idnumber;
    }

    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getIdvalidedate() {
        return idvalidedate;
    }

    public void setIdvalidedate(String idvalidedate) {
        this.idvalidedate = idvalidedate;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCountry() {
        return country == null ?"CN":country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
