package com.web.Plance.entity.HomeFlight.Request.Flight;

/**
 * 获取航班详情
 */
public class Getflightdetails {
    /**
     * 航班列表请求sessionId
     */
    private String sessionId;
    /**
     * 航班号
     */
    private String flightNo;
    /**
     * 舱位类型    N  FCY
     */
    private String cabinCode;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getCabinCode() {
        return cabinCode;
    }

    public void setCabinCode(String cabinCode) {
        this.cabinCode = cabinCode;
    }
}
