package com.web.Plance.entity.HomeFlight.Request.Order;

import java.math.BigDecimal;

/**
 * 邮寄信息实体类
 */
public class Delivery {
    /**
     * 邮编
     */
    private String zipcode;
    /**
     * 配送价格  默认传0
     */
    private BigDecimal deliveryprice;
    /**
     * 配送地址/上门地址
     */
    private String deliveryendaddress ;
    /**
     * 行政区名称
     */
    private String districtname;
    /**
     * 省名称
     */
    private String provincename;
    /**
     * 市名称
     */
    private String cityname;
    /**
     * 收件人姓名
     */
    private String recipientname;
    /**
     * 快递类型  默认为0
     */
    private Integer deliverytype;
    /**
     * 快递公司编码  从快递列表中取
     */
    private String expresscompanycode;
    /**
     * 联系人电话
     */
    private String recipientphone;

    @Override
    public String toString() {
        return "{" +
                "zipcode='" + zipcode + '\'' +
                ", deliveryprice=" + deliveryprice +
                ", deliveryendaddress='" + deliveryendaddress + '\'' +
                ", districtname='" + districtname + '\'' +
                ", provincename='" + provincename + '\'' +
                ", cityname='" + cityname + '\'' +
                ", recipientname='" + recipientname + '\'' +
                ", deliverytype=" + deliverytype +
                ", expresscompanycode='" + expresscompanycode + '\'' +
                ", recipientphone='" + recipientphone + '\'' +
                '}';
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public BigDecimal getDeliveryprice() {
        return deliveryprice;
    }

    public void setDeliveryprice(BigDecimal deliveryprice) {
        this.deliveryprice = deliveryprice;
    }

    public String getDeliveryendaddress() {
        return deliveryendaddress;
    }

    public void setDeliveryendaddress(String deliveryendaddress) {
        this.deliveryendaddress = deliveryendaddress;
    }

    public String getDistrictname() {
        return districtname;
    }

    public void setDistrictname(String districtname) {
        this.districtname = districtname;
    }

    public String getProvincename() {
        return provincename;
    }

    public void setProvincename(String provincename) {
        this.provincename = provincename;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getRecipientname() {
        return recipientname;
    }

    public void setRecipientname(String recipientname) {
        this.recipientname = recipientname;
    }

    public Integer getDeliverytype() {
        return deliverytype;
    }

    public void setDeliverytype(Integer deliverytype) {
        this.deliverytype = deliverytype;
    }

    public String getExpresscompanycode() {
        return expresscompanycode;
    }

    public void setExpresscompanycode(String expresscompanycode) {
        this.expresscompanycode = expresscompanycode;
    }

    public String getRecipientphone() {
        return recipientphone;
    }

    public void setRecipientphone(String recipientphone) {
        this.recipientphone = recipientphone;
    }
}
