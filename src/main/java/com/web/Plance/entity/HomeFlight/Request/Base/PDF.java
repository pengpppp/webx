package com.web.Plance.entity.HomeFlight.Request.Base;

/**
 * 保险相关材料
 */
public class PDF {
    /**
     * 材料名称
     */
    private String name;
    /**
     * 文件URL
     */
    private String path;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
