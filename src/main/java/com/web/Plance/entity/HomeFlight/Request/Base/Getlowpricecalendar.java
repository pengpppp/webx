package com.web.Plance.entity.HomeFlight.Request.Base;

/**
 * 低价日历接口
 */
public class Getlowpricecalendar {
    /**
     * 出发城市三字码
     */
    private String orgcitycode;
    /**
     * 到达城市三字码
     */
    private String dstcitycode;
    /**
     * 起始出发日期
     */
    private String deptdate;
    /**
     * 截止出发日期
     */
    private String enddate;
    /**
     * 起始返回日期 N
     */
    private String backdate;

    public String getOrgcitycode() {
        return orgcitycode;
    }

    public void setOrgcitycode(String orgcitycode) {
        this.orgcitycode = orgcitycode;
    }

    public String getDstcitycode() {
        return dstcitycode;
    }

    public void setDstcitycode(String dstcitycode) {
        this.dstcitycode = dstcitycode;
    }

    public String getDeptdate() {
        return deptdate;
    }

    public void setDeptdate(String deptdate) {
        this.deptdate = deptdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getBackdate() {
        return backdate;
    }

    public void setBackdate(String backdate) {
        this.backdate = backdate;
    }
}
