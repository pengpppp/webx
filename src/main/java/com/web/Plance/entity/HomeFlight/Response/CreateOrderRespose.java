package com.web.Plance.entity.HomeFlight.Response;

public class CreateOrderRespose {
    private Integer orderid;
    private String ordernumber;

    @Override
    public String toString() {
        return "{" +
                "orderid=" + orderid +
                ", ordernumber='" + ordernumber + '\'' +
                '}';
    }

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }
}
