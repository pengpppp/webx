package com.web.Plance.entity.HomeFlight.Response;

import java.util.List;

public class FlightDetails {

    /**
     * flightDetails : {"flightNo":"3U8830","departDate":"2020-06-04","departTime":"23:00","departCityCode":"BJS","departCityName":"北京","departAirportCode":"PEK","departAirportName":"首都机场","departAirportTerminal":"T3","arriveCityCode":"CKG","arriveCityName":"重庆","arriveDate":"2020-06-05","arriveTime":"01:55","arriveAirportCode":"CKG","arriveAirportName":"江北机场","arriveAirportTerminal":"T2","crossDay":1,"craftType":"320(中)","airlineCode":"3U","airlineName":"四川航空","duration":"02:55","isShare":0,"shareFlightNo":null,"shareAirline":null,"shareAirlineName":null,"mealFlag":0,"meal":"无餐","stopInformation":null,"stopNum":"0","onTimeRate":"87%","cabinList":[{"sessionId":"ee6b19b1242f43bba65c1f3084d63461","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"M","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":180,"discount":0.8,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":180,"tax":50,"fuel":0,"facePrice":180,"addPrice":0,"orgPrice":180},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"172fbe5c86124ce9b40e6d0a8ae209a0","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"M","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":180,"discount":0.8,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":180,"tax":50,"fuel":0,"facePrice":180,"addPrice":0,"orgPrice":180},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":135}},{"sessionId":"bc68db76d9bb4cada5ea1347e6926721","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"Q","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":200,"discount":0.9,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":200,"tax":50,"fuel":0,"facePrice":200,"addPrice":0,"orgPrice":200},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"a2e6c8e3e17a4cf3b35dbe216a03ab72","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"R","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":930,"discount":4.2,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":930,"tax":50,"fuel":0,"facePrice":930,"addPrice":0,"orgPrice":930},"child":{"price":950,"tax":0,"fuel":0,"facePrice":950,"addPrice":0,"orgPrice":950},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":135}},{"sessionId":"acd67f7fb64a4e0386eecd47f41b3f0b","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"R","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":940,"discount":4.3,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":940,"tax":50,"fuel":0,"facePrice":940,"addPrice":0,"orgPrice":940},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"f2a55a955522435c83bed6f73f813fcf","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"R","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":941,"discount":4.3,"billType":3,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":941,"tax":50,"fuel":0,"facePrice":950,"addPrice":12,"orgPrice":929},"child":{"price":1088,"tax":0,"fuel":0,"facePrice":1085,"addPrice":0,"orgPrice":1088},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":75}},{"sessionId":"e48484e5530045659bf50fddd5314e0c","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"R","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":941,"discount":4.3,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":941,"tax":50,"fuel":0,"facePrice":950,"addPrice":11,"orgPrice":930},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"650b2bf8c1e94b40968141c18fe50663","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"L","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":1370,"discount":6.3,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1370,"tax":50,"fuel":0,"facePrice":1370,"addPrice":0,"orgPrice":1370},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"f3599b09bccf4609b58c4b6db3ef46f1","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"L","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":1381,"discount":6.4,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1381,"tax":50,"fuel":0,"facePrice":1390,"addPrice":14,"orgPrice":1367},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"dce0e846eda94408944cf104c7ad626e","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"L","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":1381,"discount":6.4,"billType":3,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1381,"tax":50,"fuel":0,"facePrice":1390,"addPrice":13,"orgPrice":1368},"child":{"price":1088,"tax":0,"fuel":0,"facePrice":1085,"addPrice":0,"orgPrice":1088},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":75}},{"sessionId":"0bb4413c6ed544ed9ecd7d45d6cb9e46","cabinClass":"A","cabinClassName":"经济舱","cabinCode":"A","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":"2","cabinPrice":1600,"discount":7.3,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1600,"tax":50,"fuel":0,"facePrice":1600,"addPrice":0,"orgPrice":1600},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"ef9b88ad8ffe4a4fa455a10b0b9ababd","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"H","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":1790,"discount":8.2,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1790,"tax":50,"fuel":0,"facePrice":1790,"addPrice":0,"orgPrice":1790},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"e1bc25c242fd482f867173df1ae70ac0","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"H","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":1809,"discount":8.3,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1809,"tax":50,"fuel":0,"facePrice":1820,"addPrice":21,"orgPrice":1788},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"420610332c5a48dfa3be9f80c73e1383","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"H","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":1809,"discount":8.3,"billType":3,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1809,"tax":50,"fuel":0,"facePrice":1820,"addPrice":19,"orgPrice":1790},"child":{"price":1088,"tax":0,"fuel":0,"facePrice":1085,"addPrice":0,"orgPrice":1088},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":75}},{"sessionId":"4f4b7975ee81400b806e9ae4148fb044","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"T","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2010,"discount":9.2,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2010,"tax":50,"fuel":0,"facePrice":2010,"addPrice":0,"orgPrice":2010},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"413a9624abf4430485ff30b69bcf51dd","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"T","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2029,"discount":9.4,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2029,"tax":50,"fuel":0,"facePrice":2040,"addPrice":22,"orgPrice":2007},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"2ae3327eaf4543e2bf42cb14304bdd0c","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"T","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2029,"discount":9.4,"billType":3,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2029,"tax":50,"fuel":0,"facePrice":2040,"addPrice":19,"orgPrice":2010},"child":{"price":1088,"tax":0,"fuel":0,"facePrice":1085,"addPrice":0,"orgPrice":1088},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":75}},{"sessionId":"f2d1160e882f42d99b82214744332755","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"Y","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2150,"discount":9.9,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2150,"tax":50,"fuel":0,"facePrice":2150,"addPrice":0,"orgPrice":2150},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":135}},{"sessionId":"ccd83ca7a03141ec9ca193d5fe5aee8a","cabinClass":"A","cabinClassName":"经济舱","cabinCode":"A","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":"2","cabinPrice":2159,"discount":10,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2159,"tax":50,"fuel":0,"facePrice":2170,"addPrice":23,"orgPrice":2136},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"eb9c18c3b21a4fefbd7d03d248b251bb","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"Y","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2159,"discount":10,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2159,"tax":50,"fuel":0,"facePrice":2170,"addPrice":23,"orgPrice":2136},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"04890fbc7ff046bf975003821b67c66e","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"Y","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2159,"discount":10,"billType":3,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2159,"tax":50,"fuel":0,"facePrice":2170,"addPrice":20,"orgPrice":2139},"child":{"price":1088,"tax":0,"fuel":0,"facePrice":1085,"addPrice":0,"orgPrice":1088},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":75}},{"sessionId":"77a6362925264690a25918c48c207725","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"Y","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2160,"discount":9.9,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2160,"tax":50,"fuel":0,"facePrice":2160,"addPrice":0,"orgPrice":2160},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"6593250214634f3fb17e12a0d74b9b19","cabinClass":"A","cabinClassName":"经济舱","cabinCode":"A","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":"2","cabinPrice":2161,"discount":10,"billType":2,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2161,"tax":50,"fuel":0,"facePrice":2170,"addPrice":15,"orgPrice":2146},"child":{"price":1093,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1093},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":2,"minutes":165}},{"sessionId":"37ff2b155cc34c4d874b4ac1df50a2fc","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"J","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"1","cabinPrice":2805,"discount":4.3,"billType":1,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2805,"tax":50,"fuel":0,"facePrice":2820,"addPrice":38,"orgPrice":2767},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"366681cee2e542749e2812dc8858cef2","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"J","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"1","cabinPrice":2811,"discount":4.3,"billType":2,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2811,"tax":50,"fuel":0,"facePrice":2820,"addPrice":14,"orgPrice":2797},"child":{"price":3267,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3267},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":2,"minutes":165}},{"sessionId":"c8118eedcb8f4214acb80421d4258951","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"J","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"1","cabinPrice":2820,"discount":4.3,"billType":1,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2820,"tax":50,"fuel":0,"facePrice":2820,"addPrice":0,"orgPrice":2820},"child":{"price":3260,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3260},"baby":{"price":660,"tax":0,"fuel":0,"facePrice":660,"addPrice":0,"orgPrice":660}},"time":{"type":1,"minutes":20}},{"sessionId":"c231e39e3615432c99079e87826a0aae","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"J","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"1","cabinPrice":2820,"discount":4.3,"billType":1,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2820,"tax":50,"fuel":0,"facePrice":2820,"addPrice":0,"orgPrice":2820},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":135}},{"sessionId":"d1fb206f76934b03a5e904f0d1e89d61","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"I","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"2","cabinPrice":3454,"discount":5.3,"billType":1,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":3454,"tax":50,"fuel":0,"facePrice":3470,"addPrice":41,"orgPrice":3413},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"0f5fad0b0ee1461ca2f54565330bbeb9","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"I","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"2","cabinPrice":3461,"discount":5.3,"billType":2,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":3461,"tax":50,"fuel":0,"facePrice":3470,"addPrice":13,"orgPrice":3448},"child":{"price":3267,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3267},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":2,"minutes":165}},{"sessionId":"9cb2fe730ec54c929ac3cf32b6abf3ff","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"I","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"2","cabinPrice":3470,"discount":5.3,"billType":1,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":3470,"tax":50,"fuel":0,"facePrice":3470,"addPrice":0,"orgPrice":3470},"child":{"price":3260,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3260},"baby":{"price":660,"tax":0,"fuel":0,"facePrice":660,"addPrice":0,"orgPrice":660}},"time":{"type":1,"minutes":20}},{"sessionId":"aa6e1654ff6645cdb82a58442b118327","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"I","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"2","cabinPrice":3470,"discount":5.3,"billType":1,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":3470,"tax":50,"fuel":0,"facePrice":3470,"addPrice":0,"orgPrice":3470},"child":{"price":3260,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3260},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":135}},{"sessionId":"fd06a72a0be24e94b1ba8349785b153f","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"C","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"7","cabinPrice":6486,"discount":10,"billType":1,"freeLuggage":null,"freeLuggageUnit":null,"overloadLuggage":null,"priceInfo":{"adult":{"price":6486,"tax":50,"fuel":0,"facePrice":6510,"addPrice":72,"orgPrice":6414},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"61847bd885224bfb8e21cdbd3af46591","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"C","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"7","cabinPrice":6510,"discount":10,"billType":1,"freeLuggage":null,"freeLuggageUnit":null,"overloadLuggage":null,"priceInfo":{"adult":{"price":6510,"tax":50,"fuel":0,"facePrice":6510,"addPrice":0,"orgPrice":6510},"child":{"price":3260,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3260},"baby":{"price":660,"tax":0,"fuel":0,"facePrice":660,"addPrice":0,"orgPrice":660}},"time":{"type":1,"minutes":20}},{"sessionId":"c2e419899b1340269c855324c898e45f","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"C","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"7","cabinPrice":6510,"discount":10,"billType":1,"freeLuggage":null,"freeLuggageUnit":null,"overloadLuggage":null,"priceInfo":{"adult":{"price":6510,"tax":50,"fuel":0,"facePrice":6510,"addPrice":0,"orgPrice":6510},"child":{"price":3260,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3260},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":135}}],"stopPointList":null}
     */

    private FlightDetailsBean flightDetails;

    public FlightDetailsBean getFlightDetails() {
        return flightDetails;
    }

    public void setFlightDetails(FlightDetailsBean flightDetails) {
        this.flightDetails = flightDetails;
    }

    @Override
    public String toString() {
        return "FlightDetails{" +
                "flightDetails=" + flightDetails.toString() +
                '}';
    }

    public static class FlightDetailsBean {
        /**
         * flightNo : 3U8830
         * departDate : 2020-06-04
         * departTime : 23:00
         * departCityCode : BJS
         * departCityName : 北京
         * departAirportCode : PEK
         * departAirportName : 首都机场
         * departAirportTerminal : T3
         * arriveCityCode : CKG
         * arriveCityName : 重庆
         * arriveDate : 2020-06-05
         * arriveTime : 01:55
         * arriveAirportCode : CKG
         * arriveAirportName : 江北机场
         * arriveAirportTerminal : T2
         * crossDay : 1
         * craftType : 320(中)
         * airlineCode : 3U
         * airlineName : 四川航空
         * duration : 02:55
         * isShare : 0
         * shareFlightNo : null
         * shareAirline : null
         * shareAirlineName : null
         * mealFlag : 0
         * meal : 无餐
         * stopInformation : null
         * stopNum : 0
         * onTimeRate : 87%
         * cabinList : [{"sessionId":"ee6b19b1242f43bba65c1f3084d63461","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"M","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":180,"discount":0.8,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":180,"tax":50,"fuel":0,"facePrice":180,"addPrice":0,"orgPrice":180},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"172fbe5c86124ce9b40e6d0a8ae209a0","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"M","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":180,"discount":0.8,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":180,"tax":50,"fuel":0,"facePrice":180,"addPrice":0,"orgPrice":180},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":135}},{"sessionId":"bc68db76d9bb4cada5ea1347e6926721","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"Q","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":200,"discount":0.9,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":200,"tax":50,"fuel":0,"facePrice":200,"addPrice":0,"orgPrice":200},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"a2e6c8e3e17a4cf3b35dbe216a03ab72","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"R","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":930,"discount":4.2,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":930,"tax":50,"fuel":0,"facePrice":930,"addPrice":0,"orgPrice":930},"child":{"price":950,"tax":0,"fuel":0,"facePrice":950,"addPrice":0,"orgPrice":950},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":135}},{"sessionId":"acd67f7fb64a4e0386eecd47f41b3f0b","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"R","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":940,"discount":4.3,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":940,"tax":50,"fuel":0,"facePrice":940,"addPrice":0,"orgPrice":940},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"f2a55a955522435c83bed6f73f813fcf","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"R","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":941,"discount":4.3,"billType":3,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":941,"tax":50,"fuel":0,"facePrice":950,"addPrice":12,"orgPrice":929},"child":{"price":1088,"tax":0,"fuel":0,"facePrice":1085,"addPrice":0,"orgPrice":1088},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":75}},{"sessionId":"e48484e5530045659bf50fddd5314e0c","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"R","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":941,"discount":4.3,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":941,"tax":50,"fuel":0,"facePrice":950,"addPrice":11,"orgPrice":930},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"650b2bf8c1e94b40968141c18fe50663","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"L","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":1370,"discount":6.3,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1370,"tax":50,"fuel":0,"facePrice":1370,"addPrice":0,"orgPrice":1370},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"f3599b09bccf4609b58c4b6db3ef46f1","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"L","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":1381,"discount":6.4,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1381,"tax":50,"fuel":0,"facePrice":1390,"addPrice":14,"orgPrice":1367},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"dce0e846eda94408944cf104c7ad626e","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"L","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":1381,"discount":6.4,"billType":3,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1381,"tax":50,"fuel":0,"facePrice":1390,"addPrice":13,"orgPrice":1368},"child":{"price":1088,"tax":0,"fuel":0,"facePrice":1085,"addPrice":0,"orgPrice":1088},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":75}},{"sessionId":"0bb4413c6ed544ed9ecd7d45d6cb9e46","cabinClass":"A","cabinClassName":"经济舱","cabinCode":"A","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":"2","cabinPrice":1600,"discount":7.3,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1600,"tax":50,"fuel":0,"facePrice":1600,"addPrice":0,"orgPrice":1600},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"ef9b88ad8ffe4a4fa455a10b0b9ababd","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"H","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":1790,"discount":8.2,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1790,"tax":50,"fuel":0,"facePrice":1790,"addPrice":0,"orgPrice":1790},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"e1bc25c242fd482f867173df1ae70ac0","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"H","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":1809,"discount":8.3,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1809,"tax":50,"fuel":0,"facePrice":1820,"addPrice":21,"orgPrice":1788},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"420610332c5a48dfa3be9f80c73e1383","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"H","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":1809,"discount":8.3,"billType":3,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":1809,"tax":50,"fuel":0,"facePrice":1820,"addPrice":19,"orgPrice":1790},"child":{"price":1088,"tax":0,"fuel":0,"facePrice":1085,"addPrice":0,"orgPrice":1088},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":75}},{"sessionId":"4f4b7975ee81400b806e9ae4148fb044","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"T","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2010,"discount":9.2,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2010,"tax":50,"fuel":0,"facePrice":2010,"addPrice":0,"orgPrice":2010},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"413a9624abf4430485ff30b69bcf51dd","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"T","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2029,"discount":9.4,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2029,"tax":50,"fuel":0,"facePrice":2040,"addPrice":22,"orgPrice":2007},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"2ae3327eaf4543e2bf42cb14304bdd0c","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"T","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2029,"discount":9.4,"billType":3,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2029,"tax":50,"fuel":0,"facePrice":2040,"addPrice":19,"orgPrice":2010},"child":{"price":1088,"tax":0,"fuel":0,"facePrice":1085,"addPrice":0,"orgPrice":1088},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":75}},{"sessionId":"f2d1160e882f42d99b82214744332755","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"Y","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2150,"discount":9.9,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2150,"tax":50,"fuel":0,"facePrice":2150,"addPrice":0,"orgPrice":2150},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":135}},{"sessionId":"ccd83ca7a03141ec9ca193d5fe5aee8a","cabinClass":"A","cabinClassName":"经济舱","cabinCode":"A","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":"2","cabinPrice":2159,"discount":10,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2159,"tax":50,"fuel":0,"facePrice":2170,"addPrice":23,"orgPrice":2136},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"eb9c18c3b21a4fefbd7d03d248b251bb","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"Y","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2159,"discount":10,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2159,"tax":50,"fuel":0,"facePrice":2170,"addPrice":23,"orgPrice":2136},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"04890fbc7ff046bf975003821b67c66e","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"Y","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2159,"discount":10,"billType":3,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2159,"tax":50,"fuel":0,"facePrice":2170,"addPrice":20,"orgPrice":2139},"child":{"price":1088,"tax":0,"fuel":0,"facePrice":1085,"addPrice":0,"orgPrice":1088},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":75}},{"sessionId":"77a6362925264690a25918c48c207725","cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"Y","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":">9","cabinPrice":2160,"discount":9.9,"billType":1,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2160,"tax":50,"fuel":0,"facePrice":2160,"addPrice":0,"orgPrice":2160},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}},"time":{"type":1,"minutes":20}},{"sessionId":"6593250214634f3fb17e12a0d74b9b19","cabinClass":"A","cabinClassName":"经济舱","cabinCode":"A","cabinName":"经济舱","cabinClassFullPrice":2170,"seatNum":"2","cabinPrice":2161,"discount":10,"billType":2,"freeLuggage":20,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2161,"tax":50,"fuel":0,"facePrice":2170,"addPrice":15,"orgPrice":2146},"child":{"price":1093,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1093},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":2,"minutes":165}},{"sessionId":"37ff2b155cc34c4d874b4ac1df50a2fc","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"J","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"1","cabinPrice":2805,"discount":4.3,"billType":1,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2805,"tax":50,"fuel":0,"facePrice":2820,"addPrice":38,"orgPrice":2767},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"366681cee2e542749e2812dc8858cef2","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"J","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"1","cabinPrice":2811,"discount":4.3,"billType":2,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2811,"tax":50,"fuel":0,"facePrice":2820,"addPrice":14,"orgPrice":2797},"child":{"price":3267,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3267},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":2,"minutes":165}},{"sessionId":"c8118eedcb8f4214acb80421d4258951","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"J","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"1","cabinPrice":2820,"discount":4.3,"billType":1,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2820,"tax":50,"fuel":0,"facePrice":2820,"addPrice":0,"orgPrice":2820},"child":{"price":3260,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3260},"baby":{"price":660,"tax":0,"fuel":0,"facePrice":660,"addPrice":0,"orgPrice":660}},"time":{"type":1,"minutes":20}},{"sessionId":"c231e39e3615432c99079e87826a0aae","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"J","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"1","cabinPrice":2820,"discount":4.3,"billType":1,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":2820,"tax":50,"fuel":0,"facePrice":2820,"addPrice":0,"orgPrice":2820},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":135}},{"sessionId":"d1fb206f76934b03a5e904f0d1e89d61","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"I","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"2","cabinPrice":3454,"discount":5.3,"billType":1,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":3454,"tax":50,"fuel":0,"facePrice":3470,"addPrice":41,"orgPrice":3413},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"0f5fad0b0ee1461ca2f54565330bbeb9","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"I","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"2","cabinPrice":3461,"discount":5.3,"billType":2,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":3461,"tax":50,"fuel":0,"facePrice":3470,"addPrice":13,"orgPrice":3448},"child":{"price":3267,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3267},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":2,"minutes":165}},{"sessionId":"9cb2fe730ec54c929ac3cf32b6abf3ff","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"I","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"2","cabinPrice":3470,"discount":5.3,"billType":1,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":3470,"tax":50,"fuel":0,"facePrice":3470,"addPrice":0,"orgPrice":3470},"child":{"price":3260,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3260},"baby":{"price":660,"tax":0,"fuel":0,"facePrice":660,"addPrice":0,"orgPrice":660}},"time":{"type":1,"minutes":20}},{"sessionId":"aa6e1654ff6645cdb82a58442b118327","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"I","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"2","cabinPrice":3470,"discount":5.3,"billType":1,"freeLuggage":40,"freeLuggageUnit":"KG","overloadLuggage":"","priceInfo":{"adult":{"price":3470,"tax":50,"fuel":0,"facePrice":3470,"addPrice":0,"orgPrice":3470},"child":{"price":3260,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3260},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":135}},{"sessionId":"fd06a72a0be24e94b1ba8349785b153f","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"C","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"7","cabinPrice":6486,"discount":10,"billType":1,"freeLuggage":null,"freeLuggageUnit":null,"overloadLuggage":null,"priceInfo":{"adult":{"price":6486,"tax":50,"fuel":0,"facePrice":6510,"addPrice":72,"orgPrice":6414},"child":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":20}},{"sessionId":"61847bd885224bfb8e21cdbd3af46591","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"C","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"7","cabinPrice":6510,"discount":10,"billType":1,"freeLuggage":null,"freeLuggageUnit":null,"overloadLuggage":null,"priceInfo":{"adult":{"price":6510,"tax":50,"fuel":0,"facePrice":6510,"addPrice":0,"orgPrice":6510},"child":{"price":3260,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3260},"baby":{"price":660,"tax":0,"fuel":0,"facePrice":660,"addPrice":0,"orgPrice":660}},"time":{"type":1,"minutes":20}},{"sessionId":"c2e419899b1340269c855324c898e45f","cabinClass":"C","cabinClassName":"公务舱","cabinCode":"C","cabinName":"商务舱","cabinClassFullPrice":6510,"seatNum":"7","cabinPrice":6510,"discount":10,"billType":1,"freeLuggage":null,"freeLuggageUnit":null,"overloadLuggage":null,"priceInfo":{"adult":{"price":6510,"tax":50,"fuel":0,"facePrice":6510,"addPrice":0,"orgPrice":6510},"child":{"price":3260,"tax":0,"fuel":0,"facePrice":3260,"addPrice":0,"orgPrice":3260},"baby":{"price":0,"tax":0,"fuel":0,"facePrice":0,"addPrice":0,"orgPrice":0}},"time":{"type":1,"minutes":135}}]
         * stopPointList : null
         */

        private String flightNo;
        private String departDate;
        private String departTime;
        private String departCityCode;
        private String departCityName;
        private String departAirportCode;
        private String departAirportName;
        private String departAirportTerminal;
        private String arriveCityCode;
        private String arriveCityName;
        private String arriveDate;
        private String arriveTime;
        private String arriveAirportCode;
        private String arriveAirportName;
        private String arriveAirportTerminal;
        private int crossDay;
        private String craftType;
        private String airlineCode;
        private String airlineName;
        private String duration;
        private int isShare;
        private Object shareFlightNo;
        private Object shareAirline;
        private Object shareAirlineName;
        private int mealFlag;
        private String meal;
        private Object stopInformation;
        private String stopNum;
        private String onTimeRate;
        private Object stopPointList;
        private List<CabinListBean> cabinList;

        @Override
        public String toString() {
            return "FlightDetailsBean{" +
                    "flightNo='" + flightNo + '\'' +
                    ", departDate='" + departDate + '\'' +
                    ", departTime='" + departTime + '\'' +
                    ", departCityCode='" + departCityCode + '\'' +
                    ", departCityName='" + departCityName + '\'' +
                    ", departAirportCode='" + departAirportCode + '\'' +
                    ", departAirportName='" + departAirportName + '\'' +
                    ", departAirportTerminal='" + departAirportTerminal + '\'' +
                    ", arriveCityCode='" + arriveCityCode + '\'' +
                    ", arriveCityName='" + arriveCityName + '\'' +
                    ", arriveDate='" + arriveDate + '\'' +
                    ", arriveTime='" + arriveTime + '\'' +
                    ", arriveAirportCode='" + arriveAirportCode + '\'' +
                    ", arriveAirportName='" + arriveAirportName + '\'' +
                    ", arriveAirportTerminal='" + arriveAirportTerminal + '\'' +
                    ", crossDay=" + crossDay +
                    ", craftType='" + craftType + '\'' +
                    ", airlineCode='" + airlineCode + '\'' +
                    ", airlineName='" + airlineName + '\'' +
                    ", duration='" + duration + '\'' +
                    ", isShare=" + isShare +
                    ", shareFlightNo=" + shareFlightNo.toString() +
                    ", shareAirline=" + shareAirline.toString() +
                    ", shareAirlineName=" + shareAirlineName.toString() +
                    ", mealFlag=" + mealFlag +
                    ", meal='" + meal + '\'' +
                    ", stopInformation=" + stopInformation.toString() +
                    ", stopNum='" + stopNum + '\'' +
                    ", onTimeRate='" + onTimeRate + '\'' +
                    ", stopPointList=" + stopPointList.toString() +
                    ", cabinList=" + cabinList +
                    '}';
        }

        public String getFlightNo() {
            return flightNo;
        }

        public void setFlightNo(String flightNo) {
            this.flightNo = flightNo;
        }

        public String getDepartDate() {
            return departDate;
        }

        public void setDepartDate(String departDate) {
            this.departDate = departDate;
        }

        public String getDepartTime() {
            return departTime;
        }

        public void setDepartTime(String departTime) {
            this.departTime = departTime;
        }

        public String getDepartCityCode() {
            return departCityCode;
        }

        public void setDepartCityCode(String departCityCode) {
            this.departCityCode = departCityCode;
        }

        public String getDepartCityName() {
            return departCityName;
        }

        public void setDepartCityName(String departCityName) {
            this.departCityName = departCityName;
        }

        public String getDepartAirportCode() {
            return departAirportCode;
        }

        public void setDepartAirportCode(String departAirportCode) {
            this.departAirportCode = departAirportCode;
        }

        public String getDepartAirportName() {
            return departAirportName;
        }

        public void setDepartAirportName(String departAirportName) {
            this.departAirportName = departAirportName;
        }

        public String getDepartAirportTerminal() {
            return departAirportTerminal;
        }

        public void setDepartAirportTerminal(String departAirportTerminal) {
            this.departAirportTerminal = departAirportTerminal;
        }

        public String getArriveCityCode() {
            return arriveCityCode;
        }

        public void setArriveCityCode(String arriveCityCode) {
            this.arriveCityCode = arriveCityCode;
        }

        public String getArriveCityName() {
            return arriveCityName;
        }

        public void setArriveCityName(String arriveCityName) {
            this.arriveCityName = arriveCityName;
        }

        public String getArriveDate() {
            return arriveDate;
        }

        public void setArriveDate(String arriveDate) {
            this.arriveDate = arriveDate;
        }

        public String getArriveTime() {
            return arriveTime;
        }

        public void setArriveTime(String arriveTime) {
            this.arriveTime = arriveTime;
        }

        public String getArriveAirportCode() {
            return arriveAirportCode;
        }

        public void setArriveAirportCode(String arriveAirportCode) {
            this.arriveAirportCode = arriveAirportCode;
        }

        public String getArriveAirportName() {
            return arriveAirportName;
        }

        public void setArriveAirportName(String arriveAirportName) {
            this.arriveAirportName = arriveAirportName;
        }

        public String getArriveAirportTerminal() {
            return arriveAirportTerminal;
        }

        public void setArriveAirportTerminal(String arriveAirportTerminal) {
            this.arriveAirportTerminal = arriveAirportTerminal;
        }

        public int getCrossDay() {
            return crossDay;
        }

        public void setCrossDay(int crossDay) {
            this.crossDay = crossDay;
        }

        public String getCraftType() {
            return craftType;
        }

        public void setCraftType(String craftType) {
            this.craftType = craftType;
        }

        public String getAirlineCode() {
            return airlineCode;
        }

        public void setAirlineCode(String airlineCode) {
            this.airlineCode = airlineCode;
        }

        public String getAirlineName() {
            return airlineName;
        }

        public void setAirlineName(String airlineName) {
            this.airlineName = airlineName;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public int getIsShare() {
            return isShare;
        }

        public void setIsShare(int isShare) {
            this.isShare = isShare;
        }

        public Object getShareFlightNo() {
            return shareFlightNo;
        }

        public void setShareFlightNo(Object shareFlightNo) {
            this.shareFlightNo = shareFlightNo;
        }

        public Object getShareAirline() {
            return shareAirline;
        }

        public void setShareAirline(Object shareAirline) {
            this.shareAirline = shareAirline;
        }

        public Object getShareAirlineName() {
            return shareAirlineName;
        }

        public void setShareAirlineName(Object shareAirlineName) {
            this.shareAirlineName = shareAirlineName;
        }

        public int getMealFlag() {
            return mealFlag;
        }

        public void setMealFlag(int mealFlag) {
            this.mealFlag = mealFlag;
        }

        public String getMeal() {
            return meal;
        }

        public void setMeal(String meal) {
            this.meal = meal;
        }

        public Object getStopInformation() {
            return stopInformation;
        }

        public void setStopInformation(Object stopInformation) {
            this.stopInformation = stopInformation;
        }

        public String getStopNum() {
            return stopNum;
        }

        public void setStopNum(String stopNum) {
            this.stopNum = stopNum;
        }

        public String getOnTimeRate() {
            return onTimeRate;
        }

        public void setOnTimeRate(String onTimeRate) {
            this.onTimeRate = onTimeRate;
        }

        public Object getStopPointList() {
            return stopPointList;
        }

        public void setStopPointList(Object stopPointList) {
            this.stopPointList = stopPointList;
        }

        public List<CabinListBean> getCabinList() {
            return cabinList;
        }

        public void setCabinList(List<CabinListBean> cabinList) {
            this.cabinList = cabinList;
        }

        public static class CabinListBean {
            @Override
            public String toString() {
                return "CabinListBean{" +
                        "sessionId='" + sessionId + '\'' +
                        ", cabinClass='" + cabinClass + '\'' +
                        ", cabinClassName='" + cabinClassName + '\'' +
                        ", cabinCode='" + cabinCode + '\'' +
                        ", cabinName='" + cabinName + '\'' +
                        ", cabinClassFullPrice=" + cabinClassFullPrice +
                        ", seatNum='" + seatNum + '\'' +
                        ", cabinPrice=" + cabinPrice +
                        ", discount=" + discount +
                        ", billType=" + billType +
                        ", freeLuggage=" + freeLuggage +
                        ", freeLuggageUnit='" + freeLuggageUnit + '\'' +
                        ", overloadLuggage='" + overloadLuggage + '\'' +
                        ", priceInfo=" + priceInfo.toString() +
                        ", time=" + time.toString() +
                        '}';
            }

            /**
             * sessionId : ee6b19b1242f43bba65c1f3084d63461
             * cabinClass : Y
             * cabinClassName : 经济舱
             * cabinCode : M
             * cabinName : 经济舱
             * cabinClassFullPrice : 2170
             * seatNum : >9
             * cabinPrice : 180
             * discount : 0.8
             * billType : 1
             * freeLuggage : 20
             * freeLuggageUnit : KG
             * overloadLuggage :
             * priceInfo : {"adult":{"price":180,"tax":50,"fuel":0,"facePrice":180,"addPrice":0,"orgPrice":180},"child":{"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090},"baby":{"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}}
             * time : {"type":1,"minutes":20}
             */

            private String sessionId;
            private String cabinClass;
            private String cabinClassName;
            private String cabinCode;
            private String cabinName;
            private int cabinClassFullPrice;
            private String seatNum;
            private int cabinPrice;
            private double discount;
            private int billType;
            private int freeLuggage;
            private String freeLuggageUnit;
            private String overloadLuggage;
            private PriceInfoBean priceInfo;
            private TimeBean time;

            public String getSessionId() {
                return sessionId;
            }

            public void setSessionId(String sessionId) {
                this.sessionId = sessionId;
            }

            public String getCabinClass() {
                return cabinClass;
            }

            public void setCabinClass(String cabinClass) {
                this.cabinClass = cabinClass;
            }

            public String getCabinClassName() {
                return cabinClassName;
            }

            public void setCabinClassName(String cabinClassName) {
                this.cabinClassName = cabinClassName;
            }

            public String getCabinCode() {
                return cabinCode;
            }

            public void setCabinCode(String cabinCode) {
                this.cabinCode = cabinCode;
            }

            public String getCabinName() {
                return cabinName;
            }

            public void setCabinName(String cabinName) {
                this.cabinName = cabinName;
            }

            public int getCabinClassFullPrice() {
                return cabinClassFullPrice;
            }

            public void setCabinClassFullPrice(int cabinClassFullPrice) {
                this.cabinClassFullPrice = cabinClassFullPrice;
            }

            public String getSeatNum() {
                return seatNum;
            }

            public void setSeatNum(String seatNum) {
                this.seatNum = seatNum;
            }

            public int getCabinPrice() {
                return cabinPrice;
            }

            public void setCabinPrice(int cabinPrice) {
                this.cabinPrice = cabinPrice;
            }

            public double getDiscount() {
                return discount;
            }

            public void setDiscount(double discount) {
                this.discount = discount;
            }

            public int getBillType() {
                return billType;
            }

            public void setBillType(int billType) {
                this.billType = billType;
            }

            public int getFreeLuggage() {
                return freeLuggage;
            }

            public void setFreeLuggage(int freeLuggage) {
                this.freeLuggage = freeLuggage;
            }

            public String getFreeLuggageUnit() {
                return freeLuggageUnit;
            }

            public void setFreeLuggageUnit(String freeLuggageUnit) {
                this.freeLuggageUnit = freeLuggageUnit;
            }

            public String getOverloadLuggage() {
                return overloadLuggage;
            }

            public void setOverloadLuggage(String overloadLuggage) {
                this.overloadLuggage = overloadLuggage;
            }

            public PriceInfoBean getPriceInfo() {
                return priceInfo;
            }

            public void setPriceInfo(PriceInfoBean priceInfo) {
                this.priceInfo = priceInfo;
            }

            public TimeBean getTime() {
                return time;
            }

            public void setTime(TimeBean time) {
                this.time = time;
            }

            public static class PriceInfoBean {
                @Override
                public String toString() {
                    return "PriceInfoBean{" +
                            "adult=" + adult.toString() +
                            ", child=" + child.toString() +
                            ", baby=" + baby.toString() +
                            '}';
                }

                /**
                 * adult : {"price":180,"tax":50,"fuel":0,"facePrice":180,"addPrice":0,"orgPrice":180}
                 * child : {"price":1090,"tax":0,"fuel":0,"facePrice":1090,"addPrice":0,"orgPrice":1090}
                 * baby : {"price":220,"tax":0,"fuel":0,"facePrice":220,"addPrice":0,"orgPrice":220}
                 */

                private AdultBean adult;
                private ChildBean child;
                private BabyBean baby;

                public AdultBean getAdult() {
                    return adult;
                }

                public void setAdult(AdultBean adult) {
                    this.adult = adult;
                }

                public ChildBean getChild() {
                    return child;
                }

                public void setChild(ChildBean child) {
                    this.child = child;
                }

                public BabyBean getBaby() {
                    return baby;
                }

                public void setBaby(BabyBean baby) {
                    this.baby = baby;
                }

                public static class AdultBean {
                    @Override
                    public String toString() {
                        return "AdultBean{" +
                                "price=" + price +
                                ", tax=" + tax +
                                ", fuel=" + fuel +
                                ", facePrice=" + facePrice +
                                ", addPrice=" + addPrice +
                                ", orgPrice=" + orgPrice +
                                '}';
                    }

                    /**
                     * price : 180
                     * tax : 50
                     * fuel : 0
                     * facePrice : 180
                     * addPrice : 0
                     * orgPrice : 180
                     */

                    private int price;
                    private int tax;
                    private int fuel;
                    private int facePrice;
                    private int addPrice;
                    private int orgPrice;

                    public int getPrice() {
                        return price;
                    }

                    public void setPrice(int price) {
                        this.price = price;
                    }

                    public int getTax() {
                        return tax;
                    }

                    public void setTax(int tax) {
                        this.tax = tax;
                    }

                    public int getFuel() {
                        return fuel;
                    }

                    public void setFuel(int fuel) {
                        this.fuel = fuel;
                    }

                    public int getFacePrice() {
                        return facePrice;
                    }

                    public void setFacePrice(int facePrice) {
                        this.facePrice = facePrice;
                    }

                    public int getAddPrice() {
                        return addPrice;
                    }

                    public void setAddPrice(int addPrice) {
                        this.addPrice = addPrice;
                    }

                    public int getOrgPrice() {
                        return orgPrice;
                    }

                    public void setOrgPrice(int orgPrice) {
                        this.orgPrice = orgPrice;
                    }
                }

                public static class ChildBean {
                    @Override
                    public String toString() {
                        return "ChildBean{" +
                                "price=" + price +
                                ", tax=" + tax +
                                ", fuel=" + fuel +
                                ", facePrice=" + facePrice +
                                ", addPrice=" + addPrice +
                                ", orgPrice=" + orgPrice +
                                '}';
                    }

                    /**
                     * price : 1090
                     * tax : 0
                     * fuel : 0
                     * facePrice : 1090
                     * addPrice : 0
                     * orgPrice : 1090
                     */

                    private int price;
                    private int tax;
                    private int fuel;
                    private int facePrice;
                    private int addPrice;
                    private int orgPrice;

                    public int getPrice() {
                        return price;
                    }

                    public void setPrice(int price) {
                        this.price = price;
                    }

                    public int getTax() {
                        return tax;
                    }

                    public void setTax(int tax) {
                        this.tax = tax;
                    }

                    public int getFuel() {
                        return fuel;
                    }

                    public void setFuel(int fuel) {
                        this.fuel = fuel;
                    }

                    public int getFacePrice() {
                        return facePrice;
                    }

                    public void setFacePrice(int facePrice) {
                        this.facePrice = facePrice;
                    }

                    public int getAddPrice() {
                        return addPrice;
                    }

                    public void setAddPrice(int addPrice) {
                        this.addPrice = addPrice;
                    }

                    public int getOrgPrice() {
                        return orgPrice;
                    }

                    public void setOrgPrice(int orgPrice) {
                        this.orgPrice = orgPrice;
                    }
                }

                public static class BabyBean {
                    @Override
                    public String toString() {
                        return "BabyBean{" +
                                "price=" + price +
                                ", tax=" + tax +
                                ", fuel=" + fuel +
                                ", facePrice=" + facePrice +
                                ", addPrice=" + addPrice +
                                ", orgPrice=" + orgPrice +
                                '}';
                    }

                    /**
                     * price : 220
                     * tax : 0
                     * fuel : 0
                     * facePrice : 220
                     * addPrice : 0
                     * orgPrice : 220
                     */

                    private int price;
                    private int tax;
                    private int fuel;
                    private int facePrice;
                    private int addPrice;
                    private int orgPrice;

                    public int getPrice() {
                        return price;
                    }

                    public void setPrice(int price) {
                        this.price = price;
                    }

                    public int getTax() {
                        return tax;
                    }

                    public void setTax(int tax) {
                        this.tax = tax;
                    }

                    public int getFuel() {
                        return fuel;
                    }

                    public void setFuel(int fuel) {
                        this.fuel = fuel;
                    }

                    public int getFacePrice() {
                        return facePrice;
                    }

                    public void setFacePrice(int facePrice) {
                        this.facePrice = facePrice;
                    }

                    public int getAddPrice() {
                        return addPrice;
                    }

                    public void setAddPrice(int addPrice) {
                        this.addPrice = addPrice;
                    }

                    public int getOrgPrice() {
                        return orgPrice;
                    }

                    public void setOrgPrice(int orgPrice) {
                        this.orgPrice = orgPrice;
                    }
                }
            }

            public static class TimeBean {
                @Override
                public String toString() {
                    return "TimeBean{" +
                            "type=" + type +
                            ", minutes=" + minutes +
                            '}';
                }

                /**
                 * type : 1
                 * minutes : 20
                 */

                private int type;
                private int minutes;

                public int getType() {
                    return type;
                }

                public void setType(int type) {
                    this.type = type;
                }

                public int getMinutes() {
                    return minutes;
                }

                public void setMinutes(int minutes) {
                    this.minutes = minutes;
                }
            }
        }
    }
}
