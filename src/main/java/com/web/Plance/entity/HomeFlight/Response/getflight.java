package com.web.Plance.entity.HomeFlight.Response;

import java.util.List;

public class getflight {

    /**
     * flightNo : 3U8819
     * departDate : 2020-06-04
     * departTime : 06:50
     * departCityCode : BJS
     * departCityName : 北京
     * departAirportCode : PEK
     * departAirportName : 首都机场
     * departAirportTerminal : T3
     * arriveCityCode : CKG
     * arriveCityName : 重庆
     * arriveDate : 2020-06-04
     * arriveTime : 12:20
     * arriveAirportCode : CKG
     * arriveAirportName : 江北机场
     * arriveAirportTerminal : T2
     * crossDay : 0
     * craftType : 319
     * airlineCode : 3U
     * airlineName : 四川航空
     * duration : 05:30
     * isShare : 0
     * shareFlightNo : null
     * shareAirline : null
     * shareAirlineName : null
     * mealFlag : 1
     * meal : 有餐
     * stopInformation : null
     * stopNum : 1
     * onTimeRate : 87%
     * stopPointList : [{"stopCityCode":"ZHY","stopCityName":"中卫","airPortCode":"ZHY","airPortName":"香山机场","arrivalTime":"09:10","departrueTime":"10:15"}]
     * lowCabin : {"cabinClass":"Y","cabinClassName":"经济舱","cabinCode":"M","cabinName":"经济舱","seatNum":">9","cabinClassFullPrice":null,"cabinPrice":180,"discount":0.8,"addPrice":0,"orgPrice":180}
     */

    private String flightNo;
    private String departDate;
    private String departTime;
    private String departCityCode;
    private String departCityName;
    private String departAirportCode;
    private String departAirportName;
    private String departAirportTerminal;
    private String arriveCityCode;
    private String arriveCityName;
    private String arriveDate;
    private String arriveTime;
    private String arriveAirportCode;
    private String arriveAirportName;
    private String arriveAirportTerminal;
    private int crossDay;
    private String craftType;
    private String airlineCode;
    private String airlineName;
    private String duration;
    private int isShare;
    private Object shareFlightNo;
    private Object shareAirline;
    private Object shareAirlineName;
    private int mealFlag;
    private String meal;
    private Object stopInformation;
    private String stopNum;
    private String onTimeRate;
    private LowCabinBean lowCabin;
    private List<StopPointListBean> stopPointList;

    public String getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(String flightNo) {
        this.flightNo = flightNo;
    }

    public String getDepartDate() {
        return departDate;
    }

    public void setDepartDate(String departDate) {
        this.departDate = departDate;
    }

    public String getDepartTime() {
        return departTime;
    }

    public void setDepartTime(String departTime) {
        this.departTime = departTime;
    }

    public String getDepartCityCode() {
        return departCityCode;
    }

    public void setDepartCityCode(String departCityCode) {
        this.departCityCode = departCityCode;
    }

    public String getDepartCityName() {
        return departCityName;
    }

    public void setDepartCityName(String departCityName) {
        this.departCityName = departCityName;
    }

    public String getDepartAirportCode() {
        return departAirportCode;
    }

    public void setDepartAirportCode(String departAirportCode) {
        this.departAirportCode = departAirportCode;
    }

    public String getDepartAirportName() {
        return departAirportName;
    }

    public void setDepartAirportName(String departAirportName) {
        this.departAirportName = departAirportName;
    }

    public String getDepartAirportTerminal() {
        return departAirportTerminal;
    }

    public void setDepartAirportTerminal(String departAirportTerminal) {
        this.departAirportTerminal = departAirportTerminal;
    }

    public String getArriveCityCode() {
        return arriveCityCode;
    }

    public void setArriveCityCode(String arriveCityCode) {
        this.arriveCityCode = arriveCityCode;
    }

    public String getArriveCityName() {
        return arriveCityName;
    }

    public void setArriveCityName(String arriveCityName) {
        this.arriveCityName = arriveCityName;
    }

    public String getArriveDate() {
        return arriveDate;
    }

    public void setArriveDate(String arriveDate) {
        this.arriveDate = arriveDate;
    }

    public String getArriveTime() {
        return arriveTime;
    }

    public void setArriveTime(String arriveTime) {
        this.arriveTime = arriveTime;
    }

    public String getArriveAirportCode() {
        return arriveAirportCode;
    }

    public void setArriveAirportCode(String arriveAirportCode) {
        this.arriveAirportCode = arriveAirportCode;
    }

    public String getArriveAirportName() {
        return arriveAirportName;
    }

    public void setArriveAirportName(String arriveAirportName) {
        this.arriveAirportName = arriveAirportName;
    }

    public String getArriveAirportTerminal() {
        return arriveAirportTerminal;
    }

    public void setArriveAirportTerminal(String arriveAirportTerminal) {
        this.arriveAirportTerminal = arriveAirportTerminal;
    }

    public int getCrossDay() {
        return crossDay;
    }

    public void setCrossDay(int crossDay) {
        this.crossDay = crossDay;
    }

    public String getCraftType() {
        return craftType;
    }

    public void setCraftType(String craftType) {
        this.craftType = craftType;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public String getAirlineName() {
        return airlineName;
    }

    public void setAirlineName(String airlineName) {
        this.airlineName = airlineName;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public int getIsShare() {
        return isShare;
    }

    public void setIsShare(int isShare) {
        this.isShare = isShare;
    }

    public Object getShareFlightNo() {
        return shareFlightNo;
    }

    public void setShareFlightNo(Object shareFlightNo) {
        this.shareFlightNo = shareFlightNo;
    }

    public Object getShareAirline() {
        return shareAirline;
    }

    public void setShareAirline(Object shareAirline) {
        this.shareAirline = shareAirline;
    }

    public Object getShareAirlineName() {
        return shareAirlineName;
    }

    public void setShareAirlineName(Object shareAirlineName) {
        this.shareAirlineName = shareAirlineName;
    }

    public int getMealFlag() {
        return mealFlag;
    }

    public void setMealFlag(int mealFlag) {
        this.mealFlag = mealFlag;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }

    public Object getStopInformation() {
        return stopInformation;
    }

    public void setStopInformation(Object stopInformation) {
        this.stopInformation = stopInformation;
    }

    public String getStopNum() {
        return stopNum;
    }

    public void setStopNum(String stopNum) {
        this.stopNum = stopNum;
    }

    public String getOnTimeRate() {
        return onTimeRate;
    }

    public void setOnTimeRate(String onTimeRate) {
        this.onTimeRate = onTimeRate;
    }

    public LowCabinBean getLowCabin() {
        return lowCabin;
    }

    public void setLowCabin(LowCabinBean lowCabin) {
        this.lowCabin = lowCabin;
    }

    public List<StopPointListBean> getStopPointList() {
        return stopPointList;
    }

    public void setStopPointList(List<StopPointListBean> stopPointList) {
        this.stopPointList = stopPointList;
    }

    @Override
    public String toString() {
        return "getflight{" +
                "flightNo='" + flightNo + '\'' +
                ", departDate='" + departDate + '\'' +
                ", departTime='" + departTime + '\'' +
                ", departCityCode='" + departCityCode + '\'' +
                ", departCityName='" + departCityName + '\'' +
                ", departAirportCode='" + departAirportCode + '\'' +
                ", departAirportName='" + departAirportName + '\'' +
                ", departAirportTerminal='" + departAirportTerminal + '\'' +
                ", arriveCityCode='" + arriveCityCode + '\'' +
                ", arriveCityName='" + arriveCityName + '\'' +
                ", arriveDate='" + arriveDate + '\'' +
                ", arriveTime='" + arriveTime + '\'' +
                ", arriveAirportCode='" + arriveAirportCode + '\'' +
                ", arriveAirportName='" + arriveAirportName + '\'' +
                ", arriveAirportTerminal='" + arriveAirportTerminal + '\'' +
                ", crossDay=" + crossDay +
                ", craftType='" + craftType + '\'' +
                ", airlineCode='" + airlineCode + '\'' +
                ", airlineName='" + airlineName + '\'' +
                ", duration='" + duration + '\'' +
                ", isShare=" + isShare +
                ", shareFlightNo=" + shareFlightNo +
                ", shareAirline=" + shareAirline +
                ", shareAirlineName=" + shareAirlineName +
                ", mealFlag=" + mealFlag +
                ", meal='" + meal + '\'' +
                ", stopInformation=" + stopInformation +
                ", stopNum='" + stopNum + '\'' +
                ", onTimeRate='" + onTimeRate + '\'' +
                ", lowCabin=" + lowCabin +
                ", stopPointList=" + stopPointList +
                '}';
    }

    public getflight()
    {

    }

    public static class LowCabinBean {
        /**
         * cabinClass : Y
         * cabinClassName : 经济舱
         * cabinCode : M
         * cabinName : 经济舱
         * seatNum : >9
         * cabinClassFullPrice : null
         * cabinPrice : 180
         * discount : 0.8
         * addPrice : 0
         * orgPrice : 180
         */
        public LowCabinBean()
        {}

        private String cabinClass;
        private String cabinClassName;
        private String cabinCode;
        private String cabinName;
        private String seatNum;
        private Object cabinClassFullPrice;
        private int cabinPrice;
        private double discount;
        private int addPrice;
        private int orgPrice;

        public String getCabinClass() {
            return cabinClass;
        }

        public void setCabinClass(String cabinClass) {
            this.cabinClass = cabinClass;
        }

        public String getCabinClassName() {
            return cabinClassName;
        }

        public void setCabinClassName(String cabinClassName) {
            this.cabinClassName = cabinClassName;
        }

        public String getCabinCode() {
            return cabinCode;
        }

        public void setCabinCode(String cabinCode) {
            this.cabinCode = cabinCode;
        }

        public String getCabinName() {
            return cabinName;
        }

        public void setCabinName(String cabinName) {
            this.cabinName = cabinName;
        }

        public String getSeatNum() {
            return seatNum;
        }

        public void setSeatNum(String seatNum) {
            this.seatNum = seatNum;
        }

        public Object getCabinClassFullPrice() {
            return cabinClassFullPrice;
        }

        public void setCabinClassFullPrice(Object cabinClassFullPrice) {
            this.cabinClassFullPrice = cabinClassFullPrice;
        }

        public int getCabinPrice() {
            return cabinPrice;
        }

        public void setCabinPrice(int cabinPrice) {
            this.cabinPrice = cabinPrice;
        }

        public double getDiscount() {
            return discount;
        }

        public void setDiscount(double discount) {
            this.discount = discount;
        }

        public int getAddPrice() {
            return addPrice;
        }

        public void setAddPrice(int addPrice) {
            this.addPrice = addPrice;
        }

        public int getOrgPrice() {
            return orgPrice;
        }

        public void setOrgPrice(int orgPrice) {
            this.orgPrice = orgPrice;
        }

        @Override
        public String toString() {
            return "LowCabinBean{" +
                    "cabinClass='" + cabinClass + '\'' +
                    ", cabinClassName='" + cabinClassName + '\'' +
                    ", cabinCode='" + cabinCode + '\'' +
                    ", cabinName='" + cabinName + '\'' +
                    ", seatNum='" + seatNum + '\'' +
                    ", cabinClassFullPrice=" + cabinClassFullPrice +
                    ", cabinPrice=" + cabinPrice +
                    ", discount=" + discount +
                    ", addPrice=" + addPrice +
                    ", orgPrice=" + orgPrice +
                    '}';
        }
    }


    public static class StopPointListBean {
        /**
         * stopCityCode : ZHY
         * stopCityName : 中卫
         * airPortCode : ZHY
         * airPortName : 香山机场
         * arrivalTime : 09:10
         * departrueTime : 10:15
         */
        public StopPointListBean()
        {
        }

        private String stopCityCode;
        private String stopCityName;
        private String airPortCode;
        private String airPortName;
        private String arrivalTime;
        private String departrueTime;

        public String getStopCityCode() {
            return stopCityCode;
        }

        public void setStopCityCode(String stopCityCode) {
            this.stopCityCode = stopCityCode;
        }

        public String getStopCityName() {
            return stopCityName;
        }

        public void setStopCityName(String stopCityName) {
            this.stopCityName = stopCityName;
        }

        public String getAirPortCode() {
            return airPortCode;
        }

        public void setAirPortCode(String airPortCode) {
            this.airPortCode = airPortCode;
        }

        public String getAirPortName() {
            return airPortName;
        }

        public void setAirPortName(String airPortName) {
            this.airPortName = airPortName;
        }

        public String getArrivalTime() {
            return arrivalTime;
        }

        public void setArrivalTime(String arrivalTime) {
            this.arrivalTime = arrivalTime;
        }

        public String getDepartrueTime() {
            return departrueTime;
        }

        public void setDepartrueTime(String departrueTime) {
            this.departrueTime = departrueTime;
        }

        @Override
        public String toString() {
            return "StopPointListBean{" +
                    "stopCityCode='" + stopCityCode + '\'' +
                    ", stopCityName='" + stopCityName + '\'' +
                    ", airPortCode='" + airPortCode + '\'' +
                    ", airPortName='" + airPortName + '\'' +
                    ", arrivalTime='" + arrivalTime + '\'' +
                    ", departrueTime='" + departrueTime + '\'' +
                    '}';
        }
    }
}

