package com.web.Plance.entity.Order.Request.CloseOrder;

import java.util.List;

/**
 * 退票附件上传接口
 */
public class Refundupload {
    /**
     * 订单ID
     */
    private Integer orderid;
    /**
     * 会员ID  N
     */
    private long memberid;

    /**
     * 附件列表
     */
    private List<AttachmentUpload> attachments;

    public List<AttachmentUpload> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<AttachmentUpload> attachments) {
        this.attachments = attachments;
    }

    public long getMemberid() {
        return memberid;
    }

    public void setMemberid(long memberid) {
        this.memberid = memberid;
    }

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }
}
