package com.web.Plance.entity.Order.Response;

import java.util.List;

/**
 * 退票附件上传返回参数
 */
public class refunduploadResponse {
    private List<Attachment> attachmentList;

    public List<Attachment> getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(List<Attachment> attachmentList) {
        this.attachmentList = attachmentList;
    }
}
