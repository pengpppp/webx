package com.web.Plance.entity.Order.Request.CloseOrder;

/**
 * 取消订单
 */
public class Cancelorder {
    /**
     * 订单ID
     */
    private Integer orderid;
    /**
     * 会员ID  N
     */
    private long memberid;

    public long getMemberid() {
        return memberid;
    }

    public void setMemberid(long memberid) {
        this.memberid = memberid;
    }

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }
}
