package com.web.Plance.entity.Order;

public class City {
    private Integer id;
    private String code;
    private String countrycnname;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCountrycnname() {
        return countrycnname;
    }

    public void setCountrycnname(String countrycnname) {
        this.countrycnname = countrycnname;
    }
}
