package com.web.Plance.entity.Order.Request.CloseOrder;

/**
 * 退票单详情接口
 */
public class Getrefundorderdetails {
    /**
     * 退票单ID
     */
    private long refundid;

    public long getRefundid() {
        return refundid;
    }

    public void setRefundid(long refundid) {
        this.refundid = refundid;
    }
}
