package com.web.Plance.entity.Order.Request.Order;

/**
 * 订单详情
 */
public class Getorderdetails {
    /**
     * 订单ID N  orderid和ordernumber至少有一个不能为空
     */
    private long orderid;
    /**
     * 订单号 N orderid和ordernumber至少有一个不能为空
     */
    private String ordernumber;
    /**
     * 会员ID  N
     */
    private long memberid;

    public long getOrderid() {
        return orderid;
    }

    public void setOrderid(long orderid) {
        this.orderid = orderid;
    }

    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public long getMemberid() {
        return memberid;
    }

    public void setMemberid(long memberid) {
        this.memberid = memberid;
    }
}
