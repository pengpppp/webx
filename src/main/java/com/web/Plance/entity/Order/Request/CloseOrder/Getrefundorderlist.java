package com.web.Plance.entity.Order.Request.CloseOrder;


/**
 * 获取退票单列表
 */
public class Getrefundorderlist {
    /**
     * 机票订单id
     */
    private long orderid;
    /**
     * 退票类型  N
     */
    private Integer refundtype;
    /**
     * 退票状态 N
     */
    private Integer status;
    /**
     * 页码 N
     */
    private Integer page;
    /**
     * 每页数量 N
     */
    private Integer pagesize;
}
