package com.web.Plance.entity.Order.Request.CloseOrder;

import com.web.Plance.entity.Order.Response.Attachment;

import java.util.List;

/**
 * 退票申请接口
 */
public class Refundapply {
    /**
     * 机票订单ID
     */
    private long orderid;
    /**
     * 退票类型 1自愿退 2非自愿退
     */
    private Integer refundtype;
    /**
     * 退票原因
     */
    private String refundreason;
    /**
     * 票号集合
     */
    private List<String> ticketnumbers;
    /**
     * 先前调用上传附件接口的返回值
     */
    private List<Attachment> attachments;

    public long getOrderid() {
        return orderid;
    }

    public void setOrderid(long orderid) {
        this.orderid = orderid;
    }

    public Integer getRefundtype() {
        return refundtype;
    }

    public void setRefundtype(Integer refundtype) {
        this.refundtype = refundtype;
    }

    public String getRefundreason() {
        return refundreason;
    }

    public void setRefundreason(String refundreason) {
        this.refundreason = refundreason;
    }

    public List<String> getTicketnumbers() {
        return ticketnumbers;
    }

    public void setTicketnumbers(List<String> ticketnumbers) {
        this.ticketnumbers = ticketnumbers;
    }

    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }
}
