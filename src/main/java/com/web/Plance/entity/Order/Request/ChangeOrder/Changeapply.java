package com.web.Plance.entity.Order.Request.ChangeOrder;

import java.util.List;

/**
 * 改签申请接口实体类
 */
public class Changeapply {
    /**
     * 机票订单ID
     */
    private long orderid;
    /**
     * 改签日期 yyyy-MM-dd
     */
    private String changedate;
    /**
     * 改签描述
     */
    private String changeremark;
    /**
     * 退票票号 票号集合
     */
    private List<String> ticketnumbers;

    public long getOrderid() {
        return orderid;
    }

    public void setOrderid(long orderid) {
        this.orderid = orderid;
    }

    public String getChangedate() {
        return changedate;
    }

    public void setChangedate(String changedate) {
        this.changedate = changedate;
    }

    public String getChangeremark() {
        return changeremark;
    }

    public void setChangeremark(String changeremark) {
        this.changeremark = changeremark;
    }

    public List<String> getTicketnumbers() {
        return ticketnumbers;
    }

    public void setTicketnumbers(List<String> ticketnumbers) {
        this.ticketnumbers = ticketnumbers;
    }
}
