package com.web.Plance.entity.Order.Request.ChangeOrder;

/**
 * 改签订单详情接口
 */
public class Getchangeorderdetails {
    /**
     * 改签订单ID
     */
    private long changeid;
    /**
     * 改签订单号
     */
    private String changeorderno;

    /**
     * 会员ID N
     */
    private long memberid;

    public long getMemberid() {
        return memberid;
    }

    public void setMemberid(long memberid) {
        this.memberid = memberid;
    }

    public long getChangeid() {
        return changeid;
    }

    public void setChangeid(long changeid) {
        this.changeid = changeid;
    }

    public String getChangeorderno() {
        return changeorderno;
    }

    public void setChangeorderno(String changeorderno) {
        this.changeorderno = changeorderno;
    }
}
