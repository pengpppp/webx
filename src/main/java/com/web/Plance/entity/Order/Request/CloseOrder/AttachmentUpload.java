package com.web.Plance.entity.Order.Request.CloseOrder;

/**
 * 退票附件
 */
public class AttachmentUpload {
    /**
     * 附件名称 带后缀
     */
    private String name;
    /**
     * 附件二进制流
     */
    private byte[] stream;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getStream() {
        return stream;
    }

    public void setStream(byte[] stream) {
        this.stream = stream;
    }
}
