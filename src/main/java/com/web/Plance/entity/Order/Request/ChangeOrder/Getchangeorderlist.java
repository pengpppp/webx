package com.web.Plance.entity.Order.Request.ChangeOrder;

/**
 * 改签订单列表接口
 */
public class Getchangeorderlist {
    /**
     * 机票订单ID
     */
    private long orderid;
    /**
     * 会员ID N
     */
    private long memberid;
    /**
     * 改签状态 N  0改签申请中  1改签待付款  2改签处理中  3改签成功 4改签取消
     */
    private Integer status;
    private Integer page;
    private Integer pagesize;

    public long getOrderid() {
        return orderid;
    }

    public void setOrderid(long orderid) {
        this.orderid = orderid;
    }

    public long getMemberid() {
        return memberid;
    }

    public void setMemberid(long memberid) {
        this.memberid = memberid;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getPagesize() {
        return pagesize;
    }

    public void setPagesize(Integer pagesize) {
        this.pagesize = pagesize;
    }
}
