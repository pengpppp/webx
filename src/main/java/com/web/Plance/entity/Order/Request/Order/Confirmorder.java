package com.web.Plance.entity.Order.Request.Order;

/**
 * 确认出票接口
 * 当订单占位成功，订单状态是待支付时，可以调用该接口，我们将扣除预存款并申请出票
 */
public class Confirmorder {
    /**
     * 订单ID
     */
    private Integer orderid;

    public Confirmorder(){}

    public Confirmorder(Integer orderid){this.orderid=orderid;}

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }
}
