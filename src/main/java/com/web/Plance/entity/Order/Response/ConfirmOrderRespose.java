package com.web.Plance.entity.Order.Response;

public class ConfirmOrderRespose {
    private Integer orderid;
    private Integer confirmStatus;
    public ConfirmOrderRespose(){}
    public ConfirmOrderRespose(Integer confirmStatus){
        this.confirmStatus=confirmStatus;
    }

    public Integer getOrderid() {
        return orderid;
    }

    public void setOrderid(Integer orderid) {
        this.orderid = orderid;
    }

    public Integer getConfirmStatus() {
        return confirmStatus;
    }

    public void setConfirmStatus(Integer confirmStatus) {
        this.confirmStatus = confirmStatus;
    }
}
