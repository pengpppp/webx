package com.web.Plance.entity.Order.Request.Order;

/**
 * 订单列表
 */
public class Getorderlist {
    private Integer pagesize;
    private Integer page;
    /**
     * 会员ID  N
     */
    private long memberid;

    public long getMemberid() {
        return memberid;
    }

    public void setMemberid(long memberid) {
        this.memberid = memberid;
    }

    public Integer getPagesize() {
        return pagesize;
    }

    public void setPagesize(Integer pagesize) {
        this.pagesize = pagesize;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }
}
