package com.web.Plance.entity.Order.Request.ChangeOrder;

/**
 * 取消改签接口
 */
public class Cancelchange {
    /**
     * 改签订单ID
     */
    private long changeid;
    /**
     * 会员ID N
     */
    private long memberid;

    public long getChangeid() {
        return changeid;
    }

    public void setChangeid(long changeid) {
        this.changeid = changeid;
    }

    public long getMemberid() {
        return memberid;
    }

    public void setMemberid(long memberid) {
        this.memberid = memberid;
    }
}
