package com.web.Controller;

import com.web.Service.LeavingService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class LeavingController {

    @Autowired
    LeavingService leavingService;

    @RequestMapping("/addLeaving")
    public Map addLeaving(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        return leavingService.Leavinginsert(String.valueOf(object.get("name")),String.valueOf(object.get("information")),String.valueOf(object.get("phone")));
    }
}
