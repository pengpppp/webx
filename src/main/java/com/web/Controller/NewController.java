package com.web.Controller;

import com.web.Service.NewService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NewController {

    @Autowired
    NewService newService;

    @RequestMapping("/getNew")
    public Object getNew(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        return newService.getNew(String.valueOf(object.get("channel")), Integer.valueOf(String.valueOf(object.get("page"))));
    }


}
