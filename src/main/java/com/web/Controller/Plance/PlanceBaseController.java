package com.web.Controller.Plance;

import com.web.Plance.entity.Order.Request.ChangeOrder.*;
import com.web.Plance.entity.Order.Request.CloseOrder.*;
import com.web.Plance.entity.Order.Request.Order.Confirmorder;
import com.web.Plance.entity.Order.Request.Order.Getorderdetails;
import com.web.Plance.entity.Order.Request.Order.Getorderlist;
import com.web.Service.Plance.PlanceBaseService;
import com.web.common.core.util.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 国内机票
 */
@RestController
public class PlanceBaseController {
    @Autowired
    PlanceBaseService planceBaseService;

    /**
     * 城市数据  参数为空  30001
     * @return
     */
    @RequestMapping("/flightCityList")
    public Object flightcitylist(@RequestBody String string)
    {
        return planceBaseService.flightcitylist();
    }

    /**
     *航空公司数据  参数为空  30002
     * @param string
     * @return
     */
    @RequestMapping("/getAirCompanyList")
    public Object getaircompanylist(@RequestBody String string)
    {
        return planceBaseService.getaircompanylist();
    }

    /**
     * 机场数据 参数为空 30003
     * @param string
     * @return
     */
    @RequestMapping("/getAirportsList")
    public Object getairportslist(@RequestBody String string)
    {
        return planceBaseService.getairportslist();
    }

    /**
     * 获取订单列表  有参数  30004
     * @param string
     * @return
     */
    @RequestMapping("/getOrderList")
    public Object getorderlist(@RequestBody String string)
    {
        Getorderlist getorderlist=JSONUtil.toObj(string, Getorderlist.class);
        return planceBaseService.getorderlist(getorderlist);
    }

    /**
     * 订单详情  有参数 30005  未测试
     * @param string
     * @return
     */
    @RequestMapping("/getOrderDetails")
    public Object getorderdetails(@RequestBody String string)
    {
        Getorderdetails getorderdetails=JSONUtil.toObj(string, Getorderdetails.class);
        return planceBaseService.getorderdetails(getorderdetails);
    }

    /**
     * 取消订单  有参数  30006 未测试
     * @param string
     * @return
     */
    @RequestMapping("/cancelOrder")
    public Object cancelorder(@RequestBody String string)
    {
        Cancelorder cancelorder =JSONUtil.toObj(string, Cancelorder.class);
        return planceBaseService.cancelorder(cancelorder);
    }

    /**
     * 确认出票  有参数  30007
     * @param string
     * @return
     */
    @RequestMapping("/confirmOrder")
    public Object confirmorder(@RequestBody String string)
    {
        Confirmorder confirmorder =JSONUtil.toObj(string, Confirmorder.class);
        return planceBaseService.confirmorder(confirmorder);
    }

    /**
     * 退票附件上传  有参数 30008
     * @param string
     * @return
     */
    @RequestMapping("/refundUpload")
    public Object refundupload(@RequestBody String string)
    {
        Refundupload refundupload =JSONUtil.toObj(string, Refundupload.class);
        return planceBaseService.refundupload(refundupload);
    }

    /**
     * 退票申请接口  有参数  30009
     * @param string
     * @return
     */
    @RequestMapping("/refundApply")
    public Object refundapply(@RequestBody String string)
    {
        Refundapply refundapply =JSONUtil.toObj(string, Refundapply.class);
        return planceBaseService.refundapply(refundapply);
    }

    /**
     * 退票单列表  有参数  30010
     * @param string
     * @return
     */
    @RequestMapping("/getRefundOrderList")
    public Object getrefundorderlist(@RequestBody String string)
    {
        Getrefundorderlist getrefundorderlist =JSONUtil.toObj(string,Getrefundorderlist.class);
        return planceBaseService.getrefundorderlist(getrefundorderlist);
    }

    /**
     * 退票单详情  有参数  30011
     * @param string
     * @return
     */
    @RequestMapping("/getRefundOrderDetails")
    public Object getrefundorderdetails(@RequestBody String string)
    {
        Getrefundorderdetails getrefundorderdetails =JSONUtil.toObj(string,Getrefundorderdetails.class);
        return planceBaseService.getrefundorderdetails(getrefundorderdetails);
    }

    /**
     * 改签申请接口  有参数  30012
     * @param string
     * @return
     */
    @RequestMapping("/changeApply")
    public Object changeapply(@RequestBody String string)
    {
        Changeapply changeapply =JSONUtil.toObj(string,Changeapply.class);
        return planceBaseService.changeapply(changeapply);
    }

    /**
     * 改签订单列表  有参数  30013
     * @param string
     * @return
     */
    @RequestMapping("/getChangeOrderList")
    public Object getchangeorderlist(@RequestBody String string)
    {
        Getchangeorderlist getchangeorderlist =JSONUtil.toObj(string,Getchangeorderlist.class);
        return planceBaseService.getchangeorderlist(getchangeorderlist);
    }

    /**
     * 改签订单详情  有参数  30014
     * @param string
     * @return
     */
    @RequestMapping("/getChangeOrderDetails")
    public Object getchangeorderdetails(@RequestBody String string)
    {
        Getchangeorderdetails getchangeorderdetails =JSONUtil.toObj(string,Getchangeorderdetails.class);
        return planceBaseService.getchangeorderdetails(getchangeorderdetails);
    }

    /**
     * 取消改签  有参数  30015
     * @param string
     * @return
     */
    @RequestMapping("/cancelChange")
    public Object cancelchange(@RequestBody String string)
    {
        Cancelchange cancelchange =JSONUtil.toObj(string,Cancelchange.class);
        return planceBaseService.cancelchange(cancelchange);
    }

    /**
     * 确认改签  有参数   30016
     * @param string
     * @return
     */
    @RequestMapping("/confirmChange")
    public Object confirmchange(@RequestBody String string)
    {
        Confirmchange confirmchange =JSONUtil.toObj(string,Confirmchange.class);
        return planceBaseService.confirmchange(confirmchange);
    }
}
