package com.web.Controller.Plance;

import com.web.Exception.PassengerException;
import com.web.Service.Plance.My.PassengerService;
import com.web.Tools.Check.check;
import com.web.Tools.Redis.RedisUtil;
import com.web.common.core.entity.JsonData;
import com.web.common.core.util.JSONUtil;
import com.web.entity.Passenger;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class PassengerController {
    @Autowired
    PassengerService passengerService;

    @Autowired
    RedisUtil redisUtil;

    /**
     * 添加乘客  30030
     * @param string
     * @param request
     * @return
     */
    @RequestMapping("/addPassenger")
    public Object addPassenger(@RequestBody String string, HttpServletRequest request) {
        try {
            String token = request.getHeader("TN");
            String mobile = String.valueOf(redisUtil.get(token)).split("-")[1];
            Passenger passenger = JSONUtil.toObj(string, Passenger.class);
            passenger.setUserid(Integer.valueOf(mobile));
            if(checkPassenger(passenger))
                throw new PassengerException();
            return passengerService.add(passenger);
        } catch (Exception e) {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 展示该用户的所有乘客信息  30031
     * @param string
     * @param request
     * @return
     */
    @RequestMapping("/showUserAllPassenger")
    public Object getUserAllPassenger(@RequestBody String string, HttpServletRequest request)
    {
        try {
            String token = request.getHeader("TN");
            String mobile = String.valueOf(redisUtil.get(token)).split("-")[1];
            Passenger passenger = new Passenger();
            passenger.setUserid(Integer.valueOf(mobile));
            return passengerService.showAll(passenger);
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    /**
     * 删除乘客信息  30032
     * @param string
     * @return
     */
    @RequestMapping("/deletePassenger")
    public Object delete(@RequestBody String string)
    {
        try {
            Passenger passenger = JSONUtil.toObj(string, Passenger.class);
            return passengerService.delete(passenger);
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    /**
     * 修改乘客信息  30033
     * @param string
     * @return
     */
    @RequestMapping("/editPassenger")
    public Object edit(@RequestBody String string)
    {
        try {
            Passenger passenger = JSONUtil.toObj(string, Passenger.class);
            return passengerService.update(passenger);
        }catch (Exception e)
        {
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 查询单个  30034
     * @param string
     * @return
     */
    @RequestMapping("/getPassemger")
    public Object get(@RequestBody String string)
    {
        try {
            JSONObject jsonObject =JSONObject.fromObject(string);
            return JsonData.buildSuccess(passengerService.querybyId(Integer.valueOf(String.valueOf(jsonObject.get("id")))));
        }catch (Exception e)
        {
            return JsonData.buildError(e.toString());
        }
    }
    private Boolean checkPassenger(Passenger passenger)
    {
        System.out.println(passenger.toString());
        try {
            if(passenger.getPassengertype()==null|| check.checkNull(passenger.getBirthday())
                    ||check.checkNull(passenger.getFirstname())
                    ||check.checkNull(passenger.getIdnumber())||check.checkNull(passenger.getIdtype())
                    ||check.checkNull(passenger.getLastname())
                    ||check.checkNull(passenger.getMobile())||check.checkNull(passenger.getPassengername()))
            {
                return true;
            }else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }

    }
}
