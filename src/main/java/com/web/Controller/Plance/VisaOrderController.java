package com.web.Controller.Plance;

import com.web.Service.Plance.Visa.VisaOrderService;
import com.web.Tools.Redis.RedisUtil;
import com.web.common.core.entity.JsonData;
import com.web.common.core.util.JSONUtil;
import com.web.entity.entityImp.Visa.VisaOrderImp;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;


@RestController
public class VisaOrderController {
    @Autowired
    VisaOrderService visaOrderService;
    @Resource
    RedisUtil redisUtil;

    /**
     * 40010
     * @param string
     * @param request
     * @return
     */
    @RequestMapping("/addVisaOrder")
    public Object add(@RequestBody String string,HttpServletRequest request)
    {
        try {
            String token = request.getHeader("TN");
            String userid = String.valueOf(redisUtil.get(token));
            userid=userid.split("-")[1];
            VisaOrderImp visaOrderImp = JSONUtil.toObj(string,VisaOrderImp.class);
            return visaOrderService.add(visaOrderImp,userid);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 40011
     * @param request
     * @return
     */
    @RequestMapping("/getMyVisaOrder")
    public Object show(@RequestBody String string,HttpServletRequest request)
    {
        try {
            String token = request.getHeader("TN");
            String userid = String.valueOf(redisUtil.get(token));
            userid=userid.split("-")[1];
            JSONObject jsonObject = JSONObject.fromObject(string);
            return visaOrderService.show(userid,String.valueOf(jsonObject.get("page")));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 40012
     * @param string
     * @return
     */
    @RequestMapping("/getVisaOrder")
    public Object showdetails(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return visaOrderService.showDetails(String.valueOf(jsonObject.get("orderid")));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 40013
     * @param string
     * @return
     */
    @RequestMapping("/concelVisaOrder")
    public Object deleteOrder(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return visaOrderService.showDetails(String.valueOf(jsonObject.get("orderid")));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }
}
