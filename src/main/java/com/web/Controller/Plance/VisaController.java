package com.web.Controller.Plance;

import com.web.Service.Plance.Visa.VisaService;
import com.web.common.core.entity.JsonData;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VisaController {
    @Autowired
    VisaService visaService;

    /**
     * 40001
     * 获取签证项目列表
     * @param string
     * @return
     */
    @RequestMapping("/getVisaList")
    public Object getAll(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return visaService.getAll(String.valueOf(jsonObject.get("page")));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 签证项目详情 40002
     * @param string
     * @return
     */
    @RequestMapping("/getVisa")
    public Object getOne(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return visaService.getOne(jsonObject.getString("id"));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }


}
