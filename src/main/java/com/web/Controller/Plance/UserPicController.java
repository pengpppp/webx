package com.web.Controller.Plance;

import com.web.Service.UserPicService;
import com.web.Tools.Redis.RedisUtil;
import com.web.common.core.entity.JsonData;
import com.web.common.core.util.JSONUtil;
import com.web.entity.Userpic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

@RestController
public class UserPicController {
    @Autowired
    UserPicService userPicService;
    @Autowired
    RedisUtil redisUtil;

    @RequestMapping("/uploadUserPic")
    public Object upload(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request)
    {
        try {
            String token = request.getHeader("TN");
            String s = String.valueOf(redisUtil.get(token));
            s=s.split("-")[1];
            return userPicService.upload(file,s);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * picurl  网址   type 类型
     * @param string
     * @return
     */
    @RequestMapping("/addUserPic")
    public Object insert(@RequestBody String string, HttpServletRequest request)
    {
        try {
            String token = request.getHeader("TN");
            String s = String.valueOf(redisUtil.get(token));
            s=s.split("-")[1];
            Userpic userpic = JSONUtil.toObj(string,Userpic.class);
            userpic.setUserid(s);
            return userPicService.insert(userpic);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 删除
     * @param string
     * @return
     */
    @RequestMapping("/deleteUserPic")
    public Object delete(@RequestBody String string)
    {
        try {
            Userpic userpic = JSONUtil.toObj(string,Userpic.class);
            return userPicService.delete(userpic.getPicurl());
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }
}
