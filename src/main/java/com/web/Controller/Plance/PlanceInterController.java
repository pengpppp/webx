package com.web.Controller.Plance;


import com.web.Plance.entity.InternetFlight.Request.Checkprice;
import com.web.Plance.entity.InternetFlight.Request.Createorder;
import com.web.Plance.entity.InternetFlight.Request.GetFlightList;
import com.web.Plance.entity.InternetFlight.Request.Getrefundrule;
import com.web.Service.Plance.PlanceInterService;
import com.web.common.core.entity.JsonData;
import com.web.common.core.util.JSONUtil;
import com.web.entity.Interorder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 国际机票
 */
@RestController
public class PlanceInterController {
    @Autowired
    PlanceInterService planceInterService;

    /**
     * 获取航班列表 有参数 30025
     * @param string
     * @return
     */
    @RequestMapping("/getInterFlightList")
    public Object getflightlist(@RequestBody String string)
    {
        GetFlightList getFlightList = JSONUtil.toObj(string,GetFlightList.class);
        return planceInterService.getFlightList(getFlightList);
    }

    /**
     * 价格校验  有参数  30026
     * @param string
     * @return
     */
    @RequestMapping("/checkPrice")
    public Object checkprice(@RequestBody String string)
    {
        Checkprice checkprice = JSONUtil.toObj(string,Checkprice.class);
        return planceInterService.checkprice(checkprice);
    }

    /**
     * 获取改退规则  有参数  30027
     * @param string
     * @return
     */
    @RequestMapping("/getInterRefundRule")
    public Object getrefundrule(@RequestBody String string)
    {
        Getrefundrule getrefundrule = JSONUtil.toObj(string,Getrefundrule.class);
        return planceInterService.getrefundrule(getrefundrule);
    }

    /**
     * 下单  有参数  30028
     * @param string
     * @return
     */
    @RequestMapping("/createInterOrder")
    public Object createorder(@RequestBody String string)
    {
        try {
            Interorder internationalorder = JSONUtil.toObj(string,Interorder.class);//记录
            Createorder createorder = JSONUtil.toObj(string,Createorder.class);
            Getrefundrule getrefundrule = new Getrefundrule();
            getrefundrule.setSessionId(createorder.getFlightSessionId());
            planceInterService.getrefundrule(getrefundrule);
            String string1 = JSONUtil.toString(planceInterService.getrefundrule(getrefundrule));
            Boolean b = Boolean.parseBoolean(String.valueOf(JSONUtil.parseJson(string1).get("data").get("ret")));
            if(b)
            {
                createorder.setCheckSessionId(String.valueOf(JSONUtil.parseJson(string1).get("data").get("sessionId")));
                return planceInterService.createorder(createorder);
            }else {
                return JsonData.buildError("验价失败");
            }
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    /**
     * 预定订单  有参数 30029
     * @param string
     * @return
     */
    @RequestMapping("/saveInterOrder")
    public Object saveOrder(@RequestBody String string)
    {try {
        Interorder internationalorder = JSONUtil.toObj(string,Interorder.class);//记录
        Createorder createorder = JSONUtil.toObj(string,Createorder.class);
        Getrefundrule getrefundrule = new Getrefundrule();
        getrefundrule.setSessionId(createorder.getFlightSessionId());
        planceInterService.getrefundrule(getrefundrule);
        String string1 = JSONUtil.toString(planceInterService.getrefundrule(getrefundrule));
        Boolean b = Boolean.parseBoolean(String.valueOf(JSONUtil.parseJson(string1).get("data").get("ret")));
        if(b)
        {
            createorder.setCheckSessionId(String.valueOf(JSONUtil.parseJson(string1).get("data").get("sessionId")));
            return planceInterService.createorder(createorder);
        }else {
            return JsonData.buildError("验价失败");
        }
    }catch (Exception e)
    {
        return JsonData.buildError(e.getMessage());
    }
    }
}
