package com.web.Controller.Plance;

import com.web.Plance.entity.HomeFlight.Request.Base.Cheapflights;
import com.web.Plance.entity.HomeFlight.Request.Base.GetInsuranceList;
import com.web.Plance.entity.HomeFlight.Request.Base.Getlowpricecalendar;
import com.web.Plance.entity.HomeFlight.Request.Flight.Getflightdetails;
import com.web.Plance.entity.HomeFlight.Request.Flight.Getflightlist;
import com.web.Plance.entity.HomeFlight.Request.Flight.Getrefundrule;
import com.web.Plance.entity.HomeFlight.Request.Order.Checkcabinprice;
import com.web.Service.Plance.My.HomePlanceService;
import com.web.Service.Plance.My.PassengerService;
import com.web.Service.Plance.PlanceHomeService;
import com.web.Service.Plance.PlanceInterService;
import com.web.Tools.Redis.RedisUtil;
import com.web.common.core.entity.JsonData;
import com.web.common.core.util.JSONUtil;
import com.web.entity.entityImp.Order.SaveOrder;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

@RestController
public class PlanceHomeController {
    @Autowired
    PlanceHomeService planceHomeService;
    @Autowired
    PlanceInterService planceInterService;
    @Autowired
    PassengerService passengerService;
    @Autowired
    HomePlanceService homePlanceService;
    @Resource
    RedisUtil redisUtil;
    /**
     * 低价日历  有参数  30017
     * @return
     */
    @RequestMapping("/getLowPriceCalendar")
    public Object getlowpricecalendar(@RequestBody String string)
    {
        Getlowpricecalendar getlowpricecalendar= JSONUtil.toObj(string, Getlowpricecalendar.class);
        return planceHomeService.getlowpricecalendar(getlowpricecalendar);
    }

    /**
     * 特价机票  有参数 30018
     * @return
     */
    @RequestMapping("/cheapFlights")
    public Object cheapflights(@RequestBody String string)
    {
        Cheapflights cheapflights = JSONUtil.toObj(string,Cheapflights.class);
        return planceHomeService.cheapflights(cheapflights);
    }

    /**
     * 保险查询  有参数  30019
     * @return
     */
    @RequestMapping("/getInsuranceList")
    public Object getInsuranceList(@RequestBody String string)
    {
        try {
            GetInsuranceList getInsuranceList = JSONUtil.toObj(string,GetInsuranceList.class);
            return planceHomeService.getInsuranceList(getInsuranceList);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 获取航班列表 有参数  30020
     * @return
     */
    @RequestMapping("/getFlightList")
    public Object getflightlist(@RequestBody String string)
    {
        try {
            Getflightlist getflightlist = JSONUtil.toObj(string,Getflightlist.class);
            return planceHomeService.getflightlist(getflightlist);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 获取航班详情  有参数  30021
     * @return
     */
    @RequestMapping("/getFlightDetails")
    public Object getflightdetails(@RequestBody String string)
    {
        try {
            Getflightdetails getflightdetails = JSONUtil.toObj(string,Getflightdetails.class);
            return planceHomeService.getflightdetails(getflightdetails);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 获取改退规则  有参数  30022
     * @return
     */
    @RequestMapping("/getRefundRule")
    public Object getrefundrule(@RequestBody String string)
    {
        try {
            Getrefundrule getrefundrule = JSONUtil.toObj(string, Getrefundrule.class);
            return planceHomeService.getrefundrule(getrefundrule);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 预定订单  30023
     * @param string
     * @param request
     * @return
     */
    @RequestMapping("/saveOrder")
    public Object saveOderTest(@RequestBody String string,HttpServletRequest request)
    {
        try {
            String token = request.getHeader("TN");
            String userid = String.valueOf(redisUtil.get(token));
            userid=userid.split("-")[1];
            SaveOrder saveOrder = JSONUtil.toObj(string, SaveOrder.class);
            Checkcabinprice checkcabinprice = homePlanceService.addCheckprice(saveOrder);
            JsonData jsonData = (JsonData)planceHomeService.checkcabinprice(checkcabinprice);

            if(jsonData.getCode().equals("1000"))
            {
                System.out.println("success");
                return homePlanceService.addTest(saveOrder,Integer.valueOf(userid));
            }else {
                System.out.println("error");
                return jsonData;
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 支付接口 支付成功则下单 失败则退款 有参数  30024
     * @return
     */
    @RequestMapping("/createOrder")
    public Object createorder(@RequestBody String string)
    {
        try {
            //TODO 支付过程  该如何支付？
            JSONObject jsonObject =JSONObject.fromObject(string);
            //homePlanceService.creatOrder(jsonObject.getString("id"));//返回T/N
            return JsonData.buildSuccess("接口未完成");
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.getMessage());
        }
    }

    /**
     * 获取单个订单  30040
     * @param string
     * @return
     */
    @RequestMapping("/getOrder")
    public Object showOrder(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return homePlanceService.get(jsonObject.getString("id"));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    /**
     * 获取本人的所有订单  30041
     * @param request
     * @return
     */
    @RequestMapping("/getMyOrder")
    public Object showAllOrder(@RequestBody String string,HttpServletRequest request)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            String token = request.getHeader("TN");
            String s = String.valueOf(redisUtil.get(token));
            s=s.split("-")[1];
            return homePlanceService.getAll(Integer.valueOf(s), String.valueOf(jsonObject.get("page")));
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }

    /**
     * 取消订单    30042    未完善
     * 取消订单应该分两个部分：
     *     一个是未支付的取消订单，直接设置状态
     *     一个是支付后的取消订单，得先取消远程的订单、再退款、接着再设置状态
     * @param string
     * @return
     */
    @RequestMapping("/concelOrder")
    public Object deleteOrder(@RequestBody String string)
    {
        try {
            JSONObject jsonObject = JSONObject.fromObject(string);
            return homePlanceService.delete(jsonObject.getString("id"));
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }
}
