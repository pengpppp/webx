package com.web.Controller;

import com.web.Service.ContentService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class ContentController {

    @Autowired
    ContentService contentService;

    /**
     * 后需，修改
     * 没有ID时，获取最后一条的信息
     * 有ID时，根据ID获取信息
     * 获取内容页面
     * @param string
     * @return
     */
    @RequestMapping("/contentShow")
    public Map contentshow(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        return contentService.contentShow(String.valueOf(object.get("id")));
    }




}
