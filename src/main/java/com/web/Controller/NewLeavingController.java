package com.web.Controller;

import com.web.Service.NewLeavingService;
import com.web.Tools.Redis.RedisUtil;
import com.web.entity.MyNew.Comment;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class NewLeavingController {

    @Autowired
    NewLeavingService newLeavingService;

    @Autowired
    RedisUtil redisUtil;

    /**
     * channel src tittle information
     * @param string
     * @return
     */
    @RequestMapping("/addNewLeaving")
    public Object addLeaving(@RequestBody String string,HttpServletRequest request)
    {
        JSONObject object = JSONObject.fromObject(string);
        object.remove("cmd");
        Comment comment = (Comment) JSONObject.toBean(object, Comment.class);
        comment.setLeavingname(String.valueOf(redisUtil.get(request.getHeader("TN"))));
        return newLeavingService.addLeaving(comment);
    }
    /**
     * channel src tittle
     * @param string
     * @return
     */
    @RequestMapping("/getNewLeaving")
    public Object getLeaving(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        int page = (Integer) object.get("page");
        object.remove("cmd");
        object.remove("page");
        Comment comment = (Comment) JSONObject.toBean(object, Comment.class);
        System.out.println(comment.toString());
        Object o = newLeavingService.getLeaving(comment, page);
        System.out.println(o.toString());
        return o;
    }

}
