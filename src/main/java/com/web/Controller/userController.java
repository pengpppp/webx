package com.web.Controller;


import com.web.Config.Aspect.annotion.RequsetHeart;
import com.web.Service.userService;
import com.web.entity.entityImp.loginEntity;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 面向用户  登录及注册相关的处理层
 */
@RestController
public class userController {
    @Autowired
    userService userService;

    /**
     * 登录
     * @param string
     * @return
     */
    @RequsetHeart
    @RequestMapping("/login")
    public loginEntity Login(@RequestBody String string, HttpServletRequest request)
    {
        JSONObject object = JSONObject.fromObject(string);
        return userService.loginService((String) object.get("mobile"),(String) object.get("loginPwd"));
    }

    /**
     * 注册
     * @param string
     * @return
     */
    //@RequsetHeart
    @RequestMapping("/register")
    public loginEntity Register(@RequestBody String string, HttpServletRequest request)
    {
        JSONObject object = JSONObject.fromObject(string);
        return userService.registerService((String)object.get("mobile"),(String)object.get("loginPwd"),(String)object.get("smsVerifyCode"),(String)object.get("verifyCodeKey"));
    }

    /**
     * 获取短信验证码
     * @param string
     * @param map
     * @return
     */
    @RequsetHeart
    @RequestMapping("/GetVerifyCode")
    public Map getVerifyCode(@RequestBody String string,HttpServletRequest request,
                             Map<String,String> map)
    {
        JSONObject object = JSONObject.fromObject(string);
        map=userService.GetVerifyCodeService((String)object.get("mobile"),(String)object.get("type"),(String)object.get("randomCode"),(String)object.get("randomCDKey"));
        return map;
    }

    /**
     * 校验短信验证码
     * @param string
     * @param map
     * @return
     */
    @RequsetHeart
    @RequestMapping("/CheckVerifyCode")
    public Map checkVerifyCode(@RequestBody String string,
                               HttpServletRequest request,Map<String,String> map)
    {
        JSONObject object = JSONObject.fromObject(string);
        return userService.CheckVerifyCodeService((String)object.get("mobile"),(String)object.get("type"),(String)object.get("smsVerifyCode"),(String)object.get("verifyCodeKey"));
    }

    /**
     * 获取图片验证码
     * @param string
     * @param map
     * @return
     */
    @RequsetHeart
    @RequestMapping("/GetRandomCode")
    public Map GetRandomCode(@RequestBody String string, HttpServletRequest request,
                                    Map<String,String> map)
    {
        JSONObject object = JSONObject.fromObject(string);
        return userService.GetRandomCodeService((String)object.get("type"));
    }

    /**
     * 校验图片验证码
     * @param string
     * @param map
     * @return
     */
    @RequsetHeart
    @RequestMapping("/ValidateRandomCode")
    public Map ValidateRandomCode(@RequestBody String string,
                                  HttpServletRequest request,Map<String,String> map)
    {
        JSONObject object = JSONObject.fromObject(string);
        return userService.ValidateRandomCodeService((String)object.get("randomImageCode"),(String)object.get("randomCDKey"));
    }
}
