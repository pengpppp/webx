package com.web.Controller;

import com.web.Service.CollectionService;
import com.web.entity.MyNew.Collection;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CollectionController {
    @Autowired
    CollectionService collectionService;

    @RequestMapping("/addCollection")
    public Object AddCollection(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        Collection collection = (Collection) JSONObject.toBean(object,Collection.class);
        return collectionService.addCollection(collection);
    }

    @RequestMapping("/getCollection")
    public Object GetCollection(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        return collectionService.getCollection(object.getString("leavingname"));
    }

}
