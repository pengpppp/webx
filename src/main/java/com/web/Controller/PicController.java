package com.web.Controller;

import com.web.Service.PicService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 面向用户  图片处理的控制层
 */
@RestController
public class PicController {
    @Autowired
    PicService picService;

    //@RequsetHeart
    @RequestMapping("/home")
    public Map Show(@RequestBody String string, HttpServletRequest request)
    {
        JSONObject object = JSONObject.fromObject(string);
        System.out.println(string);
        return picService.showhome(object.get("lastIndex"),object.get("pageSize"));
    }

    //@RequsetHeart
    @RequestMapping("/banner")
    public Map banner(@RequestBody String string, HttpServletRequest request)
    {
        return picService.showbanner();
    }
}
