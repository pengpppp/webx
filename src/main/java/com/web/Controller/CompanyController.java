package com.web.Controller;

import com.web.Service.CompanyService;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class CompanyController {

    @Autowired
    CompanyService companyService;
    /**
     * 后续需修改
     * 有id则获取id对应的值
     * 无id则获取最后一条的信息
     * 获取公司信息列表
     * @param string
     * @return
     */
    @RequestMapping("/companyShow")
    public Map showCompany(@RequestBody String string)
    {
        JSONObject object = JSONObject.fromObject(string);
        return companyService.show(String.valueOf(object.get("id")));
    }
}
