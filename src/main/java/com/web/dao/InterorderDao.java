package com.web.dao;

import com.web.entity.Interorder;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * (Interorder)表数据库访问层
 *
 * @author makejava
 * @since 2020-06-05 09:34:01
 */
public interface InterorderDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Interorder queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Interorder> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param interorder 实例对象
     * @return 对象列表
     */
    List<Interorder> queryAll(Interorder interorder);

    /**
     * 新增数据
     *
     * @param interorder 实例对象
     * @return 影响行数
     */
    int insert(Interorder interorder);

    /**
     * 修改数据
     *
     * @param interorder 实例对象
     * @return 影响行数
     */
    int update(Interorder interorder);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);
    @Select("SELECT LAST_INSERT_ID()")
    int querryid();

    /**
     * 查询总记录数
     * @return
     */
    @Select("SELECT COUNT(*) FROM newweb.interorder")
    int querycount1();
}