package com.web.dao;

import com.web.entity.Visa;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * (Visa)表数据库访问层
 *
 * @author makejava
 * @since 2020-06-15 13:42:31
 */
public interface VisaDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Visa queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Visa> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param visa 实例对象
     * @return 对象列表
     */
    List<Visa> queryAll(Visa visa);

    /**
     * 新增数据
     *
     * @param visa 实例对象
     * @return 影响行数
     */
    int insert(Visa visa);

    /**
     * 修改数据
     *
     * @param visa 实例对象
     * @return 影响行数
     */
    int update(Visa visa);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

    @Select("Select count(*) from newweb.visa")
    int queryCount();

    @Select("Select count(*) from newweb.visa where visa.key = 'T' ")
    int queryCount2();

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    @Select("select * from newweb.visa where visa.key='T' order by i desc limit #{offset}, #{limit}")
    List<Visa> queryByLimit(@Param("offset") int offset, @Param("limit") int limit);


    @Update("update newweb.visa set offtime =#{offtime} where id=#{id}")
    int updateofftime(@Param("id")String id,@Param("offtime")String offtime);


}