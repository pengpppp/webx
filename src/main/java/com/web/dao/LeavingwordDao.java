package com.web.dao;

import com.web.entity.Leavingword;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * (Leavingword)表数据库访问层
 *
 * @author makejava
 * @since 2020-05-09 09:14:26
 */
public interface LeavingwordDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Leavingword queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Leavingword> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param leavingword 实例对象
     * @return 对象列表
     */
    List<Leavingword> queryAll(Leavingword leavingword);

    /**
     * 新增数据
     *
     * @param leavingword 实例对象
     * @return 影响行数
     */
    int insert(Leavingword leavingword);

    /**
     * 修改数据
     *
     * @param leavingword 实例对象
     * @return 影响行数
     */
    int update(Leavingword leavingword);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    /**
     * 查询总记录数
     * @return
     */
    @Select("SELECT COUNT(*) FROM newweb.leavingword")
    int querycount1();
}