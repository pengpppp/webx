package com.web.dao;

import com.web.entity.Homeorder;
import com.web.entity.entityImp.getAllMyOrder;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * (Homeorder)表数据库访问层
 *
 * @author makejava
 * @since 2020-06-09 13:15:11
 */
public interface HomeorderDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Homeorder queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Homeorder> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param homeorder 实例对象
     * @return 对象列表
     */
    List<Homeorder> queryAll(Homeorder homeorder);

    /**
     * 新增数据
     *
     * @param homeorder 实例对象
     * @return 影响行数
     */
    int insert(Homeorder homeorder);

    /**
     * 修改数据
     *
     * @param homeorder 实例对象
     * @return 影响行数
     */
    int update(Homeorder homeorder);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

    @Select("SELECT LAST_INSERT_ID()")
    int querryid();

    /**
     * 查询总记录数
     * @return
     */
    @Select("SELECT COUNT(*) FROM newweb.homeorder")
    int querycount1();

    @Select("Select count(*) from newweb.homeorder where useid = #{useid}")
    int queryByUserid(Integer useid);

    @Select("select * from newweb.homeorder where useid=#{useid} order by i desc limit #{offset}, #{limit} ")
    List<getAllMyOrder> queryAllByUserLimit(@Param("useid") int useid, @Param("offset") int offset, @Param("limit") int limit);

    @Select("select * from newweb.homeorder where i=#{i}")
    Homeorder queryByI(Integer i);
}