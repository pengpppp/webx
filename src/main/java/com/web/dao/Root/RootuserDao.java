package com.web.dao.Root;

import com.web.entity.Root.Rootuser;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Rootuser)表数据库访问层
 *
 * @author makejava
 * @since 2020-06-10 09:45:27
 */
public interface RootuserDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Rootuser queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Rootuser> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param rootuser 实例对象
     * @return 对象列表
     */
    List<Rootuser> queryAll(Rootuser rootuser);

    /**
     * 新增数据
     *
     * @param rootuser 实例对象
     * @return 影响行数
     */
    int insert(Rootuser rootuser);

    /**
     * 修改数据
     *
     * @param rootuser 实例对象
     * @return 影响行数
     */
    int update(Rootuser rootuser);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    /**
     * 根据名字删除数据
     */
    @Delete(" delete from newweb.rootuser where username = #{username}")
    int deleteByUsername(String username);
}