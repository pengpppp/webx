package com.web.dao;

import com.web.entity.User.Userid;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Userid)表数据库访问层
 *
 * @author makejava
 * @since 2020-06-10 13:44:26
 */
public interface UseridDao {

    /**
     * 通过ID查询单条数据
     *
     * @param userid 主键
     * @return 实例对象
     */
    Userid queryById(String userid);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Userid> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param userid 实例对象
     * @return 对象列表
     */
    List<Userid> queryAll(Userid userid);

    /**
     * 新增数据
     *
     * @param userid 实例对象
     * @return 影响行数
     */
    int insert(Userid userid);

    /**
     * 修改数据
     *
     * @param userid 实例对象
     * @return 影响行数
     */
    int update(Userid userid);

    /**
     * 通过主键删除数据
     *
     * @param userid 主键
     * @return 影响行数
     */
    int deleteById(String userid);

}