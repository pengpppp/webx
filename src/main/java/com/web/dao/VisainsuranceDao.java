package com.web.dao;


import com.web.entity.Visainsurance;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * (Visainsurance)表数据库访问层
 *
 * @author makejava
 * @since 2020-06-13 17:46:06
 */
public interface VisainsuranceDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Visainsurance queryById(String id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Visainsurance> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param visainsurance 实例对象
     * @return 对象列表
     */
    List<Visainsurance> queryAll(Visainsurance visainsurance);

    /**
     * 新增数据
     *
     * @param visainsurance 实例对象
     * @return 影响行数
     */
    int insert(Visainsurance visainsurance);

    /**
     * 修改数据
     *
     * @param visainsurance 实例对象
     * @return 影响行数
     */
    int update(Visainsurance visainsurance);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(String id);

    @Select("Select count(*) from newweb.visainsurance")
    int queryCount();


}