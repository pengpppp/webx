package com.web.dao;

import com.web.entity.Picture;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * (Picture)表数据库访问层
 *
 * @author makejava
 * @since 2020-05-04 09:12:12
 */
public interface PictureDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Picture queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Picture> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param picture 实例对象
     * @return 对象列表
     */
    List<Picture> queryAll(Picture picture);

    /**
     * 新增数据
     *
     * @param picture 实例对象
     * @return 影响行数
     */
    int insert(Picture picture);

    /**
     * 修改数据
     *
     * @param picture 实例对象
     * @return 影响行数
     */
    int update(Picture picture);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    /**
     * 查询内容页的最后一条语句
     * @return
     */
    Picture queryLast(Integer type);

    /**
     * 倒序、根据type和ID查询记录
     * @param id
     * @param row
     * @param type
     * @return
     */
    @Select("select * from newweb.picture WHERE id <= #{id} and type=#{type} order by id desc limit 0,#{row}")
    List<Picture> queryPage(@Param("id") Integer id,@Param("row") Integer row,@Param("type")Integer type);

    /**
     * 根据type查询总记录数
     * @param type
     * @return
     */
    @Select("SELECT COUNT(*) FROM newweb.picture where type = #{type}")
    int querycount(Integer type);

    /**
     * 查询总记录数
     * @return
     */
    @Select("SELECT COUNT(*) FROM newweb.picture")
    int querycount1();

    /**
     * 倒序查询所有记录
     * @return
     */
    @Select("select * from newweb.picture order by id desc ")
    List<Picture> queryPage1();

    @Select("Select * from newweb.picture where type=#{type} LIMIT 0,1")
    Picture querytop(@Param("type")Integer type);

    @Delete("delete from newweb.picture where prcturepath = #{url}")
    int deleteone(String url);
}