package com.web.dao.exit;

import com.web.entity.exit.H5url;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (H5url)表数据库访问层
 *
 * @author makejava
 * @since 2020-05-04 10:06:51
 */
public interface H5urlDao {

    /**
     * 通过ID查询单条数据
     * @return 实例对象
     */
    H5url queryById(String url);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<H5url> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param h5url 实例对象
     * @return 对象列表
     */
    List<H5url> queryAll(H5url h5url);

    /**
     * 新增数据
     *
     * @param h5url 实例对象
     * @return 影响行数
     */
    int insert(H5url h5url);

    /**
     * 修改数据
     *
     * @param h5url 实例对象
     * @return 影响行数
     */
    int update(H5url h5url);

    /**
     * 通过主键删除数据
     *
     * @return 影响行数
     */
    int deleteById(String url);

}