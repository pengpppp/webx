package com.web.dao.MyNew;


import com.web.entity.MyNew.Mynew;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * (Mynew)表数据库访问层
 *
 * @author makejava
 * @since 2020-05-23 09:16:01
 */
public interface MynewDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Mynew queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Mynew> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param mynew 实例对象
     * @return 对象列表
     */
    List<Mynew> queryAll(Mynew mynew);

    /**
     * 新增数据
     *
     * @param mynew 实例对象
     * @return 影响行数
     */
    int insert(Mynew mynew);

    /**
     * 修改数据
     *
     * @param mynew 实例对象
     * @return 影响行数
     */
    int update(Mynew mynew);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    @Select("SELECT LAST_INSERT_ID()")
    int querryid();

    /**
     * 查询所有开启状态的新闻
     * @return
     */
    @Select("SELECT * FROM newweb.mynew where mynew.`key` is not null")
    List<Mynew> querryall();

    /**
     * 查询所有关闭状态的新闻
     */
    @Select("SELECT * FROM newweb.mynew where mynew.`key` is null")
    List<Mynew> querryallClose();

    /**
     * 查询总记录数
     * @return
     */
    @Select("SELECT COUNT(*) FROM newweb.mynew")
    int querycount1();
}