package com.web.dao.MyNew;

import com.web.entity.MyNew.Channel;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Channel)表数据库访问层
 *
 * @author makejava
 * @since 2020-05-19 17:56:36
 */
public interface ChannelDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Channel queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Channel> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param channel 实例对象
     * @return 对象列表
     */
    List<Channel> queryAll(Channel channel);

    /**
     * 新增数据
     *
     * @param channel 实例对象
     * @return 影响行数
     */
    int insert(Channel channel);

    /**
     * 修改数据
     *
     * @param channel 实例对象
     * @return 影响行数
     */
    int update(Channel channel);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}