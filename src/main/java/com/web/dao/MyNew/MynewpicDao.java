package com.web.dao.MyNew;

import com.web.entity.MyNew.Mynewpic;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * (Mynewpic)表数据库访问层
 *
 * @author makejava
 * @since 2020-05-22 13:53:35
 */
public interface MynewpicDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Mynewpic queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Mynewpic> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param mynewpic 实例对象
     * @return 对象列表
     */
    List<Mynewpic> queryAll(Mynewpic mynewpic);

    /**
     * 新增数据
     *
     * @param mynewpic 实例对象
     * @return 影响行数
     */
    int insert(Mynewpic mynewpic);

    /**
     * 修改数据
     *
     * @param mynewpic 实例对象
     * @return 影响行数
     */
    int update(Mynewpic mynewpic);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    @Select("SELECT LAST_INSERT_ID()")
    int querryid();
}