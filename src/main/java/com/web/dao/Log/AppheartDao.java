package com.web.dao.Log;

import com.web.entity.Log.Appheart;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (Appheart)表数据库访问层
 *
 * @author makejava
 * @since 2020-05-11 15:44:38
 */
public interface AppheartDao {

    /**
     * 通过ID查询单条数据
     *
     * @param im 主键
     * @return 实例对象
     */
    Appheart queryById(String im);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Appheart> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param appheart 实例对象
     * @return 对象列表
     */
    List<Appheart> queryAll(Appheart appheart);

    /**
     * 新增数据
     *
     * @param appheart 实例对象
     * @return 影响行数
     */
    int insert(Appheart appheart);

    /**
     * 修改数据
     *
     * @param appheart 实例对象
     * @return 影响行数
     */
    int update(Appheart appheart);

    /**
     * 通过主键删除数据
     *
     * @param im 主键
     * @return 影响行数
     */
    int deleteById(String im);

}