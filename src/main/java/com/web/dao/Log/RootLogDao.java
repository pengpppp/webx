package com.web.dao.Log;

import com.web.entity.Log.RootLog;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * (RootLog)表数据库访问层
 *
 * @author makejava
 * @since 2020-05-07 11:03:55
 */
public interface RootLogDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    RootLog queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<RootLog> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param rootLog 实例对象
     * @return 对象列表
     */
    List<RootLog> queryAll(RootLog rootLog);

    /**
     * 新增数据
     *
     * @param rootLog 实例对象
     * @return 影响行数
     */
    int insert(RootLog rootLog);

    /**
     * 修改数据
     *
     * @param rootLog 实例对象
     * @return 影响行数
     */
    int update(RootLog rootLog);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    /**
     * 查询日志
     * @return
     */
    @Select("SELECT COUNT(*) FROM newweb.root_log ")
    int querycount();
}