package com.web.dao;

import com.web.entity.Visaorder;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * (Visaorder)表数据库访问层
 *
 * @author makejava
 * @since 2020-06-16 14:30:30
 */
public interface VisaorderDao {

    /**
     * 通过ID查询单条数据
     *
     *
     * @return 实例对象
     */
    Visaorder queryById(String orderid);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Visaorder> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param visaorder 实例对象
     * @return 对象列表
     */
    List<Visaorder> queryAll(Visaorder visaorder);

    /**
     * 新增数据
     *
     * @param visaorder 实例对象
     * @return 影响行数
     */
    int insert(Visaorder visaorder);

    /**
     * 修改数据
     *
     * @param visaorder 实例对象
     * @return 影响行数
     */
    int update(Visaorder visaorder);

    /**
     * 通过主键删除数据
     *
     *
     * @return 影响行数
     */
    int deleteById(String orderid);

    @Select("Select count(*) from newweb.visaorder")
    int queryCount();

    @Select("Select count(*) from newweb.visaorder where userid = #{userid}")
    int queryUserCount(String userid);

    @Select("select * from newweb.visaorder where userid = #{userid} order by i desc limit #{offset}, #{limit}")
    List<Visaorder> queryUserByLimit(@Param("userid")String userid,@Param("offset") int offset, @Param("limit") int limit);
}