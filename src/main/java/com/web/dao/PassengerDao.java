package com.web.dao;

import com.web.Plance.entity.HomeFlight.Request.Order.passenger;
import com.web.entity.Passenger;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * (Passenger)表数据库访问层
 *
 * @author makejava
 * @since 2020-06-04 13:53:07
 */
public interface PassengerDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Passenger queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<Passenger> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param passenger 实例对象
     * @return 对象列表
     */
    List<Passenger> queryAll(Passenger passenger);

    /**
     * 新增数据
     *
     * @param passenger 实例对象
     * @return 影响行数
     */
    int insert(Passenger passenger);

    /**
     * 修改数据
     *
     * @param passenger 实例对象
     * @return 影响行数
     */
    int update(Passenger passenger);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);


    @Select("select * from newweb.passenger where userid = #{userid} order by id desc ")
    List<Passenger> queryByUserid(Integer userid);

    @Select("SELECT LAST_INSERT_ID()")
    int querryid();

    @Select("select * from newweb.passenger where id = #{id}")
    passenger queryBypassenger(Integer id);
}