package com.web.dao;

import com.web.entity.SmsLog;
import org.apache.ibatis.annotations.Param;
import java.util.List;

/**
 * (SmsLog)表数据库访问层
 *
 * @author makejava
 * @since 2020-05-26 13:31:11
 */
public interface SmsLogDao {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    SmsLog queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit 查询条数
     * @return 对象列表
     */
    List<SmsLog> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param smsLog 实例对象
     * @return 对象列表
     */
    List<SmsLog> queryAll(SmsLog smsLog);

    /**
     * 新增数据
     *
     * @param smsLog 实例对象
     * @return 影响行数
     */
    int insert(SmsLog smsLog);

    /**
     * 修改数据
     *
     * @param smsLog 实例对象
     * @return 影响行数
     */
    int update(SmsLog smsLog);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}