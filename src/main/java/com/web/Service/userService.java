package com.web.Service;

import com.web.Config.ConfigClass;
import com.web.Config.UserPicConfig;
import com.web.Tools.Check.check;
import com.web.Tools.ImageV.Imagebase64;
import com.web.Tools.ImageV.SCaptcha;
import com.web.Tools.Redis.RedisUtil;
import com.web.Tools.staticData.Code;
import com.web.Tools.staticData.SMS;
import com.web.common.core.util.MD5Util;
import com.web.dao.UserDao;
import com.web.dao.UseridDao;
import com.web.entity.User.User;
import com.web.entity.User.Userid;
import com.web.entity.entityImp.loginEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;

@Service
public class userService {
    @Resource
    UserDao userDao;
    @Resource
    UseridDao useridDao;

    @Autowired
    RedisUtil redisUtil;
    @Autowired
    ConfigClass configClass;

    @Autowired
    SmsLogService smsLogService;

    @Autowired
    UserPicConfig userPicConfig;

    /**
     * 登录
     * @param mobile
     * @param loginPwd
     * @return
     */
    public loginEntity loginService(String mobile, String loginPwd)
    {
        loginEntity loginEntity = new loginEntity();
        if(check.checkMobile(mobile))//检查手机号码格式
        {
            loginEntity.getData().setMobile(mobile);
            loginEntity.getData().getLoginUserInfo().setMobile(mobile);
            Userid userid = new Userid();
            userid.setMobile(mobile);
            List<Userid> u = useridDao.queryAll(userid);
            if(u.size()!=0)//检查是否有该用户
            {
                if(loginPwd!=null||!loginPwd.equals(""))//检查密码是否为空
                {
                    if(u.get(0).getLoginpwd().equals(loginPwd))//检查密码是否正确
                    {
                        userid= u.get(0);
                        userid.setToken(MD5Util.md5Hex(SMS.vcode(8)));
                        useridDao.update(userid);
                        redisUtil.set(userid.getToken(),userid.getMobile()+"-"+userid.getUserid(),-1);
                        redisUtil.set(userid.getMobile(),userid.getToken(),-1);
                        loginEntity.getData().setUserid(u.get(0).getUserid());
                        loginEntity.getData().setToken(userid.getToken());
                        loginEntity.getData().setLoginUserInfo(userDao.queryById(mobile));
                        loginEntity.setCode(Code.SUCCESS);
                        loginEntity.setMessage("登录成功");
                    }else {
                        loginEntity.setCode(Code.PWD_ERROR);
                        loginEntity.setMessage(Code.information.get(Code.PWD_ERROR));
                    }
                }else {
                    loginEntity.setCode(Code.PWD_ERROR);
                    loginEntity.setMessage(Code.information.get(Code.PWD_ERROR));
                }
            }else {
                loginEntity.setCode(Code.USER_IS_NULL);
                loginEntity.setMessage(Code.information.get(Code.USER_IS_NULL));
            }
        }else {
            loginEntity.setCode(Code.PHONE_FORMAT_ERROR);
            loginEntity.setMessage(Code.information.get(Code.PHONE_FORMAT_ERROR));
        }
        return loginEntity;
    }

    /**
     * 注册
     * @param mobile
     * @param loginPwd
     * @param smsVerifyCode
     * @param verifyCodeKey
     * @return
     */
    public loginEntity registerService(String mobile,String loginPwd,String smsVerifyCode,String verifyCodeKey)
    {
        loginEntity loginEntity = new loginEntity();
        if(check.checkMobile(mobile))//先检查手机号码格式
        {
            Userid userid = new Userid();
            userid.setMobile(mobile);
            List<Userid> u = useridDao.queryAll(userid);
            if(u.size()==0)//检查是否已经有该用户
            {
                if(configClass.getUniversalCodeKey()&&verifyCodeKey.equals(configClass.getUniversalCode()))//测试用
                {
                    userid.setUserid(SMS.vcode(16));
                    userid.setLoginpwd(MD5Util.md5Hex(loginPwd));
                    userid.setToken(MD5Util.md5Hex(SMS.vcode(4)));
                    useridDao.insert(userid);
                    User user = new User();
                    user.setMobile(mobile);
                    user.setIsrealname("2");
                    user.setUserrole("24");
                    user.setCompanyauth("1");
                    user.setCompanyauthtype("0");
                    userDao.insert(user);
                    loginEntity.setCode(Code.SUCCESS);
                    loginEntity.setMessage("注册成功");
                    loginEntity.getData().setToken(userid.getToken());
                    loginEntity.getData().setUserid(useridDao.queryAll(userid).get(0).getUserid());
                    loginEntity.getData().setMobile(mobile);
                    loginEntity.getData().setLoginUserInfo(user);
                    File file=new File(userPicConfig.getPath()+userid.getUserid());
                    if(!file.exists()){//如果文件夹不存在
                        file.mkdir();//创建文件夹
                    }

                }else if(redisUtil.hasKey(verifyCodeKey))//检查是否有该验证码
                {
                    if(redisUtil.get(verifyCodeKey).equals(smsVerifyCode))//检查验证码是否正确
                    {
                        redisUtil.del(verifyCodeKey);
                        //注册
                        userid.setLoginpwd(loginPwd);
                        //md5加密
                        //userid.setToken(md_five.md5(SMS.vcode(4)));
                        userid.setToken(MD5Util.md5Hex(SMS.vcode(4)));
                        useridDao.insert(userid);
                        User user = new User();
                        user.setMobile(mobile);
                        user.setIsrealname("2");
                        user.setUserrole("24");
                        user.setCompanyauth("1");
                        user.setCompanyauthtype("0");
                        userDao.insert(user);
                        loginEntity.setCode(Code.SUCCESS);
                        loginEntity.setMessage("注册成功");
                        loginEntity.getData().setToken(userid.getToken());
                        loginEntity.getData().setUserid(useridDao.queryAll(userid).get(0).getUserid());
                        loginEntity.getData().setMobile(mobile);
                        loginEntity.getData().setLoginUserInfo(user);
                        File file=new File(userPicConfig.getPath()+userid.getUserid());
                        if(!file.exists()){//如果文件夹不存在
                            file.mkdir();//创建文件夹
                        }
                    }else {
                        loginEntity.setCode(Code.SMS_ERROR);
                        loginEntity.setMessage(Code.information.get(Code.SMS_ERROR));
                    }
                }else {
                    loginEntity.setCode(Code.SMS_LOSE);
                    loginEntity.setMessage(Code.information.get(Code.SMS_LOSE));
                }
            }else {
                loginEntity.setCode(Code.REGISTERED);
                loginEntity.setMessage(Code.information.get(Code.REGISTERED));
            }
        }else {
            loginEntity.setCode(Code.PHONE_FORMAT_ERROR);
            loginEntity.setMessage(Code.information.get(Code.PHONE_FORMAT_ERROR));
        }
        return loginEntity;
    }

    /**
     * 获取短信验证码
     * @param mobile
     * @param type
     * @param randomCode
     * @param randomCDKey
     * @return
     */
    public HashMap<String,String> GetVerifyCodeService(String mobile, String type, String randomCode, String randomCDKey)
    {
        String verifyCodeKey;
        String smsVerifyCode;
        HashMap<String,String> map =new HashMap<String, String>()
        {{
            put("code","");
            put("message","");
            put("verifyCodeKey","");
        }};

        if (!check.checkMobile(mobile))//检查号码格式是否正确,错误的话直接返回
        {
            map.put("code",Code.PHONE_FORMAT_ERROR);
            map.put("message",Code.information.get(Code.PHONE_FORMAT_ERROR));
            return map;
        }
        if(configClass.getUniversalCodeKey()&&randomCDKey.equals(configClass.getUniversalCode()))//硬编码，测试用//////////////////////////////////////////////////////////////////////////////////
        {

        }
        else if(redisUtil.hasKey(mobile)&&(int)redisUtil.get(mobile)>=5)//如果验证码的次数超过五次，则直接返回，设置code状态
        {
            map.put("code",Code.SMS_PASS);
            map.put("message",Code.information.get(Code.SMS_PASS));
            return map;
        }
        if(null==type||"".equals(type))//参数判断
        {
            map.put("code",Code.PARAMETER_ERROR);
            map.put("message",Code.information.get(Code.PARAMETER_ERROR));
            return map;
        }
        if(type.trim().equals("1")||type.trim().equals("3"))//如果需要带图片的类型下没带图片，同样返回错误
        {
            if(randomCDKey==null||randomCode==null||"".equals(randomCDKey)||"".equals(randomCode))
            {
                map.put("code",Code.PARAMETER_ERROR);
                map.put("message",Code.information.get(Code.PARAMETER_ERROR));
                return map;
            }
            //验证图片及图片验证码
            if(!redisUtil.hasKey(randomCDKey))
            {
                map.put("code",Code.GRA_LOSE);
                map.put("message",Code.information.get(Code.GRA_LOSE));
                return map;
            }
            if(!redisUtil.get(randomCDKey).equals(randomCode))
            {
                map.put("code",Code.GRA_ERROR);
                map.put("message",Code.information.get(Code.GRA_ERROR));
                return map;
            }
        }
        if(!redisUtil.hasKey(mobile))//如果该手机当天内第一次请求发送验证码
        {
            smsVerifyCode = SMS.vcode(4);//获取验证码
            verifyCodeKey =SMS.vKey();//获取验证码的key
            redisUtil.set(verifyCodeKey,smsVerifyCode, configClass.getSms_time());//持续时间15分钟
            redisUtil.set(mobile,1,configClass.getSms_phone_time());
            redisUtil.set(mobile+"1","锁定中，该缓存消失前不能再次发送验证码",configClass.getSms_lock_time());
        }else
        {
            if(redisUtil.hasKey(mobile+(int)redisUtil.get(mobile)))
            {
                map.put("code", Code.SMS_REACQUIRE);
                map.put("message",Code.information.get(Code.SMS_REACQUIRE));
                return map;
            }else {
                smsVerifyCode = SMS.vcode(4);//获取验证码
                verifyCodeKey = SMS.vKey();//获取验证码的key
                redisUtil.set(verifyCodeKey,smsVerifyCode, configClass.getSms_time());//持续时间15分钟
                redisUtil.incr(mobile,1);
                redisUtil.set(mobile+(int)redisUtil.get(mobile),"锁定中，该缓存消失前不能再次发送验证码",configClass.getSms_lock_time());
            }
        }
        //发送验证码
        if(smsLogService.Send(smsVerifyCode,mobile))
        {
            map.put("code",Code.SUCCESS);

            map.put("verifyCodeKey",verifyCodeKey);
        }else {
            map.put("code",Code.FAIL);
            map.put("message","短信验证码发送失败");
        }
        return map;

    }

    /**
     * 校验短信验证码
     * @return
     */
    public HashMap<String,Object> CheckVerifyCodeService(String mobile,String type,String smsVerifyCode,String verifyCodeKey)
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code","");
            put("message","");
            put("signData","");
        }};

        if(check.checkMobile(mobile))//先检查手机号码格式
        {
            if(configClass.getUniversalCodeKey()&&configClass.getUniversalCode().equals(verifyCodeKey))//测试用//////////////////////////////////
            {
                map.put("code",Code.SUCCESS);
                map.put("message","验证码正确");
                if(type.equals("3"))
                {
                    User loginUserInfo=userDao.queryById(mobile);
                    map.put("loginUserInfo",loginUserInfo);
                }
            }
            else if(redisUtil.hasKey(verifyCodeKey))//检查是否有该验证码
            {
                if (redisUtil.get(verifyCodeKey).equals(smsVerifyCode))//检查验证码是否正确
                {
                    map.put("code",Code.SUCCESS);
                    map.put("message","验证码正确");
                    if(type.equals("3"))
                    {
                        User loginUserInfo=userDao.queryById(mobile);
                        map.put("loginUserInfo",loginUserInfo);
                    }
                }else {
                    map.put("code",Code.SMS_ERROR);
                    map.put("message",Code.information.get(Code.SMS_ERROR));
                }
            }else {
                map.put("code",Code.SMS_LOSE);
                map.put("message",Code.information.get(Code.SMS_LOSE));
            }
        }else {
            map.put("code",Code.PHONE_FORMAT_ERROR);
            map.put("message",Code.information.get(Code.PHONE_FORMAT_ERROR));
        }
        return map;
    }

    /**
     * 获取图形验证码
     * @param type
     * @return
     */
    public HashMap<String,Object> GetRandomCodeService(String type)
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code","");
            put("message","");
            put("randomImageCode","");
            put("randomCDKey","");
        }};
        SCaptcha sCaptcha = new SCaptcha();//渲染图片
        String randomCDKey = SMS.vKey();//获取随机的KEY值
        redisUtil.set(randomCDKey,sCaptcha.getCode(),configClass.getSms_time());//存入缓存
        try {
            String randomImageCode = URLEncoder.encode(Imagebase64.getImageBinary(sCaptcha.getBuffImg()), "UTF-8");//编码后的图片流
            map.put("code",Code.SUCCESS);
            map.put("message","发送图片验证码成功");
            map.put("randomImageCode",randomImageCode);
            map.put("randomCDKey",randomCDKey);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 校验图形验证码
     * @param randomImageCode
     * @param randomCDKey
     * @return
     */
    public HashMap<String,Object> ValidateRandomCodeService(String randomImageCode,String randomCDKey)
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code","");
            put("message","");
        }};
        if(configClass.getUniversalCodeKey()&&randomCDKey.equals(configClass.getUniversalCode()))//测试用//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        {
            map.put("code",Code.SUCCESS);
            map.put("message","图片验证码正确");
        }
        else if(redisUtil.hasKey(randomCDKey))
        {
            if(redisUtil.get(randomCDKey).equals(randomImageCode))
            {
                map.put("code",Code.SUCCESS);
                map.put("message","图片验证码正确");
            }else {
                map.put("code",Code.GRA_ERROR);
                map.put("message",Code.information.get(Code.GRA_ERROR));
            }
        }else {
            map.put("code",Code.GRA_LOSE);
            map.put("message",Code.information.get(Code.GRA_LOSE));
        }
        return map;
    }


}
