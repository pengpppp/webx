package com.web.Service;

import com.web.Tools.SMS.SendSMS;
import com.web.dao.SmsLogDao;
import com.web.entity.SmsLog;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SmsLogService {
    @Resource
    SmsLogDao smsLogDao;
    @Autowired
    SendSMS sendSMS;
    public Boolean Send(String code,String mobile)
    {
        boolean b =true;
        JSONObject object = JSONObject.fromObject(sendSMS.SendSms(code,mobile));
        if(String.valueOf(object.get("Code")).equals("OK"))
        {
            JSONObject object1 = JSONObject.fromObject(sendSMS.querySms(mobile,object.getString("BizId")));
            if(object1.getString("Code").equals("OK"))
            {
                JSONArray list = object1.optJSONArray("SmsSendDetailDTOs");
                for (int i = 0; i < list.size(); i++) {
                    JSONObject obj = (JSONObject) list.opt(i);
                    SmsLog smsLog  = (SmsLog) JSONObject.toBean(obj,SmsLog.class);
                    smsLogDao.insert(smsLog);
                    //newww.getList().add((newImp) JSONObject.toBean(obj,newImp.class));
                }
            }else {
                System.out.println(object1.getString("Message")+"querryerror");
                b=false;
            }

        }else {
            System.out.println(object.getString("Message"));
            b=false;
        }
        return b;
    }
}
