package com.web.Service;

import com.web.common.core.entity.JsonData;
import com.web.dao.New.CollectionDao;
import com.web.entity.MyNew.Collection;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CollectionService {

    @Resource
    CollectionDao collectionDao;

    public Object addCollection(Collection collection)
    {
        collectionDao.insert(collection);
        return JsonData.buildSuccess();
    }

    public Object getCollection(String leavingname)
    {
        return JsonData.buildSuccess(collectionDao.queryByUser(leavingname));
    }

}
