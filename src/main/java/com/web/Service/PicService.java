package com.web.Service;

import com.web.Config.ConfigClass;
import com.web.Tools.staticData.Code;
import com.web.dao.exit.H5urlDao;
import com.web.dao.PictureDao;
import com.web.entity.Picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PicService {
    @Resource
    PictureDao pictureDao;

    @Resource
    H5urlDao h5urlDao;

    @Autowired
    ConfigClass configClass;

    /**
     *
     * @param string 编码后的二进制图片信息
     * @param h5 与该图片相连接的网址
     * @return
     */
//    public Map insert(String string,String h5)
//    {
//        HashMap<String,Object> map =new HashMap<String,Object>()
//        {{
//            put("code","");
//            put("message","");
//            put("photourl","");
//        }};
//        String name = (pictureDao.queryLast().getId()+1)+".jpg";
//        Picture picture = new Picture();
//        picture.setPrcturepath(configClass.getIp()+configClass.getImageurl() +name);
//        //picture.setType(1);
//        //picture.setDescs("图片");
//        if(h5!=null)
//        {
//            picture.setUrl(h5);
//        }
//        if(string==null)
//        {
//            map.put("code", Code.PARAMETER_ERROR);
//            map.put("message","参数错误");
//            return map;
//        }
//        try {
//            Imagebase64.base64StringToImage(URLDecoder.decode(string,"utf-8"),configClass.getPath()+name);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        pictureDao.insert(picture);
//
//        map.put("code", Code.SUCCESS);
//        map.put("message","成功");
//        map.put("photourl",picture.getPrcturepath());
//        return map;
//    }
//
//    /**
//     *上传h5页面
//     * @param string 编号后的页面文件信息
//     * @param name 页面文件名字
//     * @return
//     */
//    public Map insertH5(String string,String name) {
//        HashMap<String, Object> map = new HashMap<String, Object>() {{
//            put("code", "");
//            put("message", "");
//            put("url", "");
//        }};
//        if (string==null||name==null)
//        {
//            map.put("code", Code.PARAMETER_ERROR);
//            map.put("message","参数错误");
//            return map;
//        }
//        try {
//            H5url h5url =new H5url();
//            h5url.setUrl(configClass.getIp()+name+".html");
//            h5url.setLocation(configClass.getH5path()+name+".html");
//            h5urlDao.insert(h5url);
//            FileCode.decoderBase64File(URLDecoder.decode(string, "utf-8"), h5url.getLocation());
//            map.put("code",Code.SUCCESS);
//            map.put("message","成功");
//            map.put("url",h5url.getUrl());
//            return map;
//        }catch (Exception e)
//        {
//            e.printStackTrace();
//        }
//        map.put("code", Code.PARAMETER_ERROR);
//        map.put("message","参数错误");
//        return map;
//    }

    /**
     * 绑定
     * @param imageurl
     * @param h5url
     * @return
     */
    public Map binding(String imageurl,String h5url)
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code","");
            put("message","");
        }};
        Picture picture = new Picture();
        picture.setPrcturepath(imageurl);
        List<Picture> l = pictureDao.queryAll(picture);
        if(l==null||l.size()==0)
        {
            map.put("code",Code.FAIL);
            map.put("message","该图片不存在");
            return map;
        }
//        H5url h5url1 = new H5url();
//        h5url1.setUrl(h5url);
//        List<H5url> list = h5urlDao.queryAll(h5url1);
//        if(list==null)
//        {
//            map.put("code",Code.FAIL);
//            map.put("message","该页面不存在本服务器");
//            return map;
//        }
        picture=l.get(0);
        picture.setUrl(h5url);
        pictureDao.update(picture);
        map.put("code",Code.SUCCESS);
        map.put("message","成功绑定");
        return map;
    }

    /**
     * 这是获取导航页的
     * @return
     */
    public Map showbanner()
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code","");
            put("message","");
            put("total",0);
            put("data",new ArrayList<Picture>());
        }};
        map.put("code",Code.SUCCESS);
        map.put("message","获取首页成功");
        map.put("total",pictureDao.querycount(1));
//        //从最后一条开始查询，只查询出10条记录
//        map.put("data",pictureDao.queryPage(pictureDao.queryLast(1).getId(),configClass.getPagenumb(),1));
        //查询出所有记录
        map.put("data",pictureDao.queryPage(pictureDao.queryLast(1).getId(),(Integer) map.get("total"),1));
        return map;
    }

    /**
     * 这是获取内容页的
     * @param last
     * @param page
     * @return
     */
    public Map showhome(Object last,Object page)
    {
        Integer lastIndex=0;
        Integer pageSize=0;
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code","");
            put("message","");
            put("total",0);
            put("data",new ArrayList<Picture>());
            put("lastIndex",0);
        }};
        if(page!=null)
        {
            pageSize= (Integer) page;
            if(pageSize!=10)
            {
                configClass.setPagenumb(pageSize);
            }
        }
        if(last==null||"".equals(last)||"null".equals(last))
        {
            lastIndex=0;
        }else {
            lastIndex= (Integer) last;
        }
        if(lastIndex==1)
        {
            map.put("code",Code.FAIL);
            map.put("message","已无页面可获取");
            return map;
        }
        List<Picture> list = new ArrayList<Picture>();
        Integer count = pictureDao.querycount(2);
        if(lastIndex==0)
        {
            list=pictureDao.queryPage(pictureDao.queryLast(2).getId(),configClass.getPagenumb(),2);
        }else {
            list = pictureDao.queryPage(lastIndex - 1, configClass.getPagenumb(), 2);
            if(list.size()!=0)
                map.put("lastIndex",list.get(list.size()-1).getId());
        }
        map.put("data",list);
        if(list.size()==0)
        {
            map.put("lastIndex",lastIndex);
        }else {
            map.put("lastIndex",list.get(list.size()-1).getId());
        }
        map.put("total",count);
        map.put("code",Code.SUCCESS);
        map.put("message","获取页面成功");
        configClass.setPagenumb(10);
        return map;
    }

}
