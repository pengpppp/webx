package com.web.Service;

import com.web.Config.UserPicConfig;
import com.web.Tools.staticData.SMS;
import com.web.common.core.entity.JsonData;
import com.web.dao.PicturepathDao;
import com.web.dao.UserpicDao;
import com.web.entity.Picturepath;
import com.web.entity.Userpic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

@Service
public class UserPicService {
    @Resource
    UserpicDao userpicDao;
    @Autowired
    UserPicConfig userPicConfig;
    @Resource
    PicturepathDao picturepathDao;

    public Object upload(MultipartFile file,String userid)
    {
        String fileName = new Date().getTime() + SMS.vcode(4) + ".jpg";
        String filepath=userPicConfig.getPath()+ userid+"/"+fileName; //拼凑文件存储位置

        Picturepath picturepath =new Picturepath();
        picturepath.setLocation(filepath);
        picturepath.setUrl(userPicConfig.getUrl()+userid+"/"+fileName);
        picturepathDao.insert(picturepath);

        File dest = new File(filepath);
        try {
            if(dest!=null)
                dest.createNewFile();
            file.transferTo(dest);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return JsonData.buildSuccess(new HashMap<String,Object>(){{put("picurl",picturepath.getUrl());}});
    }

    public Object insert(Userpic userpic)
    {
        try {
            userpicDao.insert(userpic);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object delete(String picurl)
    {
        try {
            userpicDao.deleteById(picurl);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }
}
