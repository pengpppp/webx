package com.web.Service.Plance;


import com.web.Plance.Tool.PlanceHttp;
import com.web.Plance.Tool.PlancePath;
import com.web.Plance.entity.HomeFlight.Response.FlightDetails;
import com.web.Plance.entity.HomeFlight.Response.FlightList;
import com.web.Service.Plance.My.InsuranceService;
import com.web.common.core.entity.JsonData;
import com.web.common.core.util.JSONUtil;
import com.web.entity.Insurance;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlanceHomeService {
    @Autowired
    PlanceHttp planceHttp;
    @Autowired
    InsuranceService insuranceService;
    public Object getlowpricecalendar(Object object)
    {
        return planceHttp.Request(object, PlancePath.getlowpricecalendar,1);
    }
    public Object cheapflights(Object object)
    {
        return planceHttp.Request(object, PlancePath.cheapflights,1);
    }
    public Object getInsuranceList(Object object)
    {
        try {
            JsonData jsonData = (JsonData)planceHttp.Request(object, PlancePath.getInsuranceList,1);//获取远程的保险
            JSONObject jsonObject = JSONObject.fromObject(jsonData.getData().toString());
            List<Insurance> list = JSONUtil.toObj(String.valueOf(jsonObject.get("Insurances")), ArrayList.class);
            for(int i=0;i<list.size();i++)//将远程保险放进数据库
            {
                Insurance insurance = JSONUtil.toObj(JSONObject.fromObject(list.get(i)).toString(),Insurance.class);
                try {
                    insuranceService.addOther(insurance);
                }catch (Exception e)
                {}
            }
            list.addAll(insuranceService.getAllMyInsurance());
            JSONObject jsonObject1  = new JSONObject();
            jsonObject1.put("Insurances",list);
            return JsonData.buildSuccess(JSONUtil.toObj(jsonObject1.toString(),Object.class));
        }catch (Exception e)
        {
            return JsonData.buildError(e.toString());
        }
    }
    public Object getflightlist(Object object)
    {
        try {
            JsonData jsonData = (JsonData)planceHttp.Request(object, PlancePath.getflightlist,1);
            FlightList flights = JSONUtil.toObj(jsonData.getData().toString(),FlightList.class);
            //TODO 进行筛选配置化操作
            return JsonData.buildSuccess(flights);
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage()+"service");
        }
    }
    public Object getflightdetails(Object object)
    {
        try {
            JsonData jsonData = (JsonData)planceHttp.Request(object, PlancePath.getflightdetails,1);
            FlightDetails flightDetails = JSONUtil.toObj(jsonData.getData().toString(),FlightDetails.class);
            //System.out.println(flightDetails.toString());
            return JsonData.buildSuccess(flightDetails);
        }
        catch (Exception e)
        {
            return JsonData.buildError(e.getMessage());
        }
    }
    public Object getrefundrule(Object object)
    {
        return planceHttp.Request(object, PlancePath.getrefundrule,1);
    }
    public Object checkcabinprice(Object object)
    {
        return planceHttp.Request(object, PlancePath.checkcabinprice,1);
    }
    public Object createorder(Object object)
    {
        return planceHttp.Request(object, PlancePath.createorder,1);
    }
}
