package com.web.Service.Plance;

import com.web.Plance.Tool.PlanceHttp;
import com.web.Plance.Tool.PlanceInterPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanceInterService {
    @Autowired
    PlanceHttp planceHttp;
    public Object getFlightList(Object object)
    {
        return planceHttp.Request(object, PlanceInterPath.getflightlist,2);
    }
    public Object checkprice(Object object)
    {
        return planceHttp.Request(object, PlanceInterPath.checkprice,2);
    }
    public Object getrefundrule(Object object)
    {
        return planceHttp.Request(object, PlanceInterPath.getrefundrule,2);
    }
    public Object createorder(Object object)
    {
        return planceHttp.Request(object, PlanceInterPath.createorder,2);
    }
}
