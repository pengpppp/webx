package com.web.Service.Plance;

import com.web.Plance.Tool.PlanceBasePath;
import com.web.Plance.Tool.PlanceHttp;
import com.web.Plance.entity.AllRequset;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanceBaseService {

    @Autowired
    PlanceHttp planceHttp;

    public Object flightcitylist()
    {
        return planceHttp.Request(new AllRequset<String>(),PlanceBasePath.flightcitylistt,0);
    }
    public Object getaircompanylist()
    {
        return planceHttp.Request(new AllRequset<String>(),PlanceBasePath.getaircompanylist,0);
    }
    public Object getairportslist()
    {
        return planceHttp.Request(new AllRequset<String>(),PlanceBasePath.getairportslist,0);
    }
    public Object getorderlist(Object object)
    {
        return planceHttp.Request(object,PlanceBasePath.getorderlist,0);
    }
    public Object getorderdetails(Object object)
    {
        return planceHttp.Request(object,PlanceBasePath.getorderdetails,0);
    }
    public Object cancelorder(Object object)
    {
        return planceHttp.Request(object,PlanceBasePath.cancelorder,0);
    }
    public Object confirmorder(Object object)
    {
        return planceHttp.Request(object,PlanceBasePath.confirmorder,0);
    }
    public Object refundupload(Object object)
    {
        return planceHttp.Request(object,PlanceBasePath.refundupload,0);
    }
    public Object refundapply(Object object)
    {
        return planceHttp.Request(object,PlanceBasePath.refundapply,0);
    }
    public Object getrefundorderlist(Object object)
    {
        return planceHttp.Request(object,PlanceBasePath.getrefundorderlist,0);
    }
    public Object getrefundorderdetails(Object object)
    {
        return planceHttp.Request(object,PlanceBasePath.getrefundorderdetails,0);
    }
    public Object changeapply(Object object)
    {
        return planceHttp.Request(object,PlanceBasePath.changeapply,0);
    }
    public Object getchangeorderlist(Object object)
    {
        return planceHttp.Request(object,PlanceBasePath.getchangeorderlist,0);
    }
    public Object getchangeorderdetails(Object object)
    {
        return planceHttp.Request(object,PlanceBasePath.getchangeorderdetails,0);
    }
    public Object cancelchange(Object object)
    {
        return planceHttp.Request(object,PlanceBasePath.cancelchange,0);
    }
    public Object confirmchange(Object object)
    {
        return planceHttp.Request(object,PlanceBasePath.confirmchange,0);
    }
}
