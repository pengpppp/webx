package com.web.Service.Plance.Visa;

import com.google.gson.Gson;
import com.web.Tools.Check.check;
import com.web.Tools.staticData.SMS;
import com.web.common.core.entity.JsonData;
import com.web.dao.PassengerDao;
import com.web.dao.VisaDao;
import com.web.dao.VisainsuranceDao;
import com.web.dao.VisaorderDao;
import com.web.entity.entityImp.Visa.*;
import com.web.entity.Visa;
import com.web.entity.Visaorder;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class VisaOrderService {
    @Resource
    VisaorderDao visaorderDao;

    @Resource
    VisainsuranceDao visainsuranceDao;

    @Resource
    VisaDao visaDao;

    @Autowired
    Gson gson;

    @Resource
    PassengerDao passengerDao;

    private Integer pagesize=10;

    public Object add(VisaOrderImp visaOrderImp, String userid)
    {
        try {
            if(checkVisaOrder(visaOrderImp))
                throw new Exception("订单参数错误");
            Visaorder visaorder = new Visaorder(visaOrderImp);
            List<VisaOrderPassenger> list = new ArrayList<>();
            for(String id:visaOrderImp.getApplicant())
            {
                list.add(VisaOrderPassenger.passenger2visaOrderpassenger(passengerDao.queryById(Integer.valueOf(id))));
            }
            visaorder.setApplicant(list.toString());

            List<VisaInsuranceImp> list1 = new ArrayList<>();
            for(String id:visaOrderImp.getInsurances())
            {
                list1.add(VisaInsuranceImp.VisaInsurance2VisaInsuranceImp(visainsuranceDao.queryById(id)));
            }
            visaorder.setInsurances(list1.toString());

            visaorder.setUserid(userid);
            String s = SMS.vRom(7);
            visaorder.setOrderid(s);
            visaorder.setPayment("1");
            visaorder.setCreattime(new Date().getTime());
            visaorder.setPrice(sumpirce(visaOrderImp));
            visaorderDao.insert(visaorder);
            return JsonData.buildSuccess(new HashMap<String ,Object>(){{put("orderid",s);}});
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object show(String userid,String page)
    {
        Integer p ;

        try {
            if(check.checkNull(userid))
                throw new Exception("userid错误");
            Integer allnumb=visaorderDao.queryUserCount(userid);
            if(check.checkNull(page))
                p=1;
            else
                p=Integer.valueOf(page);

            if((p-1)*pagesize>allnumb)
                return JsonData.buildSuccess();
            else {
                Gson gson  = new Gson();
                List<Visaorder> list = visaorderDao.queryUserByLimit(userid,((p-1)*pagesize),pagesize);
                List<VisaOrderList> lists = new ArrayList<>();
                for (Visaorder visaorder1 : list) {
                    VisaOrderList visaOrderList = VisaOrderList.VisaOrder2VisaOrderList(visaorder1);
                    visaOrderList.setVisaList(VisaList.visa2visaList(visaDao.queryById(visaorder1.getId())));
                    lists.add(visaOrderList);
                }
                JSONObject jsonObject = JSONObject.fromObject(JsonData.buildSuccess(lists));
                jsonObject.put("total",String.valueOf(allnumb));
                return gson.fromJson(jsonObject.toString(),Object.class);
            }

        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object showDetails(String orderid)
    {
        try {
            if(check.checkNull(orderid))
                throw new Exception("orderid错误");
            Visaorder visaorder = visaorderDao.queryById(orderid);
            VisaOrderDetails visaOrderDetails = VisaOrderDetails.visaOrder2visaOrderDetails(visaorder);

            visaOrderDetails.setVisaDetails(VisaDetails.visa2visadetails(visaDao.queryById(visaorder.getId())));

            return JsonData.buildSuccess(visaOrderDetails);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object concelOrder(String orderid)
    {
        try {
            if(check.checkNull(orderid))
                throw new Exception("id错误");
            Visaorder visaorder = new Visaorder();
            visaorder.setOrderid(orderid);
            visaorder.setPayment("4");
            visaorderDao.update(visaorder);
            return JsonData.buildSuccess();
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }


    /**
     * 检查参数是否为空
     * @param visaOrderImp
     * @return
     */
    private boolean checkVisaOrder(VisaOrderImp visaOrderImp)
    {
        try {
            if(check.checkNull(visaOrderImp.getId())||check.checkNull(visaOrderImp.getApplicant().toString())||check.checkNull(visaOrderImp.getDeparturetime().toString())||
                    check.checkNull(visaOrderImp.getDeliverytype())||check.checkNull(visaOrderImp.getDeliverypath())||check.checkNull(visaOrderImp.getContactname())
                    ||check.checkNull(visaOrderImp.getContactmobile()))
            {
                return true;
            }else
                return false;
        }catch (Exception e)
        {
            e.printStackTrace();
            return true;
        }

    }

    /**
     * 计算价格
     * @param visaOrderImp
     * @return
     * @throws Exception
     */
    private Double sumpirce(VisaOrderImp visaOrderImp) throws Exception {
        try {
            Visa visa  = visaDao.queryById(visaOrderImp.getId());
            double price = visa.getCost() ;
            try {
                for(String s:visaOrderImp.getInsurances())
                {
                    price+= visainsuranceDao.queryById(s).getCost();
                }
                price = price*visaOrderImp.getApplicant().size();
            }catch (Exception e)
            {
                e.printStackTrace();
            }
            return price;
        }catch (Exception e)
        {
            e.printStackTrace();
            throw new Exception("计算价格错误");
        }


    }

}
