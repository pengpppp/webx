package com.web.Service.Plance.Visa;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.web.Tools.Check.check;
import com.web.common.core.entity.JsonData;
import com.web.dao.VisaDao;
import com.web.entity.Visa;
import com.web.entity.entityImp.Visa.VisaDetails;
import com.web.entity.entityImp.Visa.VisaInsuranceImp;
import com.web.entity.entityImp.Visa.VisaList;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class VisaService {
    @Autowired
    VisaInsuranceService visaInsuranceService;

    @Resource
    VisaDao visaDao;
    private Integer pagesize=10;
    public Object getAll(String page)
    {
        Integer p ;
        Integer allnumb=visaDao.queryCount2();
        try {
            if(check.checkNull(page))
                p=1;
            else
                p=Integer.valueOf(page);

            if((p-1)*pagesize>allnumb)
                return JsonData.buildSuccess();
            else
            {
                Gson gson  = new Gson();
                List<Visa> visas = visaDao.queryByLimit(((p-1)*pagesize),pagesize);
                List<VisaList> visaLists = new ArrayList<>();
                for (Visa visa:visas)
                {
                    visaLists.add(VisaList.visa2visaList(visa));
                }
                JSONObject jsonObject = JSONObject.fromObject(JsonData.buildSuccess(visaLists));
                jsonObject.put("total",String.valueOf(allnumb));
                return gson.fromJson(jsonObject.toString(),Object.class);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object getOne(String id)
    {
        try {
            Gson gson =new Gson();
            if(check.checkNull(id))
                throw new Exception("id不能为空");
            Visa visa= visaDao.queryById(id);
            List<String> ins = gson.fromJson(visa.getInsurance(),new TypeToken<List<String>>(){}.getType());
            List<VisaInsuranceImp> insuranceImps = new ArrayList<>();
            for(String i:ins)
            {
                VisaInsuranceImp visainsurance = visaInsuranceService.getOne(i);
                if(!check.checkNull(visainsurance.getId()))
                    insuranceImps.add(visainsurance);
            }
            VisaDetails visaDetails =VisaDetails.visa2visadetails(visa);
            visaDetails.setInsurance(insuranceImps);
            return JsonData.buildSuccess(visaDetails);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

}
