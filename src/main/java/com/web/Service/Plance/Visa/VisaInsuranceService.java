package com.web.Service.Plance.Visa;

import com.web.dao.VisainsuranceDao;
import com.web.entity.Visainsurance;
import com.web.entity.entityImp.Visa.VisaInsuranceImp;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class VisaInsuranceService {
    @Resource
    VisainsuranceDao visainsuranceDao;

    public VisaInsuranceImp getOne(String id)
    {
        try {
            System.out.println(id);
            Visainsurance visaInsurance = visainsuranceDao.queryById(id);
            return VisaInsuranceImp.VisaInsurance2VisaInsuranceImp(visaInsurance);
        }catch (Exception e)
        {
            e.printStackTrace();
            return new VisaInsuranceImp();
        }
    }

}
