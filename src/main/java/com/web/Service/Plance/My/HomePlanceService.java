package com.web.Service.Plance.My;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.web.Config.PlanceConfig;
import com.web.Plance.Tool.PlanceBasePath;
import com.web.Plance.Tool.PlanceHttp;
import com.web.Plance.Tool.PlancePath;
import com.web.Plance.entity.HomeFlight.Request.Order.Checkcabinprice;
import com.web.Plance.entity.HomeFlight.Request.Order.Createorder;
import com.web.Plance.entity.HomeFlight.Request.Order.passenger;
import com.web.Plance.entity.HomeFlight.Response.CreateOrderRespose;
import com.web.Plance.entity.Order.Request.Order.Confirmorder;
import com.web.Plance.entity.Order.Response.ConfirmOrderRespose;
import com.web.Tools.Check.check;
import com.web.Tools.staticData.SMS;
import com.web.common.core.entity.JsonData;
import com.web.dao.HomeorderDao;
import com.web.dao.InsuranceDao;
import com.web.dao.PassengerDao;
import com.web.entity.Homeorder;
import com.web.entity.Insurance;
import com.web.entity.Passenger;
import com.web.entity.entityImp.Order.Order;
import com.web.entity.entityImp.Order.SaveOrder;
import com.web.entity.entityImp.getAllMyOrder;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class HomePlanceService {
    @Resource
    HomeorderDao homeorderDao;
    @Resource
    PassengerDao passengerDao;
    @Autowired
    InsuranceService insuranceService;
    @Resource
    InsuranceDao insuranceDao;
    @Autowired
    PlanceConfig planceConfig;
    @Autowired
    PlanceHttp planceHttp;

    @Autowired
    Gson gson;

    private Integer pagesize=10;

    public Checkcabinprice addCheckprice(SaveOrder saveOrder)
    {
        Checkcabinprice checkcabinprice = new Checkcabinprice();
        checkcabinprice.setSessionId(saveOrder.getSessionid());
        List<passenger> list  = new ArrayList<>();
        for(int i :saveOrder.getPassengers())
        {
            list.add(passengerDao.queryBypassenger(i));
        }
        checkcabinprice.setPassengers(list);
        return checkcabinprice;
    }

    public Object addTest(SaveOrder saveOrder,Integer userid)
    {
        try {
            Homeorder homeorder = new Homeorder();
            homeorder.setUseid(userid);
            homeorder.setId(SMS.vRom(3));

            if(saveOrder.getDelivery()!=null&&!check.checkNull(saveOrder.getDelivery().getCityname())) {
                if(checkDelivery(saveOrder.getDelivery()))
                    throw new Exception("邮寄信息错误");
                homeorder.setDelivery(saveOrder.getDelivery().toString());
            }
            homeorder.setSessionid(saveOrder.getSessionid().toString());
            homeorder.setContactname(saveOrder.getContactname().toString());
            homeorder.setContactmobile(saveOrder.getContactmobile().toString());
            homeorder.setOrdersource(saveOrder.getOrdersource());

            List<Integer> list = gson.fromJson(saveOrder.getPassengers().toString(),new TypeToken<List<Integer>>(){}.getType());
            List<passenger> list1  = new ArrayList<>();
            for(int i :list)
            {
                list1.add(passengerDao.queryBypassenger(i));
            }
            homeorder.setPassengers(list1.toString());


            homeorder.setInsurances(saveOrder.getInsurances().toString());
            homeorder.setRemark(saveOrder.getRemark());
            homeorder.setInvices(saveOrder.getInvices().toString());
            homeorder.setFlight(saveOrder.getFlight().toString());
            homeorder.setCabin(saveOrder.getCabin().toString());
            homeorder.setPrice(sumprice(saveOrder.getCabin(),saveOrder.getPassengers(),saveOrder.getDelivery(),saveOrder.getInsurances()));
            homeorder.setPayment("1");
            homeorder.setCreattime(new Date().getTime());
            homeorderDao.insert(homeorder);
            return  JsonData.buildSuccess(new HashMap<String,Object>(){{put("id",homeorderDao.queryByI(homeorderDao.querryid()).getId());}});
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }
    }

    public Object get(String id)
    {
        try {
            Homeorder homeorder = homeorderDao.queryById(id);
            Order order = new Order();
            order=order.initOrder(homeorder);
            String s = homeorder.getPassengers();
            String s1 = homeorder.getInsurances();

            if(Long.parseLong(order.getNowtime())-Long.parseLong(order.getCreattime())>planceConfig.getPaytime())//如果现在时间-下单时间大于规定的时间，则设定为取消，否则则返还下单时间+规定的时间
            {
                Homeorder homeorder1 = new Homeorder();
                homeorder1.setId(id);
                homeorder1.setPayment("4");
                order.setPayment("4");
                homeorderDao.update(homeorder1);
            }else
                order.setCreattime(String.valueOf(Long.parseLong(order.getCreattime())+planceConfig.getPaytime()));

            List<Insurance> list1 = new ArrayList<>();//获取保险信息
            List<Integer> list = gson.fromJson(s1,new TypeToken<List<Integer>>(){}.getType());
            for(int i :list)
            {
                list1.add(insuranceService.getOne(String.valueOf(i)));
            }
            order.setInsurances(list1);

            List<Passenger> list3 = gson.fromJson(s,new TypeToken<List<Passenger>>(){}.getType());
            order.setPassengers(list3);

            return JsonData.buildSuccess(order);
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }

    }

    public Object getAll(Integer userid,String page)
    {
        try {
            Integer p ;
            Integer allnumb=homeorderDao.queryByUserid(userid);
            if(check.checkNull(page))
                p=1;
            else
                p=Integer.valueOf(page);
            if((p-1)*pagesize>allnumb)
                return JsonData.buildSuccess();
            else {
                Gson gson  = new Gson();
                List<getAllMyOrder> o = homeorderDao.queryAllByUserLimit(userid,((p-1)*pagesize),pagesize);
                for(int i = 0;i<o.size();i++)
                {
                    o.get(i).setFlight(gson.fromJson(o.get(i).getFlight().toString(), SaveOrder.FlightBean.class));
                }
                JSONObject jsonObject = JSONObject.fromObject(JsonData.buildSuccess(removeOrder(o)));
                jsonObject.put("total",allnumb);
                return gson.fromJson(jsonObject.toString(),Object.class);
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }

    }

    /**
     * 取消订单
     * 取消订单应该分两个部分：
     *     一个是未支付的取消订单，直接设置状态
     *     一个是支付后的取消订单，得先退票、再退款、接着再设置状态
     * @param id
     * @return
     */
    public  Object delete(String id)
    {
        Homeorder H = homeorderDao.queryById(id);
        if(H.getPayment().equals("1"))
        {
            Homeorder homeorder = new Homeorder();
            homeorder.setId(id);
            homeorder.setPayment("4");
            homeorderDao.update(homeorder);
        }else if(H.getPayment().equals("3")){

            //TODO 先退票  再退款  再设置状态
            Homeorder homeorder = new Homeorder();
            homeorder.setId(id);
            homeorder.setPayment("4");
            homeorderDao.update(homeorder);
        }


        return JsonData.buildSuccess();
    }

    /**
     * 下单、出票
     * @param id
     * @return
     */
    public Boolean creatOrder(String id)
    {
        try {
            Homeorder homeorder = homeorderDao.queryById(id);
            Checkcabinprice checkcabinprice = Checkcabinprice.homeorder2checkcabinpricce(homeorder);
            Createorder createorder = Createorder.homeorder2Createorder(homeorder);
            createorder=removeInsurance(createorder);//过滤掉咱们的保险
            JsonData jsonData = (JsonData) planceHttp.Request(checkcabinprice, PlancePath.checkcabinprice,1);//验舱
            if(jsonData.getCode().equals("1000"))//验舱成功
            {
                jsonData = (JsonData) planceHttp.Request(createorder, PlancePath.createorder,1);//下单
                if(jsonData.getCode().equals("1000"))//下单成功
                {
                    CreateOrderRespose createOrderRespose= (CreateOrderRespose) jsonData.getData();
                    Confirmorder confirmorder = new Confirmorder(createOrderRespose.getOrderid());
                    jsonData = (JsonData) planceHttp.Request(confirmorder, PlanceBasePath.confirmorder,0);//确认出票
                    if(jsonData.getCode().equals("1000"))//出票成功
                    {
                        ConfirmOrderRespose confirmOrderRespose  = (ConfirmOrderRespose) jsonData.getData();
                        if(confirmOrderRespose.getConfirmStatus()==1)
                        {
                            Homeorder homeorder1 = new Homeorder();
                            homeorder1.setId(id);
                            homeorder1.setOrderid(createOrderRespose.getOrderid());
                            homeorder1.setOrdernumber(createOrderRespose.getOrdernumber());
                            homeorder1.setPayment("3");
                            homeorderDao.update(homeorder1);
                            return true;//出票成功
                        }else {
                            throw new Exception("订票失败");
                        }
                    }else {
                        throw new Exception("订票失败");
                    }
                }else {
                    throw new Exception("订票失败");
                }
            }else {
                throw new Exception("验舱失败");
            }
        }catch (Exception e)
        {
            e.printStackTrace();
            return false;
        }
    }


    /**
     *计算价格
     * @param cabin 航班
     * @param passengers 乘客
     * @param delivery  邮寄信息
     * @param insurances 保险
     * @return
     */
    private double sumprice(SaveOrder.CabinBean cabin, List<Integer> passengers, SaveOrder.DeliveryBean delivery, List<Integer> insurances)
    {
        double price =0;
        Passenger passenger = new Passenger();
        for(int i=0;i<passengers.size();i++)
        {
            passenger=passengerDao.queryById(passengers.get(i));
            if(passenger.getPassengertype()==1)//成人
            {
                price+=cabin.getPriceInfo().getAdult().getPrice()+cabin.getPriceInfo().getAdult().getTax()+cabin.getPriceInfo().getAdult().getFuel();
            }else if(passenger.getPassengertype()==0) {
                price+=cabin.getPriceInfo().getChild().getPrice()+cabin.getPriceInfo().getChild().getTax()+cabin.getPriceInfo().getChild().getFuel();
            }
        }
        try {
            if(delivery!=null)
            {
                price+=delivery.getDeliveryprice();
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        try {
            if(insurances.size()!=0)
            {
                double d = 0;
                for(int i=0;i<insurances.size();i++)
                {
                    d+=insuranceDao.queryById(insurances.get(i)).getCost();
                }
                price+=(d*passengers.size());
            }
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return price;
    }

    /**
     * 取消超时的订单
     * @param list
     * @return
     */
    private List<getAllMyOrder> removeOrder(List<getAllMyOrder> list)
    {
        List<getAllMyOrder> list1 = new ArrayList<>();
        for(getAllMyOrder getAllMyOrder:list)
        {
            if("".equals(getAllMyOrder.getPayment())||getAllMyOrder.getPayment().equals("1")) {
                if (Long.parseLong(getAllMyOrder.getNowtime()) - Long.parseLong(getAllMyOrder.getCreattime()) > planceConfig.getPaytime()) {
                    Homeorder homeorder = new Homeorder();
                    homeorder.setId(getAllMyOrder.getId());
                    homeorder.setPayment("4");
                    getAllMyOrder.setPayment("4");
                    homeorderDao.update(homeorder);
                }
            }
            getAllMyOrder.setCreattime(Long.parseLong(getAllMyOrder.getCreattime())+planceConfig.getPaytime());
            list1.add(getAllMyOrder);
        }
        return list1;
    }

    /**
     * 检查邮寄信息
     * @param deliveryBean
     * @return
     */
    private boolean checkDelivery(SaveOrder.DeliveryBean deliveryBean)
    {
        try {
            if(check.checkNull(deliveryBean.getDeliveryendaddress())||check.checkNull(deliveryBean.getCityname())||check.checkNull(deliveryBean.getDistrictname())||
            check.checkNull(String.valueOf(deliveryBean.getDeliveryprice()))||check.checkNull(String.valueOf(deliveryBean.getDeliverytype()))||check.checkNull(deliveryBean.getExpresscompanycode())||
            check.checkNull(deliveryBean.getProvincename())||check.checkNull(deliveryBean.getZipcode())||check.checkNull(deliveryBean.getRecipientname())||check.checkNull(deliveryBean.getRecipientphone()))
            {
                return true;
            }else
            {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

    /**
     * 取消单个超时订单
     * @param createorder
     * @return
     */
    private Createorder removeInsurance(Createorder createorder)
    {
        List<Integer> list =new ArrayList<>();
        for(int i :createorder.getInsurances())
        {
            if(insuranceDao.queryById(i).getIsmy().equals("F"))
            {
                list.add(i);
            }
        }
        createorder.setInsurances(list);
        return createorder;
    }


}
