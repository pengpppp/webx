package com.web.Service.Plance.My;

import com.web.Plance.entity.HomeFlight.Request.Order.passenger;
import com.web.common.core.entity.JsonData;
import com.web.dao.PassengerDao;
import com.web.entity.Passenger;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;

@Service
public class PassengerService {
    @Resource
    PassengerDao passengerDao;
    public Object add(Passenger passenger)
    {
        try {
            passengerDao.insert(passenger);
            return JsonData.buildSuccess(new HashMap<String,Object>() {{
                put("id",passengerDao.querryid());
                put("passengertype",passenger.getPassengertype());
            }
            });
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage()+"service");
        }
    }

    public Object showAll(Passenger passenger)
    {
        try {
            return JsonData.buildSuccess(passengerDao.queryByUserid(passenger.getUserid()));
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage()+"service");
        }
    }
    public Object delete(Passenger passenger)
    {
        try {
            return JsonData.buildSuccess(passengerDao.deleteById(passenger.getId()));
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage()+"service");
        }
    }
    public Object update(Passenger passenger)
    {
        try {
            if(passenger.getId()==null)
                throw new  Exception();
            return JsonData.buildSuccess(passengerDao.update(passenger));
        }catch (Exception e)
        {
            return JsonData.buildError(e.getMessage()+"service");
        }
    }

    public Passenger querybyId(int id)
    {
        return passengerDao.queryById(id);
    }

    public passenger querybypassenger(int id)
    {
        return passengerDao.queryBypassenger(id);
    }

}
