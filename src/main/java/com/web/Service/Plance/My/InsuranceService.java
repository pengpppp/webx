package com.web.Service.Plance.My;

import com.web.dao.InsuranceDao;
import com.web.entity.Insurance;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class InsuranceService {
    @Resource
    InsuranceDao insuranceDao;

    public Insurance getOne(Object i)
    {
        return insuranceDao.queryById(i);
    }

    public List<Insurance> getAllMyInsurance()
    {
        Insurance insurance = new Insurance();
        insurance.setKey("T");
        insurance.setIsmy("T");
        return insuranceDao.queryAll(insurance);
    }

    public Object addOther(Insurance insurance)
    {
        insurance.setIsmy("F");
        insurance.setKey("T");
        return insuranceDao.insert(insurance);
    }

}
