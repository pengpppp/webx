package com.web.Service;

import com.web.Tools.Check.check;
import com.web.Tools.staticData.Code;
import com.web.dao.LeavingwordDao;
import com.web.entity.Leavingword;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class LeavingService {
    @Resource
    LeavingwordDao leavingwordDao;

    public Map Leavinginsert(String name,String information,String phone)
    {
        HashMap map = new HashMap()
        {{
            put("code", Code.SUCCESS);
            put("message",Code.information.get(Code.SUCCESS));
        }};
        if(name==null||"null".equals(name)||"".equals(name)
                ||information==null||"null".equals(information)||"".equals(information)
        ||phone==null||"null".equals(phone)||"".equals(phone))
        {
            map.put("code",Code.FAIL);
            map.put("message",Code.information.get(Code.FAIL));
        }
        else if(!check.checkMobile(phone))
        {
            map.put("code",Code.PHONE_FORMAT_ERROR);
            map.put("message",Code.information.get(Code.PHONE_FORMAT_ERROR));
        }
        else {
            Leavingword leavingword = new Leavingword();
            leavingword.setName(name);
            leavingword.setPhone(phone);
            leavingword.setInformation(information);
            leavingword.setCreattime(new Date());
            leavingwordDao.insert(leavingword);
        }
        return map;
    }

}
