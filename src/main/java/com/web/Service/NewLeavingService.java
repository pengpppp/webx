package com.web.Service;

import com.web.dao.New.CommentDao;
import com.web.dao.UseridDao;
import com.web.entity.MyNew.Comment;
import com.web.entity.User.Userid;
import com.web.entity.entityImp.Basic;
import com.web.entity.entityImp.GetNewLeavingBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class NewLeavingService {
    @Resource
    CommentDao commentDao;

    @Resource
    UseridDao useridDao;

    private int pagenumb=10;
    public Object addLeaving(Comment comment)
    {
        Basic basic =new Basic();
        Userid userid = new Userid();
        userid.setMobile(comment.getLeavingname());
        List<Userid> list = useridDao.queryAll(userid);
        comment.setUserid(list.get(0).getUserid());
        comment.setCreattime(new Date());
        commentDao.insert(comment);
        return basic;
    }

    public Object getLeaving(Comment comment,Integer page)
    {
        GetNewLeavingBean gnlb = new GetNewLeavingBean();
        int total = commentDao.querycount(comment);
        if((pagenumb*(page-1))<total)
        {
            gnlb.setTotal(total);
            gnlb.setData(commentDao.queryAllBy(comment.getChannel(),comment.getSrc(),comment.getTittle(),pagenumb*(page-1),pagenumb));
        }else {
            //gnlb.SetFail();
        }
        return gnlb;
    }
}
