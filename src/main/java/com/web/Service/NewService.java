package com.web.Service;

import com.web.Tools.Http.Get;
import com.web.common.core.entity.JsonData;
import com.web.dao.MyNew.MynewDao;
import com.web.dao.New.CommentDao;
import com.web.entity.entityImp.Basic;
import com.web.entity.entityImp.neww;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class NewService {
    @Resource
    CommentDao commentDao;

    @Resource
    MynewDao mynewDao;

    @Autowired
    Get get;

    private int pagesize=20;


//    public Object getNew(String channel,Integer page)
//    {
//        Basic basic = new Basic();
//        int total = mynewDao.querycount1();
//        neww neww = new neww();
//        neww.setNum(pagesize);
//        neww.setChannel(channel);
//        try {
//        if(pagesize*(page-1)<total)
//            neww.setList(mynewDao.queryAllByLimit(pagesize*(page-1),pagesize));
//        int n = total-(pagesize*(page-1));
//        if(n<20&&n>=0) {
//            neww = (com.newweb.entity.entityImp.neww) get.NewGet(channel, 20 - n, 0, neww);
//        }else if(n<0)
//        {
//            page=(pagesize*(page-1))-total;
//            neww = (com.newweb.entity.entityImp.neww) get.NewGet(channel,20,page,neww);
//        }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        basic.setData(neww);
//        return basic;
//    }
    public Object getNew(String channel,Integer page)
    {

        try {
            if(page<=0)
                throw new Exception("page错误");
            Basic basic = new Basic();
            neww neww = new neww();
            neww.setNum(pagesize);
            neww.setChannel(channel);
            if(channel.equals("easyGo"))
            {
                int total = mynewDao.querycount1();
                if(pagesize*(page-1)<total)
                    neww.setList(mynewDao.queryAllByLimit(pagesize*(page-1),pagesize));
                else
                    return JsonData.buildSuccess();
            }else {
                    neww = (com.web.entity.entityImp.neww) get.NewGet(channel,pagesize,(page-1)*pagesize,neww);
            }
            basic.setData(neww);
            return basic;
        } catch (Exception e) {
            e.printStackTrace();
            return JsonData.buildError(e.toString());
        }

    }


}
