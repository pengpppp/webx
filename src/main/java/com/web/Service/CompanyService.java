package com.web.Service;

import com.web.Tools.Check.check;
import com.web.Tools.staticData.Code;
import com.web.dao.CompanyDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class CompanyService {

    @Resource
    CompanyDao companyDao;
    /**
     * 有ID则取值
     * 没ID则获取最后一条信息
     * @return
     */
    public Map show(String id)
    {
        HashMap<String,Object> map =new HashMap<String,Object>()
        {{
            put("code", Code.SUCCESS);
            put("message",Code.information.get(Code.SUCCESS));
            //put("total",0);
        }};
        if(check.checkNull(id))
            map.put("data",companyDao.queryLast());
        else
            map.put("data",companyDao.queryById(Integer.valueOf(id)));
        return map;

    }
}
