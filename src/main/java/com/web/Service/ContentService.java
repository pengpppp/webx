package com.web.Service;

import com.web.Tools.staticData.Code;
import com.web.dao.ContentDao;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

@Service
public class ContentService {
    @Resource
    ContentDao contentDao;

    /**
     * 后需，修改  根据ID获取内容
     * 没有ID时，获取最后一条的信息
     * @param id
     * @return
     */
    public Map contentShow(String id)
    {
        HashMap map = new HashMap()
        {{
            put("code", Code.SUCCESS);
            put("message",Code.information.get(Code.SUCCESS));
        }};
        if(id==null||"null".equals(id)||"".equals(id))
        {
            map.put("data",contentDao.queryLast());
        }
        else {
            map.put("data",contentDao.queryById(Integer.valueOf(id)));
        }
        return map;
    }
}
