package com.web.Config.SpringMVC;


import com.web.Config.ConfigClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    TokenInterceptor tokenInterceptor;

    @Autowired
    ConfigClass configClass;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        for (String s :configClass.getProgram())
            registry.addViewController(s).setViewName("forward:"+s+"/index.html");
        //registry.addViewController("/admin").setViewName("forward:/admin/index.html");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
        WebMvcConfigurer.super.addViewControllers(registry);
    }
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //添加token拦截器
        InterceptorRegistration tokenInterceptorRegistration = registry.addInterceptor(tokenInterceptor);
        tokenInterceptorRegistration.addPathPatterns("/api");//若要启用根据ID跳转的功能，则必须将此处改为”/“//客户端接口  /默认访问官网  //后台操作则再说
        tokenInterceptorRegistration.excludePathPatterns("/error");
        tokenInterceptorRegistration.excludePathPatterns("/admin");
        tokenInterceptorRegistration.excludePathPatterns("/static/**");
        tokenInterceptorRegistration.excludePathPatterns("/static/**/**");
        tokenInterceptorRegistration.excludePathPatterns("/*.html");
        tokenInterceptorRegistration.excludePathPatterns("/image/*.jpg");
       // tokenInterceptorRegistration.excludePathPatterns("/insert");
    }


}

