package com.web.Config.SpringMVC;


import com.alibaba.fastjson.JSONObject;
import com.web.Config.ConfigClass;
import com.web.Tools.Check.check;
import com.web.Tools.Redis.RedisUtil;
import com.web.Tools.staticData.Code;
import com.web.Tools.staticData.ForwardPath;
import com.web.Tools.staticData.InterfaceID;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class TokenInterceptor implements HandlerInterceptor {


    @Autowired
    ConfigClass configClass;

    @Autowired
    RedisUtil redisUtil;
    class BaseResult{
        private String code;
        private String message;
        public String getCode() {
            return code;
        }
        public void setCode(String code) {
            this.code = code;
        }
        public String getMessage() {
            return message;
        }
        public void setMessage(String message) {
            this.message = message;
        }
    }

    //在这里可以拦截请求，false表示不再继续处理
    //controller以及postHandle方法，afterCompletion方法都不会执行
    //先执行preHandle，再执行doFilter
    //直接返回当前的response
    @Override
    @SneakyThrows
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String id = request.getHeader("ID");
        String token = request.getHeader("TN");
        System.out.println(id);
        try {
                if (id == null|| !check.checkID(id))//检查ID
                {
                    BaseResult baseResult = new BaseResult();
                    baseResult.setCode(Code.WHITE_LIST_ERROR);
                    baseResult.setMessage("参数头错误");
                    InterfaceID.SendErrorInformation(response, JSONObject.toJSON(baseResult));
                    return false;
                }
                if(check.checkNull(token))
                {
                    if(!check.checkLoginToken(id,token))//检查接口是否是不登录便可使用的
                    {
                        BaseResult baseResult = new BaseResult();
                        baseResult.setCode(Code.NO_PERMISSION);
                        baseResult.setMessage("请先登录");
                        InterfaceID.SendErrorInformation(response, JSONObject.toJSON(baseResult));
                        return false;
                    }
                }else {
                    if(!redisUtil.hasKey(token))
                    {
                        BaseResult baseResult = new BaseResult();
                        baseResult.setCode(Code.PARAMETER_ERROR);
                        baseResult.setMessage("登录状态过期");
                        InterfaceID.SendErrorInformation(response, JSONObject.toJSON(baseResult));
                        return false;
                    }
                }
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher(ForwardPath.map.get(id)).forward(request, response);
        return false;
    }

    @Override
    @SneakyThrows
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
    }

}


