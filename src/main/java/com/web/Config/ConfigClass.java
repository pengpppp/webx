package com.web.Config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@ConfigurationProperties(prefix = "interface.config")
public class ConfigClass {
    public ConfigClass(){
        this.sms_time=900;
        this.sms_lock_time=60;
        this.sms_phone_time=86400;
        this.path="C://pic/image/";
        this.h5path="C://pic/";
        try {
            this.ip="123.57.111.230/";
            this.imageurl="image/";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 每页获取的图片数
     */
    private Integer pagenumb;
    /**
     * 图片存储地址
     * 建议放在h5存储文件夹的子文件夹中
     * 默认为："C://pic/image/"
     */
    private  String path ;
    /**
     * h5存储文件夹地址
     * 默认为"C://pic/"
     */
    private  String h5path;
    /**
     * 本地ip地址或域名
     * 默认获取本地ip地址
     */
    private String ip;
    /**
     * 图片文件访问地址
     */
    private String imageurl;
    /**
     * ID接口配置
     * true模式可根据请求头ID转发请求
     * false模式则可直接通过 /请求名 访问
     */
    private boolean InterfaceAction;
    /**
     * 万能验证码的KEY，兼容短信验证码KEY及图片验证码KEY
     */
    private String UniversalCode;
    /**
     * 万能验证码开关，开启时方能使用万能验证码
     */
    private Boolean UniversalCodeKey;
    /**
     * 短信验证码有效时间
     * 默认为十五分钟
     */
    private Integer sms_time;
    /**
     * 短信验证码锁定时间，在锁定时间内不能再次发送验证码
     * 默认为60秒
     */
    private Integer sms_lock_time;
    /**
     * 手机的短信验证码次数有限缓存时间，默认为整整24小时
     */
    private Integer sms_phone_time;

    /**
     * 自定义的后台接口是否开启
     */
    private boolean adminkey;

    /**
     * 开发调试开关，若开启，则请求头中除ID及token外不用挂带信息
     */
    private boolean rootkey;

    /**
     * 新项目配置，加上新项目时，需配置，方可转到index
     */
    private String[] program;

    private String app_edition;

    /**
     * 获取新闻的key
     */
    private String app_key;

    /**
     * 短信接口key
     */
    private String accessKeyId;

    /**
     * 短信接口密钥
     */
    private String accessSecret;

    /**
     * 机票接口key
     */
    private String APIKey;

    /**
     * 机票接口密钥
     */
    private String Secretkey;


    public String getAPIKey() {
        return APIKey;
    }

    public void setAPIKey(String APIKey) {
        this.APIKey = APIKey;
    }

    public String getSecretkey() {
        return Secretkey;
    }

    public void setSecretkey(String secretkey) {
        Secretkey = secretkey;
    }

    public String getAccessKeyId() {
        return accessKeyId;
    }

    public void setAccessKeyId(String accessKeyId) {
        this.accessKeyId = accessKeyId;
    }

    public String getAccessSecret() {
        return accessSecret;
    }

    public void setAccessSecret(String accessSecret) {
        this.accessSecret = accessSecret;
    }

    public String getApp_key() {
        return app_key;
    }

    public void setApp_key(String app_key) {
        this.app_key = app_key;
    }

    public String getApp_edition() {
        return app_edition;
    }

    public void setApp_edition(String app_edition) {
        this.app_edition = app_edition;
    }

    public String[] getProgram() {
        return program;
    }

    public void setProgram(String[] program) {
        this.program = program;
    }

    public boolean isRootkey() {
        return rootkey;
    }

    public void setRootkey(boolean rootkey) {
        this.rootkey = rootkey;
    }

    public boolean isAdminkey() {
        return adminkey;
    }

    public void setAdminkey(boolean adminkey) {
        this.adminkey = adminkey;
    }

    public Integer getPagenumb() {
        return pagenumb;
    }

    public void setPagenumb(Integer pagenumb) {
        this.pagenumb = pagenumb;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getH5path() {
        return h5path;
    }

    public void setH5path(String h5path) {
        this.h5path = h5path;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    public Integer getSms_time() {
        return sms_time;
    }

    public void setSms_time(Integer sms_time) {
        this.sms_time = sms_time;
    }

    public Integer getSms_lock_time() {
        return sms_lock_time;
    }

    public void setSms_lock_time(Integer sms_lock_time) {
        this.sms_lock_time = sms_lock_time;
    }

    public Integer getSms_phone_time() {
        return sms_phone_time;
    }

    public void setSms_phone_time(Integer sms_phone_time) {
        this.sms_phone_time = sms_phone_time;
    }

    public Boolean getUniversalCodeKey() {
        return UniversalCodeKey;
    }

    public void setUniversalCodeKey(Boolean universalCodeKey) {
        UniversalCodeKey = universalCodeKey;
    }

    public String getUniversalCode() {
        return UniversalCode;
    }

    public void setUniversalCode(String universalCode) {
        UniversalCode = universalCode;
    }

    public boolean isInterfaceAction() {
        return InterfaceAction;
    }

    public void setInterfaceAction(boolean interfaceAction) {
        InterfaceAction = interfaceAction;
    }



}
