package com.web.Config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "visa.config")
public class VisaConfig {
    private String visapath;
    private String visaurl;

    public String getVisapath() {
        return visapath;
    }

    public void setVisapath(String visapath) {
        this.visapath = visapath;
    }

    public String getVisaurl() {
        return visaurl;
    }

    public void setVisaurl(String visaurl) {
        this.visaurl = visaurl;
    }
}
