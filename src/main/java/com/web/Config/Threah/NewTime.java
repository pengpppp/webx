package com.web.Config.Threah;



import com.web.dao.MyNew.MynewDao;
import com.web.entity.MyNew.Mynew;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Component
@EnableScheduling   // 1.开启定时任务
@EnableAsync        // 2.开启多线程
public class NewTime {

    @Resource
    MynewDao mynewDao;

    private int time = 172800000;//两天

    @PostConstruct
    @Async
    @Scheduled(cron = "0 0/30 * * * ?")
    public void CheckNewKey()
    {
        long now = new Date().getTime();
        List<Mynew> list=mynewDao.querryall();
        for (Mynew mynew : list)
        {
            if(mynew.getKey()<=now)
            {
                System.out.println(mynew.toString());
                mynew.setKey(null);
                mynewDao.update(mynew);
            }
        }
    }
}
