package com.web.Config.Aspect;

import com.web.Tools.Redis.RedisUtil;
import com.web.common.core.entity.JsonData;
import com.web.entity.entityImp.Basic;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

@Aspect
@Component
@Order(2)
public class StatusAop {
    @Autowired
    RedisUtil redisUtil;
    @Pointcut(value = "@annotation(com.web.Config.Aspect.annotion.Status)")
    public void cutService() {
    }

    /**
     * 超级管理员方能使用的函数
     * @param point
     * @return
     * @throws Throwable
     */
    @Around("cutService()")
    public Object recordSysLog(ProceedingJoinPoint point) throws Throwable {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader("TN");
        try {
            String s = (String) redisUtil.get(token);
            s = s.split("-")[3];
            if(!s.equals("9"))
            {
                Basic basic = new Basic();
                basic.SetStatus();
                return basic;
            }
        }catch (Exception e)
        {
            return JsonData.buildError("token错误");
        }
        Object result = point.proceed();
        return result;
    }
    @Pointcut(value = "execution(* com.web.Root.Controller.Status8.*.*(..) )")
    public void cutpack8()
    {
    }

    /**
     * 权限8能查看的函数
     * @param point
     * @return
     * @throws Throwable
     */
    @Around("cutpack8()")
    public Object Status8(ProceedingJoinPoint point) throws Throwable {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader("TN");
        try {
            String s = (String) redisUtil.get(token);
            s = s.split("-")[3];
            if(!s.equals("9")&&!s.equals("8"))
            {
                Basic basic = new Basic();
                basic.SetStatus();
                return basic;
            }
        }catch (Exception e)
        {
            return JsonData.buildError("token错误");
        }
        Object result = point.proceed();
        return result;
    }

    @Pointcut(value = "execution(* com.web.Root.Controller.Status7.*.*(..) )")
    public void cutpack7()
    {
    }

    /**
     * 权限7能查看的函数
     * @param point
     * @return
     * @throws Throwable
     */
    @Around("cutpack7()")
    public Object Status7(ProceedingJoinPoint point) throws Throwable {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader("TN");
        try {
            if(redisUtil.hasKey(token))
            {
                String s = (String) redisUtil.get(token);
                s = s.split("-")[3];
                System.out.println(s.equals("9"));
                if(!s.equals("9")&&!s.equals("7"))
                {
                    Basic basic = new Basic();
                    basic.SetStatus();
                    return basic;
                }
            }else {
                return JsonData.buildError("请先登录");
            }
        }catch (Exception e)
        {
            return JsonData.buildError("token错误");
        }
        Object result = point.proceed();
        return result;
    }
}
