package com.web.Config.Aspect;

import com.web.Tools.staticData.Code;
import com.web.entity.Log.RootLog;

import java.util.Date;
import java.util.Map;

/**
 * 用于生产rootlog对象
 *
 */
public class FormatLog {

    public static RootLog getroot(String im, Map map, String bussinessName, String key, String tokenvalue)
    {
        RootLog rootLog = new RootLog();
        rootLog.setIm(im);
        rootLog.setCreatetime(new Date());
        rootLog.setLogname(bussinessName+"日志");
        if(map.get("code").equals(Code.SUCCESS))
            rootLog.setSucceed("成功");
        else
            rootLog.setSucceed("失败");
        try {
            if(key.equals("1"))//1代表登录
            {
                rootLog.setUsername(String.valueOf(((Map)map.get("data")).get("name")));
                ((Map)map.get("data")).remove("name");//去除返回参数中的name
            }else if(key.equals("2"))//2代表注册
            {
                rootLog.setUsername(String.valueOf(((Map)map.get("data")).get("name")));
            }else {//如果是其他操作的话，则将token中的name取出来
                if(tokenvalue==null)
                {}else {
                    System.out.println(tokenvalue);
                    if(tokenvalue==null)
                    {
                        rootLog.setUsername("张三");
                    }else {
                        String[] SS =tokenvalue.split("-");
                        rootLog.setUsername(SS[1]);
                    }
                }
            }
        }catch (Exception e)
        {
            rootLog.setSucceed("失败" );
        }

        return rootLog;
    }
}
