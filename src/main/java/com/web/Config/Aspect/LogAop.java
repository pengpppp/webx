package com.web.Config.Aspect;

import com.web.Config.Aspect.annotion.LoginLog;
import com.web.Tools.Check.check;
import com.web.Tools.Redis.RedisUtil;
import com.web.Tools.staticData.Code;
import com.web.common.core.entity.JsonData;
import com.web.dao.Log.RootLogDao;
import com.web.entity.Log.RootLog;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Map;

/**
 * 后台
 */
@Aspect
@Component
@Order(1)
public class LogAop {

    @Autowired
    RedisUtil redisUtil;
    @Resource
    RootLogDao rootLogDao;
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Pointcut(value = "@annotation(com.web.Config.Aspect.annotion.LoginLog)")
    public void cutService() {
    }

    @Pointcut(value = "execution(* com.web.Root.Controller..*.*(..) )")
    public void cutpack()
    {
    }

    /**
     * 检查token
     * @param point
     * @return
     * @throws Throwable
     */
    @Around("cutpack()")
    public Object some(ProceedingJoinPoint point) throws Throwable {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader("TN");
        if(true) {
            if (check.checkNull(token)||!redisUtil.hasKey(token)) {
                return JsonData.buildError("token错误或者token已过期", Code.PARAMETER_ERROR);
            } else {
                Object result = point.proceed();
                return result;
            }
        }else {
            Object result = point.proceed();
            return result;
        }
    }

    @Around("cutService()")
    public Object recordSysLog(ProceedingJoinPoint point) throws Throwable {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        String token = request.getHeader("TN");

        //System.out.println(request.getSession().getAttribute("username"));
//        try {
//            //操作日志，将当前操作写入日志
//            //handle((Map) result,im,point,token);
//        } catch (Exception e) {
//            log.error("日志记录出错!", e);
//        }

        Object result = point.proceed();
        return result;
    }

    private void handle(Map map, String im, ProceedingJoinPoint point, String token) throws Exception {

        //获取拦截的方法名
        MethodSignature msig = (MethodSignature) point.getSignature();//对象名
        Object target = point.getTarget();//获取方法的目标对象
        Method currentMethod = target.getClass().getMethod(msig.getName(), msig.getParameterTypes());//获取对象的公开方法
        LoginLog annotation = currentMethod.getAnnotation(LoginLog.class);
        String bussinessName = annotation.value();//获取注解中的value属性
        String key = annotation.key();//获取注解中的key属性

        RootLog rootLog = FormatLog.getroot(im,map,bussinessName,key, String.valueOf(redisUtil.get(token)));

        //写入日志
        rootLogDao.insert(rootLog);
    }


}
