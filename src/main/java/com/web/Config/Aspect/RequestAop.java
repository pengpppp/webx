package com.web.Config.Aspect;

import com.web.Config.ConfigClass;
import com.web.Service.RequestService;
import com.web.Tools.staticData.Code;
import com.web.common.core.entity.JsonData;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * 用于客户端的 aop
 * 用于记录用户端的请求头信息
 * 目前无日志
 */
@Aspect
@Component
public class RequestAop {

    @Autowired
    ConfigClass configClass;

    @Autowired
    RequestService requestService;
//    @Pointcut("execution( * com.newweb.Controller.userController.*(..))")
//    public void poin(){}
//
//    @Before("poin()")
//    public void before()
//    {
//        System.out.println("before");
//    }

    @Around("@annotation(com.web.Config.Aspect.annotion.RequsetHeart)))")
    public Object requestheart(ProceedingJoinPoint joinPoint)throws Throwable
    {
        try {
            if(configClass.isRootkey())
            {
                //System.out.println("dddddddd");
            }else {
                HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
                if(requestService.SetHeart(request))
                {
                }else {
                    return  new HashMap(){
                        {
                            put("code", Code.PARAMETER_ERROR);
                            put("message","参数头丢失");
                        }
                    };
                }

            }
        }catch (Exception e)
        {
            return JsonData.buildError("参数头丢失");
        }


        return joinPoint.proceed();
    }


}
